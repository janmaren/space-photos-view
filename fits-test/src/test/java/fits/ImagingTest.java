package fits;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.common.ImageMetadata.ImageMetadataItem;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.taginfos.TagInfo;
import org.junit.jupiter.api.Test;

public class ImagingTest {
    @Test
    public void testImg1() throws ImageReadException, IOException {
        final ImageMetadata metadata = Imaging.getMetadata(new File("C:\\Users\\E_JMAREN\\Downloads\\eso1740a.tif"));

        // System.out.println(metadata);

        if (metadata instanceof JpegImageMetadata) {
            final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
        } else if (metadata instanceof TiffImageMetadata) {
            TiffImageMetadata tiffImageMetadata = (TiffImageMetadata) metadata;
//            List<? extends ImageMetadataItem> items = tiffImageMetadata.getItems();
//            for (ImageMetadataItem imageMetadataItem : items) {
//                System.out.println(imageMetadataItem);
//            }
            System.out.println("==================");
            List<TiffField> allFields = tiffImageMetadata.getAllFields();
            for (TiffField tiffField : allFields) {
                if (tiffField.getTagInfo().getDescription().contains("XMP")) {
                    byte[] value = (byte[]) tiffField.getTagInfo().getValue(tiffField);
                    System.out.println("Value: " + new String(value));
                }
                System.out.println(tiffField);
            }
        }
    }
    
    private static void printTagValue(final JpegImageMetadata jpegMetadata,
            final TagInfo tagInfo) {
        final TiffField field = jpegMetadata.findEXIFValueWithExactMatch(tagInfo);
        if (field == null) {
            System.out.println(tagInfo.name + ": " + "Not Found.");
        } else {
            System.out.println(tagInfo.name + ": "
                    + field.getValueDescription());
        }
    }
}
