package fits;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.junit.jupiter.api.Test;

import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;
import nom.tam.fits.BasicHDU;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.FitsFactory;
import nom.tam.fits.Header;
import nom.tam.fits.ImageData;
import nom.tam.fits.ImageHDU;
import nom.tam.util.FitsOutputStream;

public class FitsTest {
    @Test
    public void fitsSimpleWcs() throws FitsException, IOException {
        BufferedImage bufferedImage = ImageIO.read(new File("C:\\Users\\E_JMAREN\\Pictures\\plejady.jpeg"));
        Raster raster = bufferedImage.getData();
        
        int height = raster.getHeight();
        int width = raster.getWidth();
        float[][] data = new float[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgb = bufferedImage.getRGB(j, i);
                RGBEntity rHighestToRGB = RGBEntUtil.rHighestToRGB(rgb);
                float f = (float)((rHighestToRGB.red + rHighestToRGB.blue + rHighestToRGB.green) / 3.0);
                data[height - i - 1][j] = f;
            }
        }
        Fits f = new Fits();
        BasicHDU<?> hdu = FitsFactory.hduFactory(data);
        Header header = hdu.getHeader();
        header.addValue("CTYPE1", "RA---SIN", "comment1");
        header.addValue("CTYPE2", "DEC--SIN", "comment1");
        header.addValue("CRPIX1", 0, "comment1");
        header.addValue("CRPIX2", 4928/2, "comment1");
        header.addValue("CRVAL1", 57.59467763647426, "comment1");
        header.addValue("CRVAL2", 24.565816698610234, "comment1");
        header.addValue("CD1_1", -3.211212485061536E-4, "comment1");
        header.addValue("CD1_2", 8.543188685201799E-5, "comment1");
        header.addValue("CD2_1", -9.798029549706442E-5, "comment1");
        header.addValue("CD2_2", -2.962169618630912E-4, "comment1");
        f.addHDU(hdu);

        FitsOutputStream out = new FitsOutputStream(new FileOutputStream(new File("C:\\Users\\E_JMAREN\\Pictures\\plejady.fits")));     
        f.write(out);
        out.close();
        f.close();
    }
    
    @Test
    public void fitsSimple2Wcs() throws FitsException, IOException {
        BufferedImage bufferedImage = ImageIO.read(new File("C:\\Users\\E_JMAREN\\Pictures\\subplejady.png"));
        Raster raster = bufferedImage.getData();
        
        int height = raster.getHeight();
        int width = raster.getWidth();
        float[][] data = new float[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgb = bufferedImage.getRGB(j, i);
                RGBEntity rHighestToRGB = RGBEntUtil.rHighestToRGB(rgb);
                float f = (float)((rHighestToRGB.red + rHighestToRGB.blue + rHighestToRGB.green) / 3.0);
                data[height - i - 1][j] = f;
            }
        }
        Fits f = new Fits();
        BasicHDU<?> hdu = FitsFactory.hduFactory(data);
        Header header = hdu.getHeader();
        header.addValue("CTYPE1", "RA---SIN", "comment1");
        header.addValue("CTYPE2", "DEC--SIN", "comment1");
        header.addValue("CRPIX1", 0, "comment1");
        header.addValue("CRPIX2", 0, "comment1");
        header.addValue("CRVAL1", 56.30132447179641, "comment1");
        header.addValue("CRVAL2", 24.467198335264957, "comment1");
        header.addValue("CD1_1", -9.24037913540681E-5, "comment1");
        header.addValue("CD1_2", 2.9510965904803294E-4, "comment1");
        header.addValue("CD2_1", 3.2233424057154773E-4, "comment1");
        header.addValue("CD2_2", 8.49759988560222E-5, "comment1");
        f.addHDU(hdu);

        FitsOutputStream out = new FitsOutputStream(new FileOutputStream(new File("C:\\Users\\E_JMAREN\\Pictures\\subplejady.fits")));     
        f.write(out);
        out.close();
        f.close();
    }
    
    
    @Test
    public void fitsAlcyone2Wcs() throws FitsException, IOException {
        BufferedImage bufferedImage = ImageIO.read(new File("C:\\Users\\E_JMAREN\\Pictures\\alcyone\\alcyone.png"));
        Raster raster = bufferedImage.getData();
        
        int height = raster.getHeight();
        int width = raster.getWidth();
        float[][] data = new float[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgb = bufferedImage.getRGB(j, i);
                RGBEntity rHighestToRGB = RGBEntUtil.rHighestToRGB(rgb);
                float f = (float)((rHighestToRGB.red + rHighestToRGB.blue + rHighestToRGB.green) / 3.0);
                data[height - i - 1][j] = f;
            }
        }
        Fits f = new Fits();
        BasicHDU<?> hdu = FitsFactory.hduFactory(data);
        Header header = hdu.getHeader();
        header.addValue("CTYPE1", "RA---SIN", "comment1");
        header.addValue("CTYPE2", "DEC--SIN", "comment1");
        header.addValue("CRPIX1", 0, "comment1");
        header.addValue("CRPIX2", height, "comment1");
        header.addValue("CRVAL1", 56.87854064540754, "comment1");
        header.addValue("CRVAL2", 24.38865415881988, "comment1");
        header.addValue("CD1_1", -5.200439656706259E-4, "comment1");
        header.addValue("CD1_2", 1.2413684871272742E-5, "comment1");
        header.addValue("CD2_1", 1.0942814177637049E-5, "comment1");
        header.addValue("CD2_2", 4.744350282486285E-4, "comment1");
        f.addHDU(hdu);

        FitsOutputStream out = new FitsOutputStream(new FileOutputStream(new File("C:\\Users\\E_JMAREN\\Pictures\\alcyone\\alcyone.fits")));     
        f.write(out);
        out.close();
        f.close();
    }
    
    @Test
    public void readTest() throws FitsException, IOException {
        Fits f = new Fits("C:\\Users\\E_JMAREN\\Pictures\\plejady.fits");
        ImageHDU hdu = (ImageHDU) f.getHDU(0);
        
        ImageData imageData = (ImageData) hdu.getData();
        float[][] image = (float[][]) imageData.getData();
        float naxis2 = hdu.getHeader().getFloatValue("CRVAL1");
        System.out.println(naxis2);
        
    }
    
    private File classpathFile(String path) {
        URL url = FitsTest.class.getResource(path);
        File file = null;
        try {
            file = Paths.get(url.toURI()).toFile();
            return file;
        } catch (URISyntaxException e) {
            throw new IllegalStateException("unable to load " + path);
        }
    }
}
