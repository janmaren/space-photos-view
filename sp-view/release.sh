mvn clean

mvn install -P 'sp-view' -P '!win-x86-64' -P 'linux-x86-64' -P '!macosx-x86-64'
mvn install -P 'sp-view' -P 'win-x86-64' -P '!linux-x86-64' -P '!macosx-x86-64'
mvn install -P 'sp-view' -P '!win-x86-64' -P '!linux-x86-64' -P 'macosx-x86-64'

chmod u+x target/*.jar
