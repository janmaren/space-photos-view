name,abbreviation,ra[h],dec[deg]
Andromeda,And,1,35
Antila,Ant,10,-35
Apus,Aps,16,-77
Aquarius,Aqr,23,-15
Aquila,Aql,20,5
Ara,Ara,17,-55
Aries,Ari,2.2,22
Auriga,Aur,6,40
Boötes,Boo,15,30
Caelum,Cae,5,-40
Camelopardus,Cam,6,70
Cancer,Cnc,9,20
Canes Venatici,CVn,13,40
Canis Major,CMa,7,-20
Canis Minor,CMi,7.5,7
Capricornus,Cap,21,-20
Carina,Car,9,-60
Cassiopeia,Cas,1,60
Centaurus,Cen,13,-50
Cepheus,Cep,22,70
Cetus,Cet,2,-10
Chamaeleon,Cha,11,-80
Circinus,Cir,15,-60
Columba,Col,6,-35
Coma Berenices,Com,13,20
Corona Australis,CrA,19,-40
Corona Borealis,CrB,16,25
Corvus,Crv,12.4,-22
Crater,Crt,11.3,-13.8
Crux,Cru,12.4,-59
Cygnus,Cyg,21,40
Delphinus,Del,20.6,15
Dorado,Dor,5.4,-55
Draco,Dra,17,65
Equuleus,Equ,21.2,8
Eridanus,Eri,3,-20
Fornax,For,3,-30
Gemini,Gem,7,20
Grus,Gru,22,-45
Hercules,Her,17,30
Horologium,Hor,3,-60
Hydra,Hya,10,-16
Hydrus,Hyi,2,-75
Indus,Ind,21,-55
Lacerta,Lac,22.8,45
Leo,Leo,11,15
Leo Minor,LMi,10.5,35
Lepus,Lep,5.7,-18
Libra,Lib,15,-15
Lupus,Lup,15,-45
Lynx,Lyn,8,45
Lyra,Lyr,19,35
Mensa,Men,6,-76
Microscopium,Mic,21,-35
Monoceros,Mon,7,-5
Musca,Mus,12.5,-69.8
Norma,Nor,16.2,-50
Octans,Oct,22,-85
Ophiuchus,Oph,17,0
Orion,Ori,6,5
Pavo,Pav,20,-65
Pegasus,Peg,22.7,20
Perseus,Per,3,45
Phoenix,Phe,1,-50
Pictor,Pic,6,-55
Pisces,Psc,1,15
Piscis Austrinus,PsA,22,-30
Puppis,Pup,8,-40
Pyxis,Pyx,9,-30
Reticulum,Ret,4,-60
Sagitta,Sge,19.7,19
Sagittarius,Sgr,19,-25
Scorpius,Sco,17,-40
Sculptor,Scl,0,-30
Scutum,Sct,19,-10
Serpens,Ser,16,12
Sextans,Sex,10.5,2
Taurus,Tau,4.8,15
Telescopium,Tel,19,-50
Triangulum,Tri,2,30
Triangulum Australe,TrA,16,-65
Tucana,Tuc,0,-65
Ursa Major,UMa,11,50
Ursa Minor,UMi,15,77
Vela,Vel,9,-50
Virgo,Vir,13,0
Volans,Vol,8,-70
Vulpecula,Vul,20,25
