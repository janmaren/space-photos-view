package cz.jmare.graphfw.labeler;

import org.eclipse.swt.graphics.GC;

/**
 * Simply overlap. Useful when impossible to use a proper Labeler
 */
public class OverlapLabeler implements Labeler {
    @Override
    public boolean drawLabel(GC gc, String text, int x, int y) {
        gc.drawText(text, x, y, true);
        return true;
    }
}
