package cz.jmare.graphfw.labeler;

import org.eclipse.swt.graphics.GC;

public interface Labeler {
    boolean drawLabel(GC gc, String text, int x, int y);
}
