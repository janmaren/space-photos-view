package cz.jmare.graphfw.csv.rebuildconv;

import cz.jmare.graphfw.csv.RebuildConversion;

public class EscapeRebuildConversion implements RebuildConversion {
    @Override
    public String getName() {
        return "escape";
    }

    @Override
    public String convert(String str) {
        return str.replace(";", "\\;").replace(":", "\\:");
    }
}
