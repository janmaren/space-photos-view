package cz.jmare.graphfw.csv;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.maren.spview.shape.PointShape;
import org.maren.spview.table.CsvShapesUtil;
import org.maren.spview.util.CsvUtil;

import cz.jmare.exception.ExceptionMessage;
import cz.jmare.swt.util.Create;

public class CsvRedesignDialog extends TitleAreaDialog {
    public static final int FIELD_RADEC = 1;
    private String[] resultFieldsLabels;

    private String[] tooltips;
    private Text[] texts;

    private List<String[]> resultRebuildedCsv;
    private Browser origBrowser;
    private Browser newBrowser;
    private List<String[]> origCsv;
    private Text headerLinesCount;
    private ConsumerThrowing<List<String[]>> additionalDataCheck;

    private ColsAdvicer colsAdvicer;
    private final String fileName;

    private String[] resultDirectives;

    private Button fromDegreesCheck;
    private Combo radecCol1Combo;
    private Combo radecCol2Combo;

    public CsvRedesignDialog(Shell parent, List<String[]> origCsv, String[] resultFieldsLabels, String fileName) {
        super(parent);
        this.origCsv = origCsv;
        this.resultFieldsLabels = resultFieldsLabels;
        this.texts = new Text[resultFieldsLabels.length];
        this.colsAdvicer = new EmptyColsAdviser();
        this.fileName = fileName;
    }
    
    public CsvRedesignDialog(Shell parent, File file, String[] resultFieldsLabels, String fileName) {
        this(parent, readChecked(file), resultFieldsLabels, fileName);
    }
    
    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Transform CSV Columns for " + fileName);

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.BORDER);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);
        
        Create.label(composite, "Header lines count:");
        headerLinesCount = Create.text(composite, "0", 30);
        headerLinesCount.setToolTipText("How much lines are the header to be skipped");
        headerLinesCount.setText(String.valueOf(colsAdvicer.getHeaderLinesCount()));
        headerLinesCount.addModifyListener(e -> updateTable());

        for (int i = 0; i < resultFieldsLabels.length; i++) {
            int finalI = i;
            Create.label(composite, (i + 1) + " - " + resultFieldsLabels[i] + ":", l -> {
                if (resultFieldsLabels[finalI].endsWith("*")) {
                    l.setToolTipText("optional");
                }
            });
            if (i == FIELD_RADEC) {
                Text text = createRaDecControls(composite);
                texts[i] = text;
            } else {
                texts[i] = new Text(composite, SWT.BORDER);

                GridData textData = new GridData();
                textData.horizontalAlignment = SWT.FILL;
                textData.grabExcessHorizontalSpace = true;
                texts[i].setLayoutData(textData);
            }
            texts[i].addModifyListener(new ModifyListener() {
                @Override
                public void modifyText(ModifyEvent e) {
                    if (origCsv.size() <= 1000) {
                        updateTable();
                    }
                }
            });
            if (tooltips != null) {
                if (tooltips[i] != null) {
                    texts[i].setToolTipText(tooltips[i]);
                }
            }
        }
        Create.label(composite, "* means optional");
        Create.label(composite, "Original CSV:", t -> {
            GridData layoutData = new GridData();
            layoutData.horizontalSpan = 2;
            t.setLayoutData(layoutData);
        });
        
        origBrowser = new Browser(composite, SWT.BORDER);
        GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
        layoutData.horizontalSpan = 2;
        layoutData.heightHint = 600;
        origBrowser.setLayoutData(layoutData);
        if (origCsv.size() > 1000) {
            List<String[]> subList = origCsv.subList(0, 1000);
            updateBrowser(origBrowser, subList, null);
            setMessage("Preview of original CSV truncated to 1000 rows", IMessageProvider.WARNING);
        } else {
            updateBrowser(origBrowser, origCsv, null);
        }
        Create.label(composite, "Transformed CSV:", t -> {
            GridData layData = new GridData();
            layData.horizontalSpan = 1;
            t.setLayoutData(layData);
        });
        
        Create.button(composite, "Regenerate", () -> {
            updateTable();
        });
        
        newBrowser = new Browser(composite, SWT.BORDER);
        newBrowser.setLayoutData(layoutData);
        
        for (int i = 0; i < resultFieldsLabels.length; i++) {
            String directive = colsAdvicer.getDirective(i);
            if (directive != null) {
                if (i == FIELD_RADEC) {
                    setRadecByAdviser(directive);
                } else {
                    texts[i].setText(directive);
                }
            }
        }

        updateTable();
        return composite;
    }

    private Text createRaDecControls(Composite composite) {
        Composite comp = new Composite(composite, SWT.NONE);
        GridLayout gridLayout = new GridLayout(4, false);
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        comp.setLayout(gridLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.horizontalAlignment = SWT.FILL;
        comp.setLayoutData(gridData);

        fromDegreesCheck = Create.checkButton(comp, false, "From degrees");
        fromDegreesCheck.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> updateRadec()));

        radecCol1Combo = Create.combo(comp);
        populateCombo("Select RA(DEC)", radecCol1Combo);
        radecCol1Combo.select(0);
        radecCol1Combo.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> updateRadec()));

        radecCol2Combo = Create.combo(comp);
        populateCombo("Select DEC", radecCol2Combo);
        radecCol2Combo.select(0);
        radecCol2Combo.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> updateRadec()));

        Text text = Create.text(comp);
        GridData textData = new GridData();
        textData.horizontalAlignment = SWT.FILL;
        textData.grabExcessHorizontalSpace = true;
        text.setLayoutData(textData);
        text.setEditable(false);
        return text;
    }

    private void populateCombo(String label, Combo combo) {
        combo.add(label);
        String[] strings = origCsv.get(0);
        for (int i = 0; i < strings.length; i++) {
            combo.add(String.valueOf(i + 1));
        }
    }

    private void setRadecByAdviser(String directive) {
        try {
            if (directive.startsWith("degtohour ")) {
                directive = directive.substring("degtohour ".length());
                fromDegreesCheck.setSelection(true);
            }
            int index = directive.indexOf(",");
            if (index != -1) {
                radecCol1Combo.select(Integer.parseInt(directive.substring(0, index)));
                int index1 = directive.lastIndexOf(",");
                radecCol2Combo.select(Integer.parseInt(directive.substring(index1 + 1).trim()));
            } else {
                radecCol1Combo.select(Integer.parseInt(directive.trim()));
            }
        } catch (Exception e) {

        }
        updateRadec();
    }

    private void updateRadec() {
        StringJoiner sj = new StringJoiner(",");
        if (radecCol1Combo.getSelectionIndex() <= 0) {
            texts[FIELD_RADEC].setText("");
            return;
        }
        if (fromDegreesCheck.getSelection()) {
            sj.add("degtohour " + radecCol1Combo.getSelectionIndex());
        } else {
            sj.add(String.valueOf(radecCol1Combo.getSelectionIndex()));
        }
        if (radecCol2Combo.getSelectionIndex() > 0) {
            sj.add("\",\"");
            sj.add(String.valueOf(radecCol2Combo.getSelectionIndex()));
        }
        texts[FIELD_RADEC].setText(sj.toString());
    }

    @Override
    protected Point getInitialSize() {
        return new Point(700, 700);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }

        List<String[]> rebuildedRows = null;
        try {
            rebuildedRows = rebuildRows();
            if (additionalDataCheck != null) {
                if (rebuildedRows.size() > 500) {
                    rebuildedRows = rebuildedRows.subList(0, 500);
                }
                try {
                    additionalDataCheck.accept(rebuildedRows);
                } catch (IllegalArgumentException | IllegalStateException e0) {
                    if (e0.getMessage() != null) {
                        setErrorMessage(e0.getMessage());
                        return;
                    }
                    setErrorMessage(ExceptionMessage.getCombinedMessage(e0));
                    return;
                }  catch (Exception e) {
                    setErrorMessage(ExceptionMessage.getCombinedMessage(e));
                    return;
                }
            }
        } catch (Exception e) {
            setErrorMessage(ExceptionMessage.getCombinedMessage("Unable to transform", e));
            return;
        }
        this.resultRebuildedCsv = rebuildedRows;

        String[] direct = new String[resultFieldsLabels.length];
        for (int i = 0; i < resultFieldsLabels.length; i++) {
            direct[i] = texts[i].getText().trim();
        }
        this.resultDirectives = direct;
        super.buttonPressed(buttonId);
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public List<String[]> getResultRebuildedCsv() {
        return resultRebuildedCsv;
    }
    
    private void updateTable() {
        setMessage("");
        setErrorMessage(null);
        try {
            List<String[]> parseData = rebuildRows();
            if (parseData.size() > 1000) {
                List<String[]> subList = parseData.subList(0, 1000);
                updateBrowser(newBrowser, subList, resultFieldsLabels);
                setMessage("Preview of transformed CSV truncated to 1000 rows", IMessageProvider.WARNING);
            } else {
                updateBrowser(newBrowser, parseData, resultFieldsLabels);
            }
        } catch (Exception e1) {
            updateBrowser(newBrowser, null, resultFieldsLabels);
            setErrorMessage(ExceptionMessage.getCombinedMessage("Unable to transform", e1));
        }
    }

    /**
     * Get rebuilded rows without first line when the header checkbox selected
     * @return
     */
    private List<String[]> rebuildRows() {
        List<String[]> list = new ArrayList<>();
        String[] directives = new String[resultFieldsLabels.length];
        for (int i = 0; i < resultFieldsLabels.length; i++) {
            directives[i] = texts[i].getText().trim();
        }
        int skipLines = Integer.parseInt(headerLinesCount.getText());
        for (int j = skipLines; j < origCsv.size(); j++) {
            String[] origCols = origCsv.get(j);
            String[] row = RowRebuilder.buildResultRow(origCols, directives);
            list.add(row);
        }
        return list;
    }

    private static void updateBrowser(Browser browser, List<String[]> data, String[] headerFields) {
        if (data == null) {
            browser.setText("");
            return;
        }
        String html = "<html><head><style>table, th, td {\n"
                + "  border: 1px solid black;\n"
                + "  border-collapse: collapse;\n"
                + "  font-family: consolas, Arial, Helvetica, sans-serif;\n"
                + "  font-size: 11px;\n"
                + "  text-align: left;\n"
                + "}</style></head><body><table>";

        String[] row0 = data.get(0);
        html += "<tr>";
        for (int i = 0; i < row0.length; i++) {
            html += "<th style=\"font-family: cursive; text-align: center\">" + (i + 1) + "</th>";
        }
        html += "</tr>";

        if (headerFields != null) {
            html += "<tr>";
            for (int i = 0; i < headerFields.length; i++) {
                String col = headerFields[i];
                html += "<th>" + col + "</th>";
            }
            html += "</tr>";
        }
        for (int i = 0; i < data.size(); i++) {
            String[] row = data.get(i);
            html += "<tr>";
            for (String col : row) {
                if (col == null || col.isEmpty()) {
                    html += "<td>&nbsp;&nbsp;</td>";
                } else {
                    html += "<td>" + col + "</td>";
                }
            }
            html += "</tr>";
        }
        html += "</table></body></html>";
        browser.setText(html);
    }

    private static List<String[]> readChecked(File file) {
        try {
            return CsvUtil.importCsv(Files.readString(file.toPath()));
        } catch (IOException e) {
            throw new RuntimeException("Unable to read " + file);
        }
    }
    
    public void setAdditionalDataCheck(ConsumerThrowing<List<String[]>> additionalDataCheck) {
        this.additionalDataCheck = additionalDataCheck;
    }

    public void setColsAdvicer(ColsAdvicer colsAdvicer) {
        this.colsAdvicer = colsAdvicer;
    }

    public String[] getResultDirectives() {
        return resultDirectives;
    }

    public static void main(String[] args) {
        String[] splitColumns = RowRebuilder.splitColumns("abc 1, \"divide\", 3, 5 a b c 6", ',');
        String[] strings = RowRebuilder.buildResultRow(new String[]{"jedna", "dve", "tri", "ctyri", "pet"}, splitColumns);
        for (String string : strings) {
            System.out.println(string);
        }
    }

    public void setTooltips(String[] tooltips) {
        if (tooltips.length != resultFieldsLabels.length) {
            throw new IllegalArgumentException("Not the same number for tooltips as fields");
        }
        this.tooltips = tooltips;
    }

    public static List<PointShape> rebuildCsv(Shell shell, List<String[]> origCsv, ColsAdvicer adviser, String fileName) {
        String[] pointsNativeFields = {"Name*", "Equatorial Coordinates [ra, dec]", "Date [Julian day]*", "Description*", "Tags*"};

        CsvRedesignDialog csvRedesignDialog = new CsvRedesignDialog(shell, origCsv, pointsNativeFields, fileName);
        csvRedesignDialog.setAdditionalDataCheck(resultCsv -> CsvShapesUtil.importNativePointsCsv(resultCsv));
        csvRedesignDialog.setColsAdvicer(adviser);
        csvRedesignDialog.setTooltips(new String[] {
                "Number(s) for the name column.\nExamples:\n  1\n  1, \" \", 2",
                "Number(s) for RA DEC coordinates. Use degtohour when RA in degrees.\nExamples:\n  3, \" \", 4\n  degtohour 3, \" \", 4\nor even simple number like 3 when all already in one column",
                "Number for a julian date column. When in gregorian format, use datespacetime directive.\nExample:\n  9\n  datespacetime 9",
                "Number for description column. It can be also comprised with multiple columns.\nExamples:\n  5\n  5, \"-\", 6, \"-\", 7",
                "Number for tag value.\nTag has format 'TAGNAME1:VALUE1;TAGNAME2:VALUE2;...'\nUse nowhite to remove whitespaces from value\nExamples:\n  \"AMAG:\", 13\n  \"AMAG:\", 13, \";HIP:\", 14\n  \"AMAG:\", 13, \";HIP:\", nowhite 14"});
        if (csvRedesignDialog.open() == Window.OK) {
            try {
                List<String[]> rebuildedCsv = csvRedesignDialog.getResultRebuildedCsv();// PointsUtil.rebuildCsvToNative(csvText, directives, hasHeader);
                return CsvShapesUtil.importNativePointsCsv(rebuildedCsv);
            } catch (Exception e1) {
                return null;
            }
        }
        return null;
    }

    @Override
    protected void initializeBounds() {
        getShell().setSize(850, 800);
        getShell().setMinimumSize(750, 750);
    }
}
