package cz.jmare.graphfw.csv;

public class EmptyColsAdviser implements ColsAdvicer {
    public EmptyColsAdviser() {
    }

    @Override
    public String getDirective(int rowIndex) {
        return null;
    }

    @Override
    public int getHeaderLinesCount() {
        return 0;
    }
}
