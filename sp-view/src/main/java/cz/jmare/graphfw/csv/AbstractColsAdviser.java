package cz.jmare.graphfw.csv;

import java.util.List;

abstract class AbstractColsAdviser implements ColsAdvicer {
    protected int headerLinesCount;
    protected String[] directives;
    protected List<String[]> origCsv;
    protected int fieldsLength;

    public AbstractColsAdviser(List<String[]> origCsv, int fieldsLength) {
        this.origCsv = origCsv;
        this.fieldsLength = fieldsLength;
    }

    protected void build() {
        directives = new String[fieldsLength];
    }

    protected boolean isBuilded() {
        return directives != null;
    }

    /**
     * Get directive for given field
     * @param rowIndex zero based row index
     * @return
     */
    public String getDirective(int rowIndex) {
        if (!isBuilded()) {
            build();
        }
        return directives[rowIndex];
    }

    public int getHeaderLinesCount() {
        if (!isBuilded()) {
            build();
        }
        return headerLinesCount;
    }
}
