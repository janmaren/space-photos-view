package cz.jmare.graphfw.csv;

public interface ColsAdvicer {

    /**
     * Get directive for given field
     * @param rowIndex zero based row index
     * @return
     */
    String getDirective(int rowIndex);

    int getHeaderLinesCount();
}
