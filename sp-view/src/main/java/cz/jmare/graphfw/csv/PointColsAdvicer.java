package cz.jmare.graphfw.csv;

import java.util.List;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.math.astro.RaDec;

public class PointColsAdvicer extends AbstractColsAdviser {
    public PointColsAdvicer(List<String[]> origCsv, int fieldsLength) {
        super(origCsv, fieldsLength);
    }

    protected void build() {
        directives = new String[fieldsLength];
        if (origCsv.size() < 1) {
            return;
        }
        int[] findRaDecCols = findRaDecCols(origCsv.get(0));
        if (findRaDecCols == null) {
            if (origCsv.size() < 2) {
                return;
            }
            findRaDecCols = findRaDecCols(origCsv.get(1));
            if (findRaDecCols == null) {
                return;
            }
            headerLinesCount = 1;
        } else {
            headerLinesCount = 0;
        }

        if (findRaDecCols.length == 1) {
            directives[1] = String.valueOf(findRaDecCols[0] + 1);
        } else {
            boolean greater24 = false;
            greater24 = isGreater24(findRaDecCols, greater24);
            directives[1] = (greater24 ? "degtohour " : "") + (findRaDecCols[0] + 1) + ", \" \", " + (findRaDecCols[1] + 1);
        }

        if (headerLinesCount > 0) {
            String[] row = origCsv.get(0);
            for (int i = 0; i < row.length; i++) {
                if (row[i].toLowerCase().contains("name")) {
                    directives[0] = String.valueOf(i + 1);
                    break;
                }
            }
            for (int i = 0; i < row.length; i++) {
                if (row[i].toLowerCase().contains("desc")) {
                    directives[3] = String.valueOf(i + 1);
                    break;
                }
            }
        }
    }

    protected boolean isGreater24(int[] findRaDecCols, boolean greater24) {
        int from = headerLinesCount > 0 ? 1 : 0;
        for (int i = from; i < origCsv.size(); i++) {
            String[] row = origCsv.get(i);
            RaDec raDec;
            try {
                raDec = ACooUtil.parseCoords(row[findRaDecCols[0]].trim() + " " + row[findRaDecCols[1]].trim(), true);
                if (raDec.ra >= 24.0) {
                    greater24 = true;
                }
            } catch (Exception e) {
                greater24 = false;
                break;
            }
        }
        return greater24;
    }

    public static int[] findRaDecCols(String[] row) {
        for (int i = 0; i < row.length; i++) {
            try {
                RaDec raDec = ACooUtil.parseCoords(row[i].trim(), false);
                if (raDec.ra >= 360) {
                    continue;
                }
                return new int[] {i};
            } catch (Exception e) {
                // no action
            }
        }
        
        for (int i = 0; i < row.length - 1; i++) {
            try {
                RaDec raDec = ACooUtil.parseCoords(row[i].trim() + " " + row[i + 1].trim(), false);
                if (raDec.ra >= 360) {
                    throw new RuntimeException();
                }
                return new int[] {i, i + 1};
            } catch (Exception e) {
                try {
                    RaDec raDec = ACooUtil.parseCoords(row[i].trim() + " " + row[i + 1].trim(), true);
                    if (raDec.ra >= 360) {
                        continue;
                    }
                    return new int[] {i, i + 1};
                } catch (Exception e1) {
                    // no action
                }
            }
        }

        return null;
    }
}
