package cz.jmare.graphfw.csv.rebuildconv;

import cz.jmare.data.util.JulianUtil;
import cz.jmare.graphfw.csv.RebuildConversion;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

public class DateTimeRebuildConversion implements RebuildConversion {
    private static final DateTimeFormatter FORMATTER =
            new DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd HH:mm:ss").parseLenient().appendPattern(".SSS")
                    .toFormatter();
    @Override
    public String getName() {
        return "datespacetime";
    }

    @Override
    public String convert(String str) {
        try {
            LocalDateTime dateTime = LocalDateTime.parse(str, FORMATTER);
            double jd = JulianUtil.toJulianExactly(dateTime.getYear(), dateTime.getMonthValue(), dateTime.getDayOfMonth(), dateTime.getHour(), dateTime.getMinute(), dateTime.getSecond() + (dateTime.get(ChronoField.MILLI_OF_SECOND) / 1000.0));
            return String.valueOf(jd);
        } catch (Exception e) {
            throw new IllegalStateException("datespacetime can parse date with format yyyy-MM-dd HH:mm:ss.SSS but it's \"" + str + "\"");
        }
    }
}
