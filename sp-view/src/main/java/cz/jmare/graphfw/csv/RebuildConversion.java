package cz.jmare.graphfw.csv;

public interface RebuildConversion {
    String getName();

    String convert(String str);
}
