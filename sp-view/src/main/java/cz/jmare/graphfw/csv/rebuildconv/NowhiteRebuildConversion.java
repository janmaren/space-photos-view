package cz.jmare.graphfw.csv.rebuildconv;

import cz.jmare.graphfw.csv.RebuildConversion;

public class NowhiteRebuildConversion implements RebuildConversion {
    @Override
    public String getName() {
        return "nowhite";
    }

    @Override
    public String convert(String str) {
        return str.replaceAll("\\s", "");
    }
}
