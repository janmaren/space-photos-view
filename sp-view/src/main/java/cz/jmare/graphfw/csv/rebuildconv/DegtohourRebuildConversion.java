package cz.jmare.graphfw.csv.rebuildconv;

import cz.jmare.graphfw.csv.RebuildConversion;

public class DegtohourRebuildConversion implements RebuildConversion {
    @Override
    public String getName() {
        return "degtohour";
    }

    @Override
    public String convert(String str) {
        if (str == null || str.isBlank()) {
            throw new IllegalStateException("Unable to use degtohour because operans is empty");
        }
        double parseDouble = 0;
        try {
            parseDouble = Double.parseDouble(str.trim());
        } catch (NumberFormatException e) {
            throw new IllegalStateException("Unable to use degtohour because " + str + " is not a number. Did you set correctly header lines?");
        }
        try {
            return String.valueOf(parseDouble / 15.0);
        } catch (Exception e) {
            throw new IllegalStateException("Unable to divide " + str + " by 15 because");
        }
    }
}
