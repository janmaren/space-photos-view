package cz.jmare.graphfw.csv;

public interface ConsumerThrowing<T> {
    void accept(T t) throws Exception;
}