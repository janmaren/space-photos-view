package cz.jmare.graphfw.csv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.jmare.graphfw.csv.rebuildconv.DateTimeRebuildConversion;
import cz.jmare.graphfw.csv.rebuildconv.DegtohourRebuildConversion;
import cz.jmare.graphfw.csv.rebuildconv.EscapeRebuildConversion;
import cz.jmare.graphfw.csv.rebuildconv.NowhiteRebuildConversion;

public class RowRebuilder {

    private static final Class<?>[] CONVERSION_CLASSES = {DateTimeRebuildConversion.class,
        DegtohourRebuildConversion.class, EscapeRebuildConversion.class, NowhiteRebuildConversion.class};

    static Map<String, RebuildConversion> convMap = getConversionsMap();

    /**
     * Build row, which has the same number as directives items. It uses inputCsvCols and using directives it maps input columns to result columns.
     * @param inputCsvCols input columns preparsed
     * @param directives directives where there is only a number starting from 1 pointing to inputCsvCols as an index or multiple numbers and static strings divided by comma
     * @return
     */
    public static String[] buildResultRow(String[] inputCsvCols, String[] directives) {
        String[] row = new String[directives.length];
        for (int i = 0; i < directives.length; i++) {
            if (directives[i] == null) {
                row[i] = "";
                continue;
            }
            String text = directives[i].trim();
            if (text.isEmpty()) {
                row[i] = "";
                continue;
            }
            StringBuilder sb = new StringBuilder();
            String[] splitColumns;
            try {
                splitColumns = splitColumns(text, ',');
            } catch (Exception e1) {
                throw new IllegalStateException("Invalid input " + text);
            }
            for (int k = 0; k < splitColumns.length; k++) {
                String string = splitColumns[k].trim();
                if (string.startsWith("\"")) {
                    if (string.length() == 1) {
                        throw new IllegalStateException("Text not ends with '\"'");
                    }
                    if (!string.endsWith("\"")) {
                        throw new IllegalStateException("Text not ends with '\"'");
                    }
                    sb.append(string.substring(1, string.length() - 1));
                } else {
                    String[] split = string.trim().split("\\s+");
                    int index = -1;
                    List<String> directs = new ArrayList<>();
                    for (int j = 0; j < split.length; j++) {
                        String token = split[j];
                        try {
                            int currentIndex = Integer.parseInt(token);
                            if (index != -1) {
                                throw new IllegalStateException("Multiple numbers " + currentIndex + " and " + index + " in \"" + text + "\"");
                            }
                            index = currentIndex;
                        } catch (NumberFormatException e) {
                            directs.add(token);
                        }
                    }
                    if (index == -1) {
                        throw new IllegalStateException("Not present a number of column in " + text);
                    }
                    if (index > inputCsvCols.length || index < 1) {
                        throw new IllegalStateException("Wrong number of column");
                    }
                    String out = inputCsvCols[index - 1];
                    for (String direct : directs) {
                        RebuildConversion rebuildConversion = convMap.get(direct);
                        if (rebuildConversion == null) {
                            throw new IllegalStateException("\"" + direct + " doesn't exist");
                        }
                        out = rebuildConversion.convert(out);
                    }
                    sb.append(out);
                }
            }
            row[i] = sb.toString();
        }
        return row;
    }

    public static String[] splitColumns(String line, char delimiter) {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (int i = 0; i < line.length(); i++) {
            char ch = line.charAt(i);
            if (Character.isWhitespace(ch)) {
                continue;
            }
            if (ch == '"') {
                int j = i + 1;
                while (j < line.length()) {
                    if (line.charAt(j) == '"') {
                        if (j + 1 < line.length() && line.charAt(j + 1) == '"') {
                            j++;
                        } else {
                            break;
                        }
                    }
                    j++;
                }
                String substring = line.substring(i + 1, j + 1);
                substring = "\"" + substring.replace("\"\"", "\"");
                arrayList.add(substring);
                i = j + 1;
            } else {
                int j = i;
                while (j < line.length() && line.charAt(j) != delimiter) {
                    j++;
                }
                arrayList.add(line.substring(i, j));
                i = j;
            }
        }
        if (line.endsWith(String.valueOf(delimiter))) {
            arrayList.add("");
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    public static Map<String, RebuildConversion> getConversionsMap() {
        Set<Class<?>> classes = Set.of(CONVERSION_CLASSES);

        Map<String, RebuildConversion> map = new HashMap<>();
        for (Class<?> clazz : classes) {
            try {
                RebuildConversion obj = (RebuildConversion) clazz.getDeclaredConstructor().newInstance();
                map.put(obj.getName(), obj);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        return map;
    }
}
