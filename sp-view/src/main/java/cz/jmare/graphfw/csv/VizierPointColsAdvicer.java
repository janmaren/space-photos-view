package cz.jmare.graphfw.csv;

import org.maren.spview.table.CsvShapesUtil;

import java.util.List;
import java.util.StringJoiner;

public class VizierPointColsAdvicer implements ColsAdvicer {
    private final List<String[]> origCsv;
    private final int fieldsLength;
    private boolean degrees;

    protected String[] directives;

    private Integer raIndex;

    private Integer decIndex;

    private Integer nameIndex;

    public VizierPointColsAdvicer(List<String[]> origCsv, int fieldsLength) {
        this.origCsv = origCsv;
        this.fieldsLength = fieldsLength;
        build();
    }

    protected void build() {
        directives = new String[fieldsLength];
        if (origCsv.size() < 3) {
            return;
        }
        degrees = true;
        String[] header = origCsv.get(0);
        int[] vizierRaDecIndexes = CsvShapesUtil.findVizierRaDecIndexes(header);
        if (vizierRaDecIndexes != null) {
            raIndex = vizierRaDecIndexes[0];
            decIndex = vizierRaDecIndexes[1];
            String unitRa = origCsv.get(1)[vizierRaDecIndexes[0]];
            degrees = unitRa != null && unitRa.contains("deg");
            directives[1] = (degrees ? "degtohour " : "") + (raIndex + 1) + ",\" \"," + (decIndex + 1);
        }

        nameIndex = CsvShapesUtil.find("Name", header);
        if (nameIndex != -1) {
            directives[0] = String.valueOf(nameIndex + 1);
        }

        StringJoiner tags = new StringJoiner(",\";\",");
        for (int i = 0; i < header.length; i++) {
            if (vizierRaDecIndexes != null) {
                if (vizierRaDecIndexes[0] == i || vizierRaDecIndexes[1] == i) {
                    continue;
                }
            }
            String key = header[i];
            key = key.replaceAll("\\s+", "");
            key = key.replaceAll(":", "");
            tags.add("\"" + key + ":\"," + (i + 1) + "");
        }
        if (tags.length() > 0) {
            directives[4] = tags.toString();
        }
    }

    public Integer getRaIndex() {
        return raIndex;
    }

    public Integer getDecIndex() {
        return decIndex;
    }

    @Override
    public int getHeaderLinesCount() {
        return 3;
    }

    @Override
    public String getDirective(int rowIndex) {
        return directives[rowIndex];
    }

    public boolean isDegrees() {
        return degrees;
    }

    public Integer getNameIndex() {
        return nameIndex;
    }

    public String[] getDirectives() {
        return directives;
    }
}
