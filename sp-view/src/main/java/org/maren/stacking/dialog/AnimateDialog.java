package org.maren.stacking.dialog;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import cz.jmare.image.AnimCreationConfig;
import cz.jmare.swt.util.Create;

public class AnimateDialog extends TitleAreaDialog {
    private AnimCreationConfig animCreationConfig;
    private Text delayText;
    private String resultInputDir;
    private String resultOutputFile;
    private Text inputDirectoryText;
    private Text outputFileText;
    private Button loopCheck;
    private Text lastFrameTimesText;
    
    public AnimateDialog(Shell parent, AnimCreationConfig animCreationConfig) {
        super(parent);
        this.animCreationConfig = animCreationConfig;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Animation Config");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Delay [millis]:");
        delayText = Create.text(composite, String.valueOf(animCreationConfig.getDelay()), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 30;
            t.setLayoutData(layoutData);
            t.setToolTipText("Percents how much the pixel must be bright to be a part of detected object");
        });
        
        Create.label(composite, "Loop:");
        loopCheck = Create.checkButton(composite, animCreationConfig.isLoop());

        Create.label(composite, "Last frame times:");
        lastFrameTimesText = Create.text(composite, String.valueOf(animCreationConfig.getLastFrameTimes()), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 30;
            t.setLayoutData(layoutData);
            t.setToolTipText("How much times the last frame to show (delay of last image)");
        });

        return composite;
    }

    
    @Override
    protected Point getInitialSize() {
        return new Point(600, 500);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }
        
        animCreationConfig.delay(Integer.parseInt(delayText.getText().trim()));
        animCreationConfig.lastFrameTimes(Integer.parseInt(lastFrameTimesText.getText().trim()));
        animCreationConfig.loop(loopCheck.getSelection());
        resultInputDir = inputDirectoryText.getText().trim();
        resultOutputFile = outputFileText.getText().trim();
        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        try {
            Integer val = Integer.valueOf(delayText.getText().trim());
            if (val < 0) {
                return "Delay must be positive";
            }
        } catch (NumberFormatException e) {
            return "Delay must be a number";
        }
        try {
            Integer val = Integer.valueOf(lastFrameTimesText.getText().trim());
            if (val < 1) {
                return "Last frame times must be 1 or greater";
            }
        } catch (NumberFormatException e) {
            return "Last frame times must be a number";
        }
        if ("".equals(inputDirectoryText.getText().trim())) {
            return "No input directory";
        }
        if ("".equals(outputFileText.getText().trim())) {
            return "No output directory";
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public String getResultInputDir() {
        return resultInputDir;
    }

    public String getResultOutputFile() {
        return resultOutputFile;
    }
}
