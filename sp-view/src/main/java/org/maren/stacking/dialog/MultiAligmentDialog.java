package org.maren.stacking.dialog;

import java.util.List;
import java.util.function.Supplier;

import cz.jmare.image.PixelSelectionType;
import cz.jmare.image.util.FormatUtil;
import cz.jmare.swt.util.SComposite;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import cz.jmare.image.MultiAligmentConfig;
import cz.jmare.image.resample.BilinearResampler;
import cz.jmare.image.resample.NearestNeighbourResampler;
import cz.jmare.image.resample.Resampler;
import cz.jmare.image.resample.SharpenResampler;
import cz.jmare.swt.util.Create;

public class MultiAligmentDialog extends TitleAreaDialog {
    private List<Supplier<Resampler>> RESAMPLERS = List.of(NearestNeighbourResampler::new, BilinearResampler::new, SharpenResampler::new);
    private List<String> RESAMPLERS_ITEMS = List.of("Nearest Neighbour", "Bilinear", "Sharpening");
    
    private MultiAligmentConfig multiAligmentConfig;
    private Text minPixelBrightnessText;
    private Text minRoundnessText;
    private Text minPixelsNumberText;
    private Combo resamplersCombo;
    private Button confEasy;
    private Button confExpert;

    private Text maxRmseText;

    private static int lastConf = 0;
    private Text percentileBrightnessText;
    private Button minBrightRadio;
    private Button percBrightRadio;
    private Button autoRadio;

    public MultiAligmentDialog(Shell parent, MultiAligmentConfig multiAligmentConfig) {
        super(parent);
        this.multiAligmentConfig = multiAligmentConfig;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Multi Aligment Config");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "");
        Create.label(composite, "");

        Create.label(composite, "Config mode:");
        Composite confRadios = SComposite.createGridCompositeIntoGridComposite(composite, 3);
        confEasy = Create.radioButton(confRadios, lastConf == 0, "Easy");
        confEasy.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {switchEasy();}));
        confExpert = Create.radioButton(confRadios, lastConf == 1, "Expert");
        confExpert.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {switchExpert();}));

        Create.label(composite, "Star pixels selection:");
        Composite radios = SComposite.createGridCompositeIntoGridComposite(composite, 3);
        percBrightRadio = Create.radioButton(radios, multiAligmentConfig.getPixelSelectionType() == PixelSelectionType.PERCENTILE_BRIGHTNESS, "Percentile");
        minBrightRadio = Create.radioButton(radios, multiAligmentConfig.getPixelSelectionType() == PixelSelectionType.MIN_BRIGHTNESS, "Min brightness");
        autoRadio = Create.radioButton(radios, multiAligmentConfig.getPixelSelectionType() == PixelSelectionType.AUTOMATIC, "Automatic");
        minBrightRadio.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            onSwitchRadio();
        }));
        percBrightRadio.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            onSwitchRadio();
        }));
        autoRadio.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            onSwitchRadio();
        }));

        Create.label(composite, "Percentile brightness:");
        percentileBrightnessText = Create.text(composite, multiAligmentConfig.getPercentileBrightness() != null ? FormatUtil.multi100(multiAligmentConfig.getPercentileBrightness()) : "", t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 50;
            t.setLayoutData(layoutData);
            t.setEnabled(multiAligmentConfig.getPixelSelectionType() == PixelSelectionType.PERCENTILE_BRIGHTNESS);
        });
        percentileBrightnessText.setToolTipText("How much darkest pixels will be ignored for centroid calculation\n" +
                "for example value 99 means that only last 1% brightest pixels of histogram will be used");

        Create.label(composite, "Min. pixel brightness:");
        minPixelBrightnessText = Create.text(composite, multiAligmentConfig.getMinPixelBrightness() != null ? String.valueOf(Math.round(multiAligmentConfig.getMinPixelBrightness() * 100)) : "", t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 50;
            t.setLayoutData(layoutData);
            t.setEnabled(multiAligmentConfig.getPixelSelectionType() == PixelSelectionType.MIN_BRIGHTNESS);
        });
        minPixelBrightnessText.setToolTipText("How the pixel must be bright [%] to be counted. Leave empty to detect the value");

        Create.label(composite, "Max. RMSE:");
        maxRmseText = Create.text(composite, String.valueOf(multiAligmentConfig.getMaxRmse()), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 30;
            t.setLayoutData(layoutData);
        });
        maxRmseText.setToolTipText("Maximum acceptable root mean square error in pixels. Images with greater value will be excluded");

        Create.label(composite, "Obj. with pixels number:");
        minPixelsNumberText = Create.text(composite, String.valueOf(multiAligmentConfig.getMinPixelsNumber()), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 30;
            t.setLayoutData(layoutData);
            t.setToolTipText("What number of pixels (with min pixel brightness) the object must have at least to be used for aligment");
        });

        Create.label(composite, "Min. roundness:");
        minRoundnessText = Create.text(composite, (multiAligmentConfig.getMinRoundness() != null ? String.valueOf(Math.round(multiAligmentConfig.getMinRoundness() * 100)) : ""), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 30;
            t.setLayoutData(layoutData);
            t.setToolTipText("When populated then objects with roundness lesser than this value will be ignored. 100% means it must be just round circle, 0% it may be flat line");
        });
        
        Create.label(composite, "Resampling algorithm:");
        resamplersCombo = Create.combo(composite, SWT.READ_ONLY, c -> {
            c.setItems(RESAMPLERS_ITEMS.toArray(new String[RESAMPLERS_ITEMS.size()]));
            Resampler resampler = multiAligmentConfig.getResamplerProvider().get();
            for (int i = 0; i < RESAMPLERS.size(); i++) {
                Supplier<Resampler> supplier = RESAMPLERS.get(i);
                if (resampler.getClass().isAssignableFrom(supplier.get().getClass())) {
                    c.select(i);
                    break;
                }
            }
            if (c.getSelectionIndex() < 0) {
                c.select(0);
            }
        });

        if (lastConf == 0) {
            switchEasy();
        } else {
            switchExpert();
        }

        return composite;
    }

    private void onSwitchRadio() {
        if (minBrightRadio.getSelection()) {
            minPixelBrightnessText.setEnabled(true);
            percentileBrightnessText.setEnabled(false);
        }
        if (percBrightRadio.getSelection()) {
            minPixelBrightnessText.setEnabled(false);
            percentileBrightnessText.setEnabled(true);
        }
        if (autoRadio.getSelection()) {
            minPixelBrightnessText.setEnabled(false);
            percentileBrightnessText.setEnabled(false);
        }
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(600, 600);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }

        if (minBrightRadio.getSelection()) {
            multiAligmentConfig.pixelSelectionType(PixelSelectionType.MIN_BRIGHTNESS);
        } else
        if (percBrightRadio.getSelection()) {
            multiAligmentConfig.pixelSelectionType(PixelSelectionType.PERCENTILE_BRIGHTNESS);
        } else
        if (autoRadio.getSelection()) {
            multiAligmentConfig.pixelSelectionType(PixelSelectionType.AUTOMATIC);
        }
        if (minBrightRadio.getSelection()) {
            multiAligmentConfig.minPixelBrightness(Float.parseFloat(minPixelBrightnessText.getText().trim()) / 100f);
        }
        if (percBrightRadio.getSelection()) {
            multiAligmentConfig.percentileBrightness(Float.parseFloat(percentileBrightnessText.getText().trim()) / 100f);
        }
        if (!maxRmseText.getText().isBlank()) {
            multiAligmentConfig.maxRmse(Float.parseFloat(maxRmseText.getText().trim()));
        }
        if (isExpert() && !minPixelsNumberText.getText().isBlank()) {
            multiAligmentConfig.minPixelsNumber(Integer.parseInt(minPixelsNumberText.getText().trim()));
        }
        if (isExpert() && !minRoundnessText.getText().isBlank()) {
            Float roundness = (float) (Integer.parseInt(minRoundnessText.getText().trim()) / 100f);
            multiAligmentConfig.minRoundness(roundness);
        }
        if (isExpert()) {
            multiAligmentConfig.resamplerProvider(RESAMPLERS.get(resamplersCombo.getSelectionIndex()));
        }

        lastConf = confEasy.getSelection() ? 0 : 1;

        super.buttonPressed(buttonId);
    }

    private void switchEasy() {
        //minPixelBrightnessText.setEnabled(true);
        maxRmseText.setEnabled(true);
        minPixelsNumberText.setEnabled(false);
        minRoundnessText.setEnabled(false);
        resamplersCombo.setEnabled(false);
    }

    private void switchExpert() {
        //minPixelBrightnessText.setEnabled(true);
        maxRmseText.setEnabled(true);
        minPixelsNumberText.setEnabled(true);
        minRoundnessText.setEnabled(true);
        resamplersCombo.setEnabled(true);
    }

    protected boolean isExpert() {
        return confExpert.getSelection();
    }

    protected String valid() {
        if (minBrightRadio.getSelection()) {
            if (!minPixelBrightnessText.getText().isBlank()) {
                try {
                    Float val = Float.valueOf(minPixelBrightnessText.getText().trim());
                    if (val < 0 || val > 100) {
                        return "Brightnes must be in range 0 to 100%";
                    }
                } catch (NumberFormatException e) {
                    return "Invalid min. pixel brightness";
                }
            } else {
                return "Min pixel brightness must be populated";
            }
        }
        if (percBrightRadio.getSelection()) {
            if (!percentileBrightnessText.getText().isBlank()) {
                try {
                    Float val = Float.valueOf(percentileBrightnessText.getText().trim());
                    if (val < 0 || val > 100) {
                        return "Percentile brightnes must be in range 0 to 100%";
                    }
                } catch (NumberFormatException e) {
                    return "Invalid percentile brightness";
                }
            } else {
                return "Percentile brightness must be populated";
            }
        }

        if (!maxRmseText.getText().isBlank()) {
            try {
                Float val = Float.valueOf(maxRmseText.getText().trim());
                if (val < 0 || val > 100) {
                    return "Max RMSE must be in range 0 to 100%";
                }
            } catch (NumberFormatException e) {
                return "Invalid max RMSE";
            }
        }
        if (isExpert() && !minPixelsNumberText.getText().isBlank()) {
            try {
                Integer val = Integer.valueOf(minPixelsNumberText.getText().trim());
                if (val < 1) {
                    return "obj. with pixels number must be at least 1";
                }
            } catch (NumberFormatException e) {
                return "Invalid obj. with pixels number";
            }
        }
        if (isExpert() &&  !minRoundnessText.getText().isBlank()) {
            try {
                int parseInt = Integer.parseInt(minRoundnessText.getText().trim());
                if (parseInt < 0 || parseInt > 100) {
                    return "Flatness must be in range 0..100%";
                }
            } catch (NumberFormatException e) {
                return "Flatness must be an integer number";
            }
        }

        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }
}
