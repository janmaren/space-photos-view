package org.maren.stacking.dialog;

import java.io.File;
import java.util.Locale;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.maren.stacking.util.DcrawFinderUtil;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.swt.util.Create;

public class ConfigDialog extends TitleAreaDialog {

    private Text dcrawPathText;
    private Button hotpixelRemovingCheck;
    private Text minHotpixelBrightnessText;
    private Button darksSubtractingCheck;
    private Button useDcrawCheck;
    private Text hotInterpDimText;

    public ConfigDialog(Shell parent) {
        super(parent);
    }

    protected Control createDialogArea(Composite parent) {
        //setMessage("Settings like proxy to be able download rasters, csv from a place behind proxy...");

        AppConfig appConfig = AppConfig.getInstance();
        ConfigStore configStore = appConfig.getConfigStore();
        
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Config");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);


        Create.label(composite, "Subtract darks:").setToolTipText("When disabled then darks not used for subtracting (but maybe to remove hotpixels).");
        darksSubtractingCheck = Create.checkButton(composite, configStore.getBoolValue("darks.subtract", true));

        Create.label(composite, "Hotpixel removing using darks:").setToolTipText("Whether detect and fix hot pixels. Dark frames must be present.");
        hotpixelRemovingCheck = Create.checkButton(composite, configStore.getBoolValue("hotpixel.remove", true));
        hotpixelRemovingCheck.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            minHotpixelBrightnessText.setEnabled(hotpixelRemovingCheck.getSelection());
            hotInterpDimText.setEnabled(hotpixelRemovingCheck.getSelection());
        }));

        Create.label(composite, "Hotpixel min brightness:").setToolTipText("How much brightness[%] must the pixel in dark frame have to be a hotpixel");
        minHotpixelBrightnessText = Create.text(composite, String.format(Locale.ENGLISH, "%.2f", 100 * configStore.getDoubleValue("hotpixel.min.brightness", 0.05)), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 50;
            t.setLayoutData(layoutData);
            t.setToolTipText("Typically from 5% in a dark frame is a hotpixel");
        });
        minHotpixelBrightnessText.setEnabled(hotpixelRemovingCheck.getSelection());

        Create.label(composite, "Hotpixel interpolation dimension:").setToolTipText("Odd numbers like 3, 5, 7 mean dimensions to use for interpolation to remove hot pixel.\nNo value means an auto value");
        hotInterpDimText = Create.text(composite, configStore.getValue("hot.interp.dim", ""), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 50;
            t.setLayoutData(layoutData);
            t.setToolTipText("Leave unpopulated to use an auto value or enter an odd number, for example 9");
        });
        hotInterpDimText.setEnabled(hotpixelRemovingCheck.getSelection());

        Create.label(composite, "Use dcraw:").setToolTipText("When enabled then dcraw can be used for raw to tiff conversion.\nNeeds restart");
        useDcrawCheck = Create.checkButton(composite, configStore.getBoolValue("use.dcraw", false));
        useDcrawCheck.addSelectionListener(SelectionListener.widgetSelectedAdapter((e) -> dcrawPathText.setEnabled(useDcrawCheck.getSelection())));

        Create.label(composite, "Dcraw Path:");
        dcrawPathText = Create.text(composite, String.valueOf(configStore.getValue("dcraw.path", "")), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 300;
            t.setLayoutData(layoutData);
            t.setToolTipText("Path to dcraw program on disk. It helps to read formats as NEF or CR2");
        });
        dcrawPathText.setEnabled(useDcrawCheck.getSelection());

        return composite;
    }

    
    @Override
    protected Point getInitialSize() {
        return new Point(650, 450);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }

        AppConfig appConfig = AppConfig.getInstance();
        ConfigStore configStore = appConfig.getConfigStore();
        configStore.putBoolValue("use.dcraw", useDcrawCheck.getSelection());
        String dcrawPath = dcrawPathText.getText().isEmpty() ? null : dcrawPathText.getText().trim();
        configStore.putValue("dcraw.path", dcrawPath);
        DcrawFinderUtil.checkDcrawExists(dcrawPath);

        configStore.putBoolValue("darks.subtract", darksSubtractingCheck.getSelection());
        configStore.putBoolValue("hotpixel.remove", hotpixelRemovingCheck.getSelection());

        if (hotpixelRemovingCheck.getSelection() && !minHotpixelBrightnessText.getText().isBlank()) {
            if (!minHotpixelBrightnessText.getText().isBlank()) {
                Double value = Double.parseDouble(minHotpixelBrightnessText.getText().trim());
                configStore.putDoubleValue("hotpixel.min.brightness", value / 100f);
            } else {
                configStore.putDoubleValue("hotpixel.min.brightness", null);
            }
        }
        if (hotpixelRemovingCheck.getSelection()) {
            Integer val = null;
            if (!hotInterpDimText.getText().isBlank()) {
                val = Integer.parseInt(hotInterpDimText.getText().trim());
            }
            configStore.putIntValue("hot.interp.dim", val);
        }

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        if (!dcrawPathText.getText().isEmpty()) {
            if (!new File(dcrawPathText.getText().trim()).isFile()) {
                return "Path " + dcrawPathText.getText() + " not exists";
            }
            try {
                DcrawFinderUtil.checkDcrawExists(dcrawPathText.getText().trim());
            } catch (Exception e) {
                return "It's not the right verions of dcraw (supporting -6 parameter)";
            }
        }
        if (!minHotpixelBrightnessText.getText().isBlank()) {
            try {
                Double.parseDouble(minHotpixelBrightnessText.getText().trim());
            } catch (NumberFormatException e) {
                return "Invalid hotpixel brightness number";
            }
        }
        if (!hotInterpDimText.getText().isBlank()) {
            try {
                int val = Integer.parseInt(hotInterpDimText.getText().trim());
                if (val % 2 != 1) {
                    return "Hotpixel interpolation dimension must be odd";
                }
                if (val < 3) {
                    return "Hotpixel interpolation dimension must be 3 or greater";
                }
            } catch (NumberFormatException e) {
                return "Invalid hotpixel interpolation number";
            }
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }
}
