package org.maren.stacking.dialog;

import cz.jmare.image.combine.Stacker;
import cz.jmare.swt.util.SComposite;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import cz.jmare.image.StackingConfig;
import cz.jmare.image.combine.AvgStacker;
import cz.jmare.image.combine.PercentileStacker;
import cz.jmare.swt.util.Create;

public class StackDialog extends TitleAreaDialog {
    private StackingConfig stackingConfig;
    private Combo methodCombo;
    private Text percentileText;
    private Button clipMinCheck;
    private Button clipMaxCheck;

    public StackDialog(Shell parent, StackingConfig stackingConfig) {
        super(parent);
        this.stackingConfig = stackingConfig;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Stacking Config");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 30;
        composite.setLayout(layout);

        Create.label(composite, "Method:");
        methodCombo = Create.combo(composite, SWT.READ_ONLY, t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 150;
            t.setLayoutData(layoutData);
            t.setToolTipText("Percents how much the pixel must be bright to be a part of detected object");
            t.setItems("Average", "Percentile (e.g. median)");
            if (stackingConfig.getStacker().getClass() == AvgStacker.class) {
                t.select(0);
            } else if (stackingConfig.getStacker().getClass() == PercentileStacker.class) {
                t.select(1);
            }
        });
        methodCombo.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            doEnablement();
        }));

        Create.label(composite, "Percentile:");
        String percVal = "50";
        if (stackingConfig.getStacker() instanceof PercentileStacker) {
            PercentileStacker percentileStacker = (PercentileStacker) stackingConfig.getStacker();
            percVal = String.valueOf(Math.round(percentileStacker.getPercentFraction() * 100));
        }
        percentileText = Create.text(composite, percVal, t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 50;
            t.setLayoutData(layoutData);
            t.setToolTipText("Percentile, 50% is median");
        });

        Create.label(composite, "Clipping:");
        Composite clipCom = SComposite.createGridCompositeIntoGridComposite(composite, 2);
        clipMinCheck = Create.checkButton(clipCom, false, "Clip min");
        clipMinCheck.setToolTipText("When enabled then do not count pixel color with lowest value among all input files");
        clipMaxCheck = Create.checkButton(clipCom, false, "Clip max");
        clipMaxCheck.setToolTipText("When enabled then do not count pixel color with largest value among all input files");
        Stacker stacker = stackingConfig.getStacker();
        if (stacker instanceof AvgStacker) {
            AvgStacker avgStacker = (AvgStacker) stacker;
            clipMinCheck.setSelection(avgStacker.isClipMin());
            clipMaxCheck.setSelection(avgStacker.isClipMax());
        }

        doEnablement();

        return composite;
    }

    private void doEnablement() {
        percentileText.setEnabled(methodCombo.getSelectionIndex() == 1);
        clipMinCheck.setEnabled(methodCombo.getSelectionIndex() == 0);
        clipMaxCheck.setEnabled(methodCombo.getSelectionIndex() == 0);
    }


    @Override
    protected Point getInitialSize() {
        return new Point(600, 500);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }

        if (methodCombo.getSelectionIndex() == 0) {
            AvgStacker stacker = new AvgStacker();
            stacker.setClipMin(clipMinCheck.getSelection());
            stacker.setClipMax(clipMaxCheck.getSelection());
            stackingConfig.stacker(stacker);
        } else if (methodCombo.getSelectionIndex() == 1) {
            Integer val = Integer.valueOf(percentileText.getText().trim());
            stackingConfig.stacker(new PercentileStacker(val / 100f));
        }
        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        if (methodCombo.getSelectionIndex() == 1) {
            try {
                Integer val = Integer.valueOf(percentileText.getText().trim());
                if (val < 0 || val > 100) {
                    return "Percentile must be in range 0 to 100%";
                }
            } catch (NumberFormatException e) {
                return "Invalid percentile";
            }
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }
}
