package org.maren.stacking.dialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.image.util.ImPathUtil;
import cz.jmare.swt.util.Create;

public class InputTypeDialog extends TitleAreaDialog {
    private final String[] NAMES = {"Base file", "Lights directory or files", "Darks directory or files", "Flats directory or files"};

    private final Type[] TYPES = {Type.BASE, Type.LIGHT, Type.DARK, Type.FLAT};

    List<String> names;

    List<Type> types;

    private final List<String> paths;

    private Combo typeCombo;

    private Type resultType;

    public InputTypeDialog(Shell parent, List<String> paths) {
        super(parent);
        this.paths = paths;
        names = new ArrayList<>(Arrays.stream(NAMES).collect(Collectors.toList()));
        types = new ArrayList<>(Arrays.stream(TYPES).collect(Collectors.toList()));
        if (paths.size() > 1 || (paths.size() == 1 && new File(paths.get(0)).isDirectory())) {
            names.remove(0);
            types.remove(0);
        } else if (paths.size() == 1 && new File(paths.get(0)).isFile()) {
            String[] strings = ImPathUtil.parseNameSuffix(paths.get(0));
            String format = strings[1].length() > 1 ? strings[1].substring(1) : "";
            Set<String> allReadFormats = RasImageFactory.getAllReadFormats();
            if (!allReadFormats.contains(format)) {
                names.remove(0);
                types.remove(0);
            }
        }
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Choose input file type");
        setMessage(paths.stream().collect(Collectors.joining(", ")));

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Type:");
        typeCombo = Create.combo(composite, SWT.READ_ONLY, t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 150;
            t.setLayoutData(layoutData);
            t.setItems(names.toArray(new String[names.size()]));
        });

        boolean darkSuggest = ((paths.size() == 1 && new File(paths.get(0)).isDirectory() || new File(paths.get(0)).isFile()) && paths.get(0).toLowerCase().contains("dark"));
        int suggestIndex = (darkSuggest) ? types.indexOf(Type.DARK) : -1;
        if (suggestIndex == -1) {
            boolean lightSuggest = ((paths.size() == 1 && new File(paths.get(0)).isDirectory() || new File(paths.get(0)).isFile()) && paths.get(0).toLowerCase().contains("light"));
            suggestIndex = (lightSuggest) ? types.indexOf(Type.LIGHT) : -1;
        }
        if (suggestIndex == -1) {
            boolean flatSuggest = ((paths.size() == 1 && new File(paths.get(0)).isDirectory() || new File(paths.get(0)).isFile()) && paths.get(0).toLowerCase().contains("flat"));
            suggestIndex = (flatSuggest) ? types.indexOf(Type.FLAT) : -1;
        }


        typeCombo.select(Math.max(0, suggestIndex));

        return composite;
    }

    
    @Override
    protected Point getInitialSize() {
        return new Point(500, 300);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }

        resultType = types.get(typeCombo.getSelectionIndex());

        super.buttonPressed(buttonId);
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public Type getResultType() {
        return resultType;
    }

    public static enum Type {
        BASE, LIGHT, DARK, FLAT
    }
}
