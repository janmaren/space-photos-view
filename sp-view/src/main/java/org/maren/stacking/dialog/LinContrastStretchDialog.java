package org.maren.stacking.dialog;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import cz.jmare.image.postprocess.LinearContrastStretchTrans;
import cz.jmare.swt.util.Create;

public class LinContrastStretchDialog extends TitleAreaDialog {
    private LinearContrastStretchTrans resultLinearContrastStretchTrans;
    private LinearContrastStretchTrans linearContrastStretchTrans;
    
    private Text r1Text;
    private Text s1Text;
    private Text r2Text;
    private Text s2Text;
    
    public LinContrastStretchDialog(Shell parent, LinearContrastStretchTrans linearContrastStretchTrans) {
        super(parent);
        this.linearContrastStretchTrans = linearContrastStretchTrans;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Linear Contrast Stretch Config");
        setMessage("Mapping where R input values 0-R1-R2-255 will be mapped to S output values 0-S1-S2-255");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 30;
        composite.setLayout(layout);

        GridData layoutData = new GridData();
        layoutData.widthHint = 50;

        Create.label(composite, "R1:");
        r1Text = Create.text(composite, "", t -> {
            t.setText(String.valueOf(linearContrastStretchTrans.getR1()));
            t.setToolTipText("Lower threshold for input value");
        });
        r1Text.setLayoutData(layoutData);
        
        Create.label(composite, "S1:");
        s1Text = Create.text(composite, "", t -> {
            t.setText(String.valueOf((int) linearContrastStretchTrans.getS1()));
            t.setToolTipText("Output value for lower threshold");
        });
        s1Text.setLayoutData(layoutData);
        
        Create.label(composite, "R2:");
        r2Text = Create.text(composite, "", t -> {
            t.setText(String.valueOf(linearContrastStretchTrans.getR2()));
            t.setToolTipText("Higher threshold for input value");
        });
        r2Text.setLayoutData(layoutData);
        
        Create.label(composite, "S2:");
        s2Text = Create.text(composite, "", t -> {
            t.setText(String.valueOf((int) linearContrastStretchTrans.getS2()));
            t.setToolTipText("Output value for higher threshold");
        });
        s2Text.setLayoutData(layoutData);
        
        return composite;
    }

    
    @Override
    protected Point getInitialSize() {
        return new Point(600, 500);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }
        
        resultLinearContrastStretchTrans = new LinearContrastStretchTrans(
                Integer.valueOf(r1Text.getText().trim()), Integer.valueOf(s1Text.getText().trim()),
                Integer.valueOf(r2Text.getText().trim()), Integer.valueOf(s2Text.getText().trim()));
        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        Integer r1;
        try {
            r1 = Integer.valueOf(r1Text.getText().trim());
            if (r1 < 0 || r1 > 255) {
                return "Value must be in range 0..255";
            }
        } catch (NumberFormatException e) {
            return "R1 must be a number";
        }
        Integer s1;
        try {
            s1 = Integer.valueOf(s1Text.getText().trim());
            if (s1 < 0 || s1 > 255) {
                return "Value must be in range 0..255";
            }
        } catch (NumberFormatException e) {
            return "S1 must be a number";
        }
        Integer r2;
        try {
            r2 = Integer.valueOf(r2Text.getText().trim());
            if (r2 < 0 || r2 > 255) {
                return "Value must be in range 0..255";
            }
        } catch (NumberFormatException e) {
            return "R2 must be a number";
        }
        Integer s2;
        try {
            s2 = Integer.valueOf(s2Text.getText().trim());
            if (s2 < 0 || s2 > 255) {
                return "Value must be in range 0..255";
            }
        } catch (NumberFormatException e) {
            return "S2 must be a number";
        }
        if (r1 > r2) {
            return "r1 must be lower than r2";
        }
        if (s1 > s2) {
            return "s1 must be lower than s2";
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public LinearContrastStretchTrans getResultLinearContrastStretchTrans() {
        return resultLinearContrastStretchTrans;
    }
}
