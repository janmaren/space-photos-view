package org.maren.stacking.dialog;

import java.io.File;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.maren.stacking.StackingApp;
import org.maren.stacking.util.VideoToImages;

import cz.jmare.image.util.ImPathUtil;
import cz.jmare.swt.util.Create;

public class ImportVideoDialog extends TitleAreaDialog {
    private Text outputDirText;
    private File video;
    private Thread thread;
    private VideoToImages videoToImages;
    private Label statusLabel;
    private Button importButton;
    private StackingApp stackingApp;
    
    public ImportVideoDialog(Shell parent, File video, StackingApp stackingApp) {
        super(new Shell(parent.getDisplay()));
        setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE);
        setBlockOnOpen(false);
        this.video = video;
        this.stackingApp = stackingApp;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Importing Video");
        setMessage("Spliting into image files in a directory");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Input Video:");
        Create.label(composite, video.toString());
        Create.label(composite, "Output Directory:");
        String name = video.getName();
        String[] parseNameSuffix = ImPathUtil.parseNameSuffix(name);
        outputDirText = Create.text(composite, video.getParent() + File.separator + parseNameSuffix[0], t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 400;
            t.setLayoutData(layoutData);
            t.setToolTipText("Directory where images will be stored");
        });
        
        Create.label(composite);
        Composite buttons = new Composite(composite, SWT.NONE);
        buttons.setLayout(new GridLayout(2, true));
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.horizontalAlignment = SWT.FILL;
        buttons.setLayoutData(gridData);
        
        importButton = Create.button(buttons, "Import", () -> {
            File outputDirFile;
            try {
                outputDirFile = new File(outputDirText.getText().trim());
                outputDirFile.mkdirs();
            } catch (Exception e) {
                setMessage("Invalid output directory");
                return;
            }
            importButton.setEnabled(false);
            videoToImages = new VideoToImages(video, outputDirFile, this);
            thread = new Thread(videoToImages);
            thread.start();
        });
        
        Create.button(buttons, "Interrupt", () -> {
            if (thread != null) {
                thread.interrupt();
            }
        });
        
        statusLabel = Create.label(composite, "");
        GridData statGridData = new GridData();
        statGridData.grabExcessHorizontalSpace = true;
        statGridData.horizontalAlignment = SWT.FILL;
        statGridData.horizontalSpan = 2;
        statusLabel.setLayoutData(statGridData);
        
        return composite;
    }

    
    @Override
    protected Point getInitialSize() {
        return new Point(600, 400);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CLOSE_ID) {
            if (thread.isAlive()) {
                setMessage("Unable to close - import still running");
                return;
            }
            super.buttonPressed(IDialogConstants.CANCEL_ID);
            return;
        }
        
        super.buttonPressed(buttonId);
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.CLOSE_ID, IDialogConstants.CLOSE_LABEL, false);
    }

    public void progressFile(File outputFile) {
        statusLabel.setText(outputFile.toString());
    }

    public void finished() {
        statusLabel.setText("");
        if (videoToImages.getException() == null) {
            setMessage("Finished import into " + outputDirText.getText());
            File resultOutputDir = new File(outputDirText.getText().trim());
            stackingApp.finishedImportVideo(resultOutputDir);
        }
        importButton.setEnabled(true);
    }
}
