package org.maren.stacking.dialog;

import java.util.List;
import java.util.function.Supplier;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import cz.jmare.image.SingleAligmentConfig;
import cz.jmare.image.crop.RadiusCentroidCropper;
import cz.jmare.image.resample.BilinearResampler;
import cz.jmare.image.resample.NearestNeighbourResampler;
import cz.jmare.image.resample.Resampler;
import cz.jmare.image.resample.SharpenResampler;
import cz.jmare.swt.util.Create;

public class SingleAligmentDialog extends TitleAreaDialog {
    private List<Supplier<Resampler>> RESAMPLERS = List.of(NearestNeighbourResampler::new, BilinearResampler::new, SharpenResampler::new);
    private List<String> RESAMPLERS_ITEMS = List.of("Nearest Neighbour", "Bilinear", "Sharpening");
    private SingleAligmentConfig singleAligmentConfig;
    private Text minPixelBrightnessText;
    private Text aroundCenterXText;
    private Text aroundCenterYText;
    private Button cropCheck;
    private Combo resamplersCombo;
    
    public SingleAligmentDialog(Shell parent, SingleAligmentConfig singleAligmentConfig) {
        super(parent);
        this.singleAligmentConfig = singleAligmentConfig;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Single Aligment Config");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 30;
        composite.setLayout(layout);

        Create.label(composite, "Min. Pixel Brightness:");
        minPixelBrightnessText = Create.text(composite, String.valueOf(Math.round(singleAligmentConfig.getMinPixelBrightness() * 100)), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 30;
            t.setLayoutData(layoutData);
            t.setToolTipText("Percents how much the pixel must be bright to be a part of detected object");
        });

        Create.label(composite, "Crop:");
        boolean cropEnabled = singleAligmentConfig.getCropFactory() != null;
        cropCheck = Create.checkButton(composite, cropEnabled, b -> {
            b.setToolTipText("When enabled an addidtional crop will be made on aligned image");
        }, c -> {
            if (c.getSelection()) {
                aroundCenterXText.setEnabled(true);
                aroundCenterYText.setEnabled(true);
            } else {
                aroundCenterXText.setEnabled(false);
                aroundCenterYText.setEnabled(false);
            }
        });
        
        String aroundX = "";
        String aroundY = "";
        if (singleAligmentConfig.getCropFactory() instanceof RadiusCentroidCropper) {
            RadiusCentroidCropper radiusCentroidCropper = (RadiusCentroidCropper) singleAligmentConfig.getCropFactory();
            aroundX = String.valueOf(radiusCentroidCropper.getCropRadiusX());
            aroundY = String.valueOf(radiusCentroidCropper.getCropRadiusY());
        }
        Create.label(composite, "Crop Around Center X:");
        aroundCenterXText = Create.text(composite, aroundX, t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 50;
            t.setLayoutData(layoutData);
            t.setEnabled(cropEnabled);
            t.setToolTipText("How many pixels from the centre of dominant object to crop to the left and right");
        });
        
        Create.label(composite, "Crop Around Center Y:");
        aroundCenterYText = Create.text(composite, aroundY, t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 50;
            t.setLayoutData(layoutData);
            t.setEnabled(cropEnabled);
            t.setToolTipText("How many pixels from the centre of dominant object to crop up and down");
        });
        
        Create.label(composite, "Resampling algorithm:");
        resamplersCombo = Create.combo(composite, SWT.READ_ONLY, c -> {
            c.setItems(RESAMPLERS_ITEMS.toArray(new String[RESAMPLERS_ITEMS.size()]));
            Resampler resampler = singleAligmentConfig.getResamplerProvider().get();
            for (int i = 0; i < RESAMPLERS.size(); i++) {
                Supplier<Resampler> supplier = RESAMPLERS.get(i);
                if (resampler.getClass().isAssignableFrom(supplier.get().getClass())) {
                    c.select(i);
                    break;
                }
            }
            if (c.getSelectionIndex() < 0) {
                c.select(0);
            }
        });
        
        return composite;
    }

    
    @Override
    protected Point getInitialSize() {
        return new Point(600, 500);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }

        singleAligmentConfig.minPixelBrightness(Integer.parseInt(minPixelBrightnessText.getText().trim()) / 100f);
        if (cropCheck.getSelection()) {
            singleAligmentConfig.cropFactory(new RadiusCentroidCropper(Integer.parseInt(aroundCenterXText.getText().trim()), Integer.parseInt(aroundCenterYText.getText().trim())));
        }
        singleAligmentConfig.resamplerProvider(RESAMPLERS.get(resamplersCombo.getSelectionIndex()));
        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        try {
            Integer.valueOf(minPixelBrightnessText.getText().trim());
        } catch (NumberFormatException e) {
            return "Invalid min pixel brightness";
        }
        if (cropCheck.getSelection()) {
            try {
                Integer.valueOf(aroundCenterXText.getText().trim());
            } catch (NumberFormatException e) {
                return "Invalid value around center x";
            }
            try {
                Integer.valueOf(aroundCenterYText.getText().trim());
            } catch (NumberFormatException e) {
                return "Invalid value around center y";
            }
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }
}
