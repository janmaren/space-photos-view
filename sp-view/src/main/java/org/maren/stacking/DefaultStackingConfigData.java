package org.maren.stacking;

import java.util.HashMap;
import java.util.Map;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;

public class DefaultStackingConfigData {
    public static Map<String, String> DATA = new HashMap<>();

    static {
        DATA.put("darks.subtract", "true");

        DATA.put("hotpixel.remove", "true");
    }

    public static void populateConfig(boolean override) {
        AppConfig instance = AppConfig.getInstance();
        ConfigStore configStore = instance.getConfigStore();
        Map<String, String> data = DATA;
        for (Map.Entry<String, String> entry : data.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String cVal = configStore.getValue(key, null);
            if (override || cVal == null) {
                configStore.putValue(key, value);
            }
        }
    }
}
