package org.maren.stacking;

import javax.imageio.spi.IIORegistry;

import org.eclipse.swt.widgets.Display;
import org.maren.stacking.util.ExternalUtilsFinder;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import cz.jmare.config.AppConfig;
import cz.jmare.fits.image.io.FITSReaderSpi;

import java.io.PrintStream;

public class StackingMain {

    public static void main(String[] args) {
        AppConfig instance = AppConfig.getInstance();
        instance.init(".astacking");
        System.setProperty("DEV_HOME", instance.getExistingDataDir().toString());

        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.stop();

        DefaultStackingConfigData.populateConfig(false);

        IIORegistry registry = IIORegistry.getDefaultInstance();
        registry.registerServiceProvider(new FITSReaderSpi());

        if (instance.getConfigStore().getBoolValue("use.dcraw", false)) {
            new Thread(new ExternalUtilsFinder()).start();
        }

        PrintStream origOut = System.out;
        PrintStream origErr = System.err;

        StackingApp awin = new StackingApp();
        if (args != null && args.length > 0) {
            awin.setInitialOpenBaseFile(args[0]);
        }
        if (args != null && args.length > 1) {
            awin.setInitialLightsDirectory(args[1]);
        }
        if (args != null && args.length > 2) {
            awin.setInitialOutputRootDirectory(args[2]);
        }
        awin.setBlockOnOpen(true);
        try {
            awin.open();
        } catch (Throwable e) {
            System.setOut(origOut);
            System.setErr(origErr);
            e.printStackTrace();
        }
        Display.getCurrent().dispose();
    }
}
