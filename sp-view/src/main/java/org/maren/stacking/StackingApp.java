package org.maren.stacking;

import static org.eclipse.swt.events.SelectionListener.widgetSelectedAdapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SwtCallable;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.maren.spview.wcstransform.image.RasImageData;
import org.maren.stacking.dialog.AnimateDialog;
import org.maren.stacking.dialog.ConfigDialog;
import org.maren.stacking.dialog.InputTypeDialog;
import org.maren.stacking.dialog.MultiAligmentDialog;
import org.maren.stacking.dialog.SingleAligmentDialog;
import org.maren.stacking.dialog.StackDialog;
import org.maren.stacking.util.ExeUtil;
import org.maren.stacking.util.FilesInDirUtil;
import org.maren.stacking.util.LastFileUtil;
import org.maren.stacking.util.ProgressIndicator;
import org.maren.stacking.util.RotUtil;
import org.maren.stacking.util.RotateImageDataUtil;
import org.maren.stacking.util.TaskType;
import org.maren.stacking.util.UnprocessableFilesException;
import org.maren.swt.util.BufImageSWT;
import org.maren.swt.util.StackCompositeUtil;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.image.AligmentType;
import cz.jmare.image.ImageProcess;
import cz.jmare.image.ImageProcessConfig;
import cz.jmare.image.PixelSelectionType;
import cz.jmare.image.RotationCounterClockwise;
import cz.jmare.image.align.FindAnalyzerUtil;
import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.ras.RGBPix;
import cz.jmare.image.ras.RasBufImage;
import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.stat.HistogramStat;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.FileConstants;
import cz.jmare.image.util.ImFilesUtil;
import cz.jmare.image.util.RGBPixUtil;
import cz.jmare.os.AppHelper;
import cz.jmare.os.PlatformType;
import cz.jmare.pureraster.DrawScreenInfo;
import cz.jmare.pureraster.PositionData;
import cz.jmare.pureraster.RasterToolListener;
import cz.jmare.rastercomposite.RasterComposite;
import cz.jmare.swt.action.ActionOperation;
import cz.jmare.swt.action.ActionProcess;
import cz.jmare.swt.color.ColorUtil;
import cz.jmare.swt.console.ConsoleCompositeSimple;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.key.KeyLogger;
import cz.jmare.swt.key.MouseLogger;
import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.SComposite;
import cz.jmare.swt.util.SGroup;

public class StackingApp extends ApplicationWindow implements RasterToolListener {
    private final static List<String> SUPPORTED_VIDEO_FORMATS = List.of("mp4", "mov", "mkv", "ts", "ps", "adts", "dpx");
    public static final String NO_BASE = "<No base file>";
    public static final String THE_BASE = "Base file: ";
    public static final String NO_LIGHT = "<No lights directory>";
    public static final String THE_LIGHT = "Light files: ";
    public static final String NO_DARK = "<No darks directory>";
    public static final String THE_DARK = "Darks dir: ";
    public static final String NO_FLAT = "<No flats directory>";
    public static final String THE_FLAT = "Flats dir: ";

    public static final String THE_OUTPUT_DIR = "Output dir: ";

    StatusLineManager slm = new StatusLineManager();
    ToolBarManager toolbarManager = new ToolBarManager();
    private MenuManager menuFile;

    private MenuManager menuHelp;
    MenuManager menuManager = new MenuManager();
    private RasterComposite rasterComposite;

    private Thread thread;
    private List<Centroid> centroids = new ArrayList<>();
    private Button alignCheck;
    private Button combineCheck;
    private Text colorShowLabel;
    private RasImageRead showingRasImage;
    private Button baseRadioButton;
    private Button lightRadioButton;
    private Button darkRadioButton;
    private Button stackedRadioButton;
    private Button runButton;
    private Button stopButton;
    private Group alignComposite;
    private Group combineComposite;
    private static Color starsColor;
    private static Color excenColor;
    private Combo alignTypeCombo;
    
    private StyledText alignInfoText;
    private Combo combineTypeCombo;
    private StyledText combineInfoText;

    private ConsoleCompositeSimple consoleCompositeSimpleBottom;
    private Button showBrightNess;
    private Label baseFileLabel;
    private Label darkDirLabel;

    private Label flatDirLabel;

    private Label rootOutputDirLabel;

    private ActionProcess openBaseFileOperation;

    private ActionProcess openLightsDirOperation;
    private ActionProcess openDarksDirOperation;

    private ImageProcessConfig imageProcessConfig;
    private ActionProcess openFlatsDirOperation;
    private Button flatRotButton;
    private Button darkRotButton;

    private ActionOperation configAboutOperation;

    private Image baseImage;

    private Image lightImage;

    private Image stackedImage;

    private Image darkImage;

    private volatile Image dark90Image;

    private volatile Image dark180Image;

    private volatile Image dark270Image;

    private Image flatImage;

    private volatile Image flat90Image;

    private volatile Image flat180Image;

    private volatile Image flat270Image;

    private ExecutorService executor = Executors.newFixedThreadPool(5);
    private String initialOpenBaseFile;

    private String initialLightsDirectory;

    private Button flatRadioButton;
    private Label lightsDirLabel;

    private boolean hasImageBeenShowed;
    private Button removeLightsButton;
    private Button removeDarkButton;
    private Button removeFlatButton;
    private Button removeBaseButton;
    private String initialOutputRootDirectory;

    public StackingApp() {
        super(null);
        menuManager.add(menuFile = new MenuManager("File"));
        menuManager.add(menuHelp = new MenuManager("Help"));
        addMenuBar();
        addToolBar(SWT.FLAT | SWT.WRAP);
        addStatusLine();
    }

    protected Control createContents(Composite parent) {
        KeyLogger.init();
        MouseLogger.initWithStatus(slm);


        imageProcessConfig = new ImageProcessConfig();
        applyGlobalConfigs();
        imageProcessConfig.align(true);
        imageProcessConfig.combine(true);
        imageProcessConfig.align(AligmentType.MULTI);
        imageProcessConfig.stack(true);
        imageProcessConfig.animate(false);

        SashForm sashComposite = new SashForm(parent, SWT.VERTICAL);
        GridLayout gridLayout = new GridLayout();
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        sashComposite.setLayout(gridLayout);
        GridData sashLayoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
        sashComposite.setLayoutData(sashLayoutData);
        sashComposite.setBackground(getShell().getDisplay().getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
        sashComposite.addMouseTrackListener(new MouseTrackListener() {
            private Cursor origCursor;
            @Override
            public void mouseEnter(MouseEvent e) {
                origCursor = sashComposite.getCursor();
                sashComposite.setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_HAND));
            }

            @Override
            public void mouseExit(MouseEvent e) {
                sashComposite.setCursor(origCursor);
            }

            @Override
            public void mouseHover(MouseEvent e) {

            }
        });
        
        Composite composite = new Composite(sashComposite, SWT.NONE);
        
        consoleCompositeSimpleBottom = new ConsoleCompositeSimple(sashComposite, SWT.NONE);
        sashComposite.setWeights(80, 20);
        FormLayout formLayout = new FormLayout ();
        formLayout.marginWidth = 4;
        formLayout.marginHeight = 1;
        formLayout.spacing = 3;
        composite.setLayout (formLayout);

        FormData data = null;

        Composite consoleCompositeSimple = new Composite(composite, SWT.BORDER);
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(100, -1); // -150
        data.bottom = new FormAttachment(100, 0);
        consoleCompositeSimple.setLayoutData(data);
 
        Composite rightEditGroup = new Composite(composite, SWT.BORDER);
        data = new FormData ();
        data.left = new FormAttachment (100, -300);
        data.right = new FormAttachment (100, 0);
        data.top = new FormAttachment (0, 0);
        data.bottom = new FormAttachment (consoleCompositeSimple, 0, SWT.DEFAULT);
        rightEditGroup.setLayoutData (data);
        rightEditGroup.setLayout(new GridLayout());
 
        createRightContent(rightEditGroup);

        Group imageManageGroup = new Group(composite, SWT.NONE);
        imageManageGroup.setText("Color");
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(rightEditGroup, 0, SWT.DEFAULT);
        data.top = new FormAttachment(consoleCompositeSimple, -65, SWT.DEFAULT);
        data.bottom = new FormAttachment(consoleCompositeSimple, 0, SWT.DEFAULT);
        imageManageGroup.setLayoutData(data);
        imageManageGroup.setLayout(new GridLayout());
        colorShowLabel = new Text(imageManageGroup, SWT.READ_ONLY);
        GridData csData = new GridData();
        csData.grabExcessHorizontalSpace = true;
        csData.horizontalAlignment = SWT.FILL;
        colorShowLabel.setLayoutData(csData);
        
        Composite leftUpComposite = SGroup.createNoMarginGridComposite(composite, 1);

        ImageUtil.put("remove", "/image/remove.png");

        Composite baseRowComp = StackCompositeUtil.createGridCompositeIntoGridComposite(leftUpComposite, 3, gd -> {gd.grabExcessVerticalSpace = false; gd.verticalAlignment = SWT.BEGINNING;});
        baseFileLabel = Create.label(baseRowComp, NO_BASE, l -> {
            GridData layoutData = new GridData();
            layoutData.grabExcessHorizontalSpace = true;
            layoutData.horizontalAlignment = SWT.FILL;
            l.setLayoutData(layoutData);
        });
        removeBaseButton = Create.imageButton(baseRowComp, ImageUtil.get("remove"), "Remove", () -> {
            doSetBaseFile(null, true);
        });
        removeBaseButton.setEnabled(false);

        Composite lightsRowComp = StackCompositeUtil.createGridCompositeIntoGridComposite(leftUpComposite, 3, gd -> {gd.grabExcessVerticalSpace = false; gd.verticalAlignment = SWT.BEGINNING;});
        lightsDirLabel = Create.label(lightsRowComp, NO_LIGHT, l -> {
            GridData layoutData = new GridData();
            layoutData.grabExcessHorizontalSpace = true;
            layoutData.horizontalAlignment = SWT.FILL;
            l.setLayoutData(layoutData);
        });
        removeLightsButton = Create.imageButton(lightsRowComp, ImageUtil.get("remove"), "Remove", () -> {
            doSetLightsFile(null);
        });
        removeLightsButton.setEnabled(false);

        Composite darkRowComp = StackCompositeUtil.createGridCompositeIntoGridComposite(leftUpComposite, 3, gd -> {gd.grabExcessVerticalSpace = false; gd.verticalAlignment = SWT.BEGINNING;});
        darkDirLabel = Create.label(darkRowComp, NO_DARK, l -> {
            GridData layoutData = new GridData();
            layoutData.grabExcessHorizontalSpace = true;
            layoutData.horizontalAlignment = SWT.FILL;
            l.setLayoutData(layoutData);
        });

        ImageUtil.put("rotcc-90", "/image/rotcc90.png");
        ImageUtil.put("rotcc-180", "/image/rotcc180.png");
        ImageUtil.put("rotcc-270", "/image/rotcc270.png");
        ImageUtil.put("rot-no", "/image/rotno.png");

        darkRotButton = Create.imageButton(darkRowComp, ImageUtil.get("rot-no"), "Rotate", () -> {
            RotationCounterClockwise nextRotation = RotUtil.getNextRotation(imageProcessConfig.getDarkRotation());
            imageProcessConfig.darkRotation(nextRotation);
            darkRotButton.setImage(ImageUtil.get(RotUtil.getRotationImage(nextRotation)));
            darkRotButton.setToolTipText(RotUtil.getHint(nextRotation));
            switchDarkAsync();
        });
        darkRotButton.setToolTipText(RotUtil.getHint(null));
        darkRotButton.setEnabled(false);
        removeDarkButton = Create.imageButton(darkRowComp, ImageUtil.get("remove"), "Remove", () -> {
            doSetDarkFile(null);
        });
        removeDarkButton.setEnabled(false);

        Composite flatRowComp = StackCompositeUtil.createGridCompositeIntoGridComposite(leftUpComposite, 3, gd -> {gd.grabExcessVerticalSpace = false; gd.verticalAlignment = SWT.BEGINNING;});
        flatDirLabel = Create.label(flatRowComp, NO_FLAT, l -> {
            GridData layoutData = new GridData();
            layoutData.grabExcessHorizontalSpace = true;
            layoutData.horizontalAlignment = SWT.FILL;
            l.setLayoutData(layoutData);
        });
        flatRotButton = Create.imageButton(flatRowComp, ImageUtil.get("rot-no"), "Rotate", () -> {
            RotationCounterClockwise nextRotation = RotUtil.getNextRotation(imageProcessConfig.getFlatRotation());
            imageProcessConfig.flatRotation(nextRotation);
            flatRotButton.setImage(ImageUtil.get(RotUtil.getRotationImage(nextRotation)));
            flatRotButton.setToolTipText(RotUtil.getHint(nextRotation));
            switchFlatExecute();
        });
        flatRotButton.setToolTipText(RotUtil.getHint(null));
        flatRotButton.setEnabled(false);
        removeFlatButton = Create.imageButton(flatRowComp, ImageUtil.get("remove"), "Remove", () -> {
            doSetFlatFile(null);
        });
        removeFlatButton.setEnabled(false);

        rootOutputDirLabel = Create.label(leftUpComposite, THE_OUTPUT_DIR, l -> {
            GridData layoutData = new GridData();
            layoutData.grabExcessHorizontalSpace = true;
            layoutData.horizontalAlignment = SWT.FILL;
            l.setLayoutData(layoutData);
        });

        Composite radiosComposite = new Composite(leftUpComposite, SWT.NONE);
        radiosComposite.setLayout(new RowLayout());
        baseRadioButton = Create.radioButton(radiosComposite, true, "Base", sel -> {if (baseRadioButton.getSelection()) switchBaseExecute();});
        lightRadioButton = Create.radioButton(radiosComposite, false, "Light", sel -> {if (lightRadioButton.getSelection()) switchLightExecute();});
        darkRadioButton = Create.radioButton(radiosComposite, false, "Dark", sel -> {if (darkRadioButton.getSelection()) switchDarkAsync();});
        flatRadioButton = Create.radioButton(radiosComposite, false, "Flat", sel -> {if (flatRadioButton.getSelection()) switchFlatExecute();});
        stackedRadioButton = Create.radioButton(radiosComposite, false, "Stacked", sel -> {if (stackedRadioButton.getSelection()) switchStackedExecute();});
        Label progressLabel = new Label(radiosComposite, SWT.NONE);
        progressLabel.setText("Text");

        rasterComposite = new RasterComposite(leftUpComposite, SWT.NONE, RasterComposite.FEATURE_NO | RasterComposite.FEATURE_DRAW_CURSOR);
        rasterComposite.setRasterToolListener(this);
        GridData rasterGridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        rasterComposite.setLayoutData(rasterGridData);
        rasterComposite.setDrawObjectsListener((GC gc, DrawScreenInfo drawScreenInfo) -> {
            Float requiredRoundness = null;
            try {
                requiredRoundness = imageProcessConfig.getMultiAligmentConfig().getMinRoundness();
            } catch (Exception e) {
            }
            Color origColor = gc.getForeground();
            for (Centroid centroid : centroids) {
                gc.setForeground(starsColor);
                int screenX = drawScreenInfo.toScreenX(centroid.x);
                int screenY = drawScreenInfo.toScreenY(centroid.y);
                if (screenX < 0 || screenY < 0 || screenX > drawScreenInfo.width || screenY > drawScreenInfo.height) {
                    continue;
                }
                
                int minRadius = drawScreenInfo.distRounded(centroid.minRadius);
                if (minRadius > 1) {
                    gc.drawOval(screenX - minRadius, screenY - minRadius, 2 * minRadius, 2 * minRadius);
                } else {
                    gc.drawPoint(screenX, screenY);
                }

                if (requiredRoundness != null && centroid.number >= 25 && centroid.minRadius / centroid.maxRadius < requiredRoundness) {
                    gc.setForeground(excenColor);
                    int maxRadius = drawScreenInfo.distRounded(centroid.maxRadius);
                    if (maxRadius > 1) {
                        gc.drawOval(screenX - maxRadius, screenY - maxRadius, 2 * maxRadius, 2 * maxRadius);
                    } else {
                        gc.drawPoint(screenX, screenY);
                    }
                }
            }
            gc.setForeground(origColor);
        });
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(rightEditGroup, 0, SWT.DEFAULT);
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(imageManageGroup, 0, SWT.DEFAULT);
        leftUpComposite.setLayoutData(data);
        rasterComposite.setRasterCursorMoveListener((positionData, mouseEvent) -> showColor(positionData, mouseEvent));

        
        initActions();
        populateMenuFile();
        populateHelpFile();
        menuManager.update(true);
        menuFile.update(true);
        menuHelp.update(true);
        toolbarManager.update(true);
        
        initDrop();

        starsColor = ColorUtil.getColor("#2CF084");
        excenColor = ColorUtil.getColor("#FF0000");

        openBaseFileOperation.setEnabled(true);

        if (initialOpenBaseFile != null) {
            doSetBaseFile(initialOpenBaseFile);
        }
        if (initialLightsDirectory != null) {
            if (initialOpenBaseFile == null) {
                doSetLightsFile(List.of(initialLightsDirectory));
            } else {
                setLightsDirFile(List.of(initialLightsDirectory));
            }
        }
        setRootOutputDirectory(initialOutputRootDirectory);

        ProgressIndicator.run(progressLabel, getShell().getDisplay());

        setInfoMessage("Open lights directory or drag and drop it to the application");

        return composite;
    }

    private void showColor(PositionData positionData, MouseEvent mouseEvent) {
        if (showingRasImage != null && positionData.imageX != null && positionData.imageY != null && (mouseEvent.stateMask & SWT.CONTROL) != SWT.CONTROL) {
            RGBPix rgb = new RGBPix();
            showingRasImage.getRGB((int) (double) positionData.imageX, (int) (double) positionData.imageY, rgb);
            String colorText = "RGB: " + rgb.red + ", " + rgb.green + ", " + rgb.blue + "\t" + RGBPixUtil.toRGBHexString(rgb);
            String brightnessText = "\t\tBrightness: " + (String.format(Locale.ENGLISH, "%.0f", (rgb.red + rgb.green + rgb.blue) / 3.0 / 255.0 * 100) + "%");
            colorText += brightnessText;
            colorShowLabel.setText(colorText);
        }
    }

    private void createRightContent(Composite rightEditGroup) {
        ImageUtil.put("run", "/image/control-right.png");
        ImageUtil.put("stop", "/image/stop.png");
        rightEditGroup.setLayout(new GridLayout());

        GridData configRightGridData = new GridData();
        configRightGridData.widthHint = 60;
        configRightGridData.grabExcessHorizontalSpace = true;
        configRightGridData.horizontalAlignment = SWT.END;
        
        Composite launchComposite = SComposite.createGridCompositeIntoGridComposite(rightEditGroup, 3, (gd) -> {
            gd.grabExcessVerticalSpace = false;
        });
        alignCheck = Create.checkButton(launchComposite, imageProcessConfig.isAligment(), "Align");
        alignCheck.addSelectionListener(widgetSelectedAdapter(e -> {
            imageProcessConfig.align(alignCheck.getSelection());
        }));
        combineCheck = Create.checkButton(launchComposite, imageProcessConfig.isCombine(), "Combine");
        combineCheck.addSelectionListener(widgetSelectedAdapter(e -> {
            imageProcessConfig.combine(combineCheck.getSelection());
        }));

        runButton = Create.button(launchComposite, SWT.NONE, "RUN ", b->{
            b.setImage(ImageUtil.get("run"));
        }, (selEvent) -> {
            try {
                if ((selEvent.stateMask & SWT.CONTROL) == SWT.CONTROL) {
                    imageProcessConfig.debug(true);
                }
                consoleCompositeSimpleBottom.clear();
            } catch (Exception e) {
                setErrorMessage(e);
                return;
            }
            disableControls();
            thread = new Thread(() -> {
                try {
                    if (!validate()) {
                        return;
                    }
                    asyncExec(() -> disableControls());
                    if (ImageProcess.doImageProcess(imageProcessConfig)) {
                        getShell().getDisplay().asyncExec(() -> {
                            if (imageProcessConfig.isStack()) {
                                stackedImage = null;
                                switchStackedExecute();
                            } else {
                                switchBaseExecute();
                            }
                        });
                    }
                } finally {
                    asyncExec(() -> {
                        enableControls();
                    });
                }
            });
            thread.start();
        });
        stopButton = Create.button(launchComposite, "STOP ", b->{
            b.setImage(ImageUtil.get("stop"));
            b.setEnabled(false);
        }, () -> {
            if (thread != null) {
                thread.interrupt();
            }
        });

        Create.label(launchComposite);
        
        alignComposite = SGroup.createGridGroupIntoGridComposite(rightEditGroup, "Align", 4, gd -> {});
        Create.label(alignComposite, "Type:");
        alignTypeCombo = Create.combo(alignComposite, SWT.BORDER | SWT.READ_ONLY, c -> {
            GridData layoutData = new GridData();
            layoutData.horizontalSpan = 2;
            c.setLayoutData(layoutData);
            c.setItems("Single", "Multi");
            c.select(imageProcessConfig.getAligmentType() == AligmentType.MULTI ? 1 : 0);
            c.addSelectionListener(widgetSelectedAdapter(e -> {
                imageProcessConfig.align(alignTypeCombo.getSelectionIndex() == 0 ? AligmentType.SINGLE : AligmentType.MULTI);
                populateAlignInfoText();
            }));
            c.setToolTipText("Single means the photo contains one dominant object.\nMulti means there are many objects, like stars");
        });
        Create.button(alignComposite, SWT.NONE, "Config", (b) -> {
            b.setLayoutData(configRightGridData);
        }, (e) -> {
            if (alignTypeCombo.getSelectionIndex() == 0) {
                SingleAligmentDialog singleAligmentDialog = new SingleAligmentDialog(getShell(), imageProcessConfig.getSingleAligmentConfig());
                if (singleAligmentDialog.open() == Window.OK) {
                    populateAlignInfoText();
                }
            } else {
                MultiAligmentDialog multiAligmentDialog = new MultiAligmentDialog(getShell(), imageProcessConfig.getMultiAligmentConfig());
                if (multiAligmentDialog.open() == Window.OK) {
                    populateAlignInfoText();
                    showBrightNess.setEnabled(imageProcessConfig.getMultiAligmentConfig().getMinPixelBrightness() != null);
                }
            }
        });
        GridData infoLayoutData = new GridData();
        infoLayoutData.horizontalSpan = 4;
        infoLayoutData.heightHint = 50;
        infoLayoutData.grabExcessHorizontalSpace = true;
        infoLayoutData.horizontalAlignment = SWT.FILL;
        alignInfoText = new StyledText(alignComposite, SWT.READ_ONLY | SWT.WRAP);
        alignInfoText.setLayoutData(infoLayoutData);
        alignInfoText.setBackground(getShell().getBackground());
        populateAlignInfoText();
        
        Create.label(alignComposite, "Preview:", t -> {
            t.setToolTipText("Preview od star detection using defined brightness. Available only when brightness defined - config dialog");
        });
        showBrightNess = Create.button(alignComposite, "Show", () -> {
            try {
                Float minPixelBrightness = null;
                CachedImage cachedImage = imageProcessConfig.getBaseFile();
                if (imageProcessConfig.getAligmentType() == AligmentType.MULTI) {
                    minPixelBrightness = imageProcessConfig.getMultiAligmentConfig().getMinPixelBrightness();
                    CentroidsAnalyzer centroidsAnalyzer = null;
                    if (imageProcessConfig.getMultiAligmentConfig().getPixelSelectionType() == PixelSelectionType.AUTOMATIC) {
                        centroidsAnalyzer = FindAnalyzerUtil.findAnalyzer(cachedImage, imageProcessConfig.getMultiAligmentConfig());
                    } else {
                        Float baseMinPixelBrightness = minPixelBrightness;
                        if (imageProcessConfig.getMultiAligmentConfig().getPixelSelectionType() == PixelSelectionType.PERCENTILE_BRIGHTNESS) {
                            minPixelBrightness = HistogramStat.calcThresholdByPercentil(cachedImage.getRasImageRead(), imageProcessConfig.getMultiAligmentConfig().getPercentileBrightness());
                            baseMinPixelBrightness = HistogramStat.calcThresholdByPercentil(cachedImage.getRasImageRead(), imageProcessConfig.getMultiAligmentConfig().getPercentileBrightness());
                        }
                        centroidsAnalyzer = new CentroidsAnalyzer(cachedImage, baseMinPixelBrightness, imageProcessConfig.getMultiAligmentConfig().getMinPixelsNumber());
                    }
                    centroids = centroidsAnalyzer.centroidsSortedScoreDesc();
                } else if (imageProcessConfig.getAligmentType() == AligmentType.SINGLE) {
                    minPixelBrightness = imageProcessConfig.getSingleAligmentConfig().getMinPixelBrightness();
                    CentroidsAnalyzer centroidsAnalyzer = new CentroidsAnalyzer(cachedImage, minPixelBrightness, imageProcessConfig.getMultiAligmentConfig().getMinPixelsNumber());
                    centroids = centroidsAnalyzer.centroidsSortedScoreDesc();
                }
                if (minPixelBrightness == null) {
                    setErrorMessage("No custom brightness defined in configuration");
                    return;
                }
                //slm.setMessage("View objects with at least " + imageProcessConfig.getMultiAligmentConfig().getMinPixelsNumber() + " pixels with brightness at least " + Math.round(minPixelBrightness * 100) + "%");
                //CentroidsAnalyzer centroidsAnalyzer = new CentroidsAnalyzer(cachedImage, minPixelBrightness, imageProcessConfig.getMultiAligmentConfig().getMinPixelsNumber());
                //centroids = centroidsAnalyzer.centroidsSortedScoreDesc();
                rasterComposite.refresh();
            } catch (Exception e) {
                setErrorMessage(e.getMessage());
                return;
            }
        });
        showBrightNess.setEnabled(false);
        showBrightNess.setToolTipText("Min pixel brightness must be set manually in config");
        Create.button(alignComposite, "Clear", () -> {
            centroids = List.of();
            rasterComposite.refresh();
        });
        
        combineComposite = SGroup.createGridGroupIntoGridComposite(rightEditGroup, "Combine", 4, gd -> {});
        Create.label(combineComposite, "Type:");
        combineTypeCombo = Create.combo(combineComposite, SWT.BORDER | SWT.READ_ONLY, c -> {
            GridData layoutData = new GridData();
            layoutData.horizontalSpan = 1;
            c.setLayoutData(layoutData);
            c.setItems("Stack", "Animation");
            c.select(0);
            c.addSelectionListener(widgetSelectedAdapter(e -> {
                populateCombineInfoText();
            }));
        });
        combineTypeCombo.addSelectionListener(widgetSelectedAdapter(e -> {
            if (combineTypeCombo.getSelectionIndex() == 0) {
                imageProcessConfig.stack(true);
                imageProcessConfig.animate(false);
            } else if (combineTypeCombo.getSelectionIndex() == 1) {
                imageProcessConfig.stack(false);
                imageProcessConfig.animate(true);
            }
        }));
        Create.button(combineComposite, SWT.NONE, "Config", (b) -> {
            b.setLayoutData(configRightGridData);
        }, (e) -> {
            if (combineTypeCombo.getSelectionIndex() == 0) {
                StackDialog stackDialog = new StackDialog(getShell(), imageProcessConfig.getStackingConfig());
                if (stackDialog.open() == Window.OK) {
                    populateCombineInfoText();
                }
            } else {
                AnimateDialog animateDialog = new AnimateDialog(getShell(), imageProcessConfig.getAnimCreationConfig());
                if (animateDialog.open() == Window.OK) {

                }
            }
        });
        
        combineInfoText = new StyledText(combineComposite, SWT.READ_ONLY | SWT.WRAP);
        combineInfoText.setLayoutData(infoLayoutData);
        combineInfoText.setBackground(getShell().getBackground());
        combineInfoText.setLayoutData(infoLayoutData);
        populateCombineInfoText();
        

        alignCheck.addSelectionListener(widgetSelectedAdapter(e -> {
            recursiveSetEnabled(alignComposite, alignCheck.getSelection());
        }));
        combineCheck.addSelectionListener(widgetSelectedAdapter(e -> {
            recursiveSetEnabled(combineComposite, combineCheck.getSelection());
        }));
    }
    
    private boolean validate() {
        Future<int[]> baseFuture;
        Future<int[]> darkFuture = null;
        Future<int[]> flatFuture = null;

        List<CachedImage> lightFiles = imageProcessConfig.getLightFiles();
        if (lightFiles == null || lightFiles.isEmpty()) {
            asyncExec(() -> {
                setErrorMessage("No light files entered");
            });
            return false;
        }

        CachedImage baseFile = imageProcessConfig.getBaseFile();
        if (baseFile == null) {
            String name = lightFiles.get(0).getFile().getName();
            AtomicBoolean bool = new AtomicBoolean();
            getShell().getDisplay().syncExec(() -> {
                MessageBox messageBox = new MessageBox(getShell(), SWT.YES | SWT.NO | SWT.CANCEL| SWT.ICON_WARNING);
                messageBox.setMessage("No base file entered, set " + name + " as base and continue?");
                int result = messageBox.open();
                if (result == SWT.YES) {
                    doSetBaseFile(lightFiles.get(0).getFile().toString());
                    bool.set(true);
                } else {
                    bool.set(false);
                }
            });
            if (!bool.get()) {
                asyncExec(() -> {
                    setErrorMessage("No base file entered");
                });
                return false;
            }
        }

        baseFuture = baseImage != null ? executor.submit(() -> new int[] {baseImage.getImageData().width, baseImage.getImageData().height}) : executor.submit(() -> {RasImageRead imageRead = imageProcessConfig.getBaseFile().getRasImageRead(); return new int[] {imageRead.getWidth(), imageRead.getHeight()};});

        List<CachedImage> darkFiles = imageProcessConfig.getDarkFiles();
        if (darkFiles != null) {
            if (darkFiles.isEmpty()) {
                asyncExec(() -> {
                    setErrorMessage("Dark images directory is empty");
                });
                return false;
            } else {
                darkFuture = executor.submit(() -> {
                    RasImageRead imageRead = darkFiles.get(0).getRasImageRead();
                    return new int[]{imageRead.getWidth(), imageRead.getHeight()};
                });
            }
        }

        List<CachedImage> flatFiles = imageProcessConfig.getFlatFiles();
        if (flatFiles != null) {
            if (flatFiles.isEmpty()) {
                asyncExec(() -> {
                    setErrorMessage("Flat images directory is empty");
                });
                return false;
            } else {
                flatFuture = executor.submit(() -> {
                    RasImageRead imageRead = flatFiles.get(0).getRasImageRead();
                    return new int[]{imageRead.getWidth(), imageRead.getHeight()};
                });
            }
        }

        if (baseFuture != null && darkFuture != null) {
            int[] baseDim = ExeUtil.getFromFuture(baseFuture);
            int[] darkDim = ExeUtil.getFromFuture(darkFuture);
            int bwidth = baseDim[0];
            int bheight = baseDim[1];
            int dwidth = darkDim[0];
            int dheight = darkDim[1];

            if (bwidth != dwidth && bheight != dheight && bwidth != dheight && bheight != dwidth) {
                asyncExec(() -> {setErrorMessage("Base and dark image have different dimensions: " + bwidth + ", " + bheight + " vs " + dwidth + ", " + dheight);});
                return false;
            }
            Image currentDarkImage = getCurrentDarkImage();
            int dwidth2 = currentDarkImage.getImageData().width;
            int dheight2 = currentDarkImage.getImageData().height;
            if (bwidth != dwidth2 || bheight != dheight2) {
                asyncExec(() -> {setErrorMessage("Dark image is rotated in wrong way - dimensions not match");});
                return false;
            }
        }

        if (baseFuture != null && flatFuture != null) {
            int[] baseDim = ExeUtil.getFromFuture(baseFuture);
            int[] flatDim = ExeUtil.getFromFuture(flatFuture);
            int bwidth = baseDim[0];
            int bheight = baseDim[1];
            int fwidth = flatDim[0];
            int fheight = flatDim[1];
            if (bwidth != fwidth && bheight != fheight && bwidth != fheight && bheight != fwidth) {
                asyncExec(() -> {setErrorMessage("Base and flat image have different dimensions: " + bwidth + ", " + bheight + " vs " + fwidth + ", " + fheight);});
                return false;
            }
            Image currentFlatImage = getCurrentFlatImage();
            int dwidth2 = currentFlatImage.getImageData().width;
            int dheight2 = currentFlatImage.getImageData().height;
            if (bwidth != dwidth2 || bheight != dheight2) {
                asyncExec(() -> {setErrorMessage("Flat image is rotated in wrong way - dimensions not match");});
                return false;
            }
        }

        return true;
    }

    private void disableControls() {
        runButton.setEnabled(false);
        stopButton.setEnabled(true);
        
        alignCheck.setEnabled(false);
        combineCheck.setEnabled(false);

        recursiveSetEnabled(alignComposite, false);
        recursiveSetEnabled(combineComposite, false);

        showBrightNess.setEnabled(false);

        darkRotButton.setEnabled(false);
        flatRotButton.setEnabled(false);
        openLightsDirOperation.setEnabled(false);
        openDarksDirOperation.setEnabled(false);
        openFlatsDirOperation.setEnabled(false);
        openBaseFileOperation.setEnabled(false);
    }
    
    private void enableControls() {
        runButton.setEnabled(true);
        stopButton.setEnabled(false);
        
        alignCheck.setEnabled(true);
        combineCheck.setEnabled(true);

        if (alignCheck.getSelection()) {
            recursiveSetEnabled(alignComposite, true);
        }
        if (combineCheck.getSelection()) {
            recursiveSetEnabled(combineComposite, true);
        }

        Float minPixelBrightness = null;
        if (imageProcessConfig.getAligmentType() == AligmentType.MULTI) {
            minPixelBrightness = imageProcessConfig.getMultiAligmentConfig().getMinPixelBrightness();
        } else if (imageProcessConfig.getAligmentType() == AligmentType.SINGLE) {
            minPixelBrightness = imageProcessConfig.getSingleAligmentConfig().getMinPixelBrightness();
        }
        showBrightNess.setEnabled(minPixelBrightness != null);

        darkRotButton.setEnabled(imageProcessConfig.getDarkFiles() != null);
        flatRotButton.setEnabled(imageProcessConfig.getFlatFiles() != null);
        openLightsDirOperation.setEnabled(true);
        openDarksDirOperation.setEnabled(true);
        openFlatsDirOperation.setEnabled(true);
        openBaseFileOperation.setEnabled(true);
    }
    
    private void recursiveSetEnabled(Control ctrl, boolean enabled) {
        if (ctrl instanceof Composite) {
            Composite comp = (Composite) ctrl;
            comp.setEnabled(enabled);
            for (Control c : comp.getChildren()) {
                recursiveSetEnabled(c, enabled);
            }
        } else {
            ctrl.setEnabled(enabled);
        }
    }
    
    private void populateMenuFile() {
        menuFile.add(openBaseFileOperation);
        menuFile.add(openLightsDirOperation);
        menuFile.add(openDarksDirOperation);
        menuFile.add(openFlatsDirOperation);
    }

    private void populateHelpFile() {
        menuHelp.add(configAboutOperation);
    }

    private void initActions() {
        ImageUtil.put("folder", "/image/folder.png");

        openBaseFileOperation = new ActionProcess(slm, "Open Base File", "folder", SWT.ALT | 'B', () -> {
            FileDialog fd = new FileDialog(getShell(), SWT.OPEN | SWT.MULTI);
            fd.setText("Image Files to Open");
            LastFileUtil.setPath(fd);
            List<String> suffixes = getDialogSupportedSuffixes();
            String[] filterExt = suffixes.toArray(new String[suffixes.size()]);
            fd.setFilterExtensions(filterExt);
            String path = null;
            path = fd.open();
            LastFileUtil.updateFilePath(path);
            doSetBaseFile(path);
        });

        openLightsDirOperation = new ActionProcess(slm, "Open Lights Directory", "folder", SWT.ALT | 'L', () -> {
            DirectoryDialog fd = new DirectoryDialog(getShell(), SWT.OPEN);
            fd.setText("Lights Images Directory to Open");
            LastFileUtil.setPath(fd);
            String path = fd.open();
            LastFileUtil.updateDirPath(path);
            doSetLightsFile(path == null ? null : List.of(path));
        });

        openDarksDirOperation = new ActionProcess(slm, "Open Darks Directory", "folder", SWT.ALT | 'D', () -> {
            DirectoryDialog fd = new DirectoryDialog(getShell(), SWT.OPEN);
            fd.setText("Dark Images Directory to Open");
            LastFileUtil.setPath(fd);
            String path = fd.open();
            LastFileUtil.updateDirPath(path);
            doSetDarkFile(path == null ? null : List.of(path));
        });

        openFlatsDirOperation = new ActionProcess(slm, "Open Flats Directory", "folder", SWT.ALT | 'F', () -> {
            DirectoryDialog fd = new DirectoryDialog(getShell(), SWT.OPEN);
            fd.setText("Flat Images Directory to Open");
            LastFileUtil.setPath(fd);
            String path = fd.open();
            doSetFlatFile(path == null ? null : List.of(path));
        });

        configAboutOperation = new ActionOperation("Config", "folder", SWT.ALT | 'C', () -> {
            new ConfigDialog(getShell()).open();
            applyGlobalConfigs();
        });
    }

    private void doSetBaseFile(String path) {
        setBaseFile(path);
        switchBaseExecute();
    }

    private void doSetBaseFile(String path, boolean disableAddingBase) {
        setBaseFile(path);
        switchBaseExecute(disableAddingBase);
    }

    private void doSetLightsFile(List<String> paths) {
        setLightsDirFile(paths);
        switchLightExecute();
    }

    private void doSetDarkFile(List<String> paths) {
        setDarkDirFile(paths);
        switchDarkAsync();
    }

    private void doSetFlatFile(List<String> paths) {
        setFlatDirFile(paths);
        switchFlatExecute();
    }

    private void asyncExec(Runnable runnable) {
        getShell().getDisplay().asyncExec(runnable);
    }

    private boolean syncExec(Runnable runnable) {
        try {
            return syncCall(() -> {
                try {
                    runnable.run();
                    return true;
                } catch (Exception e) {
                    setErrorMessage(e);
                    return false;
                }
                }, Boolean.class);
        } catch (Exception e) {
            return false;
        }
    }

    private <T> T syncCall(Callable<T> callable, Class<T> clazz) throws Exception {
        SwtCallable<T, ? extends Exception> swtCallable = () -> callable.call();
        return getShell().getDisplay().syncCall(swtCallable);
    }

    private void execute(Runnable runnable, TaskType taskType) {
        executor.submit(() -> {
            ProgressIndicator.addTask(taskType);
            try {
                runnable.run();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                ProgressIndicator.removeTask(taskType);
            }
        });
    }

    private void applyGlobalConfigs() {
        ConfigStore configStore = AppConfig.getInstance().getConfigStore();
        if (configStore.getValue("hotpixel.min.brightness", null) != null) {
            imageProcessConfig.minHotPixelsBrightness((float) configStore.getDoubleValue("hotpixel.min.brightness", 0.5));
        }
        imageProcessConfig.subtrackDarks(configStore.getBoolValue("darks.subtract", true));
        imageProcessConfig.removeHotPixels(configStore.getBoolValue("hotpixel.remove", true));
        imageProcessConfig.hotpixeInterpolationDimension(configStore.getIntValue("hot.interp.dim", null));
    }

    private static List<String> getDialogSupportedSuffixes() {
        ArrayList<String> suffixes = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for (String supportedSuffix : RasImageFactory.getAllReadFormats()) {
            if (sb.length() > 0) {
                sb.append(";");
            }
            sb.append("*." + supportedSuffix
                    + (AppHelper.getPlatform() == PlatformType.LINUX ? ";*." + supportedSuffix.toUpperCase() : ""));
            suffixes.add("*." + supportedSuffix
                    + (AppHelper.getPlatform() == PlatformType.LINUX ? ";*." + supportedSuffix.toUpperCase() : ""));
        }
        
        suffixes.add(0, sb.toString());
        String videos = SUPPORTED_VIDEO_FORMATS.stream().map(s -> "*." + s + ";*." + s.toUpperCase()).collect(Collectors.joining(";"));
        suffixes.add(videos);
        return suffixes;
    }

    private void setBaseFile(String path) {
        if (path != null && !new File(path).isFile()) {
            throw new UnprocessableFilesException();
        }
        if (path != null) {
            FilesInDirUtil.checkImageFilePresent(List.of(path));
        }
        baseImage = null;
        CachedImage cachedImage = path != null ? new CachedImage(new File(path)) : null;
        imageProcessConfig.baseFile(cachedImage);
        baseFileLabel.setText(path != null ? (THE_BASE + path)  : NO_BASE);
        removeBaseButton.setEnabled(path != null);
    }

    private void setLightsDirFile(List<String> paths) {
        FilesInDirUtil.checkImageFilePresent(paths);
        lightImage = null;
        List<CachedImage> cachedImages = paths != null ? ImFilesUtil.getFilesListCachedByPaths(paths) : null;
        imageProcessConfig.lightFiles(cachedImages);
        lightsDirLabel.setText(paths != null ? (THE_LIGHT + paths.stream().collect(Collectors.joining(", "))) : NO_LIGHT);
        removeLightsButton.setEnabled(paths != null);
        updateRootOutputDirectoryLabel();
    }

    private void setDarkDirFile(List<String> paths) {
        FilesInDirUtil.checkImageFilePresent(paths);
        darkImage = null;
        darkRotButton.setEnabled(paths != null);
        List<CachedImage> cachedImages = paths != null ? ImFilesUtil.getFilesListCachedByPaths(paths) : null;
        imageProcessConfig.darkFiles(cachedImages);
        darkDirLabel.setText(paths != null ? (THE_DARK + paths.stream().collect(Collectors.joining(", "))) : NO_DARK);
        removeDarkButton.setEnabled(paths != null);
    }

    private void setFlatDirFile(List<String> paths) {
        FilesInDirUtil.checkImageFilePresent(paths);
        flatImage = null;
        flatRotButton.setEnabled(paths != null);
        List<CachedImage> cachedImages = paths != null ? ImFilesUtil.getFilesListCachedByPaths(paths) : null;
        imageProcessConfig.flatFiles(cachedImages);
        flatDirLabel.setText(paths != null ? (THE_FLAT + paths.stream().collect(Collectors.joining(", "))) : NO_FLAT);
        removeFlatButton.setEnabled(paths != null);
    }

    private void setRootOutputDirectory(String initialOutputRootDirectory) {
        imageProcessConfig.rootOutputDir(initialOutputRootDirectory != null ? new File(initialOutputRootDirectory) : null);
        updateRootOutputDirectoryLabel();
    }

    private void updateRootOutputDirectoryLabel() {
        File rootOutputDir = imageProcessConfig.getRootOutputDir();
        rootOutputDirLabel.setText(THE_OUTPUT_DIR + " " + (rootOutputDir != null ? rootOutputDir : ""));
    }

    private void switchBaseExecute(boolean disableAddingBase) {
        execute(() -> {switchBase(disableAddingBase);}, TaskType.LOADING_BASE);
    }

    private void switchBaseExecute() {
        execute(() -> {switchBase(false);}, TaskType.LOADING_BASE);
    }

    private void switchBase(boolean disableAddingBase) {
        if (!disableAddingBase && imageProcessConfig.getBaseFile() == null && imageProcessConfig.getLightFiles() != null && !imageProcessConfig.getLightFiles().isEmpty()) {
            syncExec(() -> {
                String name = imageProcessConfig.getLightFiles().get(0).getFile().getName();
                MessageBox messageBox = new MessageBox(getShell(), SWT.YES | SWT.NO | SWT.CANCEL| SWT.ICON_WARNING);
                messageBox.setMessage("No base file entered, set " + name + " as base and continue?");
                int result = messageBox.open();
                if (result == SWT.YES) {
                    setBaseFile(imageProcessConfig.getLightFiles().get(0).getFile().toString());
                }
            });
        }
        if (imageProcessConfig.getBaseFile() == null) {
            asyncExec(() -> {
                applyImage(null);
                setErrorMessage("Unable to show base file - not defined");
            });
            return;
        }
        asyncExec(() -> {
            switchRadio(baseRadioButton);
        });
        try {
            if (baseImage == null) {
                baseImage = loadImage(imageProcessConfig.getBaseFile());
            }
            showingRasImage = RasImageData.getInstance(baseImage.getImageData());
            asyncExec(() -> {
                applyImage(baseImage);
            });
        } catch (Exception e) {
            asyncExec(() -> {
                applyImage(null);
                setErrorMessage(ExceptionMessage.getCombinedMessage("Unable to show base file", e));
            });
        }
    }

    private void switchLight() {
        if (imageProcessConfig.getLightFiles() == null || imageProcessConfig.getLightFiles().isEmpty()) {
            asyncExec(() -> {
                applyImage(null);
                setErrorMessage("Unable to show light file - not defined");
            });
            return;
        }
        asyncExec(() -> {
            switchRadio(lightRadioButton);
        });
        try {
            if (lightImage == null) {
                lightImage = loadImage(imageProcessConfig.getLightFiles().get(0));
            }
            showingRasImage = RasImageData.getInstance(lightImage.getImageData());
            asyncExec(() -> {
                applyImage(lightImage);
            });
        } catch (Exception e) {
            asyncExec(() -> {
                applyImage(null);
                setErrorMessage(ExceptionMessage.getCombinedMessage("Unable to show light file", e));
            });
        }
    }

    private void switchRadio(Button radioSelected) {
        baseRadioButton.setSelection(radioSelected == baseRadioButton);
        lightRadioButton.setSelection(radioSelected == lightRadioButton);
        darkRadioButton.setSelection(radioSelected == darkRadioButton);
        flatRadioButton.setSelection(radioSelected == flatRadioButton);
        stackedRadioButton.setSelection(radioSelected == stackedRadioButton);
    }

    private void switchStackedExecute() {
        execute(() -> switchStacked(), TaskType.LOADING_STACKED);
    }

    private void switchStacked() {
        asyncExec(() -> {
            switchRadio(stackedRadioButton);
        });

        try {
            if (imageProcessConfig.getRootOutputDir() == null) {
                throw new IllegalStateException("Unknown directory with stacked file (lights not set)");
            }
            File stackOutputFile = FileConstants.getStackOutputFile(imageProcessConfig.getRootOutputDir());
            if (stackedImage == null) {
                stackedImage = loadImage(new CachedImage(stackOutputFile));
            }
            showingRasImage = RasImageData.getInstance(stackedImage.getImageData());
            asyncExec(() -> {
                applyImage(stackedImage);
            });
        } catch (Exception e) {
            asyncExec(() -> {
                applyImage(null);
                setErrorMessage(ExceptionMessage.getCombinedMessage("Unable to show stacked file", e));
            });
        }
    }

    private void switchLightExecute() {
        execute(() -> {switchLight();}, TaskType.LOADING_LIGHT);
    }

    private void switchDarkAsync() {
        execute(() -> {switchDark();}, TaskType.LOADING_DARK);
    }

    private void switchDark() {
        List<CachedImage> darkFiles = imageProcessConfig.getDarkFiles();
        if (darkFiles == null || darkFiles.isEmpty()) {
            asyncExec(() -> {
                applyImage(null);
                setErrorMessage("Darks directory is empty");
            });
            return;
        }
        asyncExec(() -> {
            switchRadio(darkRadioButton);
        });


        File file = darkFiles.get(0).getFile();
        try {
            if (darkImage == null) {
                darkImage = loadImage(new CachedImage(file));
                dark90Image = new Image(getShell().getDisplay(), RotateImageDataUtil.rotate(darkImage.getImageData(), RotationCounterClockwise.ROT_90));
                dark180Image = new Image(getShell().getDisplay(), RotateImageDataUtil.rotate(darkImage.getImageData(), RotationCounterClockwise.ROT_180));
                dark270Image = new Image(getShell().getDisplay(), RotateImageDataUtil.rotate(darkImage.getImageData(), RotationCounterClockwise.ROT_270));
            }
            asyncExec(() -> {
                Image currentDarkImage = getCurrentDarkImage();
                showingRasImage = RasImageData.getInstance(currentDarkImage.getImageData());
                applyImage(currentDarkImage);
            });
        } catch (Exception e) {
            asyncExec(() -> {
                applyImage(null);
                setErrorMessage(ExceptionMessage.getCombinedMessage("Unable to show dark file", e));
            });
        }
    }


    private void switchFlatExecute() {
        execute(() -> {switchFlat();}, TaskType.LOADING_FLAT);
    }

    private void switchFlat() {
        List<CachedImage> flatFiles = imageProcessConfig.getFlatFiles();
        if (flatFiles == null || flatFiles.isEmpty()) {
            asyncExec(() -> {
                applyImage(null);
                setErrorMessage("Flats directory is empty");
            });
            return;
        }
        asyncExec(() -> {
            baseRadioButton.setSelection(false);
            darkRadioButton.setSelection(false);
            flatRadioButton.setSelection(true);
            stackedRadioButton.setSelection(false);
        });


        File file = flatFiles.get(0).getFile();
        try {
            if (flatImage == null) {
                flatImage = loadImage(new CachedImage(file));
                flat90Image = new Image(getShell().getDisplay(), RotateImageDataUtil.rotate(flatImage.getImageData(), RotationCounterClockwise.ROT_90));
                flat180Image = new Image(getShell().getDisplay(), RotateImageDataUtil.rotate(flatImage.getImageData(), RotationCounterClockwise.ROT_180));
                flat270Image = new Image(getShell().getDisplay(), RotateImageDataUtil.rotate(flatImage.getImageData(), RotationCounterClockwise.ROT_270));
            }
            asyncExec(() -> {
                Image currentFlatImage = getCurrentFlatImage();
                showingRasImage = RasImageData.getInstance(currentFlatImage.getImageData());
                applyImage(currentFlatImage);
            });
        } catch (Exception e) {
            asyncExec(() -> {
                applyImage(null);
                setErrorMessage(ExceptionMessage.getCombinedMessage("Unable to show flat file", e));
            });
        }
    }


    private Image getCurrentDarkImage() {
        RotationCounterClockwise darkRotation = imageProcessConfig.getDarkRotation();
        if (darkRotation == null) {
            return darkImage;
        } else if (darkRotation == RotationCounterClockwise.ROT_90) {
            return dark90Image;
        } else if (darkRotation == RotationCounterClockwise.ROT_180) {
            return dark180Image;
        } else if (darkRotation == RotationCounterClockwise.ROT_270) {
            return dark270Image;
        } else {
            return null;
        }
    }

    private Image getCurrentFlatImage() {
        RotationCounterClockwise flatRotation = imageProcessConfig.getFlatRotation();
        if (flatRotation == null) {
            return flatImage;
        } else if (flatRotation == RotationCounterClockwise.ROT_90) {
            return flat90Image;
        } else if (flatRotation == RotationCounterClockwise.ROT_180) {
            return flat180Image;
        } else if (flatRotation == RotationCounterClockwise.ROT_270) {
            return flat270Image;
        } else {
            return null;
        }
    }

    private Image loadImage(CachedImage cachedImage) throws IOException {
        ImageData imageData;
        RasImageRead rasImage = cachedImage.getRasImageRead();
        if (rasImage instanceof RasBufImage) {
            RasBufImage rasBufImage = (RasBufImage) rasImage;
            imageData = BufImageSWT.toImageData(rasBufImage);
        } else {
            imageData = BufImageSWT.toImageData(rasImage);
        }
        return new Image(getShell().getDisplay(), imageData);
    }

    private void applyImage(Image image) {
        if (!hasImageBeenShowed) {
            rasterComposite.setImageAndCenterFitRedraw(image);
        } else {
            rasterComposite.setImageAndRedraw(image);
        }
        if (image != null) {
            hasImageBeenShowed = true;
        }
    }
    
    
    private void initDrop() {
        DropTarget dropTarget = new DropTarget(getShell(), DND.DROP_MOVE | DND.DROP_COPY);
        dropTarget.setTransfer(new Transfer[] { FileTransfer.getInstance() });
        dropTarget.addDropListener(new DropTargetAdapter() {
            public void drop(DropTargetEvent event) {
                Object data = event.data;
                if (data instanceof String[]) {
                    String[] strs = (String[]) data;
                    if (strs.length == 0) {
                        return;
                    }
                    List<String> paths = Arrays.asList(strs);
                    Shell shell = getShell();
                    executor.submit(() -> {
                        try {
                            InputTypeDialog.Type resultType = syncCall(() -> {
                                InputTypeDialog inputTypeDialog = new InputTypeDialog(shell, paths);
                                if (inputTypeDialog.open() == IDialogConstants.OK_ID) {
                                    return inputTypeDialog.getResultType();
                                }
                                return null;
                            }, InputTypeDialog.Type.class);
                            if (resultType != null) {
                                if (resultType == InputTypeDialog.Type.BASE) {
                                    syncExec(() -> {
                                        setBaseFile(paths.get(0));
                                    });
                                    switchBaseExecute();
                                } else {
                                    if (imageProcessConfig == null) {
                                        throw new IllegalStateException("Base file must be loaded first");
                                    }
                                    switch (resultType) {
                                        case LIGHT -> {
                                            syncExec(() -> {
                                                setLightsDirFile(paths);
                                            });
                                            switchLightExecute();
                                        }
                                        case DARK -> {
                                            syncExec(() -> {
                                                setDarkDirFile(paths);
                                            });
                                            switchDarkAsync();
                                        }
                                        case FLAT -> {
                                            syncExec(() -> {
                                                setFlatDirFile(paths);
                                            });
                                            switchFlatExecute();
                                        }
                                        default -> throw new RuntimeException("Unknown type " + resultType);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            setErrorMessage("Action failed" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
                        }
                    });
                }
            }
        });
    }
    
    @Override
    protected StatusLineManager createStatusLineManager() {
        return slm;
    }
    
    @Override
    protected ToolBarManager createToolBarManager(int style) {
        return toolbarManager;
    }
    
    @Override
    protected MenuManager createMenuManager() {
        return menuManager;
    }
    
    protected void initializeBounds() {
        getShell().setText("Stacking");
        getShell().setSize(900, 900);
        getShell().setMinimumSize(700, 500);
    }
    
    @Override
    public boolean close() {
        ProgressIndicator.destroy();
        executor.shutdownNow();
        return super.close();
    }

    private void populateAlignInfoText() {
        if (imageProcessConfig != null) {
            if (alignTypeCombo.getSelectionIndex() == 0) {
                alignInfoText.setText(imageProcessConfig.getSingleAligmentConfig().toString());
            } else {
                alignInfoText.setText(imageProcessConfig.getMultiAligmentConfig().toString());
            }
        }
    }

    private void populateCombineInfoText() {
        if (imageProcessConfig != null) {
            if (combineTypeCombo.getSelectionIndex() == 0) {
                combineInfoText.setText(imageProcessConfig.getStackingConfig().toString());
            } else {
                combineInfoText.setText(imageProcessConfig.getAnimCreationConfig().toString());
            }
        }
    }

    public void finishedImportVideo(File resultOutputDir) {
        List<CachedImage> list = ImFilesUtil.filesListCached(resultOutputDir);
        if (list.size() > 0) {
            File baseFile = list.get(0).getFile();
            imageProcessConfig.baseFile(new CachedImage(baseFile));
            //setBaseImageFile(baseFile);
            switchBaseExecute();
            enableControls();
        }
    }

    public void setInitialOpenBaseFile(String initialOpenBaseFile) {
        this.initialOpenBaseFile = initialOpenBaseFile;
    }

    public void setErrorMessage(String str) {
        slm.setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_WARNING), str != null ? str : "Operation failed");
    }

    public void setErrorMessage(Exception e) {
        slm.setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_WARNING), e.getMessage() != null ? e.getMessage() : "Operation failed");
    }

    public void setInfoMessage(String str) {
        if (str == null) {
            return;
        }
        slm.setMessage(ImageUtil.getSystemImage16(SWT.ICON_INFORMATION), str);
    }

    public void setInitialLightsDirectory(String initialLightsDirectory) {
        this.initialLightsDirectory = initialLightsDirectory;
    }

    public void setInitialOutputRootDirectory(String initialOutputRootDirectory) {
        this.initialOutputRootDirectory = initialOutputRootDirectory;
    }

    @Override
    public boolean mouseClickEvent(PositionData positionData, int button, int stateMask, boolean up) {
        return false;
    }

    @Override
    public void drawEvent(GC gc, DrawScreenInfo drawScreenInfo, PositionData positionData) {

    }
}
