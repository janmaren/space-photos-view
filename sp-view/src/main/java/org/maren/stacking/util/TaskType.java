package org.maren.stacking.util;

public enum TaskType {
    LOADING_BASE("Loading Base File"), LOADING_STACKED("Loading Stacked"),
    LOADING_DARK("Loading Dark"), LOADING_FLAT("Loading Flat"), LOADING_LIGHT("Loading Light");

    private String text;

    TaskType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
