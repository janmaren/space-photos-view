package org.maren.stacking.util;


import cz.jmare.image.util.PathUtil;
import cz.jmare.os.AppHelper;
import cz.jmare.os.PlatformType;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;

public class LastFileUtil {
    private static String lastDir;

    public static void setPath(FileDialog fileDialog) {
        if (AppHelper.getPlatform() == PlatformType.LINUX) {
            if (lastDir == null) {
                lastDir = System.getProperty("user.home");
            }
            fileDialog.setFilterPath(lastDir);
        }
    }

    public static void updateFilePath(String lastFi) {
        if (lastFi == null) {
            return;
        }
        lastFi = PathUtil.toFullPathAndLastPart(lastFi)[0];
        lastDir = lastFi;
    }

    public static void updateDirPath(String lastFi) {
        if (lastFi == null) {
            return;
        }
        lastDir = lastFi;
    }

    public static void setPath(DirectoryDialog fd) {
        if (AppHelper.getPlatform() == PlatformType.LINUX) {
            fd.setFilterPath(lastDir);
        }
    }
}
