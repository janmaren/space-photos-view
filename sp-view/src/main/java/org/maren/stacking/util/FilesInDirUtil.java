package org.maren.stacking.util;

import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;

import java.io.File;
import java.util.List;

public class FilesInDirUtil {
    public static RasImageRead getFirstImageInDir(File dir) {
        List<CachedImage> cachedImages = ImFilesUtil.filesListCached(dir);
        if (cachedImages.isEmpty()) {
            throw new IllegalStateException("Dir " + dir + " is empty");
        }
        return cachedImages.get(0).getRasImageRead();
    }

    public static void checkImageFilePresent(List<String> paths) {
        if (paths == null) {
            return;
        }
        List<CachedImage> cachedImages = ImFilesUtil.getFilesListCachedByPaths(paths);
        if (cachedImages.isEmpty()) {
            throw new UnprocessableFilesException();
        }
    }
}
