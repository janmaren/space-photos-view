package org.maren.stacking.util;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.eclipse.swt.widgets.Display;
import org.jcodec.api.FrameGrab;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.AWTUtil;
import org.maren.stacking.dialog.ImportVideoDialog;

public class VideoToImages implements Runnable {
    private File videoFile;
    private File outputDir;
    private Exception exception;
    private ImportVideoDialog importVideoDialog;
    
    public VideoToImages(File videoFile, File outputDir, ImportVideoDialog importVideoDialog) {
        super();
        this.videoFile = videoFile;
        this.outputDir = outputDir;
        this.importVideoDialog = importVideoDialog;
    }
    
    @Override
    public void run() {
        Display display = importVideoDialog.getShell().getDisplay();
        try {
            FrameGrab grab = FrameGrab.createFrameGrab(NIOUtils.readableChannel(videoFile));
            Picture picture;
            int number = 1;
            while (null != (picture = grab.getNativeFrame())) {
                BufferedImage bufferedImage = AWTUtil.toBufferedImage(picture);
                String name = "frame_" + String.format("%07d", number++) + ".tif";
                File outputFile = new File(outputDir, name);
                display.asyncExec(() -> {
                    importVideoDialog.progressFile(outputFile);
                });
                ImageIO.write(bufferedImage, "tif", outputFile);
                if (Thread.currentThread().isInterrupted()) {
                    display.asyncExec(() -> {
                        importVideoDialog.setMessage("Interrupted");
                    });
                    throw new RuntimeException("Interrupted");
                }
            }
        } catch (Exception e) {
            exception = e;
        } finally {
            display.asyncExec(() -> {
                importVideoDialog.finished();
            });
        }
    }

    public Exception getException() {
        return exception;
    }
}
