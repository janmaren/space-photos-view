package org.maren.stacking.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import org.maren.swt.util.ExecLauncher;

import cz.jmare.image.ras.ImageConverter;
import cz.jmare.image.util.ImPathUtil;

public class DcrawConverter implements ImageConverter {
    private static String[] NEF_PARS = {"-T",  "-W", "-6"};
    
    private static String[] CR2_PARS = {"-T", "-w", "-W", "-6"};
    
    private static Map<String, String[]> FORM_PARS = Map.of("nef", NEF_PARS, "NEF", NEF_PARS, "cr2", CR2_PARS, "CR2", CR2_PARS);
    
    @Override
    public void convert(File inputFile, File outputFile) throws IOException {
        String[] suffix = ImPathUtil.parseNameSuffix(inputFile.getName());
        if (suffix[1].length() <= 2) {
            throw new IOException("Unsupported format of " + inputFile);
        }
        String format = suffix[1].substring(1);
        String[] pars = FORM_PARS.get(format);
        if (pars == null) {
            throw new IOException("Unsupported format of " + inputFile);
        }
        List<String> command = new ArrayList<String>();
        command.add(getDcrawPath());
        for (String string : pars) {
            command.add(string);
        }
        command.add("-c");
        command.add(inputFile.toString());
        byte[] bytes = ExecLauncher.execReadOutput(command);
        if (bytes.length < 100 && allBytesAreChars(bytes)) {
            throw new IOException("Unable to convert " + inputFile + " to tif");
        }
        Files.write(outputFile.toPath(), bytes);
    }

    private boolean allBytesAreChars(byte[] bytes) {
        for (byte aByte : bytes) {
            if (!Character.isLetterOrDigit(aByte) && !Character.isWhitespace(aByte) && !Character.isAlphabetic(aByte)) {
                return false;
            }
        }
        return true;
    }

    public static String getDcrawPath() {
        AppConfig appConfig = AppConfig.getInstance();
        ConfigStore configStore = appConfig.getConfigStore();
        return configStore.getValue("dcraw.path", "dcraw");
    }

    @Override
    public Set<String> getSupportedFormats() {
        return FORM_PARS.keySet();
    }
}
