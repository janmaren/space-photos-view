package org.maren.stacking.util;

import cz.jmare.image.RotationCounterClockwise;

public class RotUtil {
    public static RotationCounterClockwise getNextRotation(RotationCounterClockwise currRot) {
        if (currRot == null) {
            return RotationCounterClockwise.ROT_90;
        }
        if (currRot == RotationCounterClockwise.ROT_90) {
            return RotationCounterClockwise.ROT_180;
        }
        if (currRot == RotationCounterClockwise.ROT_180) {
            return  RotationCounterClockwise.ROT_270;
        }
        if (currRot == RotationCounterClockwise.ROT_270) {
            return null;
        }
        throw new IllegalArgumentException("Unknown rot " + currRot);
    }

    public static String getRotationImage(RotationCounterClockwise currRot) {
        if (currRot == null) {
            return "rot-no";
        }
        if (currRot == RotationCounterClockwise.ROT_90) {
            return "rotcc-90";
        }
        if (currRot == RotationCounterClockwise.ROT_180) {
            return "rotcc-180";
        }
        if (currRot == RotationCounterClockwise.ROT_270) {
            return "rotcc-270";
        }
        throw new IllegalArgumentException("Unknown rot " + currRot);
    }

    public static String getHint(RotationCounterClockwise currRot) {
        if (currRot == null) {
            return "No rotation";
        }
        if (currRot == RotationCounterClockwise.ROT_90) {
            return "Rotation 90\u00B0 counter clockwise";
        }
        if (currRot == RotationCounterClockwise.ROT_180) {
            return "Rotation 180\u00B0 counter clockwise";
        }
        if (currRot == RotationCounterClockwise.ROT_270) {
            return "Rotation 270\u00B0 counter clockwise";
        }
        throw new IllegalArgumentException("Unknown rot " + currRot);
    }
}
