package org.maren.stacking.util;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import java.util.ArrayList;
import java.util.List;

public class ProgressIndicator {
    private static volatile List<TaskType> runTasksNames = new ArrayList<>();

    private static int phase;

    private static Label label;

    private static Display display;
    private static Thread thread;

    private static class Loop implements Runnable {
        private boolean hasText;

        @Override
        public void run() {
            Thread currentThread = Thread.currentThread();
            while (!currentThread.isInterrupted()) {
                if (runTasksNames.size() > 0) {
                    String str;
                    str = switch(phase) {
                        case 0 -> "|";
                        case 1 -> "/";
                        case 2 -> "-";
                        case 3 -> "\\";
                        case 4 -> "|";
                        case 5 -> "/";
                        case 6 -> "-";
                        case 7 -> "\\";
                        default -> "|";
                    };
                    display.asyncExec(() -> {
                        label.setText(str);
                        try {
                            TaskType taskType = runTasksNames.get(0);
                            label.setToolTipText(taskType.getText());
                        } catch (Exception e) {
                            // no handling
                        }
                    });
                    hasText = true;
                    phase++;
                    if (phase > 7) {
                        phase = 0;
                    }
                } else {
                    if (hasText) {
                        display.asyncExec(() -> {
                            label.setText("");
                        });
                        hasText = false;
                    }
                }

                try {
                    Thread.sleep(hasText ? 100 : 250);
                } catch (InterruptedException e) {
                    return;
                }
            }
        }

    }

    public static void addTask(TaskType taskType) {
        runTasksNames.add(taskType);
    }

    public static synchronized void removeTask(TaskType taskType) {
        runTasksNames.remove(taskType);
    }

    public static synchronized void run(Label label, Display display) {
        if (thread != null) {
            return;
        }
        ProgressIndicator.display = display;
        ProgressIndicator.label = label;
        thread = new Thread(new Loop());
        thread.start();
    }

    public static synchronized void destroy() {
        if (thread == null) {
            return;
        }
        thread.interrupt();
        thread = null;
    }
}
