package org.maren.stacking.util;

import java.util.concurrent.Future;

public class ExeUtil {
    public static <T> T getFromFuture(Future<T> future) {
        T t = null;
        try {
            t = future.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return t;
    }
}
