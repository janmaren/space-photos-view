package org.maren.stacking.util;

public class UnprocessableFilesException extends RuntimeException {
    private static final long serialVersionUID = 1921623286999341235L;

	public UnprocessableFilesException() {
        super("File or directory doesn't contain supported images");
    }
}
