package org.maren.stacking.util;

import cz.jmare.image.ras.RasImageFactory;

public class ExternalUtilsFinder implements Runnable {
    @Override
    public void run() {
        try {
            DcrawFinderUtil.checkDcrawExists(DcrawConverter.getDcrawPath());
            RasImageFactory.registerConverter(new DcrawConverter());
        } catch (Exception e) {
            // unable launch dcraw
        }
    }
}
