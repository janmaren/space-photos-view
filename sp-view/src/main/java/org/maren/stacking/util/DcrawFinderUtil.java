package org.maren.stacking.util;

import cz.jmare.image.ras.RasImageFactory;
import org.maren.swt.util.ExecLauncher;

import java.io.IOException;
import java.util.List;

public class DcrawFinderUtil {
    public static void checkDcrawExists(String dcrawPath) {
        if (dcrawPath == null) {
            dcrawPath = "dcraw";
        }
        try {
            byte[] execReadOutput = ExecLauncher.execReadOutput(List.of(dcrawPath));
            String string = new String(execReadOutput);
            if (string.contains("Usage")) {
                if (string.contains("-6 ")) {
                    RasImageFactory.registerConverter(new DcrawConverter());
                } else {
                    throw new IllegalStateException("Dcraw exists but not the right version with -6 parameter");
                }
            } else {
                throw new IllegalStateException("Dcraw doesn't exist on path " + dcrawPath);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Dcraw doesn't exist on path " + dcrawPath);
        }
    }
}
