package org.maren.swt.util;

import cz.jmare.file.PathUtil;
import org.eclipse.swt.SWT;

public class SwtImageFileUtil {
    public static int getSwtImageType(String filename) {
        String[] strings = PathUtil.parseFullPathSuffix(filename);
        String extension = strings[1].toLowerCase();
        return switch (extension) {
            case ".jpg", ".jpeg" -> SWT.IMAGE_JPEG;
            case ".bmp" -> SWT.IMAGE_BMP;
            case ".ico" -> SWT.IMAGE_ICO;
            case ".png" -> SWT.IMAGE_PNG;
            default -> throw new IllegalStateException("Unexpected value: " + extension);
        };
    }
}
