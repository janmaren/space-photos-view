package org.maren.swt.util;

import cz.jmare.math.astro.RahDec;
import org.maren.gis.type.Spheric;
import org.maren.statis.entity.Coordinate;

import cz.jmare.data.util.GonUtil;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.geometry.entity.Point;

public class ConvTypesUtil {
    public static Spheric toSpheric(RaDec raDec) {
        return new Spheric(Math.toRadians(raDec.ra), Math.toRadians(raDec.dec));
    }
    
    public static RaDec toNormalizedRaDec(Spheric spheric) {
        return new RaDec(Math.toDegrees(GonUtil.normalizeRadZeroTwoPi(spheric.lon)), Math.toDegrees(GonUtil.normalizeRadMinusPiPi(spheric.lat)));
    }

    public static RahDec toNormalizedRahDec(Spheric spheric) {
        return new RahDec(Math.toDegrees(GonUtil.normalizeRadZeroTwoPi(spheric.lon)) / 15.0, Math.toDegrees(GonUtil.normalizeRadMinusPiPi(spheric.lat)));
    }
    
    public static double angularDistanceRad(Spheric spheric1, Spheric spheric2) {
        double beta1Rad = spheric1.lat;
        double beta2Rad = spheric2.lat;
        double lambda1Rad = spheric1.lon;
        double lambda2Rad = spheric2.lon;
        
        double sum = Math.sin(beta1Rad) * Math.sin(beta2Rad) + Math.cos(beta1Rad) * Math.cos(beta2Rad) * Math.cos(lambda1Rad - lambda2Rad);
        if (sum > 1.0) {
            if (sum > 1 + 1e-5) {
                return Double.NaN;
            } else {
                return 0;
            }
        }
        return Math.acos(sum);
    }
    
    public static double getRaHours(Spheric spheric) {
        double hoursDecimal = 12 * spheric.lon / Math.PI;
        if (hoursDecimal >= 24.0) {
            hoursDecimal = hoursDecimal % 24;
        } else if (hoursDecimal < 0) {
            hoursDecimal = hoursDecimal % 24 + 24.0;
        }
        return hoursDecimal;
    }
    
    public static Coordinate toCoordinate(Point point) {
        return new Coordinate(point.x, point.y);
    }
    
    public static Point toPoint(Coordinate coordinate) {
        return new Point(coordinate.x, coordinate.y);
    }

    public static double calcAngularDistance(Spheric centreSpheric, Spheric newSpheric) {
        //cos(A) = sin(Decl.1) * sin(Decl.2) + cos(Decl.1) * cos(Decl.2) * cos(RA.1 - RA.2)
        double sum = Math.sin(centreSpheric.lat) * Math.sin(newSpheric.lat) + Math.cos(centreSpheric.lat) * Math.cos(newSpheric.lat) * Math.cos(centreSpheric.lon - newSpheric.lon);
        return Math.acos(sum); // radians
    }
}
