package org.maren.swt.util;

import java.awt.image.BufferedImage;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;

import cz.jmare.image.ras.RGBPix;
import cz.jmare.image.ras.RasBufImage;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.RGBPixUtil;

public class BufImageSWT {
    public static ImageData toImageData(BufferedImage bufferedImage) {
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();

        ImageData imageData = new ImageData(width, height, 24, new PaletteData(0xFF0000, 0xFF00, 0xFF));
        RasBufImage rasBufImage = RasBufImage.getInstance(bufferedImage);
        RGBPix rgbPix = new RGBPix();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                rasBufImage.getRGB(x, y, rgbPix);
                imageData.setPixel(x, y, RGBPixUtil.rgbToRHighest(rgbPix));
                if ((bufferedImage.getType() == BufferedImage.TYPE_4BYTE_ABGR
                        || bufferedImage.getType() == BufferedImage.TYPE_INT_ARGB)) {
                    int alpha = rgbPix.alpha;
                    imageData.setAlpha(x, y, alpha);
                }
            }
        }

        return imageData;
    }
    
    public static ImageData toImageData(RasImageRead rasBufImage) {
        int width = rasBufImage.getWidth();
        int height = rasBufImage.getHeight();

        ImageData imageData = new ImageData(width, height, 24, new PaletteData(0xFF0000, 0xFF00, 0xFF));
        RGBPix rgbPix = new RGBPix();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                rasBufImage.getRGB(x, y, rgbPix);
                imageData.setPixel(x, y, RGBPixUtil.rgbToRHighest(rgbPix));
            }
        }

        return imageData;
    }
}
