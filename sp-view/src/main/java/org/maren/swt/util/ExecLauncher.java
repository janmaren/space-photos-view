package org.maren.swt.util;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import cz.jmare.file.FileUtil;

public class ExecLauncher {
    public static byte[] execReadOutputCheckStatus(List<String> command) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(command);
        Map<String, String> environ = builder.environment();
        environ.put("PATH", System.getenv("PATH"));
        final Process godot = builder.start();
        godot.getOutputStream().close();
        byte[] bytes = FileUtil.getFile(godot.getInputStream());
        try {
            godot.waitFor();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted");
        }
        int exitValue = godot.exitValue();
        if (exitValue != 0) {
            throw new IOException("An error during run");
        }
        return bytes;
    }
    
    public static byte[] execReadOutput(List<String> command) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(command);
        Map<String, String> environ = builder.environment();
        environ.put("PATH", System.getenv("PATH"));
        final Process godot = builder.start();
        godot.getOutputStream().close();
        return FileUtil.getFile(godot.getInputStream());
    }
    
    public static void main(String[] args) throws InterruptedException, IOException {
        byte[] exec = execReadOutput(List.of("dcraw", "-T", "-w", "-W", "-c", "C:\\temp\\conv2\\DSC_0001.NEF"));
        System.out.println("size:" +exec.length);
        //Files.write(Paths.get("c:\\temp\\out.tif"), exec);
    }
}
