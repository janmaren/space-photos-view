package org.maren.spview.objfetch;

import static org.maren.spview.objfetch.TagPattern.A_WRAP_PATTERN;
import static org.maren.spview.objfetch.TagPattern.getTagBegin;
import static org.maren.spview.objfetch.TagPattern.getTagEnd;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.regex.Matcher;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.regexp.PatternTraverser;


public class SimbadObjFetch {
    public static void main(String[] args) throws IOException {
        List<FetchedCelestObj> pars = new ArrayList<>();
        String string = Files.readString(Paths.get("C:\\Users\\jmarencik\\Downloads\\sn2022.htm"));
        StringJoiner sj = new StringJoiner("|", "(?s)(?i)", "");
        sj.add(getTagBegin("tableB", "table"));
        sj.add(getTagEnd("tableE", "table"));
        sj.add(getTagBegin("trB", "tr"));
        sj.add(getTagEnd("trE", "tr"));
        sj.add(getTagBegin("tdB", "td"));
        sj.add(getTagEnd("tdE", "td"));

        PatternTraverser patternTraverser = new PatternTraverser(sj.toString(), string, new Consumer<Matcher>() {
            boolean inTable;

            boolean behindHeader;

            String ra;

            String dec;

            boolean inTr;

            FetchedCelestObj currObj = null;

            int tdBeginIndex = -1;

            String td;

            @Override
            public void accept(Matcher m) {
                if (m.group("tableE") != null) {
                    inTable = false;
                } else  if (inTable) {
                    if (m.group("tableE") != null) {
                        inTable = false;
                    } else {
                        if (behindHeader) {
                            if (m.group("trE") != null) {
                                currObj.setRaDec(ACooUtil.parseCoords(ra + " " + dec));
                                pars.add(currObj);
                                inTr = false;
                            } else {
                                if (m.group("trB") != null) {
                                    inTr = true;
                                    currObj = new FetchedCelestObj();
                                } else {
                                    if (inTr) {
                                        if (m.group("tdB") != null) {
                                            td = m.group("tdB");
                                            tdBeginIndex = m.end();
                                        } else if (m.group("tdE") != null && td != null)  {
                                            if (td.contains("\"lon\"")) {
                                                ra = string.substring(tdBeginIndex, m.start()).trim();
                                            } else if (td.contains("\"lat\"")) {
                                                dec = string.substring(tdBeginIndex, m.start()).trim();
                                            } else {
                                                String val = string.substring(tdBeginIndex, m.start()).trim();
                                                if ((val.contains("<a") || val.contains("<A")) && val.contains("submit")) {
                                                    Matcher mat = A_WRAP_PATTERN.matcher(val);
                                                    if (mat.find()) {
                                                        currObj.setName(mat.group(1));
                                                    }
                                                }
                                            }
                                            td = null;
                                            tdBeginIndex = -1;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (m.group("trE") != null) {
                                behindHeader = true;
                            }
                        }
                    }
                } else {
                    if (m.group("tableB") != null) {
                        if (m.group().contains("id=\"datatable\"")) {
                            inTable = true;
                        }
                    }
                }
            }
        });
        patternTraverser.traverse();
    }
}
