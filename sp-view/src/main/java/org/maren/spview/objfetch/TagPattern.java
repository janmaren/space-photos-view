package org.maren.spview.objfetch;

import java.util.regex.Pattern;

//https://www.logicbig.com/tutorials/core-java-tutorial/java-regular-expressions/regex-lookbehind.html
public class TagPattern {
    private static final String TAG_BEGIN_PATTERN = "<${tagName}[^>]*>";

    private static final String TAG_END_PATTERN = "</${tagName}>";

    public static final Pattern A_WRAP_PATTERN = Pattern.compile("(?i)<a[^>]*>([^<]*)<\\/a>");

    public static String getTagBegin(String label, String tagName) {
        String pattern = TAG_BEGIN_PATTERN.replace("${tagName}", tagName);
        return wrapWithLabel(label, pattern);
    }

    public static String getTagEnd(String label, String tagName) {
        String pattern = TAG_END_PATTERN.replace("${tagName}", tagName);
        return wrapWithLabel(label, pattern);
    }

    public static String wrapWithLabel(String label, String pattern) {
        return "(?<" + label + ">" + pattern + ")";
    }
}
