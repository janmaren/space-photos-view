package org.maren.spview.objfetch;

import cz.jmare.math.astro.RaDec;

public class FetchedCelestObj {
    private String name;

    private RaDec raDec;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RaDec getRaDec() {
        return raDec;
    }

    public void setRaDec(RaDec raDec) {
        this.raDec = raDec;
    }
}
