package org.maren.spview.astrometry;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import cz.jmare.http.HUrl;
import cz.jmare.http.RequestData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class AstrometryApiClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(AstrometryApiClient.class);

    public static final String ASTROMETRY_URL = "http://nova.astrometry.net";

    public static final String ASTROMETRY_API_URL = ASTROMETRY_URL + "/api";

    private String apiKey;

    private String imageFile;

    private String proxyHostname;

    private Integer proxyPort;
    private Long subid;

    private Long jobId;

    public AstrometryApiClient(String apiKey, String imageFile) throws Exception {
        this.apiKey = apiKey;
        this.imageFile = imageFile;
        upload();
    }

    public AstrometryApiClient(String apiKey, String imageFile, String proxyHost, Integer proxyPort) throws Exception {
        this.apiKey = apiKey;
        this.imageFile = imageFile;
        this.proxyHostname = proxyHost;
        this.proxyPort = proxyPort;
        upload();
    }

    private void upload() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        String sessionJson = getSessionIdByApiKey(apiKey, createRequestData(proxyHostname, proxyPort));
        JsonNode sessionNode = objectMapper.readTree(sessionJson);
        if (!"success".equals(sessionNode.get("status").asText())) {
            LOGGER.error("Unable to obtain session id: " + sessionNode.get("errormessage").asText());
            throw new IllegalStateException("Unable to obtain session id: " + sessionNode.get("errormessage").asText());
        }
        String sessionId = sessionNode.get("session").asText();
        LOGGER.info("Session id is {}", sessionId);

        String subidJson = uploadAndGetSubid(imageFile, sessionId, createRequestData(proxyHostname, proxyPort));
        if (subidJson == null) {
            LOGGER.error("Unable to upload " + imageFile + " by sessionId " + sessionId);
            throw new IllegalStateException("Unable to upload " + imageFile + " by sessionId " + sessionId);
        }

        JsonNode jsonNode = objectMapper.readTree(subidJson);
        if (!"success".equals(jsonNode.get("status").asText())) {
            LOGGER.error("Unable to upload " + imageFile + " by sessionId " + sessionId);
            throw new IllegalStateException("Unable to upload " + imageFile + " by sessionId " + sessionId);
        }
        subid = jsonNode.get("subid").asLong();
        LOGGER.info("Uploaded, subid is {}", subid);
    }

    private RequestData createRequestData(String proxyHostname, Integer proxyPort) {
        RequestData requestData = new RequestData();
        requestData.setConnectTimeout(7000);
        requestData.setReadTimeout(120000);
        requestData.setFollowRedirects(true);
        if (proxyHostname != null) {
            requestData.setProxy(proxyHostname, proxyPort);
        }
        return requestData;
    }

    public byte[] getWcsFile() {
        try {
            if (jobId == null) {
                findJobId();
                if (jobId == null) {
                    return null;
                }
            }

            return downloadWcsFitsFileByJobid(jobId, createRequestData(proxyHostname, proxyPort));
        } catch (Exception e) {
        }
        return null;
    }

    public byte[] getNewFitsFile() {
        try {
            if (jobId == null) {
                findJobId();
                if (jobId == null) {
                    return null;
                }
            }

            return downloadNewFitsFileByJobid(jobId, createRequestData(proxyHostname, proxyPort));
        } catch (Exception e) {
        }
        return null;
    }

    private void findJobId() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String jobJson = getJobBySubid(subid, createRequestData(proxyHostname, proxyPort));
        JsonNode getJobsNode = objectMapper.readTree(jobJson);
        JsonNode jobsNode = getJobsNode.get("jobs");
        if (jobsNode instanceof ArrayNode) {
            ArrayNode node = (ArrayNode) jobsNode;
            if (node.size() > 0) {
                JsonNode jsonNode = node.get(0);
                if (!jsonNode.isNull()) {
                    jobId = jsonNode.asLong();
                    LOGGER.info("Job id is {}", jobId);
                }
            }
        }
    }

    public byte[] waitAndGetWcsFile(Long timeout) {
        long until = timeout != null ? System.currentTimeMillis() + timeout : Long.MAX_VALUE;
        while (System.currentTimeMillis() < until) {
            byte[] wcsFile = getWcsFile();
            if (wcsFile != null) {
                return wcsFile;
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                return null;
            }
        }
        return null;
    }

    public byte[] waitAndGetNewFitsFile(Long timeout) {
        long until = timeout != null ? System.currentTimeMillis() + timeout : Long.MAX_VALUE;
        while (System.currentTimeMillis() < until) {
            byte[] newFitsFile = getNewFitsFile();
            if (newFitsFile != null) {
                return newFitsFile;
            }

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                return null;
            }
        }
        return null;
    }

    public static String getSessionIdByApiKey(String apiKey, RequestData requestData) {
        return HUrl.postTextReturnString(ASTROMETRY_API_URL + "/login", "request-json={\"apikey\": \""+apiKey+"\"}", "UTF-8", requestData);
    }

    public static String uploadAndGetSubid(String file, String session, RequestData requestData) {
        return HUrl.postFieldsFilesMultipartReturnString(ASTROMETRY_API_URL + "/upload",
                                                                    Map.of("request-json", "{\"publicly_visible\": \"n\", \"allow_modifications\": \"d\", \"session\": \""+session+"\", \"allow_commercial_use\": \"d\"}"), Map.of("file", new File(file)), requestData);
    }

    public static String getJobBySubid(long subId, RequestData requestData) {
        return HUrl.getString(ASTROMETRY_API_URL + "/submissions/" + subId, requestData);
    }

    public static byte[] downloadWcsFitsFileByJobid(long jobId, RequestData requestData) {
        return HUrl.getByteArray(ASTROMETRY_URL + "/wcs_file/" + jobId, requestData);
    }

    public static byte[] downloadNewFitsFileByJobid(long jobId, RequestData requestData) {
        return HUrl.getByteArray(ASTROMETRY_URL + "/new_fits_file/" + jobId, requestData);
    }

    public void setProxy(String proxyHostname, int proxyPort) {
        this.proxyHostname = proxyHostname;
        this.proxyPort = proxyPort;
    }

    public static void main(String[] args) throws Exception {
        AstrometryApiClient astrometryApiClient =
                new AstrometryApiClient("irgnzgslciijvpvn", "C:\\Users\\jmarencik\\Downloads\\ngc507_1600.jpg"/*, "localhost", 8888*/);
        byte[] bytes = astrometryApiClient.waitAndGetWcsFile(null);
        Files.write(Paths.get("C:\\Users\\jmarencik\\Downloads\\out2.fit"), bytes);
    }
}
