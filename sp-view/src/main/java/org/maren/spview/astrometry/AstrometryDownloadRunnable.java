package org.maren.spview.astrometry;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;

public class AstrometryDownloadRunnable implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AstrometryDownloadRunnable.class);

    private final static long timeout = 10 * 60 * 1000;

    private boolean doWcsFits;

    private boolean doNewFits;

    private String inputImage;
    private volatile AstrometryResultConsumer astrometryResultConsumer;

    private String astrometryApiKey;

    public AstrometryDownloadRunnable(String inputImage, String astrometryApiKey, boolean doWcsFits, boolean doNewFits, AstrometryResultConsumer astrometryResultConsumer) {
        this.inputImage = inputImage;
        this.astrometryApiKey = astrometryApiKey;
        this.doWcsFits = doWcsFits;
        this.doNewFits = doNewFits;
        this.astrometryResultConsumer = astrometryResultConsumer;
    }

    @Override
    public void run() {
        ConfigStore configStore = AppConfig.getInstance().getConfigStore();
        AstrometryApiClient astrometryApiClient;
        try {
            if (!configStore.getValue("proxy.host", "").isBlank()) {
                astrometryApiClient = new AstrometryApiClient(astrometryApiKey, inputImage, configStore.getValue("proxy.host", null), configStore.getIntValue("proxy.port", 8080));
            } else {
                astrometryApiClient = new AstrometryApiClient(astrometryApiKey, inputImage);
            }
        } catch (Exception e) {
            LOGGER.error("Unable to plate solve {}, {}", inputImage, e.getMessage() != null ? e.getMessage() : "", e);
            notifyResult(null, null);
            return;
        }

        if (Thread.currentThread().isInterrupted()) {
            notifyResult(null, null);
        }

        long start = System.currentTimeMillis();
        File wcsFile = null;
        File newFitsFile = null;
        if (doWcsFits) {
            byte[] bytes = astrometryApiClient.waitAndGetWcsFile(timeout);
            wcsFile = writeToTmpFile(bytes);
        }

        if (Thread.currentThread().isInterrupted()) {
            notifyResult(null, null);
        }

        if (doNewFits) {
            byte[] bytes = astrometryApiClient.waitAndGetNewFitsFile(timeout);
            newFitsFile = writeToTmpFile(bytes);
        }

        notifyResult(wcsFile, newFitsFile);
        System.out.println("Downloaded, time " + (System.currentTimeMillis() - start) / 1000);
    }

    void notifyResult(File wcsFile, File newFitsFile) {
        AstrometryResultConsumer consumer = astrometryResultConsumer;
        if (consumer != null) {
            LOGGER.info("Notified with files {}, {}", wcsFile, newFitsFile);
            consumer.astrometryResultFiles(wcsFile, newFitsFile);
        }
    }

    private static File writeToTmpFile(byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        try {
            Path tempFile = Files.createTempFile(null, ".fits");
            Files.write(tempFile, bytes);
            File file = tempFile.toFile();
            file.deleteOnExit();
            return file;
        } catch (IOException e) {
            return null;
        }
    }

    public void setAstrometryResultConsumer(AstrometryResultConsumer astrometryResultConsumer) {
        this.astrometryResultConsumer = astrometryResultConsumer;
    }
}
