package org.maren.spview.astrometry;

import java.io.File;

public interface AstrometryResultConsumer {
    void astrometryResultFiles(File wcsFile, File newFitsFile);
}
