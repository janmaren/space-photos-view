package org.maren.spview;

public class NearestSelectionObject {
    private ObjectType objectType;

    private Object object;

    private String text;
    private Double distanceDegs;

    public transient Integer tableIndex;

    public NearestSelectionObject(ObjectType objectType, Object object, String text, Double distanceDegs) {
        this.objectType = objectType;
        this.object = object;
        this.text = text;
        this.distanceDegs = distanceDegs;
    }

    public ObjectType getObjectType() {
        return objectType;
    }

    public Object getObject() {
        return object;
    }

    public String getText() {
        return text;
    }

    public Double getDistanceDegs() {
        return distanceDegs;
    }

    public static enum ObjectType {
        RASTER, POINT, CIRCLE, STAR
    }
}
