package org.maren.spview.rasterpos.pixeltospheric;

import cz.jmare.math.astro.RaDec;

public interface PixelToSpheric {
    /**
     * Get RaDec of concrete pixel
     * @param imageX X coordinate of current image (possibly shrinked)
     * @param imageY Y coordinate of current image (possibly shrinked)
     * @return RaDec
     */
    RaDec toRaDec(double imageX, double imageY);

    /**
     * Create copy with updated division
     * @param division new division
     * @return copy
     */
    PixelToSpheric toPixelToSpheric(double division);
}
