package org.maren.spview.rasterpos.pixeltospheric;

import cz.jmare.fits.header.WCS2d;
import cz.jmare.math.astro.RaDec;

public class WCS2dRevertedPixelToSpheric implements PixelToSpheric {
    private WCS2d wcs2d;

    /**
     * Origin height when division=1
     */
    private int height;

    private double division = 1;

    public WCS2dRevertedPixelToSpheric(WCS2d wcs2d, int height) {
        this.wcs2d = wcs2d;
        this.height = height;
    }

    @Override
    public RaDec toRaDec(double imageX, double imageY) {
        return wcs2d.pixelToFK5(imageX * division, (height - imageY * division));
    }

    @Override
    public PixelToSpheric toPixelToSpheric(double division) {
        WCS2dRevertedPixelToSpheric wcs2dRevertedPixelToSpheric = new WCS2dRevertedPixelToSpheric(wcs2d, height);
        wcs2dRevertedPixelToSpheric.division = division;
        return wcs2dRevertedPixelToSpheric;
    }
}
