package org.maren.spview.rasterpos.pixeltospheric;

import cz.jmare.fits.header.WCS2d;
import cz.jmare.math.astro.RaDec;

public class WCS2dPixelToSpheric implements PixelToSpheric {
    private WCS2d wcs2d;

    private double division = 1;

    public WCS2dPixelToSpheric(WCS2d wcs2d) {
        this.wcs2d = wcs2d;
    }

    @Override
    public RaDec toRaDec(double imageX, double imageY) {
        return wcs2d.pixelToFK5(imageX * division, imageY * division);
    }

    @Override
    public PixelToSpheric toPixelToSpheric(double division) {
        WCS2dPixelToSpheric wcs2dPixelToSpheric = new WCS2dPixelToSpheric(wcs2d);
        wcs2dPixelToSpheric.division = division;
        return wcs2dPixelToSpheric;
    }
}
