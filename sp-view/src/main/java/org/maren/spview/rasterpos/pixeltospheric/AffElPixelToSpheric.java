package org.maren.spview.rasterpos.pixeltospheric;

import cz.jmare.math.astro.RaDec;
import org.maren.statis.affine.AffineElements;
import org.maren.statis.entity.Coordinate;

public class AffElPixelToSpheric implements PixelToSpheric {
    private AffineElements affineElements;

    private double division = 1;

    public AffElPixelToSpheric(AffineElements affineElements) {
        this.affineElements = affineElements;
    }

    @Override
    public RaDec toRaDec(double imageX, double imageY) {
        Coordinate point2 = affineElements.transform(imageX * division, imageY * division);
        return new RaDec(point2.x, point2.y);
    }

    @Override
    public PixelToSpheric toPixelToSpheric(double division) {
        AffElPixelToSpheric affElPixelToSpheric = new AffElPixelToSpheric(affineElements);
        affElPixelToSpheric.division = division;
        return affElPixelToSpheric;
    }
}
