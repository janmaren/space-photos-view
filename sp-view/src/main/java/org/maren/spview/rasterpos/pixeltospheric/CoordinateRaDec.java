package org.maren.spview.rasterpos.pixeltospheric;

import cz.jmare.math.astro.RaDec;
import org.maren.statis.entity.Coordinate;

/**
 * Useful to map image coordinates to spheric
 */
public class CoordinateRaDec {
    public Coordinate coordinate;

    public RaDec raDec;

    public CoordinateRaDec(Coordinate coordinate, RaDec raDec) {
        this.coordinate = coordinate;
        this.raDec = raDec;
    }
}
