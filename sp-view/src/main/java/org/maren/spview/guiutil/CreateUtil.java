package org.maren.spview.guiutil;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class CreateUtil {
    public static Text text(Composite parent, String value, int widthInGrid, int heightInGrid) {
        Text text = new Text(parent, SWT.BORDER);
        text.setText(value);
        GridData gridData = new GridData();
        gridData.widthHint = widthInGrid;
        gridData.heightHint = heightInGrid;
        text.setLayoutData(gridData);
        return text;
    }
}
