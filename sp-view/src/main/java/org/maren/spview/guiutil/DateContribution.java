package org.maren.spview.guiutil;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.eclipse.jface.action.ControlContribution;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.maren.spview.SPView;
import org.maren.spview.app.DateTimeDialog;
import org.maren.spview.util.DateTimeUtil;

import cz.jmare.data.util.JulianUtil;
import cz.jmare.swt.util.Create;

public class DateContribution extends ControlContribution {
    private Label dateTimeZoneText;

    private LocalDateTime dateTime = LocalDateTime.now();

    private double timeOffset = 0;
    private SPView spView;

    public DateContribution(SPView spView) {
        super(null);
        this.spView = spView;
        timeOffset = getOffset();
    }

    @Override
    protected Control createControl(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout(1, false);
        layout.marginTop = 0;
        layout.marginHeight = 0;
        composite.setLayout(layout);
        dateTimeZoneText = Create.label(composite, "");
        GridData layoutData = new GridData();
        layoutData.widthHint = 190;
        layoutData.grabExcessVerticalSpace = true;
        layoutData.verticalAlignment = SWT.CENTER;
        dateTimeZoneText.setLayoutData(layoutData);
        dateTimeZoneText.setText("");
        dateTimeZoneText.addMouseListener(MouseListener.mouseDownAdapter(e -> {
            DateTimeDialog dateTimeDialog = new DateTimeDialog(parent.getShell(), dateTime, timeOffset);
            if (dateTimeDialog.open() == Window.OK) {
                dateTime = dateTimeDialog.getResultDateTime();
                timeOffset = dateTimeDialog.getResultZone();
                updateDateTimeZoneText();
                spView.updateDateTimeZone();
            }
        }));

        dateTimeZoneText.addMouseTrackListener(new MouseTrackListener() {
            private Cursor origCursor;
            @Override
            public void mouseEnter(MouseEvent e) {
                origCursor = dateTimeZoneText.getCursor();
                //dateTimeZoneText.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_GRAY));
                dateTimeZoneText.setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_HAND));
            }

            @Override
            public void mouseExit(MouseEvent e) {
                //dateTimeZoneText.setBackground(origColor);
                dateTimeZoneText.setCursor(origCursor);
            }

            @Override
            public void mouseHover(MouseEvent e) {
            }
        });
        dateTimeZoneText.setToolTipText("Click to set date, time or zone offset");

        updateDateTimeZoneText();

        return composite;
    }

    public void updateDateTimeZoneText() {
        dateTimeZoneText.setText(DateTimeUtil.format(dateTime) + " " + DateTimeUtil.getOffsetHoursString());
    }

    private static double getOffset() {
        OffsetDateTime now1 = OffsetDateTime.now();
        int totalSeconds = now1.getOffset().getTotalSeconds();
        return totalSeconds / 3600.0;
    }

    public void setJulianDate(Double jd) {
        OffsetDateTime offsetDateTime = JulianUtil.toOffsetDateTime(jd, timeOffset);
        this.dateTime = offsetDateTime.toLocalDateTime();
        updateDateTimeZoneText();
    }

    public double getJulianDate() {
        OffsetDateTime offsetDateTime = OffsetDateTime.of(dateTime, ZoneOffset.ofTotalSeconds((int) (timeOffset * 3600)));
        return JulianUtil.toJulianDate(offsetDateTime);
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
        updateDateTimeZoneText();
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }
}
