package org.maren.spview;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.maren.spview.util.DateTimeUtil;
import org.maren.stacking.util.ExternalUtilsFinder;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import cz.jmare.config.AppConfig;
import cz.jmare.swt.util.SMessage;

public class SPViewMain {

    public static void main(String[] args) {
        Locale.setDefault(Locale.ENGLISH);
        AppConfig instance = AppConfig.getInstance();
        instance.init(".sp-view");
        System.setProperty("DEV_HOME", instance.getExistingDataDir().toString());

        initLogger();
        
        org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SPViewMain.class);
        LOGGER.info("SPViewMain starting...");

        new Thread(new ExternalUtilsFinder()).start();
        List<String> files = getFiles(args);
        boolean recursive = hasSwitch(args, "r");
        DefaultConfigData.populateConfig(hasSwitch(args, "defaults"));
        DateTimeUtil.setOffsetHours(instance.getConfigStore().getDoubleValue("time.offset", 0));
        SPView spView = new SPView(files, recursive);
        spView.setBlockOnOpen(true);
        try {
            spView.open();
        } catch (Exception e) {
            SMessage.reportException(e, spView.getShell());
        }
    }
    
    private static List<String> getFiles(String[] args) {
        ArrayList<String> files = new ArrayList<>();
        for (String string : args) {
            if (!string.startsWith("-")) {
                files.add(string);
            }
        }
        return files;
    }
    
    private static boolean hasSwitch(String[] args, String switchh) {
        for (String string : args) {
            if (string.startsWith("-")) {
                if (string.substring(1).equals(switchh)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private static void initLogger() {
        boolean toConsole = System.getProperty("DEBUG") != null;
        LoggerContext logCtx = (LoggerContext) LoggerFactory.getILoggerFactory();

        PatternLayoutEncoder logEncoder = new PatternLayoutEncoder();
        logEncoder.setContext(logCtx);
        logEncoder.setPattern("%-12date{YYYY-MM-dd HH:mm:ss.SSS} %-5level - %msg%n");
        logEncoder.start();

        ConsoleAppender<ILoggingEvent> logConsoleAppender = null;
        if (toConsole) {
            logConsoleAppender = new ConsoleAppender<ILoggingEvent>();
            logConsoleAppender.setContext(logCtx);
            logConsoleAppender.setName("myconsole");
            logConsoleAppender.setEncoder(logEncoder);
            logConsoleAppender.start();
        }

        logEncoder = new PatternLayoutEncoder();
        logEncoder.setContext(logCtx);
        logEncoder.setPattern("%-12date{YYYY-MM-dd HH:mm:ss.SSS} %-5level - %msg%n");
        logEncoder.start();

        RollingFileAppender<ILoggingEvent> logFileAppender = new RollingFileAppender<ILoggingEvent>();
        logFileAppender.setContext(logCtx);
        logFileAppender.setName("logFile");
        logFileAppender.setEncoder(logEncoder);
        logFileAppender.setAppend(true);
        logFileAppender.setFile(AppConfig.getInstance().getExistingDataDir() + "/logs/log.log");

        TimeBasedRollingPolicy<?> logFilePolicy = new TimeBasedRollingPolicy<Object>();
        logFilePolicy.setContext(logCtx);
        logFilePolicy.setParent(logFileAppender);
        logFilePolicy.setFileNamePattern("logs/log-%d{yyyy-MM-dd_HH}.log");
        logFilePolicy.setMaxHistory(7);
        logFilePolicy.start();

        logFileAppender.setRollingPolicy(logFilePolicy);
        logFileAppender.start();

        Logger log = logCtx.getLogger(Logger.ROOT_LOGGER_NAME);
        log.setAdditive(false);
        log.setLevel(Level.DEBUG);
        if (toConsole) {
            log.addAppender(logConsoleAppender);
        }
        log.detachAppender("console");
        log.addAppender(logFileAppender);
    }
}
