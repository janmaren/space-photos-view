package org.maren.spview.task;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.progress.OperationInterruptedException;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.Shell;
import org.maren.spview.SPView;
import org.maren.spview.horizons.EphRequest;
import org.maren.spview.horizons.IntType;
import org.maren.spview.minorplanet.MinorPlanetComet;
import org.maren.spview.minorplanet.SmallObjectsFetcher;
import org.maren.spview.shape.PointShape;
import org.maren.spview.util.ObjectsListUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.maren.spview.horizons.NasaHorizonsDownloadManager.downloadEphsCached;

public class DownEphsTask implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(DownEphsTask.class);
    private final Shell shell;
    private SPView spView;

    public DownEphsTask(SPView spView) {
        this.spView = spView;
        shell = this.spView.getShell();
    }
    
    @Override
    public void run() {
        LOGGER.info("Started DownEphsTask");
        ConfigStore configStore = AppConfig.getInstance().getConfigStore();
        String proxyHost = configStore.getValue("proxy.host",null);
        Integer proxyPort = configStore.getIntValue("proxy.port", 8080);

        List<String> names = new ArrayList<>();
        names.addAll(getObjectNames());
        names.addAll(getCometNames());

        OffsetDateTime from = OffsetDateTime.now();
        OffsetDateTime to = OffsetDateTime.now().plus(1, ChronoUnit.DAYS);


        EphRequest ephRequest = new EphRequest(names, from,
                                               to, 1, IntType.HOUR);
        try {
            List<List<PointShape>> lists = downloadEphsCached(ephRequest, proxyHost, proxyPort, 7 * 24);
            if (lists.size() < ephRequest.getRequiredNames().size()) {
                if (lists.isEmpty()) {
                    shell.getDisplay().asyncExec(() -> spView.setError("Problems to download any ephemeris"));
                } else {
                    shell.getDisplay().asyncExec(() -> spView.setInfo("Downloaded ephemeris of " + lists.size() + "/" + ephRequest.getRequiredNames().size() + " objects"));
                }
            } else {
                shell.getDisplay().asyncExec(() -> spView.setInfo("Downloaded ephemeris of all " + ephRequest.getRequiredNames().size() + " objects"));
            }
        } catch (OperationInterruptedException e) {
            LOGGER.info("Predownload interrupted");
        } catch (SWTException e) {
            if (e.getMessage() != null && !e.getMessage().contains("disposed")) {
                LOGGER.error("Problems to download ephemeris", e);
            }
        } catch (Exception e) {
            LOGGER.error("Problems to download ephemeris", e);
            shell.getDisplay().asyncExec(() -> spView.setError(ExceptionMessage.getCombinedMessage("Problems to download ephemeris", e), e));
            return;
        }
    }

    private List<String> getObjectNames() {
        List<String> names = new ArrayList<>();
        names.addAll(ObjectsListUtil.parseObjectsList(ObjectsListUtil.loadResourcesTextFile("/objects/planets.txt")));
        names.addAll(ObjectsListUtil.parseObjectsList(ObjectsListUtil.loadResourcesTextFile("/objects/dwarf-planets.txt")));
        names.addAll(ObjectsListUtil.parseObjectsList(ObjectsListUtil.loadResourcesTextFile("/objects/asteroids.txt")));
        return names;
    }

    private List<String> getCometNames() {
        List<MinorPlanetComet> smallObjectNames = new ArrayList<>();
        try {
            smallObjectNames = SmallObjectsFetcher.getComets();
        } catch (Exception e) {
            LOGGER.error("Problems to download object names", e);
            shell.getDisplay().asyncExec(() -> spView.setError(ExceptionMessage.getCombinedMessage("Unable to download objects", e), e));
            return List.of();
        }
        List<String[]> namesAndShorted = SmallObjectsFetcher.shortSmallObjectNames(smallObjectNames);
        namesAndShorted = namesAndShorted.stream().filter(ns -> ns[1] != null).collect(Collectors.toList());
        List<String> names = namesAndShorted.stream().map(ns -> ns[1]).collect(Collectors.toList());
        return names;
    }
}
