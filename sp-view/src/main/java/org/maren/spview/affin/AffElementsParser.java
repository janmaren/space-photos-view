package org.maren.spview.affin;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Function;

import org.maren.statis.affine.AffineElements;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.GonUtil;
import cz.jmare.data.util.HourUtil;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.TwoPoints;

/**
 * Parses lines where each element is on own line.
 * It expects that on each line will be a number using order: A, D, B, E, C, F
 * https://pro.arcgis.com/en/pro-app/latest/help/data/imagery/world-files-for-raster-datasets.htm
 */
public class AffElementsParser {
    private static final String RF_POINT = "RF_POINT=";

    public static AffineElements parseLines(List<String> lines) {
        if (lines.size() < 6) {
            throw new IllegalStateException("Not six lines provided");
        }
        double[] doubles = new double[6];
        for (int i = 0; i < 6; i++) {
            String line = lines.get(i).trim();
            double d = Double.parseDouble(line);
            doubles[i] = d;
        }
        return new AffineElements(doubles[0], doubles[2], doubles[4], doubles[1], doubles[3], doubles[5]);
    }
    
    public static boolean containsRefPoints(List<String> lines) {
        for (String line : lines) {
            if (line.startsWith(RF_POINT)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Parses format like where RA is in hours:
     * <pre>RF_POINT=1.1, 2.5, 3:49:09.74, 24:03:12.29; 2.1, 2.5, 3:48:06.54, 24:59:18.29; 5.1, 2.5, 3:44:52.53, 24:06:48.02</pre>
     * @param lines
     * @return ref points, image is double, double, real is double(DEG)-ra, double(DEG)-dec
     */
    public static List<TwoPoints> parseRefDoubleAndDegPoints(List<String> lines) {
        Function<String, Point> sourceParser = coor1 -> {
            String[] coor1Spl = coor1.split(",");
            if (coor1Spl.length != 2) {
                throw new IllegalStateException("Wrong format of " + coor1);
            }
            return new Point(Double.parseDouble(coor1Spl[0].trim()), Double.parseDouble(coor1Spl[1].trim()));
        };
        Function<String, Point> targetParser = coor2 -> {
            RaDec parsePolarDeg = ACooUtil.parseCoords(coor2);
            return new Point(parsePolarDeg.ra, parsePolarDeg.dec);
        };
        return parseRefPoints(lines, sourceParser, targetParser);
    }
    
    /**
     * Parses format like:
     * <pre>RF_POINT={[src_coord1], [targ_coord1]}; {[src_coord2], [targ_coord2]}; {[src_coord3], [targ_coord3]}</pre>
     * @param lines where on one of them starts with RF_POINT=...
     * @param sourceParser convert source string with comma to x, y Point
     * @param targetParser convert target string with comma to x, y Point
     * @return
     */
    public static List<TwoPoints> parseRefPointsOld(List<String> lines, Function<String, Point> sourceParser, Function<String, Point> targetParser) {
        String line = null;
        for (String lin : lines) {
            if (lin.startsWith(RF_POINT)) {
                line = lin;
                break;
            }
        }
        List<TwoPoints> result = new ArrayList<TwoPoints>();
        String values = line.substring(RF_POINT.length());
        String[] twins = values.split(";");
        for (String twin : twins) {
            twin = twin.trim();
            if (!twin.startsWith("{") || !twin.endsWith("}")) {
                throw new IllegalStateException("Wrong format of " + twin);
            }
            twin = twin.substring(1, twin.length() - 1);
            int l1 = twin.indexOf("[");
            int r1 = twin.indexOf("]", l1 + 1);
            int l2 = twin.indexOf("[", r1 + 1);
            int r2 = twin.indexOf("]", l2 + 1);
            String coor1 = twin.substring(l1 + 1, r1).trim();
            String coor2 = twin.substring(l2 + 1, r2).trim();
            
            Point sourcePoint = sourceParser.apply(coor1);
            Point targetPoint = targetParser.apply(coor2);
            TwoPoints twoPoints = new TwoPoints(sourcePoint, targetPoint);
            result.add(twoPoints);
        }
        return result;
    }
    
    /**
     * Parses format like:
     * <pre>RF_POINT=src_coord1.x, src_coord1.y, targ_coord1.x, targ_coord1.y; src_coord2.x, src_coord2.y, targ_coord2.x, targ_coord2.y; src_coord3.x, src_coord3.y, targ_coord3.x, targ_coord3.y</pre>
     * @param lines where on one of them starts with RF_POINT=...
     * @param sourceParser convert source string with comma to x, y Point
     * @param targetParser convert target string with comma to x, y Point
     * @return
     */
    public static List<TwoPoints> parseRefPoints(List<String> lines, Function<String, Point> sourceParser, Function<String, Point> targetParser) {
        String line = null;
        for (String lin : lines) {
            if (lin.startsWith(RF_POINT)) {
                line = lin;
                break;
            }
        }
        if (line == null) {
            throw new IllegalStateException("Not an rfpoint file - RF_POINT tag not found");
        }
        List<TwoPoints> result = new ArrayList<TwoPoints>();
        String values = line.substring(RF_POINT.length());
        String[] twins = values.split(";");
        for (String twin : twins) {
            twin = twin.trim();
            String[] split = twin.split(",");
            if (split.length != 4) {
                throw new IllegalArgumentException("Unable to parse " + twin);
            }
            Point sourcePoint = sourceParser.apply(split[0] + ", " + split[1]);
            Point targetPoint = targetParser.apply(split[2] + ", " + split[3]);
            TwoPoints twoPoints = new TwoPoints(sourcePoint, targetPoint);
            result.add(twoPoints);
        }
        return result;
    }
    
    public static String formatRefPointsOld(List<TwoPoints> twoPoints, Function<Point, String> sourceToString, Function<Point, String> targetToString) {
        StringJoiner stringJoiner = new StringJoiner("; ");
        for (TwoPoints twoPoints2 : twoPoints) {
            String srcStr = sourceToString.apply(twoPoints2.getPoint1());
            String tarStr = targetToString.apply(twoPoints2.getPoint2());
            stringJoiner.add("{[" + srcStr + "], [" + tarStr + "]}");
        }
        return RF_POINT + stringJoiner.toString();
    }
    
    public static String formatRefPoints(List<TwoPoints> twoPoints, Function<Point, String> sourceToString, Function<Point, String> targetToString) {
        StringJoiner stringJoiner = new StringJoiner("; ");
        for (TwoPoints twoPoints2 : twoPoints) {
            String srcStr = sourceToString.apply(twoPoints2.getPoint1());
            String tarStr = targetToString.apply(twoPoints2.getPoint2());
            stringJoiner.add(srcStr + ", " + tarStr);
        }
        return RF_POINT + stringJoiner.toString();
    }
    
    public static String formatRefDoubleAndDegPoints(List<Point> imageCoords, List<RaDec> realCoords) {
        List<TwoPoints> twoPointsList = new ArrayList<TwoPoints>();
        for (int i = 0; i < imageCoords.size(); i++) {
            RaDec polarDeg = realCoords.get(i);
            TwoPoints twoPoints = new TwoPoints(imageCoords.get(i), new Point(polarDeg.ra, polarDeg.dec));
            twoPointsList.add(twoPoints);
        }
        return formatRefDoubleAndDegPoints(twoPointsList);
    }
    
    public static String formatRefDoubleAndDegPoints(List<TwoPoints> twoPoints) {
        Function<Point, String> sourceToString = new Function<Point, String>() {
            @Override
            public String apply(Point point) {
                return String.format("%.3f", point.x) + ", " + String.format("%.3f", point.y);
            }
        };
        Function<Point, String> targetToString = new Function<Point, String>() {
            @Override
            public String apply(Point point) {
                return HourUtil.formatHours(ACooUtil.degreesToHours(point.x)) + ", " + GonUtil.formatDegrees(point.y);
            }
        };
        return formatRefPoints(twoPoints, sourceToString, targetToString);
    }
}
