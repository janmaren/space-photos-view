package org.maren.spview.sphericcanvas.draw;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.maren.gis.sphericcanvas.CoordState;

import cz.jmare.swt.color.ColorUtil;

public class CenterCrossDraw {
    public static Color crossColor = ColorUtil.getColorByRGB(255, 128, 0);

    public static Color crossColor2 = ColorUtil.getColorByRGB(48, 160, 144);
 
    public static void drawCenterCross(GC gc, CoordState coordState) {
        Color origColor = gc.getForeground();
        gc.setForeground(crossColor2);
        int centerScreenX = coordState.getCenterScreenX();
        int centerScreenY = coordState.getCenterScreenY();
        gc.drawLine(centerScreenX - 16, centerScreenY, centerScreenX - 4, centerScreenY);
        gc.drawLine(centerScreenX + 4, centerScreenY, centerScreenX + 16, centerScreenY);
    
        gc.drawLine(centerScreenX, centerScreenY - 16, centerScreenX, centerScreenY - 4);
        gc.drawLine(centerScreenX, centerScreenY + 4, centerScreenX, centerScreenY + 16);
    
    
        gc.setForeground(crossColor);
        gc.drawOval(centerScreenX - 4, centerScreenY - 4, 8, 8);
        gc.setForeground(origColor);
    }

}
