package org.maren.spview.sphericcanvas.draw;

import org.eclipse.swt.graphics.GC;

public class StarPointDraw {

    public static void drawStar(GC gc, int scrX, int scrY, double magnitude) {
            if (magnitude < 2) {
                gc.fillRectangle(scrX - 2, scrY - 2, 5, 5);
            } else if (magnitude < 3.5) {
                gc.setAlpha(189);
                gc.fillRectangle(scrX - 2, scrY - 2, 5, 5);
                gc.setAlpha(255);
                gc.drawRectangle(scrX - 1, scrY - 1, 2, 2);
            } else if (magnitude < 4.5) {
                gc.setAlpha(89);
                gc.fillRectangle(scrX - 2, scrY - 2, 5, 5);
                gc.setAlpha(255);
                gc.drawRectangle(scrX - 1, scrY - 1, 2, 2);
            } else if (magnitude < 5) {
                gc.setAlpha(49);
                gc.fillRectangle(scrX - 2, scrY - 2, 5, 5);
                gc.setAlpha(255);
                gc.drawRectangle(scrX - 1, scrY - 1, 2, 2);
            } else if (magnitude < 7) {
                gc.drawRectangle(scrX - 1, scrY - 1, 2, 2);
            } else if (magnitude < 10) {
                gc.setAlpha(89);
                gc.drawRectangle(scrX - 1, scrY - 1, 2, 2);
                gc.setAlpha(255);
            } else {
                gc.drawPoint(scrX, scrY);
            }
            
            
            //gc.drawPoint(scrX, scrY);
    //        if (diameter >= 8) {
    //            gc.fillRectangle(scrX - 3, scrY - 3, 7, 7);
    //        } else
    //        if (diameter >= 7) {
    //        	gc.fillRectangle(scrX - 2, scrY - 2, 5, 5);
    //        } else if (diameter >= 4) {
    //            gc.setAlpha(89);
    //            gc.fillRectangle(scrX - 2, scrY - 2, 5, 5);
    //            gc.setAlpha(255);
    //        } else if (diameter > 3) {
    //        	gc.drawRectangle(scrX - 1, scrY - 1, 2, 2);
    //        } else if (diameter > 2) {
    //        	gc.setAlpha(89);
    //        	gc.drawRectangle(scrX - 1, scrY - 1, 2, 2);
    //        	gc.setAlpha(255);
    //        }
        }

}
