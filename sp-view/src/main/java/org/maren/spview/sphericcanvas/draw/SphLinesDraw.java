package org.maren.spview.sphericcanvas.draw;

import static org.maren.gis.util.draw.SphericLinesDraw.drawGreyDarkUniverseLines;

import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.maren.gis.sphericcanvas.CoordState;

public class SphLinesDraw {
    static {
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        fontRegistry.put("smallest-font", new FontData[]{new FontData("smaller-font", 4, SWT.NORMAL)});
        fontRegistry.put("smaller-font", new FontData[]{new FontData("smaller-font", 7, SWT.NORMAL)});
    }
    
    public static void drawUniverseLines(GC gc, CoordState coordState, boolean showLinesNumbers) {
        Font origFont = gc.getFont();
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        gc.setFont(fontRegistry.get(coordState.getZoom() < 200 ? "smallest-font" : "smaller-font"));
        drawGreyDarkUniverseLines(gc, coordState, showLinesNumbers);
        gc.setFont(origFont);
    }

    public static void drawEarthLines(GC gc, CoordState coordState, boolean showLinesNumbers) {
        Font origFont = gc.getFont();
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        gc.setFont(fontRegistry.get(coordState.getZoom() < 200 ? "smallest-font" : "smaller-font"));
        drawEarthLines(gc, coordState, showLinesNumbers);
        gc.setFont(origFont);
    }
}
