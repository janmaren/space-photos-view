package org.maren.spview.sphericcanvas.draw;

import java.util.Collection;
import java.util.Set;

import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.type.Spheric;
import org.maren.gis.util.draw.SphericLinesDraw;
import org.maren.spview.app.Constellation;
import org.maren.swt.util.ConvTypesUtil;

import cz.jmare.swt.color.ColorUtil;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.astro.RahDec;
import cz.jmare.math.geometry.entity.LineSegmentFloat;

public class ConstellationsDraw {
    private static Color constellColor = ColorUtil.getColorByRGB(100, 100, 255);
    
    static {
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        fontRegistry.put("smallest-cons-font", new FontData[]{new FontData("smaller-font", 4, SWT.NORMAL)});
        fontRegistry.put("smaller-cons-font", new FontData[]{new FontData("smaller-font", 7, SWT.NORMAL)});
    }
    
    public static void drawConstellations(GC gc, CoordState coordState, Collection<Constellation> constellations) {
        Color origColor = gc.getForeground();
        gc.setForeground(constellColor);
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        Font origFont = gc.getFont();
        gc.setFont(fontRegistry.get(coordState.getZoom() < 250 ? "smallest-cons-font" : "smaller-cons-font"));
        for (Constellation constellation : constellations) {
            Set<LineSegmentFloat> lines = constellation.lines;
            for (LineSegmentFloat lineSegmentFloat : lines) {
                Spheric sph1 = ConvTypesUtil.toSpheric(new RaDec(lineSegmentFloat.x1, lineSegmentFloat.y1));
                Spheric sph2 = ConvTypesUtil.toSpheric(new RaDec(lineSegmentFloat.x2, lineSegmentFloat.y2));
                ScreenPoint sp1 = coordState.toScreenCoordinates(sph1);
                ScreenPoint sp2 = coordState.toScreenCoordinates(sph2);
                if (!sp1.visibleHemis && !sp2.visibleHemis) {
                    continue;
                }
                SphericLinesDraw.drawCroppedLine(gc, sph1, sph2, coordState);
            }
            if (constellation.nameRa != null && constellation.nameDec != null) {
                ScreenPoint label = coordState.toScreenCoordinates(ConvTypesUtil.toSpheric(new RahDec(constellation.nameRa, constellation.nameDec).toRaDec()));
                if (label.visibleHemis && coordState.getZoom() > 101) {
                    gc.drawString(constellation.name, label.point.x, label.point.y, true);
                }
            }
        }
        gc.setFont(origFont);
        gc.setForeground(origColor);
    }
}
