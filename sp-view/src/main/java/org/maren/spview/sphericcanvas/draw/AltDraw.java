package org.maren.spview.sphericcanvas.draw;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;
import org.maren.gis.projection.SphProjection;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.type.Spheric;
import org.maren.gis.util.PointInt;
import org.maren.swt.util.ConvTypesUtil;

import cz.jmare.data.util.AstroConversionUtil;
import cz.jmare.math.astro.AzAlt;
import cz.jmare.math.astro.RahDec;
import cz.jmare.math.geo.GeoLoc;

public class AltDraw {
    public static void drawAlt(GC gc, CoordState coordState, GeoLoc geoLoc, double jd) {
        Color origColor = gc.getForeground();
        gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY));
        int screenHeight = coordState.getScreenHeight();
        int screenWidth = coordState.getScreenWidth();
        for (int i = 0; i < screenHeight; i += 5) {
            for (int j = 0; j < screenWidth; j += 5) {
                Spheric spheric = coordState.toSpheric(j, i);
                if (spheric != null) {
                    RahDec rahDec = ConvTypesUtil.toNormalizedRahDec(spheric);
                    AzAlt azAlt = AstroConversionUtil.toAzAlt(rahDec, geoLoc, jd);
                    if (azAlt.alt < 0) {
                        gc.drawPoint(j, i);
                    }
                }
            }
        }
        gc.setForeground(origColor);
    }

    public static void drawAzLine(GC gc, CoordState coordState, GeoLoc geoLoc, double jd) {
        Color origColor = gc.getForeground();
        gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_YELLOW));
        Integer lastX = null;
        Integer lastY = null;
        Spheric lastSph = null;
        int invisibleCount = 0;
        AzAlt azAlt = new AzAlt(0, 0);
        for (int i = 0; i <= 360; i += 5) {
            azAlt.az = i;
            RahDec rahDec = AstroConversionUtil.toRahDec(azAlt, geoLoc, jd);
            Spheric sph = ConvTypesUtil.toSpheric(rahDec.toRaDec());
            SphProjection.ScreenPoint visibleScreenPoint = coordState.toScreenCoordinates(sph);
            boolean visible = visibleScreenPoint.visibleHemis;
            if (!visible) {
                invisibleCount++;
            } else {
                invisibleCount = 0;
            }
            PointInt screenCoordinates = visibleScreenPoint.point;
            if (lastX != null && lastY != null && invisibleCount < 2) {
                gc.drawLine(lastX, lastY, screenCoordinates.x, screenCoordinates.y);
            } else {
                if (lastX != null && lastY != null && visibleScreenPoint.visibleHemis) {
                    drawCroppedLine(gc, lastSph, sph, coordState);
                }
            }
            lastX = screenCoordinates.x;
            lastY = screenCoordinates.y;
            lastSph = sph;

            if (visible) {
                if (i == 90) {
                    gc.drawString("E", screenCoordinates.x + 1, screenCoordinates.y, true);
                } else if (i == 180) {
                    gc.drawString("S", screenCoordinates.x + 1, screenCoordinates.y, true);
                } else if (i == 270) {
                    gc.drawString("W", screenCoordinates.x + 1, screenCoordinates.y, true);
                } else if (i == 0) {
                    gc.drawString("N", screenCoordinates.x + 1, screenCoordinates.y, true);
                }
            }
        }

        gc.setForeground(origColor);
    }

    public static void drawCroppedLine(GC gc, Spheric sph1, Spheric sph2, CoordState coordState) {
        SphProjection.ScreenPoint vsp1 = coordState.toScreenCoordinates(sph1);
        SphProjection.ScreenPoint vsp2 = coordState.toScreenCoordinates(sph2);
        if (!vsp1.visibleHemis && !vsp2.visibleHemis) {
            return;
        }
        gc.drawLine(vsp1.point.x, vsp1.point.y, vsp2.point.x, vsp2.point.y);
    }
}
