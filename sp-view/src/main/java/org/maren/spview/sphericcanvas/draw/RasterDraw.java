package org.maren.spview.sphericcanvas.draw;

import java.util.List;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Transform;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.spview.app.CorruptedFileListener;
import org.maren.spview.item.RasterItem;
import org.maren.spview.item.ReferencedImage;
import org.maren.statis.affine.AffTrans;
import org.maren.statis.entity.Coordinate;

import cz.jmare.swt.status.StatusUtil;

public class RasterDraw {

    public static void drawRasters(GC gc, CoordState coordState, List<RasterItem> rasters, CorruptedFileListener corruptedFileListener) {
        Transform origTransform = new Transform(gc.getDevice());
        gc.getTransform(origTransform);
        Transform transform = new Transform(gc.getDevice());
    
        for (RasterItem rasterItem : rasters) {
            if (rasterItem.getImageToSphericCoords() != null) {
                ReferencedImage referencedImage;
                try {
                    referencedImage = rasterItem.getReferencedImage(coordState.getZoom());
                } catch (Exception e) {
                    StatusUtil.errorMessage("Problem to load " + rasterItem.getFilePath());
                    if (corruptedFileListener != null) {
                        corruptedFileListener.acceptCorrupted(rasterItem);
                        break;
                    }
                    continue;
                }
    
                float[] elements = RasterDrawUtil.elementsOfOrientedImage(referencedImage, coordState);

                if (elements == null) {
                    continue;
                }

                ImageData imageData = referencedImage.getImageData();
                Image image = new Image(gc.getDevice(), imageData);
    
                Coordinate leftTop = AffTrans.transformByMatrixFloats(new Coordinate(0, 0), elements);
                Coordinate rightTop = AffTrans.transformByMatrixFloats(new Coordinate(image.getBounds().width - 1, 0), elements);
                Coordinate rightBottom = AffTrans.transformByMatrixFloats(new Coordinate(image.getBounds().width - 1, image.getBounds().height - 1), elements);
                Coordinate leftBottom = AffTrans.transformByMatrixFloats(new Coordinate(0, image.getBounds().height - 1), elements);
                double minX = Math.min(Math.min(rightTop.x, leftTop.x), Math.min(rightBottom.x, leftBottom.x));
                double maxX = Math.max(Math.max(rightTop.x, leftTop.x), Math.max(rightBottom.x, leftBottom.x));
                double minY = Math.min(Math.min(rightTop.y, leftTop.y), Math.min(rightBottom.y, leftBottom.y));
                double maxY = Math.max(Math.max(rightTop.y, leftTop.y), Math.max(rightBottom.y, leftBottom.y));
    
                if ((maxX - minX) < 1 || (maxY - minY) < 1 || (maxX < 0) || (minX > coordState.getScreenWidth())
                        || (minY >= coordState.getScreenHeight()) || (maxY < 0)) {
                    image.dispose();
                    continue;
                }
    
                transform.setElements(elements[0], elements[1], elements[2], elements[3], elements[4], elements[5]);
                gc.setTransform(transform);
                gc.drawImage(image, 0, 0);
                image.dispose();
            } else {
                gc.setTransform(origTransform);
                ImageData imageData = rasterItem.getFullReferencedImage().getImageData();
                Image image = new Image(gc.getDevice(), imageData);
                gc.drawImage(image, 0, 0);
                image.dispose();
            }
        }
    
        transform.dispose();
        gc.setTransform(origTransform);
        origTransform.dispose();
    }

}
