package org.maren.spview.sphericcanvas.draw;

import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.type.Spheric;
import org.maren.gis.util.PointInt;

import cz.jmare.swt.color.ColorUtil;
import cz.jmare.math.astro.Galactic;
import cz.jmare.math.astro.RaDec;

public class GalacticLinesDraw {
    static {
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        fontRegistry.put("smallest-font", new FontData[]{new FontData("smaller-font", 4, SWT.NORMAL)});
        fontRegistry.put("smaller-font", new FontData[]{new FontData("smaller-font", 7, SWT.NORMAL)});
    }
    
    public static void drawUniverseLines(GC gc, CoordState coordState, boolean showLinesNumbers) {
        Color origColor = gc.getForeground();
        gc.setForeground(ColorUtil.getColorByRGB(0, 162, 232));

        int origAlpha = gc.getAlpha();
        gc.setAlpha(140);
        Font origFont = gc.getFont();
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        gc.setFont(fontRegistry.get(coordState.getZoom() < 200 ? "smallest-font" : "smaller-font"));
        
        GalacticLinesDraw.drawGalacticEquator(gc, coordState, true);
        
        gc.setFont(origFont);
    
        gc.setAlpha(origAlpha);
        
        gc.setForeground(origColor);
    }

    public static void drawGalacticEquator(GC gc, CoordState coordState, boolean showLabel) {
        Integer lastX = null;
        Integer lastY = null;
        int invisibleCount = 0;
        for (int i = 0; i <= 360; i += 10) {
            Galactic galactic = new Galactic(i, 0);
            RaDec raDecJ2000 = galactic.toRaDecJ2000();
            Spheric sph = Spheric.normalizedSpheric(Math.toRadians(raDecJ2000.ra), Math.toRadians(raDecJ2000.dec));
            ScreenPoint visibleScreenPoint = coordState.toScreenCoordinates(sph);
            boolean visible = visibleScreenPoint.visibleHemis;
            if (!visible) {
                invisibleCount++;
            } else {
                invisibleCount = 0;
            }
            PointInt screenCoordinates = visibleScreenPoint.point;
            if (lastX != null && lastY != null && invisibleCount < 2) {
                gc.drawLine(lastX, lastY, screenCoordinates.x, screenCoordinates.y);
                if (coordState.getZoom() >= 80 && showLabel) {
                    gc.drawString(i == 360 ? "0" : String.valueOf(i), screenCoordinates.x, screenCoordinates.y - 2, true);
                }
            }
            lastX = screenCoordinates.x;
            lastY = screenCoordinates.y;
        }
    }
    
    public static void drawCroppedLine(GC gc, Spheric sph1, Spheric sph2, CoordState coordState) {
        ScreenPoint vsp1 = coordState.toScreenCoordinates(sph1);
        ScreenPoint vsp2 = coordState.toScreenCoordinates(sph2);
        if (!vsp1.visibleHemis && !vsp2.visibleHemis) {
            return;
        }
        gc.drawLine(vsp1.point.x, vsp1.point.y, vsp2.point.x, vsp2.point.y);
    }
}
