package org.maren.spview.sphericcanvas.draw;

import static org.maren.swt.util.ConvTypesUtil.toPoint;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.util.PointInt;
import org.maren.spview.item.RasterItem;
import org.maren.spview.item.ReferencedImage;
import org.maren.spview.rasterpos.pixeltospheric.CoordinateRaDec;
import org.maren.spview.rasterpos.pixeltospheric.PixelToSpheric;
import org.maren.statis.affine.AffTrans;
import org.maren.statis.affine.AffineElements;
import org.maren.statis.affine.AffineTransCoefsCalculator;
import org.maren.statis.entity.ControlPoint;
import org.maren.statis.entity.Coordinate;
import org.maren.swt.util.ConvTypesUtil;

import cz.jmare.math.astro.RaDec;
import cz.jmare.math.geometry.entity.LineDoubleCrossableSegment;
import cz.jmare.math.geometry.entity.Point;

public class RasterDrawUtil {

    public static final int BASIC_GRID = 15;

    public static final int BASIC_ONSCREEN_GRID = 15;

    /**
     * Return affine elements to transform from image coordinates 0..imageWidth,0..imageHeight to screen coordinates 0..screenWidth, 0..screenHeight
     * @param referencedImage
     * @param coordState
     * @return
     */
    public static float[] elementsOfOrientedImage(ReferencedImage referencedImage, CoordState coordState) {
        // control points: imagePoint-screenPoint
        List<ControlPoint> commonRefPoints = createRefPoints(referencedImage, coordState, BASIC_GRID, false);
        if (commonRefPoints.size() < 3) {
            return null;
        }
        List<ControlPoint> refPoints = commonRefPoints;

        int grid = BASIC_ONSCREEN_GRID;
        int max = Math.min(referencedImage.getImageData().width, referencedImage.getImageData().height) / 2;

        while (grid <= max) {
            List<ControlPoint> screenRefPoints = createRefPoints(referencedImage, coordState, grid, true);
            if (screenRefPoints.size() >= 7) {
                refPoints = screenRefPoints;
                break;
            }
            grid *= 2;
        }

        AffineElements commonElements = AffineTransCoefsCalculator.calcCoefs(commonRefPoints);
        AffineElements affineElements = AffineTransCoefsCalculator.calcCoefs(refPoints);
        if (affineElements == null) {
            affineElements = commonElements;
        } else {
            double catan = Math.atan(commonElements.a * commonElements.c + commonElements.b * commonElements.d);
            double atan = Math.atan(affineElements.a * affineElements.c + affineElements.b * affineElements.d);
            if (Math.abs(catan - atan) > 0.001) {
                affineElements = commonElements; // affineElements generated with bad points (slew too high), using rather common elements
            }
        }
        
        if (affineElements == null) {
            return null;
        }

        float[] elem = new float[6];
        elem[0] = (float) affineElements.a;
        elem[1] = (float) affineElements.d;
        elem[2] = (float) affineElements.b;
        elem[3] = (float) affineElements.e;
        elem[4] = (float) affineElements.c;
        elem[5] = (float) affineElements.f;

        return elem;
    }

    private static List<ControlPoint> createRefPoints(ReferencedImage referencedImage, CoordState coordState, int grid, boolean onlyOnScreen) {
        Map<Integer, List<CoordinateRaDec>> gridToCooRadecs = referencedImage.getImageToRealCoords().getGridToCooRadecs();

        List<CoordinateRaDec> coordinateRaDec = gridToCooRadecs.computeIfAbsent(grid,
                g -> createCoordinateRaDec(referencedImage, g));

        List<ControlPoint> refPoints = new ArrayList<>();

        for (CoordinateRaDec cooRaDec : coordinateRaDec) {
            ScreenPoint vsp = coordState.toScreenCoordinates(ConvTypesUtil.toSpheric(cooRaDec.raDec));
            if (!vsp.visibleHemis) {
                continue;
            }
            if (onlyOnScreen) {
                if (vsp.point.x < 0) {
                    continue;
                }
                if (vsp.point.y < 0) {
                    continue;
                }
                if (vsp.point.x > coordState.getScreenWidth()) {
                    continue;
                }
                if (vsp.point.y > coordState.getScreenHeight()) {
                    continue;
                }
            }
            PointInt tint = vsp.point;
            Point target = new Point(tint.x, tint.y);
            refPoints.add(new ControlPoint(cooRaDec.coordinate.x, cooRaDec.coordinate.y, target.x, target.y));
        }

        return refPoints;
    }

    private static List<CoordinateRaDec> createCoordinateRaDec(ReferencedImage referencedImage, int grid) {
        PixelToSpheric pixelToSpheric = referencedImage.getImageToRealCoords().getPixelToSpheric();
        List<CoordinateRaDec> list = new ArrayList<>();
        int quatW = referencedImage.getImageData().width / grid;
        int quatH = referencedImage.getImageData().height / grid;
        for (int i = 0; i < grid - 1; i++) {
            int y = quatH * (i + 1);
            for (int j = 0; j < grid - 1; j++) {
                int x = quatW * (j + 1);
                Coordinate im = new Coordinate(x, y);
                RaDec raDec = pixelToSpheric.toRaDec(im.x, im.y);
                list.add(new CoordinateRaDec(im, raDec));
            }
        }
        return list;
    }

    public static boolean pointInRasterItem(int x, int y, RasterItem rasterItem, CoordState coordState) {
        ReferencedImage referencedImage = rasterItem.getReferencedImage(coordState.getZoom());
        float[] elements = elementsOfOrientedImage(referencedImage, coordState);
        if (elements == null) {
            return false;
        }
        Coordinate leftTop = AffTrans.transformByMatrixFloats(new Coordinate(0, 0), elements);
        Coordinate rightTop = AffTrans.transformByMatrixFloats(new Coordinate(referencedImage.getImageData().width - 1, 0), elements);
        Coordinate rightBottom = AffTrans.transformByMatrixFloats(new Coordinate(referencedImage.getImageData().width - 1, referencedImage.getImageData().height - 1), elements);
        Coordinate leftBottom = AffTrans.transformByMatrixFloats(new Coordinate(0, referencedImage.getImageData().height - 1), elements);
        List<Point> polygon = List.of(toPoint(leftTop), toPoint(rightTop), toPoint(rightBottom), toPoint(leftBottom));
        return isPointInPolygon(new Point(x, y), Integer.MAX_VALUE - 1, polygon);
    }

    /**
     * Return true when point is in polygon
     * @param point
     * @param maxX max value in which polygon is. For example width of screen
     * @param polygon the polygon
     * @return
     */
    private static boolean isPointInPolygon(Point point, double maxX, List<Point> polygon) {
        LineDoubleCrossableSegment baseSegment = new LineDoubleCrossableSegment(point.x, point.y, maxX, point.y);
        Point lastPoint = polygon.get(polygon.size() - 1);
        int number = 0;
        for (Point point2 : polygon) {
            LineDoubleCrossableSegment testSegment = new LineDoubleCrossableSegment(lastPoint.x, lastPoint.y, point2.x, point2.y);
            if (baseSegment.crossection(testSegment) != null) {
                number++;
            }
            lastPoint = point2;
        }
        return number % 2 != 0;
    }

    /**
     * Return true when point is in polygon. The max value is also calculated (less efficient)
     * @param point
     * @param polygon the polygon
     * @return
     */
    public static boolean isPointInPolygon(Point point, List<Point> polygon) {
        double maxX = -Double.MAX_VALUE;
        for (Point point2 : polygon) {
            if (point2.x > maxX) {
                maxX = point2.x;
            }
        }
        return isPointInPolygon(point, maxX + 1, polygon);
    }
}
