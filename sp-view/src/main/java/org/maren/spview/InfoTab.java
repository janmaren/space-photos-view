package org.maren.spview;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

public class InfoTab extends Composite {
    @SuppressWarnings("unused")
    private SPViewDeskComposite spViewComposite;
	private StyledText resultStyledText;

    public InfoTab(Composite parent, int style, SPViewDeskComposite spViewComposite) {
        super(parent, style);
        this.spViewComposite = spViewComposite;

        setLayout(new GridLayout(1, false));

        resultStyledText = new StyledText(this, SWT.BORDER | SWT.V_SCROLL);
    	GridData gridData = new GridData();
    	gridData.horizontalSpan = 2;
    	gridData.grabExcessHorizontalSpace = true;
    	gridData.horizontalAlignment = SWT.FILL;
    	gridData.grabExcessVerticalSpace = true;
    	gridData.verticalAlignment = SWT.FILL;
        resultStyledText.setLayoutData(gridData);
    }
    
    public static void main(String[] args){
        double value = 7.125;
        System.out.println(String.format("%02.0f",value));
    }
}
