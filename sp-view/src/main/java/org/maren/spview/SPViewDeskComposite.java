package org.maren.spview;

import static org.maren.spview.util.StyledUtil.*;
import static org.maren.swt.util.ConvTypesUtil.toSpheric;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringJoiner;

import cz.jmare.data.util.AstroConversionUtil;
import cz.jmare.graphfw.labeler.SkipOverlapLabeler;
import cz.jmare.math.astro.AzAlt;
import cz.jmare.math.astro.RahDec;
import cz.jmare.math.geo.GeoLoc;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.maren.gis.projection.OrthoUniSphProjection;
import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.sphericcanvas.CursorChangedListener;
import org.maren.gis.sphericcanvas.DrawListener;
import org.maren.gis.sphericcanvas.FindSelectionListener;
import org.maren.gis.sphericcanvas.SphericCanvas;
import org.maren.gis.sphericcanvas.ToolListener;
import org.maren.gis.sphericcanvas.zoom.FactorZoomCalculator;
import org.maren.gis.type.Spheric;
import org.maren.gis.util.PointInt;
import org.maren.gis.util.draw.BorderDraw;
import org.maren.spview.app.*;
import org.maren.spview.item.RasterItem;
import org.maren.spview.sphericcanvas.draw.*;
import org.maren.swt.util.ConvTypesUtil;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.GonUtil;
import cz.jmare.data.util.HourUtil;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.swt.color.ColorUtil;
import cz.jmare.math.astro.RaDec;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.status.StatusUtil;
import cz.jmare.swt.util.Create;
import org.maren.swt.util.SwtImageFileUtil;

public class SPViewDeskComposite extends Composite implements CursorChangedListener, CorruptedFileListener, DrawListener {
   
    private Label raDecLabel;
    private Label azAltLabel;
    private SphericCanvas sphericCanvas;
    private StyledText infoText;
    private Text enterCoordText;
    private RastersTable rastersTable;
    private boolean useDegrees;
    private Clipboard clipboard;
    private Label distLabel;
    private Button obtainCoordsButton;
    private Label angleLabel;
    private DrawShapesListener drawShapesListener;
    private boolean showCross = true;
    private Color starsColor = ColorUtil.getColor("#2CF084");
    private Color crossColor = ColorUtil.getColorByRGB(255, 128, 0);
    private List<CelestialObject> stars = List.of();
    private Double magnitudeLimit;
    private Collection<Constellation> constellations;
    private boolean showGrid = true;
    private boolean showLinesNumbers = true;

    private boolean showUnderAlt;

    private List<RasterItem> rasters = new ArrayList<RasterItem>();
    private CorruptedFileListener corruptedFileListener;
    private Table selectionsTable;

    private NearSelectionClickedListener nearSelectionClickedListener;
    private GeoLoc geoLoc;
    private Double julianDate;

    private final static int RA_DEC_ENTER_COLOR = SWT.COLOR_WIDGET_BACKGROUND;
    private Button degreesSwitchButton;

    public SPViewDeskComposite(Composite parent, int style) {
        super(parent, style);
        GridLayout layout = new GridLayout();
        layout.marginHeight = 0;
        setLayout(layout);
        createPanelContent();
    }

    protected void createPanelContent() {
        ImageUtil.put("go", "/image/cross-go.png");
        ImageUtil.put("dropper", "/image/cross-grab.png");
        ImageUtil.put("copy", "/image/copy.png");
        ImageUtil.put("paste", "/image/paste.png");
        ImageUtil.put("send", "/image/cross-send.png");
        ImageUtil.put("degrees", "/image/degrees.png");

        // --------- top canvas ---------
        sphericCanvas = new SphericCanvas(this, new OrthoUniSphProjection(), this);
        sphericCanvas.setZoomCalculator(new FactorZoomCalculator(1.35, 1e-9, 1e9), 100);
        GridData gridDataMain = new GridData(SWT.FILL, SWT.FILL, true, true);
        sphericCanvas.setLayoutData(gridDataMain);
        sphericCanvas.setZoom(200);

        // --------- bottom under canvas part ---------
        Composite bottomLeftRightComp = Create.noMarginGridComposite(this, 2);
        GridData leftRightData = new GridData(SWT.FILL, SWT.BEGINNING, true, false);
        leftRightData.heightHint = 250;
        bottomLeftRightComp.setLayoutData(leftRightData);

        Composite toolsComp = new Composite(bottomLeftRightComp, SWT.BORDER);
        GridData toolsData = new GridData();
        toolsData.horizontalSpan = 2;
        toolsData.grabExcessHorizontalSpace = true;
        toolsData.horizontalAlignment = SWT.FILL;
        toolsData.heightHint = 40;
        toolsComp.setLayoutData(toolsData);
        toolsComp.setLayout(new GridLayout(8, false));


        // ------ bottom left content ------
        Create.label(toolsComp, "RA, DEC:");
        enterCoordText = Create.text(toolsComp, "", 200, "Coordinates to go, format: 'h:m:r.d, d:m:s.d'");
        enterCoordText.addKeyListener(KeyListener.keyPressedAdapter(e -> {
            if (e.keyCode == SWT.CR || e.keyCode == SWT.LF || e.keyCode == SWT.KEYPAD_CR) {
                doGo();
            }
        }));

        obtainCoordsButton = Create.imageButton(toolsComp, ImageUtil.get("dropper"), "Get Coordinates", () -> {
            Spheric spheric0 = sphericCanvas.getSpheric0();
            RaDec raDec0 = ConvTypesUtil.toNormalizedRaDec(spheric0);
            enterCoordText.setText(ACooUtil.formatRaDec(raDec0, useDegrees, false));
        });
        Create.imageButton(toolsComp, ImageUtil.get("copy"), "Copy Coordinates", () -> {
            String trim = enterCoordText.getText().trim();
            if ("".equals(trim)) {
                return;
            }
            TextTransfer textTransfer = TextTransfer.getInstance();
            clipboard.setContents(new Object[]{trim}, new Transfer[]{textTransfer});
        });
        Create.imageButton(toolsComp, ImageUtil.get("paste"), "Paste Coordinates", () -> {
            TextTransfer textTransfer = TextTransfer.getInstance();
            Object contents = clipboard.getContents(textTransfer);
            if (contents == null || !(contents instanceof String)) {
                return;
            }
            enterCoordText.setText(contents.toString());
        });
        degreesSwitchButton = Create.imageButton(toolsComp, SWT.TOGGLE, ImageUtil.get("degrees"), "Use Degrees for RA", b -> {}, () -> {
            useDegrees(degreesSwitchButton.getSelection());
            if (degreesSwitchButton.getSelection()) {
                StatusUtil.infoMessage("Using degrees for RA");
            } else {
                StatusUtil.infoMessage("Using hours for RA");
            }
        });

        // bottom left wrapper
        Composite leftBottomComp = marginGridComposite(bottomLeftRightComp, 1);
        leftBottomComp.setBackground(getShell().getDisplay().getSystemColor(RA_DEC_ENTER_COLOR));

        GridData leftBottomData = new GridData(SWT.FILL, SWT.FILL, true, true);
        leftBottomComp.setLayoutData(leftBottomData);

        // bottom right wrapper
        Composite rightBottomComp = marginGridComposite(bottomLeftRightComp, 1);
        GridData rightBottomData = new GridData();
        rightBottomData.verticalAlignment = SWT.FILL;
        rightBottomData.grabExcessVerticalSpace = true;
        rightBottomComp.setLayoutData(rightBottomData);


        sphericCanvas.setCursorChangedListener(this);

        GridData groupData = new GridData(SWT.FILL, SWT.FILL, true, true);
        infoText = new StyledText(leftBottomComp, SWT.WRAP | SWT.READ_ONLY | SWT.V_SCROLL);
        infoText.setLayoutData(groupData);
        infoText.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));

        // ------ bottom right content ------
        Composite raDecComposite = Create.noMarginGridComposite(rightBottomComp, 4);
        GridData raDecData = new GridData();
        raDecData.verticalAlignment = SWT.TOP;
        raDecData.horizontalAlignment = SWT.BEGINNING;
        raDecComposite.setLayoutData(raDecData);
        Create.label(raDecComposite, "RA, DEC:");
        raDecLabel = Create.label(raDecComposite, "", label -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 160;
            layoutData.horizontalSpan = 3;
            label.setLayoutData(layoutData);
        });
        Create.label(raDecComposite, "AZ, ALT:");
        azAltLabel = Create.label(raDecComposite, "", label -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 170;
            layoutData.horizontalSpan = 3;
            label.setLayoutData(layoutData);
        });
        Create.label(raDecComposite, "Ang. Dist:");
        distLabel = Create.label(raDecComposite, "", label -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 100;
            label.setLayoutData(layoutData);
        });
        Create.label(raDecComposite, "Angle:");
        angleLabel = Create.label(raDecComposite, "", label -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 100;
            label.setLayoutData(layoutData);
        });


        selectionsTable = new Table(rightBottomComp, SWT.FULL_SELECTION);
        selectionsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        selectionsTable.setHeaderVisible(true);
        selectionsTable.setHeaderBackground(getShell().getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
        selectionsTable.setLinesVisible(true);
        TableColumn typeColumn = new TableColumn(selectionsTable, SWT.LEFT);
        typeColumn.setWidth(20);
        typeColumn.setResizable(false);
        typeColumn.setAlignment(SWT.LEFT);
        TableColumn selColumn = new TableColumn(selectionsTable, SWT.LEFT);
        selColumn.setWidth(360);
        selColumn.setResizable(true);
        selColumn.setAlignment(SWT.LEFT);
        selColumn.setText("Object Resolution");
        selectionsTable.setBackground(getShell().getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
        selectionsTable.setEnabled(false);

        selectionsTable.addMouseListener(MouseListener.mouseDownAdapter(e -> {
            int index = selectionsTable.getSelectionIndex();
            if (index != -1) {
                TableItem item = selectionsTable.getItem(index);
                NearestSelectionObject nearestSelectionObject = (NearestSelectionObject) item.getData();
                nearSelectionClickedListener.onNearSelectionClicked(nearestSelectionObject, e.stateMask);
            }
        }));
        selectionsTable.addMouseListener(MouseListener.mouseDoubleClickAdapter(e -> {
            TableItem item = selectionsTable.getItem(selectionsTable.getSelectionIndex());
            NearestSelectionObject nearestSelectionObject = (NearestSelectionObject) item.getData();
            nearSelectionClickedListener.onNearSelectionClicked(nearestSelectionObject, e.stateMask | SWT.SHIFT);
        }));
        selectionsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
    }

    private void doGo() {
        try {
            RaDec raDec = ACooUtil.parseCoords(enterCoordText.getText(), useDegrees);
            sphericCanvas.setSpheric0(toSpheric(raDec)).refresh();
        } catch (Exception e) {
            StatusUtil.warnMessage(ExceptionMessage.getCombinedMessage("Unable to parse RA or DEC", e));
        }
    }


    public void showDistAndAngle(Spheric centreSpheric, Spheric newSpheric) {
        double distance = ConvTypesUtil.calcAngularDistance(centreSpheric, newSpheric);
        distLabel.setText(GonUtil.formatDegrees(Math.toDegrees(distance), false));

        double dDec = newSpheric.lat - centreSpheric.lat;
        double dRa = newSpheric.lon - centreSpheric.lon;
        double atan = Math.atan(dDec / dRa);
        double degrees = Math.toDegrees(atan);
        angleLabel.setText(String.valueOf(GonUtil.formatDegrees(degrees)));
    }

    @Override
    public void onCursorChanged(Spheric newSpheric, int stateMask) {
        if ((stateMask & SWT.CTRL) == SWT.CTRL) {
            return;
        }
        if (newSpheric == null) {
            raDecLabel.setText("");
            azAltLabel.setText("");
            return;
        }
        String raHoursStr = useDegrees ? GonUtil.formatDegrees(newSpheric.getRaHours() * 15, false) : HourUtil.formatHours(newSpheric.getRaHours());
        String decDegStr = GonUtil.formatRadAsDegrees(newSpheric.lat);
        raDecLabel.setText(raHoursStr + " " + decDegStr);

        if (geoLoc != null && julianDate != null) {
            RahDec rahDec = ConvTypesUtil.toNormalizedRahDec(newSpheric);
            AzAlt azAlt = AstroConversionUtil.toAzAlt(rahDec, geoLoc, julianDate);
            azAltLabel.setText(ACooUtil.formatAzAlt(azAlt, false));
        }
        
        // angle
        showDistAndAngle(newSpheric, sphericCanvas.getSpheric0());
    }

    public void setStars(List<CelestialObject> stars) {
        this.stars = stars;
        sphericCanvas.refresh();
    }
    
    public void setRasters(List<RasterItem> rasters) {
        this.rasters = rasters;
        sphericCanvas.refresh();
    }

    public void selectRasterItem(RasterItem ra, boolean additive) {
        rastersTable.selectRasterItem(ra, additive);
    }

    protected void showStarInfo(CelestialObject vi) {
        infoText.setText("");
        if (vi == null) {
            return;
        }
        addBoldText(infoText, "HIP " + vi.getHip());
        if (vi.getHd() != null) {
            addText(infoText, ", ");
            addBoldText(infoText, "HD " + vi.getHd());
        }
        if (vi.getName() != null) {
            addText(infoText, ", ");
            addBoldText(infoText, vi.getName());
        }
        addLF(infoText);
        Spheric spheric = ConvTypesUtil.toSpheric(vi.getCoords());
        addText(infoText, "RA, DEC: ");
        addInvText(infoText, ACooUtil.formatRaDecProfi(ConvTypesUtil.toNormalizedRaDec(spheric)));
        if (geoLoc != null && julianDate != null) {
            RahDec rahDec = ConvTypesUtil.toNormalizedRahDec(spheric);
            AzAlt azAlt = AstroConversionUtil.toAzAlt(rahDec, geoLoc, julianDate);
            addLF(infoText);
            addText(infoText, "AZ, ALT: ");
            addInvText(infoText, " " + ACooUtil.formatAzAlt(azAlt, false));
        }
        addLF(infoText);
        StringJoiner secondLine = new StringJoiner(", ");
        if (vi.getConstellationName() != null) {
            secondLine.add("CONS " + vi.getConstellationName());
        }
        if (vi.getMagnitude() != null) {
            secondLine.add("MAG " + vi.getMagnitude());
        }
        if (vi.getSpetralType() != null) {
            secondLine.add("SpType " + vi.getSpetralType());
        }
        if (vi.getDistance() != null) {
            BigDecimal bd = new BigDecimal(vi.getDistanceLy());
            bd = bd.setScale(4, RoundingMode.HALF_UP);
            double lyRound = bd.doubleValue();
            secondLine.add("Dist " + vi.getDistance() + " pc (" + lyRound + " ly)");
        }
        addText(infoText, secondLine.toString());
    }

    public void setGrid(Boolean showGrid) {
        this.showGrid = showGrid;
        sphericCanvas.refresh();
    }

    public void setStarsColor(Color color) {
        this.starsColor = color;
        sphericCanvas.refresh();
    }

    public void moveRasterToCenter(RasterItem rasterItem) {
        ImageToSphericCoords screenToRealCoords = rasterItem.getImageToSphericCoords();
        if (screenToRealCoords == null) {
            return;
        }

        RaDec middleRaDec = screenToRealCoords.getPixelToSpheric().toRaDec(rasterItem.getWidth() / 2.0, rasterItem.getHeight() / 2.0);

        sphericCanvas.setSpheric0(ConvTypesUtil.toSpheric(middleRaDec));
    }

    public void setCenter(Spheric spheric0) {
        sphericCanvas.setSpheric0(spheric0);
    }
    
    public void setCenterAndRefresh(Spheric spheric0) {
        sphericCanvas.setSpheric0(spheric0).refresh();
    }
    
    public void refresh() {
        sphericCanvas.refresh();
    }
    
    public double setZoom(double zoom) {
        sphericCanvas.setZoom(zoom);
        return sphericCanvas.getZoom();
    }
    
    public void showCross(boolean b) {
        this.showCross = b;
        //goCoordsButton.setEnabled(b);
        obtainCoordsButton.setEnabled(b);
        sphericCanvas.refresh();
    }

    public void setRastersTable(RastersTable rastersTable) {
        this.rastersTable = rastersTable;
    }

    public void useDegrees(boolean useDegrees) {
        this.useDegrees = useDegrees;
    }

    public void setClipboard(Clipboard clipboard) {
        this.clipboard = clipboard;
    }

    @Override
    public void acceptCorrupted(RasterItem rasterItem) {
        rastersTable.remove(rasterItem);
    }

    public RaDec getRaDec0() {
        Spheric spheric0 = sphericCanvas.getSpheric0();
        RaDec raDec0 = ConvTypesUtil.toNormalizedRaDec(spheric0);
        return raDec0;
    }

    public double getArcsecondsPerPixel() {
        return 1.7 / (sphericCanvas.getZoom() / 121064);
    }

    public SphericCanvas getSphericCanvas() {
        return sphericCanvas;
    }

    public void setPointClickedListener(FindSelectionListener pointClickedListener) {
        sphericCanvas.setFindSelectionListener(pointClickedListener);
    }

    public void setMagnitudeLimit(Double mag) {
        magnitudeLimit = mag;
        sphericCanvas.refresh();
    }

    public void setDrawShapesListener(DrawShapesListener drawShapesListener) {
        this.drawShapesListener = drawShapesListener;
    }

    public void setToolListener(ToolListener newTool) {
        sphericCanvas.setToolListener(newTool);
    }

    @Override
    public void draw(GC gc, CoordState coordState) {
        SkipOverlapLabeler labeler = new SkipOverlapLabeler();

        RasterDraw.drawRasters(gc, coordState, rasters, corruptedFileListener);

        if (showUnderAlt && geoLoc != null && julianDate != null) {
            AltDraw.drawAlt(gc, coordState, geoLoc, julianDate);
            AltDraw.drawAzLine(gc, coordState, geoLoc, julianDate);
        }

        if (sphericCanvas.getSphProjection() instanceof OrthoUniSphProjection) {
            BorderDraw.drawGreyDarkSphericBorder(gc, coordState);
        }
        
        if (showGrid) {
            SphLinesDraw.drawUniverseLines(gc, coordState, showLinesNumbers);
            
            GalacticLinesDraw.drawUniverseLines(gc, coordState, showLinesNumbers);
        }
        
        if (constellations != null) {
            ConstellationsDraw.drawConstellations(gc, coordState, constellations);
        }
        
        drawStars(gc, coordState);

        if (this.drawShapesListener != null) {
            this.drawShapesListener.drawShapes(gc, coordState, labeler);
        }
        
        if (showCross) {
            CenterCrossDraw.drawCenterCross(gc, coordState);
        }
    }
    
    private void drawStars(GC gc, CoordState coordState) {
        Color origColor = gc.getForeground();
        Color origBackground = gc.getBackground();
        gc.setBackground(starsColor);
        gc.setForeground(starsColor);
        int leftX = -5;
        int rightX = coordState.getScreenWidth() + 5;
        int topY = -5;
        int bottomY = coordState.getScreenHeight() + 5;
        for (CelestialObject vectorItem : stars) {
            RaDec coordsRaDec = vectorItem.getCoords();
            Spheric sph = ConvTypesUtil.toSpheric(coordsRaDec);
            ScreenPoint visibleScreenPoint = coordState.toScreenCoordinates(sph);
            PointInt screenCoordinates = visibleScreenPoint.point;
            if (!visibleScreenPoint.visibleHemis) {
                continue;
            }
            if (vectorItem.getMagnitude() != null) {
                double diameter = showWithDiameter(vectorItem.getMagnitude(), magnitudeLimit, coordState.getZoom());
                if (diameter != 100) {
                    if (screenCoordinates.x < leftX || screenCoordinates.y >= rightX || screenCoordinates.y < topY || screenCoordinates.y > bottomY) {
                        continue;
                    }
                    StarPointDraw.drawStar(gc, screenCoordinates.x, screenCoordinates.y, vectorItem.getMagnitude());
                }
            }
        }
        gc.setForeground(origColor);
        gc.setBackground(origBackground);
    }
    
    private static double showWithDiameter(double magnitude, Double magnitudeLimit, double zoom) {
        if (magnitudeLimit != null) {
            if (magnitude > magnitudeLimit) {
                return 100;
            }
        }
        int dia = (int) (Math.pow(14 - magnitude, 1.50) * (zoom) / 3500);
        if (dia <= 2) {
            return 100;
        }
        return magnitude;
    }
    
    
    private FoundCelestialObject findNearestCelestialObj(CoordState coordState, Spheric clickSpheric) {
        double nearestDistance = Double.MAX_VALUE;
        CelestialObject nearestVectorItem = null;
        for (CelestialObject celestObj : stars) {
            ScreenPoint screenCoordinates = coordState.toScreenCoordinates(ConvTypesUtil.toSpheric(celestObj.getCoords()));
            if (!screenCoordinates.visibleHemis) {
                continue;
            }
            Double mag = celestObj.getMagnitude();
            if (mag == null) {
                continue;
            }
            double showWithDiameter = showWithDiameter(mag, magnitudeLimit, coordState.getZoom());
//            double d = mag == null || mag == 0 ? 10 : (zoom / 7.0) / ((mag * mag));
//            if (d < 1) {
//                continue;
//            }
            if (showWithDiameter == 100) {
                continue;
            }
            double dist = GonUtil.angularDistanceDeg(ConvTypesUtil.toNormalizedRaDec(clickSpheric), celestObj.getCoords());
            if (dist < nearestDistance) {
                nearestDistance = dist;
                nearestVectorItem = celestObj;
            }
        }
        if (nearestDistance != Double.MAX_VALUE) {
            return new FoundCelestialObject(nearestVectorItem, nearestDistance) ;
        }
        return null;
    }

    public List<NearestSelectionObject> findStars(CoordState coordState, Spheric clickSpheric, double distDeg) {
        List<NearestSelectionObject> nearestStars = new ArrayList<>();
        RaDec raDec = ConvTypesUtil.toNormalizedRaDec(clickSpheric);
        for (CelestialObject celestObj : stars) {
            Double mag = celestObj.getMagnitude();
            if (mag == null) {
                continue;
            }
            double showWithDiameter = showWithDiameter(mag, magnitudeLimit, coordState.getZoom());
            if (showWithDiameter == 100) {
                continue;
            }
            double dist = GonUtil.angularDistanceDeg(raDec, celestObj.getCoords());
            if (dist < distDeg) {
                String name = celestObj.getName();
                if (name == null) {
                    name = "HIP" + celestObj.getHip();
                }
                if (celestObj.getConstellationName() != null) {
                    name += ", " + celestObj.getConstellationName();
                }
                NearestSelectionObject nearestSelectionObject = new NearestSelectionObject(NearestSelectionObject.ObjectType.STAR, celestObj, name, dist);
                nearestStars.add(nearestSelectionObject);
            }
        }
        return nearestStars;
    }

    public FoundCelestialObject onClick(int screenX, int screenY, int stateMask, CoordState coordState) {
        Spheric clickSpheric = coordState.toSpheric(screenX, screenY);
        if (clickSpheric == null) {
            return null;
        }
        FoundCelestialObject foundObj = findNearestCelestialObj(coordState, clickSpheric);
        if (foundObj != null) {
            Spheric spheric = ConvTypesUtil.toSpheric(foundObj.celestialObject.getCoords());
            ScreenPoint scrClick = coordState.toScreenCoordinates(new Spheric(spheric.lon + Math.toRadians(foundObj.distance), spheric.lat));
            int dX = scrClick.point.x - screenX;
            int dY = scrClick.point.y - screenY;
            double diffScreen = Math.sqrt(dX * dX + dY * dY);
            if (diffScreen < 6) {
                Spheric spheric0 = ConvTypesUtil.toSpheric(foundObj.celestialObject.getCoords());
                sphericCanvas.forceDraw(gc -> {
                    ScreenPoint screenCoordinates = coordState.toScreenCoordinates(spheric0);
                    gc.setForeground(crossColor);
                    gc.drawOval(screenCoordinates.point.x - 6, screenCoordinates.point.y - 6, 11, 11);
                });
                showStarInfo(foundObj.celestialObject);
                if ((stateMask & SWT.SHIFT) != 0) {
                    sphericCanvas.setSpheric0(spheric0);
                }
                return foundObj;
            }
        }
        return null;
    }

    public List<NearestSelectionObject> findRasters(int x, int y, CoordState coordState) {
        List<NearestSelectionObject> nearest = new ArrayList<>();
        for (int i = rasters.size() - 1; i >= 0; i--) {
            RasterItem rasterItem = rasters.get(i);
            if (rasterItem.getImageToSphericCoords() != null) {
                if (RasterDrawUtil.pointInRasterItem(x, y, rasterItem, coordState)) {
                    String name = rasterItem.getName();
                    if (!rasterItem.getGroup().getName().isEmpty()) {
                        name += " (" + rasterItem.getGroup().getName() + ")";
                    }
                    NearestSelectionObject nearestSelectionObject = new NearestSelectionObject(NearestSelectionObject.ObjectType.RASTER, rasterItem, name, (double) (rasters.size() - i));
                    nearest.add(nearestSelectionObject);
                }
            }
        }
        return nearest;
    }

    public void setConstellations(Collection<Constellation> constellations) {
        this.constellations = constellations;
    }

    public void clearInfo() {
        infoText.setText("");
    }
    
    public StyledText getInfoStyledText() {
        return infoText;
    }

    public void setNearestSelections(List<NearestSelectionObject> nearestSelections) {
        selectionsTable.removeAll();
        for (NearestSelectionObject nearestSelection : nearestSelections) {
            String typeStr = "";
            switch(nearestSelection.getObjectType()) {
                case STAR: typeStr = "S"; break;
                case POINT: typeStr = "P"; break;
                case CIRCLE: typeStr = "C"; break;
                case RASTER: typeStr = "R"; break;
            };

            TableItem tableItem = new TableItem(selectionsTable, SWT.NONE);
            tableItem.setText(new String[] {typeStr, nearestSelection.getText()});
            tableItem.setData(nearestSelection);
        }
        if (nearestSelections.size() > 0) {
            selectionsTable.setEnabled(true);
            selectionsTable.setSelection(0);
        } else {
            selectionsTable.setEnabled(false);
        }
    }

    public void setNearSelectionClickedListener(NearSelectionClickedListener nearSelectionClickedListener) {
        this.nearSelectionClickedListener = nearSelectionClickedListener;
    }

    public void setShowUnderAlt(boolean showUnderAlt) {
        this.showUnderAlt = showUnderAlt;
    }

    public void setGeoLoc(GeoLoc geoLoc) {
        this.geoLoc = geoLoc;
    }

    public void setJulianDate(Double julianDate) {
        this.julianDate = julianDate;
    }

    public GeoLoc getGeoLoc() {
        return geoLoc;
    }

    public Double getJulianDate() {
        return julianDate;
    }

    public static Composite marginGridComposite(Composite parent, int columns) {
        Composite composite = new Composite(parent, SWT.BORDER);
        GridLayout gridLayout = new GridLayout(columns, false);
        gridLayout.marginHeight = 2;
        gridLayout.marginWidth = 2;
        composite.setLayout(gridLayout);
        return composite;
    }

    public void saveImageToFile(String savePath) {
        Image image = sphericCanvas.getImage();
        ImageLoader saver = new ImageLoader();
        saver.data = new ImageData[] { image.getImageData() };
        saver.save(savePath, SwtImageFileUtil.getSwtImageType(savePath));
    }

    public void saveImageToFile(String savePath, int width) {
        Image image = sphericCanvas.getImage();
        ImageData imageData = image.getImageData();
        double ratio = imageData.width / (double) width;
        int height = (int) (imageData.height / ratio);
        Image resized = resize(image, width, height);
        ImageLoader saver = new ImageLoader();
        saver.data = new ImageData[] { resized.getImageData() };
        saver.save(savePath, SwtImageFileUtil.getSwtImageType(savePath));
        resized.dispose();
    }

    public static Image resize(Image image, int width, int height) {
        Image scaled = new Image(Display.getDefault(), width, height);
        GC gc = new GC(scaled);
        gc.setAntialias(SWT.ON);
        gc.setInterpolation(SWT.HIGH);
        gc.drawImage(image, 0, 0,image.getBounds().width, image.getBounds().height, 0, 0, width, height);
        gc.dispose();
        return scaled;
    }

    public void setMapBackgroundColor(Color mapBackgroundColor) {
        sphericCanvas.setBackgroundColor(mapBackgroundColor);
    }
}
