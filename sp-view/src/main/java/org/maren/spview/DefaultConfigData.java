package org.maren.spview;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;

import java.time.OffsetTime;
import java.util.HashMap;
import java.util.Map;

public class DefaultConfigData {
    public static Map<String, String> DATA = new HashMap<>();

    static {
        // ${name} must be present and must be replaced
        DATA.put("tns.last.novas.url", "https://www.wis-tns.org/search?&discovered_period_value=${period}&discovered_period_units=${periodUnits}&unclassified_at=0&classified_sne=0&include_frb=0&name=${name}&name_like=0&isTNS_AT=all&public=all&ra=&decl=&radius=&coords_unit=arcsec&reporting_groupid%5B%5D=null&groupid%5B%5D=null&classifier_groupid%5B%5D=null&objtype%5B%5D=null&at_type%5B%5D=null&date_start%5Bdate%5D=&date_end%5Bdate%5D=&discovery_mag_min=&discovery_mag_max=${discMagMax}&internal_name=&discoverer=&classifier=&spectra_count=&redshift_min=&redshift_max=&hostname=&ext_catid=&ra_range_min=&ra_range_max=&decl_range_min=&decl_range_max=&discovery_instrument%5B%5D=null&classification_instrument%5B%5D=null&associated_groups%5B%5D=null&official_discovery=0&official_classification=0&at_rep_remarks=&class_rep_remarks=&frb_repeat=all&frb_repeater_of_objid=&frb_measured_redshift=0&frb_dm_range_min=&frb_dm_range_max=&frb_rm_range_min=&frb_rm_range_max=&frb_snr_range_min=&frb_snr_range_max=&frb_flux_range_min=&frb_flux_range_max=&num_page=50&format=csv");

        DATA.put("eso.dss.fits.url", "http://archive.eso.org/dss/dss/image?ra=${ra}&dec=${dec}&equinox=J2000&name=&x=${width}&y=${height}&Sky-Survey=${survey}&mime-type=download-fits&statsmode=WEBFORM");

        DATA.put("minor.planet.url", "https://minorplanetcenter.net");

        DATA.put("time.offset", getDefaultZone());

        DATA.put("simbad.url", "https://simbad.cds.unistra.fr/simbad/sim-id?output.format=ASCII&Ident=${queryName}");
    }

    public static void populateConfig(boolean override) {
        AppConfig instance = AppConfig.getInstance();
        ConfigStore configStore = instance.getConfigStore();
        Map<String, String> data = DATA;
        for (Map.Entry<String, String> entry : data.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String cVal = configStore.getValue(key, null);
            if (override || cVal == null) {
                configStore.putValue(key, value);
            }
        }
    }

    private static String getDefaultZone() {
        return String.valueOf(OffsetTime.now().getOffset().getTotalSeconds() / 3600.0);
    }
}
