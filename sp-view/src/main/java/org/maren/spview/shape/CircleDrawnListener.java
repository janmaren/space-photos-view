package org.maren.spview.shape;

public interface CircleDrawnListener {
    void onCircleDrawn(CircleShape circleShape);
}
