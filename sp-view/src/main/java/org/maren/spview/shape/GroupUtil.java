package org.maren.spview.shape;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;

public class GroupUtil {
    public final static String DEFAULT_GROUP_NAME = "";

    public static GroupAtt createGroup(String name, Map<String, GroupAtt> groups) {
        List<RGBEntity> avoidColors = groups.entrySet().stream().map(g -> new RGBEntity(g.getValue().getColor())).collect(Collectors.toList());
        RGBEntity background1 = new RGBEntity(0, 0, 0);
        RGBEntity background2 = new RGBEntity(255, 255, 255);
        RGBEntity rgb = UniqueColorUtil.selectUniqueColor(avoidColors, 100, background1, background2);
        String color = RGBEntUtil.toRGBHexString(rgb);
        return new GroupAtt(name, color);
    }

    /**
     * Get default group when exists or create new one
     * @param groups
     */
    public static GroupAtt getOrCreateDefaultGroup(Map<String, GroupAtt> groups) {
        return getOrCreateGroup(groups, DEFAULT_GROUP_NAME);
    }

    /**
     * Get group by name when exists or create it.
     */
    public static GroupAtt getOrCreateGroup(Map<String, GroupAtt> groups, String name) {
        GroupAtt groupAtt = groups.get(name);
        if (groupAtt == null) {
            groupAtt = GroupUtil.createGroup(name, groups);
            groups.put(name, groupAtt);
        }
        return groupAtt;
    }

    public static List<String> getGroupsNames(Map<String, GroupAtt> groups) {
        return groups.entrySet().stream().map(e -> e.getValue().getName()).collect(Collectors.toList());
    }

    public static int removeUnusedGroups(Collection<? extends Shape> shapes, Map<String, GroupAtt> groups) {
        int removedCount = 0;
        Set<String> usedGroups = new HashSet<>();
        for (Shape shape : shapes) {
            String name = shape.getGroup().getName();
            usedGroups.add(name);
        }

        Iterator<Map.Entry<String, GroupAtt>> grIterator = groups.entrySet().iterator();
        while (grIterator.hasNext()) {
            Map.Entry<String, GroupAtt> next = grIterator.next();
            String groupName = next.getKey();
            if (DEFAULT_GROUP_NAME.equals(groupName)) {
                continue;
            }
            if (!usedGroups.contains(groupName)) {
                removedCount++;
                grIterator.remove();
            }
        }
        return removedCount;
    }
}
