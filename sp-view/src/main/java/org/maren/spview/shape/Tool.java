package org.maren.spview.shape;

import org.maren.gis.sphericcanvas.ToolListener;

public interface Tool<T extends Shape> extends ToolListener, ToolResultLisSetter<T> {

}
