package org.maren.spview.shape;

public class FoundShape {
    public Shape shape;
    
    /**
     * Angular distance in degrees
     */
    public double distance;

    public FoundShape(Shape shape, double distance) {
        super();
        this.shape = shape;
        this.distance = distance;
    }
}
