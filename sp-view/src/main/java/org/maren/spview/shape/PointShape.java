package org.maren.spview.shape;

import cz.jmare.math.astro.RaDec;
import org.maren.gis.type.Spheric;
import org.maren.spview.tag.TagTypeUtil;
import org.maren.swt.util.ConvTypesUtil;

import java.util.*;
import java.util.regex.Pattern;

public class PointShape extends Shape {
    /**
     * Mandatory group
     */
	private GroupAtt group;
	
	/**
	 * Optional name
	 */
	private String name;

	/**
	 * Optional Julian date
	 */
	private Double jd;
	
	/**
	 * Mandatory coordinate ra dec
	 */
	private RaDec raDec;
	
	private String description;
	
	private Map<String, Object> tags = new HashMap<>();

	
    private final static String TAGS_DELIM = ";";
    private final static String TAGS_SPLIT_REGEX = "(?<!\\\\)" + Pattern.quote(TAGS_DELIM);
    private final static String TAG_DELIM = ":";
    private final static String TAG_SPLIT_REGEX = "(?<!\\\\)" + Pattern.quote(TAG_DELIM);
    private boolean favourite;
    private boolean seen;


    public PointShape(GroupAtt group, String name, RaDec raDec) {
        super();
        if (group == null) {
            throw new IllegalArgumentException("PointShape must have a group or at least an empty group should be passed");
        }
        this.group = group;
        this.name = name;
        this.raDec = raDec;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GroupAtt getGroup() {
		return group;
	}

	public void setGroup(GroupAtt group) {
		this.group = group;
	}

    public Double getJd() {
        return jd;
    }

    public void setJd(Double jd) {
        this.jd = jd;
    }

    public RaDec getRaDec() {
        return raDec;
    }

    public void setRaDec(RaDec raDec) {
        this.raDec = raDec;
    }

	@Override
	public String toString() {
		return "PointItem [group=" + (group != null ? group.getName() : "") + ", name=" + name + ", jd=" + jd + ", raDec=" + raDec + "]";
	}

    @Override
    public Spheric getCentre() {
        return ConvTypesUtil.toSpheric(raDec);
    }

    @Override
    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    @Override
    public boolean isFavourite() {
        return favourite;
    }

    @Override
    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    @Override
    public boolean isSeen() {
        return seen;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Return used tag names
     * @return
     */
    public Set<String> getTagNames() {
        return tags.keySet();
    }
    
    public String getTagsString() {
        return formatTags(tags);
    }

    /**
     * Set tags present in the string. One tag has format key:value, multiple tags are separated with semicolon, for example "key1:value1;key2:value2"
     * Empty values aren't set. This replaces origin tags map.
     * @param tagsString
     */
    public void setTagsString(String tagsString) {
        tags = new HashMap<>();
        if (tagsString == null) {
            return;
        }
        tagsString = tagsString.trim();
        if ("".equals(tagsString)) {
            return;
        }
        Map<String, Object> parsedTags = parseTags(tagsString);
        for (Map.Entry<String, Object> entry : parsedTags.entrySet()) {
            String key = entry.getKey();
            Object val = entry.getValue();
            if (val != null) {
                tags.put(key, val);
            }
        }
    }
    
    /**
     * Add some tags or remove when empty value present
     * @param tagsString
     */
    public void applyTagsString(String tagsString) {
        if (tagsString == null) {
            return;
        }
        Map<String, Object> parsedTags = parseTags(tagsString);
        toggleTags(parsedTags, this.tags);
    }

    private static void toggleTags(Map<String, Object> map, Map<String, Object> tags) {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            Object val = entry.getValue();
            if (val == null) {
                tags.remove(key);
            } else {
                tags.put(key, val);
            }
        }
    }
    
    /**
     * Add tag with a value or remove tag when empty
     * @param name
     * @param value
     */
    public void setTag(String name, String value) {
        String key = getNormalizedKey(name);
        Object convertedValue = TagTypeUtil.parseValue(value);
        if (convertedValue == null) {
            tags.remove(key);
        } else {
            tags.put(key, convertedValue);
        }
    }

    /**
     * Get value of tag. The value may be a string or appropriate type like Double, Integer...
     * @param name uppercased tag name
     * @return
     */
    public Object getTagValue(String name) {
        return tags.get(name);
    }

    public static String getNormalizedKey(String name) {
        String key = name.trim().toUpperCase();
        if (key.contains(";") || key.contains(":")) {
            throw new IllegalStateException("Key '" + key + " musn't contain colon or semicolon");
        }
        return key;
    }

    public void removeTag(String name) {
        tags.remove(getNormalizedKey(name));
    }
    
    public static Map<String, Object> parseTags(String tagsString) {
        HashMap<String, Object> tags = new HashMap<>();
        if (tagsString == null || tagsString.isBlank()) {
            return tags;
        }
        String[] split = tagsString.split(TAGS_SPLIT_REGEX);
        for (String string : split) {
            String[] keyVal = string.split(TAG_SPLIT_REGEX);
            if (keyVal.length == 1) {
                tags.put(getNormalizedKey(keyVal[0]), null);
                continue;
            }
            if (keyVal.length > 2) {
                throw new IllegalStateException("There is invalid tag '" + string + "' with multiple colons");
            }
            String value = keyVal[1].replace("\\:", ":");
            String normalizedKey = getNormalizedKey(keyVal[0]);
            Object convertedValue = TagTypeUtil.parseValue(value);
            tags.put(normalizedKey, convertedValue);
        }
        return tags;
    }

    public static String formatTags(Map<String, Object> tags) {
        StringJoiner stringJoiner = new StringJoiner(";");
        for (Map.Entry<String, Object> entry : tags.entrySet()) {
            String key = entry.getKey().toUpperCase();
            String value = TagTypeUtil.formatValue(entry.getValue());
            value = value.replace(";", "\\;").replace(":", "\\:");
            stringJoiner.add(key + ":" + value);
        }
        return stringJoiner.toString();
    }
    
    public PointShape clone() {
        PointShape pointShape = new PointShape(group, name, raDec);
        pointShape.setDescription(description);
        pointShape.setJd(jd);
        pointShape.setTagsString(getTagsString());
        return pointShape;
    }
}
