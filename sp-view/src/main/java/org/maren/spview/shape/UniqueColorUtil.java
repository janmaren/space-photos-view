package org.maren.spview.shape;

import cz.jmare.math.raster.entity.RGBEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class UniqueColorUtil {

    public static RGBEntity selectUniqueColor(Collection<RGBEntity> avoidColors, int supportDifferentNumberCount, RGBEntity background1,
            RGBEntity background2) {
        Collection<RGBEntity> candidates = createSpreadCandidates(supportDifferentNumberCount);
        RGBEntity rgbEntity = selectUniqueColor(avoidColors, candidates, background1, background2);
        List<RGBEntity> monteCarloCandidates = createMonteCarloCandidates(1000);
        if (rgbEntity == null) {
            rgbEntity = selectUniqueColor(avoidColors, monteCarloCandidates, background1, background2);
        }
        if (rgbEntity == null) {
            rgbEntity = monteCarloCandidates.get(0);
        }
        return rgbEntity;
    }

    private static Collection<RGBEntity> createSpreadCandidates(int supportDifferentNumberCount) {
        int step = (int) (255.0 / (Math.pow(supportDifferentNumberCount, 1 / 3.0) - 1.0));
        Collection<RGBEntity> candidates = new ArrayList<>();
        for (int i = 0; i <= 255; i += step) {
            for (int j = 0; j <= 255; j += step) {
                for (int k = 0; k <= 255; k += step) {
                    candidates.add(new RGBEntity(i, j, k));
                }
            }
        }
        return candidates;
    }

    private static List<RGBEntity> createMonteCarloCandidates(int count) {
        ThreadLocalRandom current = ThreadLocalRandom.current();
        List<RGBEntity> candidates = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            candidates.add(new RGBEntity(current.nextInt(0, 256), current.nextInt(0, 256), current.nextInt(0, 256)));
        }
        return candidates;
    }

    private static RGBEntity selectUniqueColor(Collection<RGBEntity> avoidColors, Collection<RGBEntity> candidates,
                                               RGBEntity background1, RGBEntity background2) {
        RGBEntity best = null;
        double bestDev = 0;
        for (RGBEntity candidate : candidates) {
            if (calcDeviation(candidate, List.of(background1, background2)) < 200) {
                continue;
            }
            double dev = calcDeviation(candidate, avoidColors);
            if (dev > bestDev || best == null) {
                bestDev = dev;
                best = candidate;
            }
        }
        return best;
    }

    private static double calcDeviation(RGBEntity rgb1, Collection<RGBEntity> rgb2) {
        double min = Double.MAX_VALUE;
        for (RGBEntity rgb : rgb2) {
            double diffR = Math.abs(rgb.red - rgb1.red);
            double diffG = Math.abs(rgb.green - rgb1.green);
            double diffB = Math.abs(rgb.blue - rgb1.blue);
            double sum = diffR + diffG + diffB;
            if (sum < min) {
                min = sum;
            }
        }
        return min;
    }

    public static void main(String[] args) {
        Collection<RGBEntity> avoid = new ArrayList<>();
        avoid.add(new RGBEntity(48, 48, 48));
        avoid.add(new RGBEntity(242, 242, 242));
        double dev = calcDeviation(new RGBEntity(242, 242, 242), avoid);
        System.out.println(dev);
        double dev2 = calcDeviation(new RGBEntity(100, 100, 100), avoid);
        System.out.println(dev2);
//        RGBEntity rgbEntity = selectUniqueColor(avoid, 30);
//        System.out.println(rgbEntity);
    }
}
