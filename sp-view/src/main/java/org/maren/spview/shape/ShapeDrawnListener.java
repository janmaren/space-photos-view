package org.maren.spview.shape;

public interface ShapeDrawnListener<T extends Shape> {
    /**
     * Add or replace orig shape
     * @param shape replacement or new shape
     * @param origShape shape to replace or null when adding new one
     */
    void onShapeComplete(T shape, T origShape);
}
