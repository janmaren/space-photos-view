package org.maren.spview.shape;

import static java.lang.Math.PI;
import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.type.Spheric;
import org.maren.gis.util.PointInt;

import cz.jmare.data.util.GonUtil;
import cz.jmare.graphfw.labeler.Labeler;
import cz.jmare.swt.color.ColorUtil;

public class DrawUtil {
    static double PI_HALF = PI / 2.0;
    static double MINUS_PI_HALF = -PI / 2.0;

    public static void drawSphCircle(GC gc, Spheric center, double radiusRad, CoordState coordState, String title, boolean withCentre, Labeler labeler) {
        if (withCentre) {
            ScreenPoint screenCoordinates = coordState.toScreenCoordinates(center);
            if (screenCoordinates.visibleHemis) {
                PointInt point = screenCoordinates.point;
                int scrX = point.x;
                int scrY = point.y;
                gc.drawLine(scrX - 2, scrY, scrX + 2, scrY);
                gc.drawLine(scrX, scrY - 2, scrX, scrY + 2);
            }
        }

        if (radiusRad <= 0) {
            return;
        }

        double cosRadius = cos(radiusRad);
        double sinRadius = sin(radiusRad);
        double sinLat1 = sin(center.lat);
        double cosLat1 = cos(center.lat);
        double latMinTol = center.lat - 1e-10;
        double latPlusTol = center.lat + 1e-10;

        Integer lastX = null;
        Integer lastY = null;
        boolean titleDrawn = title == null || labeler == null;
        for (int courseDeg = 0; courseDeg < 361; courseDeg += 2) {
            double brng = Math.toRadians(courseDeg);
            Spheric spheric = null;
            if (latMinTol < MINUS_PI_HALF) {
                spheric = new Spheric(brng, MINUS_PI_HALF + radiusRad);
            } else if (latPlusTol > PI_HALF) {
                spheric = new Spheric(brng, PI_HALF - radiusRad);
            } else {
                double lat2 = asin(sinLat1 * cosRadius + cosLat1 * sinRadius * cos(brng));
                double lon2 = center.lon + atan2(sin(brng) * sinRadius * cosLat1, cosRadius - sinLat1 * sin(lat2));
                spheric = new Spheric(lon2, lat2);
            }

            ScreenPoint screenCoordinates = coordState.toScreenCoordinates(spheric);
            if (screenCoordinates.visibleHemis) {
                if (!titleDrawn) {
                    labeler.drawLabel(gc, title, screenCoordinates.point.x, screenCoordinates.point.y);
                    titleDrawn = true;
                }
                if (lastX != null && lastY != null) {
                    gc.drawLine(lastX, lastY, screenCoordinates.point.x, screenCoordinates.point.y);
                }
                lastX = screenCoordinates.point.x;
                lastY = screenCoordinates.point.y;
            } else {
                lastX = null;
                lastY = null;
            }
        }
    }

    public static Spheric destinationPoint(Spheric center, double radiusRad, double courseDeg) {
        double brng = Math.toRadians(courseDeg);
        if (center.lat - 1e-10 < -PI / 2.0) {
            return new Spheric(brng, -PI / 2.0 + radiusRad);
        }
        if (center.lat + 1e-10 > PI / 2.0) {
            return new Spheric(brng, PI / 2.0 - radiusRad);
        }
        double lat1 = center.lat;
        double lat2 = asin(sin(lat1) * cos(radiusRad) + cos(lat1) * sin(radiusRad) * cos(brng));
        double lon2 = center.lon + atan2(sin(brng) * sin(radiusRad) * cos(center.lat), cos(radiusRad) - sin(center.lat) * sin(lat2));
        return new Spheric(lon2, lat2);
    }

    public static void drawPoint(GC gc, Spheric center, CoordState coordState, String title, String raDecStr,
                                 Labeler labeler, boolean editing) {
        ScreenPoint screenCoordinates = coordState.toScreenCoordinates(center);
        if (!screenCoordinates.visibleHemis) {
            return;
        }
        PointInt point = screenCoordinates.point;
        if (point.x < 0 || point.y < 0 || point.x > coordState.getScreenWidth() || point.y > coordState.getScreenHeight()) {
            return;
        }
        int scrX = point.x;
        int scrY = point.y;
        gc.drawOval(scrX - 2, scrY - 2, 4, 4);
        gc.drawPoint(scrX - 1, scrY);
        gc.drawPoint(scrX + 1, scrY);
        gc.drawPoint(scrX, scrY - 1);
        gc.drawPoint(scrX, scrY + 1);
        gc.drawPoint(scrX, scrY);

        if (title != null && labeler != null) {
            int x= editing ? screenCoordinates.point.x + 4 : screenCoordinates.point.x + 1;
            int y = editing ? screenCoordinates.point.y - 11 : screenCoordinates.point.y + 1;
            if (raDecStr != null) {
                title += "\n" + raDecStr;
            }
            if (!labeler.drawLabel(gc, title, x, y)) {
                y = screenCoordinates.point.y - 11;
                if (!labeler.drawLabel(gc, title, x, y)) {
                    y = screenCoordinates.point.y + 5;
                    labeler.drawLabel(gc, title, x, y);
                }
            }
        }
    }

    public static void drawFavourite(GC gc, Spheric center, CoordState coordState) {
        ScreenPoint screenCoordinates = coordState.toScreenCoordinates(center);
        if (!screenCoordinates.visibleHemis) {
            return;
        }
        PointInt point = screenCoordinates.point;
        if (point.x < 0 || point.y < 0 || point.x > coordState.getScreenWidth() || point.y > coordState.getScreenHeight()) {
            return;
        }

        Color origForeground = gc.getForeground();
        gc.setForeground(ColorUtil.getColor("#FF8080"));

        int scrX = point.x;
        int scrY = point.y;
        gc.drawOval(scrX - 3, scrY - 3, 6, 6);
        gc.drawOval(scrX - 4, scrY - 4, 8, 8);

        gc.setForeground(origForeground);
    }

    public static void drawSeen(GC gc, Spheric center, CoordState coordState) {
        ScreenPoint screenCoordinates = coordState.toScreenCoordinates(center);
        if (!screenCoordinates.visibleHemis) {
            return;
        }
        PointInt point = screenCoordinates.point;
        if (point.x < 0 || point.y < 0 || point.x > coordState.getScreenWidth() || point.y > coordState.getScreenHeight()) {
            return;
        }

        Color origForeground = gc.getForeground();
        gc.setForeground(ColorUtil.getColor("#00ffff"));

        int scrX = point.x;
        int scrY = point.y;
        gc.drawRectangle(scrX - 4, scrY - 4, 8, 8);

        gc.setForeground(origForeground);
    }

    public static void main(String[] args) {
        for (int i = 0; i < 360; i++) {
            Spheric calc = destinationPoint(new Spheric(0, toRadians(90)), toRadians(2), i);
            System.out.println(i + ": " + GonUtil.formatDegrees(toDegrees(GonUtil.normalizeRadZeroTwoPi(calc.lon))) + ", " + GonUtil.formatDegrees(toDegrees(calc.lat)));
        }
    }
}
