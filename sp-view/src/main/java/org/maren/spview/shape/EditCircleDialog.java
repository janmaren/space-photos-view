package org.maren.spview.shape;

import java.util.Map;

import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.util.SComposite;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.GonUtil;
import cz.jmare.data.util.JulianUtil;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.swt.util.Create;
import org.maren.spview.app.RadiusByLensDialog;
import org.maren.spview.util.DateTimeUtil;

public class EditCircleDialog extends TitleAreaDialog {
    private Text groupText;
    private Text nameText;
    private Text dateText;
    private Text raDecText;
    private Text radiusText;
    private Text descText;
    
    private CircleShape circleShape;

    private CircleShape resultCircleShape;

    private Map<String, GroupAtt> groups;

    public static int DRAW_BUTTON = 3;

    public EditCircleDialog(Shell parent, CircleShape circleShape, Map<String, GroupAtt> groups) {
        super(parent);
        this.circleShape = circleShape;
        this.groups = groups;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Edit Circle");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Name:");
        nameText = Create.text(composite, circleShape.getName() != null ? circleShape.getName() : "", 150);
        
        Create.label(composite, "Group:");
        groupText = Create.text(composite, circleShape.getGroup().getName(), 150);
        
        Create.label(composite, "Date:");
        Composite dateComp = SComposite.createGridCompositeIntoGridComposite(composite, 2);
        dateText = Create.text(dateComp, circleShape.getJd() != null ? JulianUtil.formatJulianRoundSeconds(circleShape.getJd() + DateTimeUtil.getOffsetDays(), 4) : "", 200);
        Create.label(dateComp, DateTimeUtil.getOffsetHoursString());

        Create.label(composite, "RA, Dec:");
        Composite raDecComposite = SComposite.createGridCompositeIntoGridComposite(composite, 2, gl -> {}, gd -> {gd.grabExcessVerticalSpace = false;});
        raDecText = Create.text(raDecComposite, circleShape.getCentreRaDec() != null ? ACooUtil.formatRaDecProfi(circleShape.getCentreRaDec()) : "", 300);
        raDecText.setToolTipText("Coordinates RA, DEC. When unable to detect units for RA the hours are expected");
        ImageUtil.put("remove", "/image/remove.png");
        Create.imageButton(raDecComposite, ImageUtil.get("remove"), "Clear", () -> {
            raDecText.setText("");});
        
        Create.label(composite, "Radius:");
        Composite radiusComposite = SComposite.createGridCompositeIntoGridComposite(composite, 3, gl -> {}, gd -> {gd.grabExcessVerticalSpace = false;});
        radiusText = Create.text(radiusComposite, circleShape.getRadiusDeg() != null ? GonUtil.formatDegrees(circleShape.getRadiusDeg(), false, ":", 0, 10) : "", 200);
        radiusText.setToolTipText("Radius in angular degrees");
        Create.imageButton(radiusComposite, ImageUtil.get("remove"), "Clear", () -> {
            radiusText.setText("");});
        ImageUtil.put("lens", "/image/lens.png");
        Create.imageButton(radiusComposite, ImageUtil.get("lens"), "Calculate radius by focal length and FOV", () -> {
            RadiusByLensDialog radiusByLensDialog = new RadiusByLensDialog(getShell());
            if (radiusByLensDialog.open() == Window.OK) {
                double resultRadius = radiusByLensDialog.getResultRadius();
                radiusText.setText(String.valueOf(resultRadius));
            }
        });

        Create.label(composite, "Description:");
        descText = Create.text(composite, circleShape.getDescription() != null ? circleShape.getDescription() : "", 300);
        descText.setToolTipText("A description about object");

        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(540, 400);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid(buttonId)) != null) {
            setErrorMessage(message);
            return; 
        }
        String grp = groupText.getText();
        GroupAtt pointSet = GroupUtil.getOrCreateDefaultGroup(groups);
        if (!grp.isBlank()) {
            pointSet = GroupUtil.getOrCreateGroup(groups, grp);
        }
        resultCircleShape = buttonId == DRAW_BUTTON ? new CircleShape(pointSet) : circleShape;
        resultCircleShape.setGroup(pointSet);
        resultCircleShape.setName(nameText.getText());
        if (!dateText.getText().trim().isEmpty()) {
            String dateStr = dateText.getText().trim();
			Double jd = null;
			try {
				jd = Double.valueOf(dateStr);
			} catch (NumberFormatException e) {
				jd = JulianUtil.parseToJulian(dateStr) - DateTimeUtil.getOffsetDays();
			}
            resultCircleShape.setJd(jd);
        }

        if (!raDecText.getText().isBlank()) {
            resultCircleShape.setCentre(ACooUtil.parseCoordsNormalized(raDecText.getText().trim(), false));
        }
        if (!radiusText.getText().isBlank()) {
            resultCircleShape.setRadiusDeg(GonUtil.parseDegrees(radiusText.getText()));
        }
        resultCircleShape.setDescription(descText.getText().isBlank() ? null : descText.getText().trim());

        if (buttonId == DRAW_BUTTON) {
            setReturnCode(DRAW_BUTTON);
            close();
            return;
        }
        super.buttonPressed(buttonId);
    }
    
    protected String valid(int buttonId) {
        String dateStr = dateText.getText().trim();
        if (!dateStr.isBlank()) {
			try {
				Double.valueOf(dateStr);
			} catch (NumberFormatException e) {
				try {
				    JulianUtil.parseToJulian(dateStr);
				} catch (Exception e1) {
					return e1.getMessage() != null ? e1.getMessage() : "wrong date";
				}
			}
        }
        if (!raDecText.getText().isBlank()) {
            try {
                ACooUtil.parseCoordsNormalized(raDecText.getText().trim(), false);
            } catch (Exception e) {
                return ExceptionMessage.getCombinedMessage("Invalid coordinates RA, Dec", e);
            }
        } else {
            if (buttonId != DRAW_BUTTON) {
                return "No RA Dec coodinates entered";
            }
        }

        if (!radiusText.getText().isBlank()) {
            try {
                GonUtil.parseDegrees(radiusText.getText().trim());
            } catch (Exception e) {
                return ExceptionMessage.getCombinedMessage("Wrong Radius", e);
            }
        } else {
            if (buttonId != DRAW_BUTTON) {
                return "radius is blank";
            }
        }
        if (nameText.getText().contains("\"")) {
            return "name musn't contain the \" character";
        }
        if (buttonId == DRAW_BUTTON && !raDecText.getText().isBlank() && !radiusText.getText().isBlank()) {
            return "Coordinates or radius must be blank";
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
        createButton(parent, DRAW_BUTTON, "Draw", false);
    }

    public CircleShape getResultShape() {
        return circleShape;
    }

    public CircleShape getResultCircleShape() {
        return resultCircleShape;
    }
}
