package org.maren.spview.shape;

import org.maren.gis.type.Spheric;

public abstract class Shape {

    public transient boolean enabled;
    public transient boolean selected;

    public abstract GroupAtt getGroup();

    public abstract void setGroup(GroupAtt group);

    public abstract Spheric getCentre();

    public abstract void setFavourite(boolean favourite);

    public abstract boolean isFavourite();

    public abstract void setSeen(boolean seen);

    public abstract boolean isSeen();
}
