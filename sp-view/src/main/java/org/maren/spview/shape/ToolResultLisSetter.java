package org.maren.spview.shape;

public interface ToolResultLisSetter<T extends Shape> {
    void setDrawListener(ShapeDrawnListener<T> shapeDrawnListener);
}
