package org.maren.spview.shape;

import cz.jmare.graphfw.labeler.Labeler;
import cz.jmare.graphfw.labeler.OverlapLabeler;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;
import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.type.Spheric;
import org.maren.swt.util.ConvTypesUtil;

import cz.jmare.data.util.GonUtil;

public class CircleTool implements Tool<CircleShape> {
    private CircleShape circleShape;
    private CircleShape origShape;
    private ShapeDrawnListener<CircleShape> shapeDrawnListener;

    private static Labeler labeler = new OverlapLabeler();

    public CircleTool(CircleShape circleShape, CircleShape origShape) {
        super();
        this.circleShape = circleShape;
        this.origShape = origShape;
    }
    
    @Override
    public void mouseUpEvent(CoordState coordState, int screenX, int screenY, int stateMask, int button) {
    	Spheric spheric = coordState.toSpheric(screenX, screenY);
        if (circleShape.centre == null) {
            circleShape.setCentre(ConvTypesUtil.toNormalizedRaDec(spheric));
        } else {
            double radiusDeg = GonUtil.angularDistanceDeg(circleShape.centre, ConvTypesUtil.toNormalizedRaDec(spheric));
            circleShape.setRadiusDeg(radiusDeg);
        }
        if (circleShape.centre != null && circleShape.getRadiusDeg() != null) {
            shapeDrawnListener.onShapeComplete(circleShape, origShape);
        }
    }

	@Override
	public void mouseDownEvent(CoordState coordState, int screenX, int screenY, int stateMask, int button) {
	}
	
    @Override
    public void drawEvent(GC gc, CoordState coordState, int screenX, int screenY) {
        Spheric centre = null;
        if (circleShape.centre != null) {
            centre = ConvTypesUtil.toSpheric(circleShape.centre);
        } else {
            centre = coordState.toSpheric(screenX, screenY);
        }
        
        if (centre == null) {
            return;
        }
        
        ScreenPoint centerPoint = coordState.toScreenCoordinates(centre);
        Color foreground = gc.getForeground();
        gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
        Spheric polarCoordinates = coordState.toSpheric(screenX, screenY);
        if (centerPoint.visibleHemis && polarCoordinates != null) {
            double angularDistance = circleShape.getRadiusDeg() != null ? Math.toRadians(circleShape.getRadiusDeg()) : ConvTypesUtil.angularDistanceRad(centre, polarCoordinates);
            FontRegistry fontRegistry = JFaceResources.getFontRegistry();
            Font origFont = gc.getFont();
            gc.setFont(fontRegistry.get(coordState.getZoom() < 250 ? "smallest-font" : "smaller-font"));
            DrawUtil.drawSphCircle(gc, centre, angularDistance, coordState, circleShape.getName(), true, labeler);
            gc.setFont(origFont);
        }
        gc.setForeground(foreground);
    }

    @Override
    public void setDrawListener(ShapeDrawnListener<CircleShape> shapeDrawnListener) {
        this.shapeDrawnListener = shapeDrawnListener;
    }
}