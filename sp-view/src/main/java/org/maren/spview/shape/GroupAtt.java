package org.maren.spview.shape;

import java.util.HashMap;

public class GroupAtt {
    private String name;
    
    private String color;

    public static final GroupAtt EMPTY_GROUP = GroupUtil.createGroup("<EMPTY_GROUP>", new HashMap<>());

    public GroupAtt(String name, String color) {
        this.name = name;
		this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
