package org.maren.spview.shape;

import cz.jmare.graphfw.labeler.Labeler;
import cz.jmare.graphfw.labeler.OverlapLabeler;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.type.Spheric;
import org.maren.swt.util.ConvTypesUtil;

public class PointTool implements Tool<PointShape> {

    private ShapeDrawnListener<PointShape> shapeDrawnListener;
    private PointShape pointShape;
    private PointShape origShape;

    private static Labeler labeler = new OverlapLabeler();

    public PointTool(PointShape pointShape, PointShape origShape) {
        this.pointShape = pointShape;
        this.origShape = origShape;
    }

    @Override
    public void mouseUpEvent(CoordState coordState, int screenX, int screenY, int stateMask, int button) {
    	Spheric spheric = coordState.toSpheric(screenX, screenY);
        this.pointShape.setRaDec(ConvTypesUtil.toNormalizedRaDec(spheric));
        shapeDrawnListener.onShapeComplete(pointShape, origShape);
    }
    
	@Override
	public void mouseDownEvent(CoordState coordState, int screenX, int screenY, int stateMask, int button) {
	}
	
    @Override
    public void drawEvent(GC gc, CoordState coordState, int screenX, int screenY) {
        Spheric spheric = coordState.toSpheric(screenX, screenY);
        if (spheric == null) {
            return;
        }
        Color foreground = gc.getForeground();
        gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
        Font origFont = gc.getFont();
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        gc.setFont(fontRegistry.get(coordState.getZoom() < 250 ? "smallest-font" : "smaller-font"));
        DrawUtil.drawPoint(gc, spheric, coordState, pointShape.getName(), null, labeler, true);
        gc.setFont(origFont);
        gc.setForeground(foreground);
    }

    @Override
    public void setDrawListener(ShapeDrawnListener<PointShape> shapeDrawnListener) {
        this.shapeDrawnListener = shapeDrawnListener;
    }
}
