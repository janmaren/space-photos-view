package org.maren.spview.shape;

import org.maren.gis.type.Spheric;
import org.maren.swt.util.ConvTypesUtil;

import cz.jmare.math.astro.RaDec;

public class CircleShape extends Shape {

    /**
     * Mandatory group
     */
    private GroupAtt group;
    
    public RaDec centre;
    
    private Double radiusDeg;
    
    private String name;
    /**
     * Optional Julian date
     */
    private Double jd;
    
    private String description;
    private boolean favourite;
    private boolean seen;

    public CircleShape(GroupAtt group) {
        super();
        this.group = group;
    }

    public CircleShape(GroupAtt group, RaDec centre, Double radiusDeg) {
        super();
        this.group = group;
        this.centre = centre;
        this.radiusDeg = radiusDeg;
    }

    @Override
    public GroupAtt getGroup() {
        return group;
    }

    @Override
    public void setGroup(GroupAtt group) {
        this.group = group;
    }

    @Override
    public Spheric getCentre() {
        return ConvTypesUtil.toSpheric(centre);
    }

    @Override
    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    @Override
    public boolean isFavourite() {
        return favourite;
    }

    @Override
    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    @Override
    public boolean isSeen() {
        return seen;
    }

    public RaDec getCentreRaDec() {
        return centre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getJd() {
        return jd;
    }

    public void setJd(Double jd) {
        this.jd = jd;
    }

    public Double getRadiusDeg() {
        return radiusDeg;
    }

    public void setRadiusDeg(Double radiusDeg) {
        this.radiusDeg = radiusDeg;
    }

    public void setCentre(RaDec centre) {
        this.centre = centre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
