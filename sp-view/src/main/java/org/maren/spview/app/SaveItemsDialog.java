package org.maren.spview.app;

import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import cz.jmare.swt.util.Create;

public class SaveItemsDialog extends TitleAreaDialog {
    private Combo groupText;
    
    private Button groupButton;
    private Button onlyEnabledButton;
    private Button onlySelectedButton;
    
    private boolean resultOnlyEnabled;
    
    private boolean resultOnlySelected;
    
    private String resultOnlyGroup;
    private List<String> groupsNames;
    private Button onlyFavouritesButton;
    private Button onlySeenButton;
    private boolean resultOnlyFavourites;
    private boolean resultOnlySeen;

    public SaveItemsDialog(Shell parent, List<String> groupsNames) {
        super(parent);
        this.groupsNames = groupsNames;
    }

    protected Control createDialogArea(Composite parent) {
        setMessage("Saving data to CSV. Saving all items unless an 'only' checkbox turned on");
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Saving");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        groupButton = Create.checkButton(composite, false, "Only group");
        groupText = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
        groupText.setItems(groupsNames.toArray(new String[groupsNames.size()]));
        groupText.setEnabled(false);
        groupButton.addSelectionListener(SelectionListener.widgetSelectedAdapter((e) -> {
            groupText.setEnabled(groupButton.getSelection());
        }));
        GridData grLayData = new GridData();
        grLayData.widthHint = 150;
        groupText.setLayoutData(grLayData);
        
        onlyEnabledButton = Create.checkButton(composite, false, "Only enabled items");
        Create.label(composite);
        
        onlySelectedButton = Create.checkButton(composite, false, "Only selected items");
        Create.label(composite);

        onlyFavouritesButton = Create.checkButton(composite, false, "Only favourite items");
        Create.label(composite);

        onlySeenButton = Create.checkButton(composite, false, "Only seen items");
        Create.label(composite);
        
        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(500, 380);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }
        
        resultOnlyEnabled = onlyEnabledButton.getSelection();
        
        resultOnlySelected = onlySelectedButton.getSelection();

        resultOnlyFavourites = onlyFavouritesButton.getSelection();

        resultOnlySeen = onlySeenButton.getSelection();
        
        if (groupButton.getSelection()) {
            resultOnlyGroup = groupText.getText();
        }

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public boolean isResultOnlyEnabled() {
        return resultOnlyEnabled;
    }

    public boolean isResultOnlySelected() {
        return resultOnlySelected;
    }

    public String getResultOnlyGroup() {
        return resultOnlyGroup;
    }

    public boolean isResultOnlyFavourites() {
        return resultOnlyFavourites;
    }

    public boolean isResultOnlySeen() {
        return resultOnlySeen;
    }
}
