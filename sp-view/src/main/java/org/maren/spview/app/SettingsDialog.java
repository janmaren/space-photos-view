package org.maren.spview.app;

import static org.maren.spview.ConfigKeys.KEY_CENTER_ADDED_RASTER;
import static org.maren.spview.ConfigKeys.KEY_MAP_BACKGROUND_COLOR;
import static org.maren.spview.ConfigKeys.KEY_SHOW_LEGEND;

import java.io.File;
import java.time.OffsetTime;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.maren.spview.ConfigKeys;
import org.maren.spview.util.AppDirsUtil;
import org.maren.spview.util.DateTimeUtil;
import org.maren.stacking.util.DcrawConverter;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.data.util.GlocUtil;
import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.math.geo.GeoLoc;
import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;
import cz.jmare.swt.color.ColorManager;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.SComposite;

public class SettingsDialog extends TitleAreaDialog {
    private String resultProxyHost;

    private Integer resultProxyPort;

    private Text proxyHostText;
    
    private Text proxyPortText;

    private Text downloadFitsDirText;

    private Text geoLocText;

    private String initialMessage = "Settings like proxy to be able download rasters, csv from a place behind proxy...";
    private FocusField focusField;
    private Combo zoneCombo;

    private Button centerRaster;
    private Text astrometryApiKeyText;

    private Text mapBackgroundColor;
    private Button showLegend;

    public SettingsDialog(Shell parent) {
        super(parent);
    }

    protected Control createDialogArea(Composite parent) {
        setMessage(initialMessage);

        AppConfig appConfig = AppConfig.getInstance();
        ConfigStore configStore = appConfig.getConfigStore();
        
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Settings");

        ImageUtil.put("color", "/image/color-dial.png");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Geo location:");
        String geoLoc = configStore.getValue(ConfigKeys.GEO_LOCATION, "");
        geoLocText = Create.text(composite, geoLoc, t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 180;
            t.setLayoutData(layoutData);
        });
        geoLocText.setToolTipText("Geographics coordinates, like\n40.71427, -74.00597\n40.71427N 74.00597W\n40 42N  74 00W");
        if (focusField == FocusField.GEO_LOC) {
            geoLocText.setFocus();
        }

        double timeOffset = configStore.getDoubleValue("time.offset", 1000.0);
        if (timeOffset == 1000) {
            timeOffset = OffsetTime.now().getOffset().getTotalSeconds() / 3600.0;
        }
        Create.label(composite, "Zone offset:");
        zoneCombo = Create.combo(composite, SWT.BORDER, c -> {});
        zoneCombo.setItems("-11", "-10", "-9", "-8", "-7", "-6", "-5", "-4", "-3", "-2", "-1", "0", "+1", "+2", "+3", "+4", "+5", "+6", "+7", "+8", "+9", "+10", "+11", "+12", "+13", "+14");
        int roundZone = (int) timeOffset;
        String offsetString = (timeOffset > 0 ? "+" : "");
        if (roundZone == timeOffset) {
            offsetString += roundZone;
        } else {
            offsetString += timeOffset;
        }
        zoneCombo.setText(offsetString);
        zoneCombo.setToolTipText( "negative is west,\n0 is UTC,\npositive is east\nadd 1 hour for daylight saving time");

        Create.label(composite, "Proxy host:");
        proxyHostText = Create.text(composite, String.valueOf(configStore.getValue("proxy.host", "")), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 180;
            t.setLayoutData(layoutData);
        });

        Create.label(composite, "Proxy port:");
        Integer portValue = configStore.getIntValue("proxy.port", null);
        proxyPortText = Create.text(composite, portValue == null ? "" : String.valueOf(portValue), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 100;
            t.setLayoutData(layoutData);
        });

        Create.label(composite, "Center added raster:");
        centerRaster = Create.checkButton(composite, configStore.getBoolValue(KEY_CENTER_ADDED_RASTER, false));
        centerRaster.setToolTipText("When new raster is added then center it");

        Create.label(composite, "Download fits directory:");
        downloadFitsDirText = Create.text(composite, String.valueOf(configStore.getValue("download.fits.directory", AppDirsUtil.getFitsDownloadPath())), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 300;
            t.setLayoutData(layoutData);
            t.setToolTipText("Path to dcraw program on disk. It helps to read formats as NEF or CR2");
        });

        Create.label(composite, "Astrometry API key:");
        astrometryApiKeyText = Create.text(composite, String.valueOf(configStore.getValue("astrometry.api.key", "")), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 200;
            t.setLayoutData(layoutData);
            t.setToolTipText("API key as described on https://nova.astrometry.net/api_help");
        });

        Create.label(composite, "Map background color:");
        Composite mapColComp = SComposite.createGridCompositeIntoGridComposite(composite, 2);
        String background = configStore.getValue(KEY_MAP_BACKGROUND_COLOR, "");
        mapBackgroundColor = Create.text(mapColComp, background == null ? "" : background, t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 100;
            t.setLayoutData(layoutData);
        });
        Create.imageButton(mapColComp, ImageUtil.get("color"), "Pick color", () -> pickColor());

        Create.label(composite, "Show legend:");
        showLegend = Create.checkButton(composite, configStore.getBoolValue(KEY_SHOW_LEGEND, true));
        showLegend.setToolTipText("Show legend on the map. Useful to disable for printing");

        return composite;
    }

    public void pickColor() {
        ColorDialog colorDialog = new ColorDialog(getShell());
        colorDialog.setText("Pick map background color");

        try {
            RGBEntity rgbEntity = new RGBEntity(mapBackgroundColor.getText());
            colorDialog.setRGB(new RGB(rgbEntity.red, rgbEntity.green, rgbEntity.blue));
        } catch (Exception e) {
        }

        RGB newRgb = colorDialog.open();
        if (newRgb != null) {
            String color = RGBEntUtil.toRGBHexString(new RGBEntity(newRgb.red, newRgb.green, newRgb.blue));
            mapBackgroundColor.setText(color);
        }
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(550, 550);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return;
        }

        AppConfig appConfig = AppConfig.getInstance();
        ConfigStore configStore = appConfig.getConfigStore();

        if (!geoLocText.getText().isEmpty()) {
            try {
                GeoLoc geoLoc = GlocUtil.parseCoordinatesNormalized(geoLocText.getText().trim());
                configStore.putValue(ConfigKeys.GEO_LOCATION, GlocUtil.formatCoordsProfi(geoLoc));
            } catch (Exception e) {
            }
        } else {
            configStore.putValue(ConfigKeys.GEO_LOCATION, null);
        }

        double zone = Double.parseDouble(zoneCombo.getText().trim());
        configStore.putDoubleValue("time.offset", zone);
        DateTimeUtil.setOffsetHours(zone);

        resultProxyHost = proxyHostText.getText().trim();
        resultProxyPort = null;
        if (!proxyPortText.getText().isBlank()) {
            resultProxyPort = Integer.valueOf(proxyPortText.getText().trim());
        }

        configStore.putValue("proxy.host", resultProxyHost.isEmpty() ? null : resultProxyHost);
        configStore.putIntValue("proxy.port", resultProxyPort);
        configStore.putValue("download.fits.directory", downloadFitsDirText.getText());
        if (!downloadFitsDirText.getText().isEmpty()) {
            if (!RasImageFactory.isConverterRegistered(DcrawConverter.class)) {
                RasImageFactory.registerConverter(new DcrawConverter());
            }
        }
        configStore.putValue("astrometry.api.key", astrometryApiKeyText.getText().isEmpty() ? null : astrometryApiKeyText.getText().trim());

        configStore.putBoolValue(KEY_CENTER_ADDED_RASTER, centerRaster.getSelection());

        if (!mapBackgroundColor.getText().isEmpty()) {
            configStore.putValue(KEY_MAP_BACKGROUND_COLOR, mapBackgroundColor.getText().trim());
        } else {
            configStore.remove(KEY_MAP_BACKGROUND_COLOR);
        }

        configStore.putBoolValue(KEY_SHOW_LEGEND, showLegend.getSelection());

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        if (!geoLocText.getText().isBlank()) {
            try {
                GlocUtil.parseCoordinatesNormalized(geoLocText.getText().trim());
            } catch (Exception e) {
                return "Invalid format of geo coordinates";
            }
        }
        if (zoneCombo.getText().isBlank()) {
            return "No zone populated";
        }
        try {
            Double.parseDouble(zoneCombo.getText().trim());
        } catch (NumberFormatException e) {
            return "Invalid zone number";
        }

        try {
            if (!proxyPortText.getText().isBlank()) {
                Integer.valueOf(proxyPortText.getText().trim());
            }
        } catch (NumberFormatException e) {
            return "Invalid proxy port";
        }
        if (!proxyHostText.getText().trim().isEmpty() && proxyPortText.getText().trim().isEmpty()) {
            return "When proxy host populated the port must be also populated";
        }
        if (!proxyPortText.getText().trim().isEmpty() && proxyHostText.getText().trim().isEmpty()) {
            return "When proxy port populated the host must be also populated";
        }
        if (!downloadFitsDirText.getText().isEmpty()) {
            if (!new File(downloadFitsDirText.getText().trim()).isDirectory()) {
                return "Path " + downloadFitsDirText.getText() + " doesn't exist";
            }
        }
        if (!mapBackgroundColor.getText().isEmpty()) {
            if (!mapBackgroundColor.getText().startsWith("#")) {
                return "color should be in format #RRGGBB";
            }
            try {
                ColorManager.getValidColor(mapBackgroundColor.getText().trim());
            } catch (Exception e) {
                return "Background map color is wrong";
            }
        }

        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public String getResultProxyHost() {
        return resultProxyHost;
    }

    public Integer getResultProxyPort() {
        return resultProxyPort;
    }

    public void setInitialMessage(String initialMessage) {
        this.initialMessage = initialMessage;
    }

    public void setFocusField(FocusField focusField) {
        this.focusField = focusField;
    }

    public static enum FocusField {
        GEO_LOC
    }
}
