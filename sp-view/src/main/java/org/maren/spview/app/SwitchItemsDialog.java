package org.maren.spview.app;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import cz.jmare.swt.util.SComposite;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.maren.spview.shape.GroupAtt;

import cz.jmare.swt.util.Create;
import org.maren.spview.shape.GroupUtil;

public class SwitchItemsDialog extends TitleAreaDialog {
    private Combo groupText;
    
    private Button groupButton;
    private Button onlyEnabledButton;
    private Button onlySelectedButton;
    
    private Map<String, GroupAtt> groups;

    private boolean resultOnlyEnabled;
    
    private boolean resultOnlySelected;
    
    private GroupAtt resultOnlyGroup;

    private Button selectCheck;

    private Button enableCheck;
    
    private Button disableCheck;
    
    private boolean resultSelect;
    
    private boolean resultEnable;
    
    private boolean resultDisable;

    Map<String, Set<String>> tagNameValues;

    private Button allBesidesButton;

    private boolean resultGroupBeside;

    private Button removeButton;

    private boolean resultRemove;
    private Button onlyFavouritesButton;
    private Button onlySeenButton;
    private boolean resultOnlyFavourites;
    private boolean resultOnlySeen;

    private boolean resultMarkFavourite;

    private boolean resultUnmarkFavourite;

    private boolean resultMarkSeen;

    private boolean resultUnmarkSeen;
    private Button unmarkFavouriteCheck;
    private Button markSeenCheck;
    private Button unmarkSeenCheck;

    private Button updateGroupButton;
    private Combo updateGroupText;

    private String resultUpdateGroup;
    private Button matchTagButton;
    private Combo matchTagNameCombo;
    private Combo matchTagValueCombo;

    private String resultTagName;

    private String resultTagValue;
    private Button allBesidesTagButton;

    private boolean besidesTag;

    public SwitchItemsDialog(Shell parent, Map<String, GroupAtt> groups, Map<String, Set<String>> tagNameValues) {
        super(parent);
        this.groups = groups;
        this.tagNameValues = tagNameValues;
    }

    private Button markFavouriteCheck;

    protected Control createDialogArea(Composite parent) {
        setMessage("For given criteria select or enable rows");
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Multiple Selection and Enabling");

        /* --- area --- */
        final Composite supComposite = new Composite(titleAreaComposite, SWT.NULL);
        GridLayout layout1 = new GridLayout();
        supComposite.setLayout(layout1);

        final Group critComposite = new Group(supComposite, SWT.NULL);
        critComposite.setText("Criteria");

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        critComposite.setLayout(layout);

        groupButton = Create.checkButton(critComposite, false, "Only group");

        Composite groupComp = SComposite.createGridCompositeIntoGridComposite(critComposite, 2);
        groupText = new Combo(groupComp, SWT.BORDER | SWT.READ_ONLY);
        List<String> itemsList = GroupUtil.getGroupsNames(groups);
        Collections.sort(itemsList);
        groupText.setItems(itemsList.toArray(new String[itemsList.size()]));
        groupText.setEnabled(false);
        groupButton.addSelectionListener(SelectionListener.widgetSelectedAdapter((e) -> {
            groupText.setEnabled(groupButton.getSelection());
            allBesidesButton.setEnabled(groupButton.getSelection());
        }));
        GridData grLayData = new GridData();
        grLayData.widthHint = 150;
        groupText.setLayoutData(grLayData);

        allBesidesButton = Create.checkButton(groupComp, false, "All besides");
        allBesidesButton.setToolTipText("When enabled then the negation applied - action for all groups besides the selected one");

        matchTagButton = Create.checkButton(critComposite, false, "Match tag");
        Composite tagComp = SComposite.createGridCompositeIntoGridComposite(critComposite, 3);
        matchTagNameCombo = Create.combo(tagComp, SWT.BORDER, c -> {c.setToolTipText("Tag name");});
        matchTagNameCombo.setEnabled(false);
        GridData grTagnData = new GridData();
        grTagnData.widthHint = 80;
        matchTagNameCombo.setLayoutData(grTagnData);
        matchTagValueCombo = Create.combo(tagComp, SWT.BORDER, c -> {c.setToolTipText("Tag value");});
        matchTagValueCombo.setEnabled(false);
        matchTagButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            matchTagNameCombo.setEnabled(matchTagButton.getSelection());
            matchTagValueCombo.setEnabled(matchTagButton.getSelection());
            allBesidesTagButton.setEnabled(matchTagButton.getSelection());
        }));
        List<String> tagNamesList = tagNameValues.entrySet().stream().map((e) -> e.getKey()).collect(Collectors.toList());
        matchTagNameCombo.setItems(tagNamesList.toArray(new String[tagNamesList.size()]));
        matchTagNameCombo.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            Set<String> strings = tagNameValues.get(matchTagNameCombo.getText().trim());
            matchTagValueCombo.setItems(strings.toArray(new String[strings.size()]));
        }));
        allBesidesTagButton = Create.checkButton(tagComp, false, "All besides");
        allBesidesTagButton.setToolTipText("When enabled then the negation applied - action for all tags besides the selected one");
        allBesidesTagButton.setEnabled(false);

        onlyEnabledButton = Create.checkButton(critComposite, false, "Only enabled items");
        Create.label(critComposite);
        
        onlySelectedButton = Create.checkButton(critComposite, false, "Only selected items");
        Create.label(critComposite);

        onlyFavouritesButton = Create.checkButton(critComposite, false, "Only favourites");
        Create.label(critComposite);

        onlySeenButton = Create.checkButton(critComposite, false, "Only seen");
        Create.label(critComposite);


        final Group actComposite = new Group(supComposite, SWT.NULL);
        actComposite.setText("Action");
        actComposite.setLayout(layout);

        selectCheck = Create.checkButton(actComposite, false, "Select");
        Create.label(actComposite);
        
        enableCheck = Create.checkButton(actComposite, false, "Enable", b -> {if (b) {disableCheck.setEnabled(false) ; disableCheck.setSelection(false);} else {disableCheck.setEnabled(true);}});
        Create.label(actComposite);

        disableCheck = Create.checkButton(actComposite, false, "Disable", b -> {if (b) {enableCheck.setEnabled(false) ; enableCheck.setSelection(false);} else {enableCheck.setEnabled(true);}});
        Create.label(actComposite);

        removeButton = Create.checkButton(actComposite, false, "Remove", b -> {if (b) {enableCheck.setEnabled(false) ; enableCheck.setSelection(false); selectCheck.setEnabled(false);disableCheck.setEnabled(false);} else {enableCheck.setEnabled(true);selectCheck.setEnabled(true);disableCheck.setEnabled(true);}});
        Create.label(actComposite);

        markFavouriteCheck = Create.checkButton(actComposite, false, "Mark favourite");
        Create.label(actComposite);

        unmarkFavouriteCheck = Create.checkButton(actComposite, false, "Unmark favourite");
        Create.label(actComposite);

        markSeenCheck = Create.checkButton(actComposite, false, "Mark seen");
        Create.label(actComposite);

        unmarkSeenCheck = Create.checkButton(actComposite, false, "Unmark seen");
        Create.label(actComposite);

        Composite sgroupComp = SComposite.createGridCompositeIntoGridComposite(actComposite, 2);
        updateGroupButton = Create.checkButton(sgroupComp, false, "Set group");
        updateGroupText = new Combo(sgroupComp, SWT.BORDER);
        Collections.sort(itemsList);
        updateGroupText.setItems(itemsList.toArray(new String[itemsList.size()]));
        updateGroupText.setEnabled(false);
        updateGroupButton.addSelectionListener(SelectionListener.widgetSelectedAdapter((e) -> {
            updateGroupText.setEnabled(updateGroupButton.getSelection());
        }));
        updateGroupText.setLayoutData(grLayData);

        return supComposite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(540, 660);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }
        
        resultOnlyEnabled = onlyEnabledButton.getSelection();
        
        resultOnlySelected = onlySelectedButton.getSelection();

        resultOnlyFavourites = onlyFavouritesButton.getSelection();

        resultOnlySeen = onlySeenButton.getSelection();
        
        if (groupButton.getSelection()) {
            resultOnlyGroup = groups.get(groupText.getText());
        }
        
        resultSelect = selectCheck.getSelection();
        
        if (enableCheck.getEnabled()) {
            resultEnable = enableCheck.getSelection();
        }
        
        if (disableCheck.getEnabled()) {
            resultDisable = disableCheck.getSelection();
        }
        
        resultGroupBeside = allBesidesButton.getSelection();

        resultRemove = removeButton.getSelection();

        resultMarkFavourite = markFavouriteCheck.getSelection();

        resultUnmarkFavourite = unmarkFavouriteCheck.getSelection();

        resultMarkSeen = markSeenCheck.getSelection();

        resultUnmarkSeen = unmarkSeenCheck.getSelection();

        if (updateGroupButton.getSelection()) {
            resultUpdateGroup = updateGroupText.getText().trim();
        }

        if (matchTagButton.getSelection()) {
            resultTagName = matchTagNameCombo.getText().trim();
            resultTagValue = matchTagValueCombo.getText().trim();
            besidesTag = allBesidesTagButton.getSelection();
        }

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        if (!enableCheck.getSelection() && !disableCheck.getSelection() && !selectCheck.getSelection() && !removeButton.getSelection() && !markFavouriteCheck.getSelection() && !unmarkFavouriteCheck.getSelection() && !markSeenCheck.getSelection() && !unmarkSeenCheck.getSelection() && !updateGroupButton.getSelection()) {
            return "No action selected";
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public boolean isResultOnlyEnabled() {
        return resultOnlyEnabled;
    }

    public boolean isResultOnlySelected() {
        return resultOnlySelected;
    }

    public GroupAtt getResultOnlyGroup() {
        return resultOnlyGroup;
    }

    public boolean isResultSelect() {
        return resultSelect;
    }

    public boolean isResultEnable() {
        return resultEnable;
    }

    public boolean isResultDisable() {
        return resultDisable;
    }

    public boolean isResultGroupBeside() {
        return resultGroupBeside;
    }

    public boolean isResultRemove() {
        return resultRemove;
    }

    public boolean isResultOnlyFavourites() {
        return resultOnlyFavourites;
    }

    public boolean isResultOnlySeen() {
        return resultOnlySeen;
    }

    public boolean isResultMarkFavourite() {
        return resultMarkFavourite;
    }

    public boolean isResultUnmarkFavourite() {
        return resultUnmarkFavourite;
    }

    public boolean isResultMarkSeen() {
        return resultMarkSeen;
    }

    public boolean isResultUnmarkSeen() {
        return resultUnmarkSeen;
    }

    public String getResultUpdateGroup() {
        return resultUpdateGroup;
    }

    public String getResultTagName() {
        return resultTagName;
    }

    public String getResultTagValue() {
        return resultTagValue;
    }

    public boolean isBesidesTag() {
        return besidesTag;
    }
}
