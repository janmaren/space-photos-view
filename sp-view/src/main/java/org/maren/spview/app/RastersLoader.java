package org.maren.spview.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;
import org.maren.spview.SPView;
import org.maren.spview.imageloader.FitsHeaderItem;
import org.maren.spview.imageloader.FitsHeaderUtil;
import org.maren.spview.imageloader.FitsImageLoader;
import org.maren.spview.imageloader.ImageAndMetadata;
import org.maren.spview.imageloader.PictureImageLoader;
import org.maren.spview.imageloader.TxtAffinMetadataUtil;
import org.maren.spview.imageloader.WcsMetadataUtil;
import org.maren.spview.item.RasterItem;
import org.maren.spview.item.RasterSet;
import org.maren.swt.util.ConvTypesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.exception.ExceptionMessage;
import cz.jmare.fits.header.InconsistentFitsException;
import cz.jmare.fits.header.WCS2d;
import cz.jmare.fits.util.FitsUtil;
import cz.jmare.math.astro.RaDec;
import cz.jmare.swt.status.StatusUtil;
import nom.tam.fits.Fits;
import nom.tam.fits.ImageHDU;

public class RastersLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(RastersLoader.class);
    
    private RastersTable rastersTable;

    private List<AddFileRequest> filesToLoad = new ArrayList<>();
    
    private Thread loadThread;

    private SPView spView;
    
    public RastersLoader(SPView spView, RastersTable rastersTable) {
        super();
        this.spView = spView;
        this.rastersTable = rastersTable;
    }
    
    public synchronized void addFiles(List<AddFileRequest> files) {
        filesToLoad.addAll(files);
        if (loadThread == null || !loadThread.isAlive()) {
            loadThread = new Thread(new LoaderRun(Display.getCurrent()));
            loadThread.start();
        }
    }
    
    public synchronized void removeFile(AddFileRequest file) {
        filesToLoad.remove(file);
    }
    
    public void destroy() {
        if (loadThread != null) {
            loadThread.interrupt();
        }
    }
    
    class LoaderRun implements Runnable {
        Display display;
        
        public LoaderRun(Display display) {
            this.display = display;
        }

        @Override
        public void run() {
            List<String> errorNames = new ArrayList<String>();
            display.asyncExec(() -> {
                StatusUtil.infoMessage("Loading");
            });
            Thread currentThread = Thread.currentThread();
            while (!currentThread.isInterrupted() && filesToLoad.size() > 0) {
                AddFileRequest identifiedFile = filesToLoad.get(0);
                LOGGER.debug("Adding {}", identifiedFile.file);
                display.asyncExec(() -> {
                    StatusUtil.infoMessage("Loading " + identifiedFile.file);
                });
                try {
                    load(identifiedFile);
                } catch (InconsistentFitsException e) { 
                    LOGGER.error("Unable to load " + identifiedFile.file.getName() + " - wcs corrupted", e);
                    errorNames.add(identifiedFile.file.getName());                
                } catch (Exception e) {
                    LOGGER.error("Unable to load " + identifiedFile.file.getName(), e);
                    errorNames.add(identifiedFile.file.getName());
                }
                removeFile(identifiedFile);
            }
            display.asyncExec(() -> {
                if (errorNames.size() > 0) {
                    StatusUtil.errorMessage("Load error: " + errorNames.stream().collect(Collectors.joining(", ")));
                } else {
                    StatusUtil.infoMessage("Loaded");
                }
                spView.rasterTableChanged();
            });
        }
        
        /**
         * Creates RasterItem and passes it. Return last RasterItem. 
         * @param identifiedFile
         * @return
         */
        private void load(AddFileRequest identifiedFile) {
            if (identifiedFile.file.getName().toLowerCase().endsWith(".fits") || identifiedFile.file.getName().toLowerCase().endsWith(".fit")) {
                try (Fits fits = new Fits(identifiedFile.file)) {
                    ImageHDU hdu = FitsUtil.imageHDU(fits);
                    List<FitsHeaderItem> headerValues = FitsHeaderUtil.getHeaderValues(hdu);
                    WCS2d wcs = new WCS2d(hdu);
                    int axesCount = FitsUtil.getAxesCount(hdu);
                    if (axesCount < 2) {
                        throw new IllegalArgumentException("1d image");
                    }
                    String filename = identifiedFile.file.toString();
                    if (axesCount == 2 || (axesCount == 3 && FitsUtil.getAxeDim(hdu, 2) == 3)) {
                        createFitsRasterItem(filename, identifiedFile.name, identifiedFile.group, fits, wcs, null, headerValues);
                    } else {
                        int[] dimCounts = new int[axesCount];
                        int[] dimPos = new int[axesCount];
                        for (int i = 0; i < axesCount; i++) {
                            dimCounts[i] = FitsUtil.getAxeDim(hdu, i);
                            dimPos[i] = 0;
                        }
                        // dimIndex x = 0, y = 1, z = 2, z2 = 3,...
                        int max = 1000;
                        String shortName = identifiedFile.name;
                        while (--max > 0) {
                            int[] pos = new int[axesCount - 2];
                            for (int i = axesCount - 1, k = 0; i > 1; i--, k++) {
                                pos[k] = dimPos[i];
                            }
                            String name = shortName + Arrays.toString(pos);
                            createFitsRasterItem(filename, name, identifiedFile.group, fits, wcs, pos, headerValues);
                            int order = 2;
                            do {
                                dimPos[order]++;
                                if (dimPos[order] >= dimCounts[order]) {
                                    dimPos[order] = 0;
                                    order++;
                                } else {
                                    break;
                                }
                            } while (order < axesCount);
                            if (order >= axesCount) {
                                break;
                            }
                        }
                    }
                    if (spView.isCenterAddedFile()) {
                        RaDec middlePolarDeg = wcs.pixelToFK5(wcs.getNAXIS()[0] / 2, wcs.getNAXIS()[1] / 2);
                        spView.setCentre(ConvTypesUtil.toSpheric(middlePolarDeg));
                    }
                } catch (Exception e) {
                    LOGGER.error("Unable to load " + identifiedFile.file, e);
                    display.asyncExec(() -> {
                        StatusUtil.errorMessage(ExceptionMessage.getCombinedMessage("Unable to load " + identifiedFile.file.getName(), e));
                    });
                }
            } else {
                PictureImageLoader pictureImageLoader = new PictureImageLoader(identifiedFile.file.toString());
                ImageToSphericCoords imageToSphericCoords = null;
                List<FitsHeaderItem> headerValues = null;
                if (identifiedFile.file.getName().toLowerCase().endsWith(".tiff") || identifiedFile.file.getName().toLowerCase().endsWith(".tif") ) {
                    WcsMetadataUtil.ImageToSphericCoordsAndHeader imageToSphericCoordsAndHeader = WcsMetadataUtil.getWcsAffinPoints(identifiedFile.file.toString(), pictureImageLoader);
                    if (imageToSphericCoordsAndHeader != null) {
                        imageToSphericCoords = imageToSphericCoordsAndHeader.imageToSphericCoords;
                        headerValues = imageToSphericCoordsAndHeader.headerValues;
                    }
                }
                if (imageToSphericCoords == null) {
                    imageToSphericCoords = TxtAffinMetadataUtil.parseAffinPoints(identifiedFile.file.toString());
                }
                RasterItem rasterItem = new RasterItem(identifiedFile.file.toString(), identifiedFile.name, identifiedFile.group, pictureImageLoader, imageToSphericCoords);
                rasterItem.setHeaderValues(headerValues);
                display.asyncExec(() -> {
                    rastersTable.addOrUpdateRasters(List.of(rasterItem));
                });
                if (spView.isCenterAddedFile()) {
                    spView.goRasterToCenter(rasterItem);
                }
            }
        }
        
        private void createFitsRasterItem(String filename, String name, RasterSet group, Fits fits, WCS2d wcs, int[] frame, List<FitsHeaderItem> headerValues) {
            FitsImageLoader fitsImageLoader = new FitsImageLoader(filename, fits, frame);
            ImageAndMetadata loadImageData = fitsImageLoader.loadImageData();
            ImageData imageData = loadImageData.imageData;
            
            int imageWidth = imageData.width;
            int imageHeight = imageData.height;

            ImageToSphericCoords screenToPolarCoords = WcsMetadataUtil.createImageToSphericCoords(wcs, imageWidth, imageHeight, true);
            RasterItem rasterItem = new RasterItem(filename, name, group, fitsImageLoader, screenToPolarCoords, loadImageData.imageData);
            rasterItem.setHeaderValues(headerValues);
            display.asyncExec(() -> {
                rastersTable.addOrUpdateRasters(List.of(rasterItem));
            });
        }
    }
}
