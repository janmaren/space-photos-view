package org.maren.spview.app;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.maren.spview.guiutil.CreateUtil;
import org.maren.spview.shape.GroupAtt;
import org.maren.spview.shape.GroupUtil;
import org.maren.spview.shape.PointShape;
import org.maren.spview.tag.TagSupported;
import org.maren.spview.util.DateTimeUtil;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.GlocUtil;
import cz.jmare.data.util.JulianUtil;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.math.geo.GeoLoc;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.SComposite;

public class EditPointDialog extends TitleAreaDialog {
    private final static String GEOLOC = TagSupported.GEOLOC.name();

    private Combo groupText;
    private Text nameText;
    private Text dateText;
    private Text raDecText;
    private Text latLongText;
    private Text descText;
    
    private PointShape pointItem;

    private Map<String, GroupAtt> groups;
    private Text tagsText;
    private String title = "Edit Point";

    public static int DRAW_BUTTON = 3;
    private PointShape resultPointShape;
    private Button favourite;
    private Button seen;

    public EditPointDialog(Shell parent, PointShape pointItem, Map<String, GroupAtt> groups) {
        super(parent);
        this.pointItem = pointItem;
        this.groups = groups;
    }
    
    public EditPointDialog(Shell parent, PointShape pointItem, Map<String, GroupAtt> groups, String title) {
        super(parent);
        this.pointItem = pointItem;
        this.groups = groups;
        this.title = title;
    }
    
    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle(title);

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Name:");
        nameText = Create.text(composite, pointItem.getName() != null ? pointItem.getName() : "", 150);
        
        Create.label(composite, "Group:");
        groupText = Create.combo(composite, SWT.BORDER , c -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 150;
            c.setLayoutData(layoutData);
            List<String> itemsList = GroupUtil.getGroupsNames(groups);
            Collections.sort(itemsList);
            c.setItems(itemsList.toArray(new String[itemsList.size()]));
            c.setText(pointItem.getGroup().getName());
        });

        Create.label(composite, "Date:");
        Composite dateComp = SComposite.createGridCompositeIntoGridComposite(composite, 2);
        dateText = Create.text(dateComp, pointItem.getJd() != null ? JulianUtil.formatJulianRoundSeconds(pointItem.getJd() + DateTimeUtil.getOffsetDays(), 4) : "", 200);
        Create.label(dateComp, DateTimeUtil.getOffsetHoursString());

        Create.label(composite, "RA, Dec:");
        Composite raDecComposite = SComposite.createGridCompositeIntoGridComposite(composite, 2, gl -> {}, gd -> {gd.grabExcessVerticalSpace = false;});
        raDecText = Create.text(raDecComposite, pointItem.getRaDec() != null ? ACooUtil.formatRaDecProfi(pointItem.getRaDec()) : "", 300);
        raDecText.setToolTipText("Coordinates RA, DEC. When unable to detect units for RA the hours are expected");
        ImageUtil.put("remove", "/image/remove.png");
        Create.imageButton(raDecComposite, ImageUtil.get("remove"), "Clear", () -> {
            raDecText.setText("");});
        
        Create.label(composite, "Geo Coordinates:");
        GeoLoc loc = pointItem.getTagValue(GEOLOC) == null ? null : (GeoLoc) pointItem.getTagValue(GEOLOC);
        latLongText = Create.text(composite, loc != null ? GlocUtil.formatCoordsProfi(loc) : "", 200);
        latLongText.setToolTipText("Coordinates latitude, longitude separated with comma. It supports multiple formats of coordinates");

        Create.label(composite, "Description:");
        descText = CreateUtil.text(composite, pointItem.getDescription() != null ? pointItem.getDescription() : "", 300, 40);
        descText.setToolTipText("A description about object");

        Create.label(composite, "Favourite:");
        favourite = Create.checkButton(composite, pointItem.isFavourite());

        Create.label(composite, "Seen:");
        seen = Create.checkButton(composite, pointItem.isSeen());

        Create.label(composite, "Tags:");
        String tagsString = pointItem.getTagsString();
        Map<String, Object> parseTags = PointShape.parseTags(tagsString);
        parseTags.remove(GEOLOC);
        tagsText = Create.text(composite, PointShape.formatTags(parseTags), 300);
        tagsText.setToolTipText("Tag has format key:value and multiple tags are separated with semicolon");

        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(540, 520);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid(buttonId)) != null) {
            setErrorMessage(message);
            return; 
        }
        String grp = groupText.getText();
        GroupAtt groupAtt = GroupUtil.getOrCreateDefaultGroup(groups);
        if (!grp.isBlank()) {
            groupAtt = GroupUtil.getOrCreateGroup(groups, grp);
        }
        resultPointShape = buttonId == DRAW_BUTTON ? new PointShape(groupAtt, nameText.getText(), null) : pointItem;
        resultPointShape.setGroup(groupAtt);
        resultPointShape.setName(nameText.getText());
        if (!dateText.getText().trim().isEmpty()) {
            String dateStr = dateText.getText().trim();
			Double jd = null;
			try {
				jd = Double.valueOf(dateStr);
			} catch (NumberFormatException e) {
				jd = JulianUtil.parseToJulian(dateStr) - DateTimeUtil.getOffsetDays();
			}
            resultPointShape.setJd(jd);
        }

        resultPointShape.setTagsString(tagsText.getText());

        if (!raDecText.getText().isBlank()) {
            resultPointShape.setRaDec(ACooUtil.parseCoordsNormalized(raDecText.getText().trim(), false));
        }
        
        if (!latLongText.getText().isBlank()) {
            resultPointShape.setTag(GEOLOC, latLongText.getText());
        }
        
        if (!descText.getText().isBlank()) {
            resultPointShape.setDescription(descText.getText());
        }

        resultPointShape.setSeen(seen.getSelection());

        resultPointShape.setFavourite(favourite.getSelection());

        if (buttonId == DRAW_BUTTON) {
            setReturnCode(DRAW_BUTTON);
            close();
            return;
        }
        super.buttonPressed(buttonId);
    }
    
    protected String valid(int buttonId) {
        String dateStr = dateText.getText().trim();
        if (!dateStr.isBlank()) {
			try {
				Double.valueOf(dateStr);
			} catch (NumberFormatException e) {
				try {
				    JulianUtil.parseToJulian(dateStr);
				} catch (Exception e1) {
					return e1.getMessage() != null ? e1.getMessage() : "wrong date";
				}
			}
        }
        if (!raDecText.getText().isBlank()) {
            try {
                ACooUtil.parseCoordsNormalized(raDecText.getText().trim(), false);
            } catch (Exception e) {
                return ExceptionMessage.getCombinedMessage("Invalid coordinates RA, Dec", e);
            }
        } else {
            if (buttonId != DRAW_BUTTON) {
                return "No RA Dec coodinates entered";
            }
        }

        if (!latLongText.getText().isBlank()) {
            try {
                GlocUtil.parseCoordinatesNormalized(latLongText.getText());
            } catch (Exception e) {
                return ExceptionMessage.getCombinedMessage("Wrong geo coordinates", e);
            }
        }
        if (nameText.getText().contains("\"")) {
            return "name musn't contain the \" character";
        }
        if (!tagsText.getText().isBlank()) {
            try {
                PointShape.parseTags(tagsText.getText().trim());
            } catch (Exception e) {
                return e.getMessage();
            }
        }
        if (buttonId == DRAW_BUTTON && !raDecText.getText().isBlank()) {
            return "Coordinates must be blank";
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
        createButton(parent, DRAW_BUTTON, "Draw", false);
    }

    public PointShape getResultPointShape() {
        return resultPointShape;
    }
}
