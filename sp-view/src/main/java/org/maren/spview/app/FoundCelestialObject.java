package org.maren.spview.app;

public class FoundCelestialObject {
    public CelestialObject celestialObject;
    /**
     * Distance in deg
     */
    public double distance;

    public FoundCelestialObject(CelestialObject celestialObject, double distance) {
        this.celestialObject = celestialObject;
        this.distance = distance;
    }
}
