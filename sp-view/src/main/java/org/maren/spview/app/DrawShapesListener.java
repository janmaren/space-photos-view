package org.maren.spview.app;

import cz.jmare.graphfw.labeler.Labeler;
import org.eclipse.swt.graphics.GC;
import org.maren.gis.sphericcanvas.CoordState;

public interface DrawShapesListener {
    void drawShapes(GC gc, CoordState coordState, Labeler labeler);
}
