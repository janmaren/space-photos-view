package org.maren.spview.app;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.swt.util.Create;

public class DownloadFitsDialog extends TitleAreaDialog {
    private final static String[] SOURCES = Arrays.asList(SourceEnum.values()).stream().map(v -> v.name()).collect(Collectors.toList()).toArray(new String[SourceEnum.values().length]);
    
    private Combo sourceCombo;
	private Combo subSourceCombo;

	private Integer resultWidth;
	
	private Integer resultHeight;
	
	private SourceEnum resultSource;
	
	private DssSubsourceEnum resultSubsource;
    private Text widthText;
    private Text heightText;

    private Text nameText;
	
    private String resultName;
    
    public DownloadFitsDialog(Shell parent) {
        super(parent);
    }

    protected Control createDialogArea(Composite parent) {
        AppConfig appConfig = AppConfig.getInstance();
        ConfigStore configStore = appConfig.getConfigStore();
        
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Download Fits");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Source:");
        sourceCombo = Create.combo(composite, SWT.READ_ONLY | SWT.BORDER, c -> {
			GridData gridData = new GridData();
			gridData.widthHint = 150;
			c.setLayoutData(gridData);
		});
        sourceCombo.setItems(SOURCES);
        sourceCombo.select(configStore.getEnumValue("downloading.source", SourceEnum.ESO_DSS).ordinal());

        Create.label(composite, "Subsource:");
        subSourceCombo = Create.combo(composite, SWT.READ_ONLY | SWT.BORDER, c -> {
            GridData gridData = new GridData();
            gridData.widthHint = 150;
            c.setLayoutData(gridData);
        });
        subSourceCombo.setItems(Arrays.asList(DssSubsourceEnum.values()).stream().map(v -> v.name()).collect(Collectors.toList()).toArray(new String[DssSubsourceEnum.values().length]));
        subSourceCombo.select(configStore.getEnumValue("downloading.subsource", DssSubsourceEnum.DSS_1).ordinal());
        
        Create.label(composite, "Width:");
        widthText = Create.text(composite, String.valueOf(configStore.getIntValue("downloading.width", 25)), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 100;
            t.setLayoutData(layoutData);
        });

        Create.label(composite, "Height:");
        heightText = Create.text(composite, String.valueOf(configStore.getIntValue("downloading.height", 25)), t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 100;
            t.setLayoutData(layoutData);
        });
        
        Create.label(composite, "Name:");
        nameText = Create.text(composite, "", t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 150;
            t.setLayoutData(layoutData);
        });
        
        return composite;
    }

    
    @Override
    protected Point getInitialSize() {
        return new Point(600, 500);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }
        resultWidth = Integer.valueOf(widthText.getText());
        resultHeight = Integer.valueOf(heightText.getText());
        resultSource = SourceEnum.values()[sourceCombo.getSelectionIndex()];
        resultSubsource = DssSubsourceEnum.values()[subSourceCombo.getSelectionIndex()];
        resultName = nameText.getText().trim().equals("") ? null : nameText.getText().trim();
        
        AppConfig appConfig = AppConfig.getInstance();
        ConfigStore configStore = appConfig.getConfigStore();
        configStore.putEnumValue("downloading.source", resultSource);
        configStore.putEnumValue("downloading.subsource", resultSubsource);
        configStore.putIntValue("downloading.width", resultWidth);
        configStore.putIntValue("downloading.height", resultHeight);
        
        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        try {
            Integer.valueOf(widthText.getText());
        } catch (NumberFormatException e) {
            return "Invalid width";
        }
        try {
            Integer.valueOf(heightText.getText());
        } catch (NumberFormatException e) {
            return "Invalid height";
        }
        if (sourceCombo.getSelectionIndex() == -1) {
            return "Invalid source";
        }
        if (subSourceCombo.getSelectionIndex() == -1) {
            return "Invalid subsource";
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public Integer getResultWidth() {
        return resultWidth;
    }

    public Integer getResultHeight() {
        return resultHeight;
    }

    public SourceEnum getResultSource() {
        return resultSource;
    }

    public DssSubsourceEnum getResultSubsource() {
        return resultSubsource;
    }

    public String getResultName() {
        return resultName;
    }
}
