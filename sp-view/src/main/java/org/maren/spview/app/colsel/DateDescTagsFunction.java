package org.maren.spview.app.colsel;

import java.util.StringJoiner;
import java.util.function.Function;

import org.maren.spview.shape.PointShape;

import cz.jmare.data.util.JulianUtil;
import org.maren.spview.util.DateTimeUtil;

public class DateDescTagsFunction implements Function<PointShape, String> {
    @Override
    public String apply(PointShape pointShape) {
        StringJoiner sj = new StringJoiner(", ");
        if (pointShape.getJd() != null) {
            sj.add(JulianUtil.formatJulianRoundSeconds(pointShape.getJd(), 1, DateTimeUtil.getOffsetHours()));
        }
        if (pointShape.getDescription() != null) {
            sj.add(pointShape.getDescription());
        }
        String tagsString = pointShape.getTagsString();
        if (!tagsString.isBlank()) {
            sj.add(tagsString);
        }
        return sj.toString();
    }
}
