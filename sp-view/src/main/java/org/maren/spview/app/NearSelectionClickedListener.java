package org.maren.spview.app;

import org.maren.spview.NearestSelectionObject;

public interface NearSelectionClickedListener {
    void onNearSelectionClicked(NearestSelectionObject nearestSelectionObject, int stateMask);
}
