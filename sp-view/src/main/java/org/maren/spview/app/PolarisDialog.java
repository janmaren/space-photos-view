package org.maren.spview.app;

import static org.maren.spview.util.StyledUtil.addBoldText;
import static org.maren.spview.util.StyledUtil.addItalicText;
import static org.maren.spview.util.StyledUtil.addLF;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.maren.spview.ConfigKeys;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.AstroConversionUtil;
import cz.jmare.data.util.GMSTUtil;
import cz.jmare.data.util.GlocUtil;
import cz.jmare.data.util.HourUtil;
import cz.jmare.data.util.JulianUtil;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.geo.GeoLoc;
import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.SComposite;

/**
 * This class shows an about box, based on TitleAreaDialog
 */
public class PolarisDialog extends TitleAreaDialog {
    private StyledText contentText;
    private Text geoText;

    private static GeoLoc lastGeoLoc;

    /**
     * MyTitleAreaDialog constructor
     *
     * @param shell
     *            the parent shell
     */
    public PolarisDialog(Shell shell) {
        super(shell);
    }

    /**
     * Closes the dialog box Override so we can dispose the image we created
     */
    public boolean close() {
        return super.close();
    }

    /**
     * Creates the dialog's contents
     *
     * @param parent
     *            the parent composite
     * @return Control
     */
    protected Control createContents(Composite parent) {
        Control contents = super.createContents(parent);

        // Set the title
        setTitle("Polaris");

        return contents;
    }

    /**
     * Creates the gray area
     *
     * @param parent
     *            the parent composite
     * @return Control
     */
    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);

        Composite composite = titleAreaComposite;

        GridLayout layout = new GridLayout();
        layout.marginLeft = 10;
        layout.marginRight = 10;
        composite.setLayout(layout);

        AppConfig appConfig = AppConfig.getInstance();
        ConfigStore configStore = appConfig.getConfigStore();
        String geoLocStr = lastGeoLoc != null ? GlocUtil.formatCoordsProfi(lastGeoLoc) : configStore.getValue(ConfigKeys.GEO_LOCATION, null);
        Composite geo = SComposite.createGridCompositeIntoGridComposite(composite, 4);
        Create.label(geo, "Geo Location");
        geoText = Create.text(geo, geoLocStr, 200);
        geoText.addModifyListener(e -> {
            GeoLoc geoLoc = getGeoLoc();
            if (geoLoc != null) {
                lastGeoLoc = geoLoc;
            }
            contentText.setText("");
            generateContentText();
        });
        Create.button(geo, "From Config", () -> {
            String value = configStore.getValue(ConfigKeys.GEO_LOCATION, null);
            if (value != null) {
                geoText.setText(value);
            }
        });

        Create.button(geo, "Now", () -> {
            contentText.setText("");
            generateContentText();
        });

        final TabFolder tabFolder = new TabFolder(composite, SWT.NONE);
        tabFolder.setLayout(new GridLayout());
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        gridData.widthHint = 200;
        tabFolder.setLayoutData(gridData);

        TabItem contentTab = new TabItem(tabFolder, SWT.NONE);
        contentTab.setText("Calculation");

        contentText = new StyledText(tabFolder, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
        GridData gdt = new GridData();
        gdt.grabExcessVerticalSpace = true;
        gdt.verticalAlignment = SWT.FILL;
        gdt.grabExcessHorizontalSpace = true;
        gdt.horizontalAlignment = SWT.FILL;
        contentText.setLayoutData(gdt);
        
        contentTab.setControl(contentText);
        
        generateContentText();
        
        return composite;
    }

    private void generateContentText() {

        double jd = JulianUtil.julianDateNow();
        addBoldText(contentText, "GMT: ");
        contentText.append(JulianUtil.formatJulianRoundSeconds(jd));
        addLF(contentText);

        double h = GMSTUtil.calcGmstHoursSimple(jd);
        addBoldText(contentText, "GMS: ");
        contentText.append(HourUtil.formatHours(h));
        addLF(contentText);

        GeoLoc geoLoc = getGeoLoc();
        if (geoLoc != null) {
            addBoldText(contentText, "Geo Location: ");
            contentText.append(GlocUtil.formatCoordsPublic(geoLoc));
            addLF(contentText);

            double lmst = GMSTUtil.toLmstHours(h, geoLoc.longitude);
            addBoldText(contentText, "LMST: ");
            contentText.append(HourUtil.formatHours(lmst));
            addLF(contentText);
            addLF(contentText);

            RaDec raDec = AstroConversionUtil.convertEquinoxExactly(ACooUtil.parseCoords("02 31 49.09456 +89 15 50.7923"), 2451545.0, JulianUtil.julianDateNow());
            raDec = AstroConversionUtil.properMotion(raDec, JulianUtil.toEpochYear(JulianUtil.julianDateJ2000()), JulianUtil.toEpochYear(JulianUtil.julianDateNow()), 44.48, -11.85);
            addBoldText(contentText, "Polaris Coordinates: ");
            contentText.append(ACooUtil.formatRaDec(raDec));
            addLF(contentText);

            double polarisHa = lmst - raDec.toRahDec().rah;
            if (polarisHa < 0) {
                polarisHa += 24.0;
            }
            addBoldText(contentText, "Polaris HA: ");
            contentText.append(HourUtil.formatHours(polarisHa));
            addLF(contentText);

            double clock = (24 - (polarisHa + 12) % 24) / 2;
            addBoldText(contentText, "HA on the Dial: ");
            contentText.append(HourUtil.formatHours(clock));
            addLF(contentText);
        } else {
            addItalicText(contentText, "Geo Location not defined, unable to calculate polaris");
        }
    }

    private GeoLoc getGeoLoc() {
        String geoLocStr = geoText.getText();
        if (geoLocStr == null || geoLocStr.isEmpty()) {
            return null;
        }
        try {
            return GlocUtil.parseCoordinatesNormalized(geoLocStr);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Creates the buttons for the button bar
     *
     * @param parent
     *            the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
    }

    @Override
    protected Point getInitialSize() {
        return new Point(600, 500);
    }
}
