package org.maren.spview.app;

import cz.jmare.swt.util.Create;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RadiusByLensDialog extends TitleAreaDialog {
    private static Double lastTelFocLength;
    private static Double lastEyeFocLength;
    private static Double lastEyeAfovLength;

    private Text telescopeFocLengthText;
    private Text eyepieFocLengthText;
    private Text eyepieAfovText;

    private double resultRadius;

    public RadiusByLensDialog(Shell parent) {
        super(parent);
    }

    protected Control createDialogArea(Composite parent) {
        setMessage("Calculate the resulting radius for the observation");
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("True Field of View Calculation");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);
        

        Create.label(composite, "Telescope focal length [mm]:");
        telescopeFocLengthText = Create.text(composite,  lastTelFocLength != null ? String.valueOf(lastTelFocLength) : "", 100);

        Create.label(composite, "Eyepiece focal length [mm]:");
        eyepieFocLengthText = Create.text(composite, lastEyeFocLength != null ? String.valueOf(lastEyeFocLength) : "", 100);

        Create.label(composite, "Eyepiece AFOV [deg]:");
        eyepieAfovText = Create.text(composite, lastEyeAfovLength != null ? String.valueOf(lastEyeAfovLength) : "", 100);
        eyepieAfovText.setToolTipText("Apparent field of view of eyepiece");

        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(400, 350);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }

        resultRadius = calculate() / 2.0;
        store();

        super.buttonPressed(buttonId);
    }

    private double calculate() {
        try {
            double telFocLength = round(Double.parseDouble(telescopeFocLengthText.getText().trim()));
            double eyeFocLength = round(Double.parseDouble(eyepieFocLengthText.getText().trim()));
            double eyeFovLength = round(Double.parseDouble(eyepieAfovText.getText().trim()));
            return eyeFovLength / (telFocLength / eyeFocLength);
        } catch (Exception e) {
            return Double.NaN;
        }
    }

    private void store() {
        try {
            lastTelFocLength = round(Double.parseDouble(telescopeFocLengthText.getText().trim()));
            lastEyeFocLength = round(Double.parseDouble(eyepieFocLengthText.getText().trim()));
            lastEyeAfovLength = round(Double.parseDouble(eyepieAfovText.getText().trim()));
        } catch (Exception e) {
        }
    }
    
    protected String valid() {
        String telescopFocStr = telescopeFocLengthText.getText().trim();
        if (telescopFocStr.isEmpty()) {
            return "Telescope FOC must be populated";
        }
        try {
            double parseDouble = Double.parseDouble(telescopFocStr);
            if (parseDouble < 0) {
                return "Telescope FOC must be positive";
            }
        } catch (NumberFormatException e) {
            return "Telescope FOC must be a valid number";
        }

        if (telescopFocStr.isEmpty()) {
            return "Eyepiece FOC must be populated";
        }
        try {
            double parseDouble = Double.parseDouble(telescopFocStr);
            if (parseDouble < 0) {
                return "Eyepiece FOC must be positive";
            }
        } catch (NumberFormatException e) {
            return "Eyepiece FOC must be a valid number";
        }

        String eyepieFovStr = eyepieAfovText.getText().trim();
        if (eyepieFovStr.isEmpty()) {
            return "Eyepiece FOV must be populated";
        }
        try {
            double parseDouble = Double.parseDouble(eyepieFovStr);
            if (parseDouble < 0) {
                return "Eyepiece FOV must be positive";
            }
        } catch (NumberFormatException e) {
            return "Eyepiece FOV must be a valid number";
        }

        double calculate = calculate();
        if (calculate > 90 || calculate < 0) {
            return "Calculated radius must be 0..90 degrees";
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public double getResultRadius() {
        return resultRadius;
    }

    public double round(double value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
