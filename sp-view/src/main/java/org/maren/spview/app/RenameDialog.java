package org.maren.spview.app;

import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.SComposite;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import java.io.File;
import java.nio.file.Paths;

public class RenameDialog extends TitleAreaDialog {
    private String suggestName;
    private Text nameText;

    private String resultName;

    private String resultDestinationDirectory;

    private Text directoryText;

    private static String lastDirectory = null;
    private Button directoryCheck;
    private Button nameCheck;

    public RenameDialog(Shell parent) {
        super(parent);
    }

    public RenameDialog(Shell parent, String suggestName) {
        super(parent);
        this.suggestName = suggestName;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Rename File");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);


        Create.label(composite, "Name:");
        Composite nameComp = SComposite.createGridCompositeIntoGridComposite(composite, 2);
        nameCheck = Create.checkButton(nameComp, true, b -> {
            nameText.setEnabled(b.getSelection());
        });
        nameText = Create.text(nameComp, suggestName != null ? suggestName : "", t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 200;
            t.setLayoutData(layoutData);
        });
        nameText.setToolTipText("Result name without suffix.\nFor multiple files an additional suffix like (x) will be appended");

        Create.label(composite, "Destination directory:");
        Composite dirComp = SComposite.createGridCompositeIntoGridComposite(composite, 2);
        directoryCheck = Create.checkButton(dirComp, false, b -> {
            directoryText.setEnabled(b.getSelection());
        });
        directoryText = Create.text(dirComp, lastDirectory != null ? lastDirectory : "", 350);
        directoryText.setEnabled(false);

        nameText.setFocus();
        nameText.selectAll();

        return composite;
    }

    
    @Override
    protected Point getInitialSize() {
        return new Point(620, 350);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }
        resultName = !nameCheck.getSelection() ? null : nameText.getText().trim();
        resultDestinationDirectory = !directoryCheck.getSelection() ? null : directoryText.getText().trim();
        if (directoryCheck.getSelection()) {
            lastDirectory = resultDestinationDirectory;
        }

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        if (!nameCheck.getSelection() && !directoryCheck.getSelection()) {
            return "No action";
        }
        if (nameCheck.getSelection()) {
            if (nameText.getText().isBlank()) {
                return "Name not populated";
            }
            if (!isFilenameValid(nameText.getText())) {
                return "Invalid characters in name";
            }
        }
        if (directoryCheck.getSelection()) {
            if (directoryText.getText().isBlank()) {
                return "Destination directory not populated";
            }
            if (!new File(directoryText.getText().trim()).isDirectory()) {
                return "Destination directory " + directoryText.getText() + " doesn't exist";
            }
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public static boolean isFilenameValid(String file) {
        File f = new File(file);
        try {
            f.getCanonicalPath();
            Paths.get(System.getProperty("user.dir"), file);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String getResultName() {
        return resultName;
    }

    public String getResultDestinationDirectory() {
        return resultDestinationDirectory;
    }
}
