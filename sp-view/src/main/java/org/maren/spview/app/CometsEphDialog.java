package org.maren.spview.app;

import cz.jmare.config.AppConfig;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.swt.util.Create;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.maren.spview.horizons.EphRequest;
import org.maren.spview.horizons.IntType;
import org.maren.spview.minorplanet.MinorPlanetComet;
import org.maren.spview.minorplanet.SmallObjectsFetcher;
import org.maren.spview.util.DateTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class CometsEphDialog extends TitleAreaDialog {
    private static final Logger LOGGER = LoggerFactory.getLogger(CometsEphDialog.class);

    private Text maxNumber;

    private EphRequest resultEphRequest;

    private Button onlyOnePointCheck;
    private Text fromText;
    private Label endLabel;
    private Button showMagCheck;
    private boolean resultShowMagCheck;
    private Text periText;

    public CometsEphDialog(Shell parent) {
        super(parent);
        new Thread(() -> {
            try {
                SmallObjectsFetcher.getComets();
            } catch (IOException e) {
            }
        }).start();
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Download Ephemeris of Comets");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Perihelion date");
        Composite periComp = createGridCompositeIntoGridComposite(composite, 5, gd -> {
        });
        periText = Create.text(periComp, "", t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 100;
            t.setLayoutData(layoutData);
        });
        periText.setText(DateTimeUtil.format(LocalDate.now()));

        Create.button(periComp, "Now", () -> {
            periText.setText(DateTimeUtil.format(LocalDate.now()));
        });

        Create.label(composite, "Ephemeris from:");
        Composite fromComp = createGridCompositeIntoGridComposite(composite, 5, gd -> {
        });
        fromText = Create.text(fromComp, "", t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 160;
            t.setLayoutData(layoutData);
        });
        fromText.setText(DateTimeUtil.format(LocalDateTime.now()));
        fromText.addModifyListener(k -> {
            showEndLabel();
        });

        Create.label(fromComp, DateTimeUtil.getOffsetHoursString());


        Create.button(fromComp, "Now", () -> {
            fromText.setText(DateTimeUtil.format(LocalDateTime.now()));
        });

        Create.label(composite, "Only One Point:");
        onlyOnePointCheck = Create.checkButton(composite, false);
        onlyOnePointCheck.setSelection(true);
        onlyOnePointCheck.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {showEndLabel();}));

        Create.label(composite, "End:");
        endLabel = Create.label(composite, "", l -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 240;
            l.setLayoutData(layoutData);
        });

        Create.label(composite, "Max number:");
        maxNumber = new Text(composite, SWT.BORDER);
        GridData grLayData = new GridData();
        grLayData.widthHint = 150;
        maxNumber.setLayoutData(grLayData);
        maxNumber.setToolTipText("Maximum number of nearest comects to perihelion to download");
        maxNumber.setText("30");

        Create.label(composite, "Show magnitude");
        showMagCheck = Create.checkButton(composite, false);

        showEndLabel();

        return composite;
    }

    private void showEndLabel() {
        try {
            OffsetDateTime from = OffsetDateTime.of(DateTimeUtil.parse(fromText.getText()), ZoneOffset.ofTotalSeconds((int) (DateTimeUtil.getOffsetHours() * 3600)));
            endLabel.setText("End: " + DateTimeUtil.format(onlyOnePointCheck.getSelection() ? from.toLocalDateTime() : from.toLocalDateTime().plusDays(1)) + DateTimeUtil.getOffsetHoursString());
        } catch (Exception e) {
        }
    }

    @Override
    protected Point getInitialSize() {
        return new Point(500, 400);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }

        List<MinorPlanetComet> comets;
        try {
            comets = SmallObjectsFetcher.getComets();
        } catch (Exception e) {
            LOGGER.error("Problems to download object names", e);
            setErrorMessage(ExceptionMessage.getCombinedMessage("Unable to download objects", e));
            return;
        }

        LocalDate periDateTime = DateTimeUtil.parseDate(periText.getText());
        int maxNumber = Integer.parseInt(this.maxNumber.getText().trim());
        Collections.sort(comets, (o1, o2) -> {
            long between1 = Math.abs(ChronoUnit.DAYS.between(o1.perihelionDate, periDateTime));
            long between2 = Math.abs(ChronoUnit.DAYS.between(o2.perihelionDate, periDateTime));
            if (between1 < between2) {
                return -1;
            }
            if (between1 > between2) {
                return 1;
            }
            return 0;
        });
        comets = comets.subList(0, comets.size() > maxNumber ? maxNumber : comets.size());

        List<String[]> namesAndShorted = SmallObjectsFetcher.shortSmallObjectNames(comets);
        namesAndShorted = namesAndShorted.stream().filter(ns -> ns[1] != null).collect(Collectors.toList());

        LocalDateTime fromDateTime = DateTimeUtil.parse(fromText.getText());
        double offset = AppConfig.getInstance().getConfigStore().getDoubleValue("time.offset", 0);
        OffsetDateTime from = fromDateTime.atOffset(ZoneOffset.ofTotalSeconds((int) (offset * 60 * 60)));
        OffsetDateTime to = from.plus(1, ChronoUnit.DAYS);

        List<String> names = namesAndShorted.stream().map(ns -> ns[1]).collect(Collectors.toList());

        EphRequest ephRequest;
        if (onlyOnePointCheck.getSelection()) {
            ephRequest = new EphRequest(names, from,
                                        from.plus(1, ChronoUnit.HOURS), 1, IntType.DAY);
        } else {
            ephRequest = new EphRequest(names, from,
                                        to, 1, IntType.HOUR);
        }

        resultEphRequest = ephRequest;

        resultShowMagCheck = showMagCheck.getSelection();

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        if (periText.getText().isBlank()) {
            return "Perihelion date time must be populated";
        }
        try {
            DateTimeUtil.parseDate(periText.getText());
        } catch (Exception e) {
            return "Invalid format of perihelion date time";
        }
        if (fromText.getText().isBlank()) {
            return "From ephemeris date time must be populated";
        }
        try {
            DateTimeUtil.parse(fromText.getText());
        } catch (Exception e) {
            return "Invalid format of from ephemeris";
        }

        if (maxNumber.getText().isBlank()) {
            return "Maximum number must be populated";
        }
        try {
            Integer.parseInt(maxNumber.getText().trim());
        } catch (NumberFormatException e) {
            return "Maximum number must be an integer number";
        }
        return null;
    }

    public EphRequest getResultEphRequest() {
        return resultEphRequest;
    }

    public boolean isResultShowMagCheck() {
        return resultShowMagCheck;
    }

    public void setResultShowMagCheck(boolean resultShowMagCheck) {
        this.resultShowMagCheck = resultShowMagCheck;
    }

    public static Composite createGridCompositeIntoGridComposite(Composite parentComposite, int columns, Consumer<GridData> gdConsumer) {
        Composite panelComposite = new Composite(parentComposite, SWT.NONE);
        GridLayout gridLayout = new GridLayout(columns, false);
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        panelComposite.setLayout(gridLayout);
        GridData gridData = new GridData();
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }

}
