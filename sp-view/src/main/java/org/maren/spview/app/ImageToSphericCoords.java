package org.maren.spview.app;

import org.maren.spview.rasterpos.pixeltospheric.CoordinateRaDec;
import org.maren.spview.rasterpos.pixeltospheric.PixelToSpheric;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImageToSphericCoords {
    private PixelToSpheric pixelToSpheric;

    private Map<Integer, List<CoordinateRaDec>> gridToCooRadecs = new HashMap<>();

    public ImageToSphericCoords() {
    }
    
    public ImageToSphericCoords(PixelToSpheric pixelToSpheric) {
        this.pixelToSpheric = pixelToSpheric;
    }

    public PixelToSpheric getPixelToSpheric() {
        return pixelToSpheric;
    }

    public Map<Integer, List<CoordinateRaDec>> getGridToCooRadecs() {
        return gridToCooRadecs;
    }
}
