package org.maren.spview.app;

import java.io.File;

import org.maren.spview.item.RasterSet;

/**
 * Request to add a file (asynchronously). Such file may contain
 * multiple images and hence more then 1 RasterItem instances may be created
 */
public class AddFileRequest {
    public File file;
    
    public String name;
    
    public RasterSet group;

    public AddFileRequest(File file, String name, RasterSet group) {
        super();
        this.file = file;
        this.name = name;
        this.group = group;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((file == null) ? 0 : file.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AddFileRequest other = (AddFileRequest) obj;
        if (file == null) {
            if (other.file != null)
                return false;
        } else if (!file.equals(other.file))
            return false;
        return true;
    }
}