package org.maren.spview.app;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.maren.spview.horizons.EphRequest;
import org.maren.spview.horizons.IntType;
import org.maren.spview.util.DateTimeUtil;
import org.maren.spview.util.ObjectsListUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.data.util.JulianUtil;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.SComposite;

public class CustomEphDialog extends TitleAreaDialog {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomEphDialog.class);

    private static String[] UNTIL_LABELS = {"Minutes", "Hours", "Days", "Months", "Years"};

    private static IntType[] UNTIL_TYPES = {IntType.MINUTE, IntType.HOUR, IntType.DAY, IntType.MONTH, IntType.YEAR};


    private static String[] STEP_LABELS = {"Minute", "Hour", "Day", "5 Days", "Month", "Year"};

    private static IntType[] STEP_TYPES = {IntType.MINUTE, IntType.HOUR, IntType.DAY, IntType.DAY, IntType.MONTH, IntType.YEAR};

    private static int[] STEP_AMOUNT = {1, 1, 1, 5, 1, 1};

    private Text maxMagnitudeText;

    private EphRequest resultEphRequest;

    private StyledText namesText;
    private Text fromText;
    private Text periodText;
    private Combo periodUnitsStep;
    private Combo step;
    private Label endLabel;
    private List<String> prepareNames;
    private Double prepareFrom;
    private Button onlyOnePoint;
    private Button showMagCheck;

    private boolean resultShowMagCheck;

    private String message;

    public CustomEphDialog(Shell parent) {
        super(parent);
    }

    public CustomEphDialog(Shell parent, List<String> prepareNames, Double prepareFrom) {
        super(parent);
        this.prepareNames = prepareNames;
        this.prepareFrom = prepareFrom;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Download Ephemeris");
        if (message != null) {
            setMessage(message, IMessageProvider.WARNING);
        } else {
            setMessage("Write an NASA Horizons identifier of object on each line.");
        }

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Names List:");
        Composite namesComp = SComposite.createGridCompositeIntoGridComposite(composite, 2);
        namesText = new StyledText(namesComp, SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
        GridData textData = new GridData(SWT.FILL, SWT.FILL, true, true);
        namesText.setLayoutData(textData);
        if (prepareNames != null) {
            namesText.setText(prepareNames.stream().collect(Collectors.joining("\n")));
        }
        namesText.addKeyListener(KeyListener.keyPressedAdapter(e -> {
            if (e.stateMask == SWT.CTRL && e.keyCode == 'a') {
                namesText.selectAll();
            }
        }));
        Composite buttonsComp = createGridCompositeIntoGridComposite(namesComp, 1, gd -> {gd.verticalAlignment = SWT.TOP;});
        Create.imageButton(buttonsComp, ImageUtil.get("folder"), "Load", () -> {
            FileDialog fd = new FileDialog(getShell(), SWT.OPEN | SWT.MULTI);
            fd.setText("Text Files to Open");
            String[] filterExt = new String[]{"*.txt;*.*", "*.txt", "*.*"};
            fd.setFilterExtensions(filterExt);
            String path = null;
            if ((path = fd.open()) != null) {
                Path loadPath = Paths.get(path);
                try {
                    String str = Files.readString(loadPath);
                    appendText(str);
                } catch (Exception e) {
                    setErrorMessage(ExceptionMessage.getCombinedMessage("Unable to read " + path, e));
                }
            }
        });
        Create.imageButton(buttonsComp, ImageUtil.get("jupiter"), "Add Main Objects and Planets", () -> {
            appendText(ObjectsListUtil.loadResourcesTextFile("/objects/planets.txt"));
        });
        Create.imageButton(buttonsComp, ImageUtil.get("dwarf-planet"), "Add Dwarf Planets", () -> {
            appendText(ObjectsListUtil.loadResourcesTextFile("/objects/dwarf-planets.txt"));
        });
        Create.imageButton(buttonsComp, ImageUtil.get("asteroid-brown"), "Add Asteroids", () -> {
            appendText(ObjectsListUtil.loadResourcesTextFile("/objects/asteroids.txt"));
        });

        Create.label(composite, "Max Magnitude:");
        maxMagnitudeText = new Text(composite, SWT.BORDER);
        GridData grLayData = new GridData();
        grLayData.widthHint = 150;
        maxMagnitudeText.setLayoutData(grLayData);

        Create.label(composite, "From:");
        Composite fromComp = createGridCompositeIntoGridComposite(composite, 5, gd -> {
        });
        fromText = Create.text(fromComp, "", t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 160;
            t.setLayoutData(layoutData);
        });
        fromText.setText(DateTimeUtil.format(prepareFrom != null ? JulianUtil.toOffsetDateTime(prepareFrom, DateTimeUtil.getOffsetHours()).toLocalDateTime() : LocalDateTime.now()));
        fromText.addModifyListener(k -> {
            showEndLabel();
        });

        Create.label(fromComp, DateTimeUtil.getOffsetHoursString());


        Create.button(fromComp, "Now", () -> {
            fromText.setText(DateTimeUtil.format(LocalDateTime.now()));
        });

        onlyOnePoint = new Button(composite, SWT.CHECK);
        onlyOnePoint.setText("Only One Point");
        onlyOnePoint.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {doRangeEnablement(); showEndLabel();}));
        onlyOnePoint.setSelection(prepareNames == null || prepareNames.size() == 0);

        Composite periodComp = createGridCompositeIntoGridComposite(composite, 5, gd -> {
        });
        periodText = Create.text(periodComp, "1", t -> {
            t.setToolTipText("Period from the date in days");
            GridData layoutData = new GridData();
            layoutData.widthHint = 50;
            t.setLayoutData(layoutData);
            t.addModifyListener(k -> {showEndLabel();});
        });
        periodUnitsStep = Create.combo(periodComp);
        periodUnitsStep.setItems(UNTIL_LABELS);
        periodUnitsStep.select(2);
        periodUnitsStep.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {showEndLabel();}));
        endLabel = Create.label(periodComp, "", l -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 240;
            l.setLayoutData(layoutData);
        });

        Create.label(composite, "Step: ");
        step = Create.combo(composite);
        step.setItems(STEP_LABELS);
        step.select(1);

        Create.label(composite, "Show magnitude");
        showMagCheck = Create.checkButton(composite, false);

        showEndLabel();
        doRangeEnablement();

        return composite;
    }

    private void doRangeEnablement() {
        if (!onlyOnePoint.getSelection()) {
            step.setEnabled(true);
            periodText.setEnabled(true);
            periodUnitsStep.setEnabled(true);
        } else {
            step.setEnabled(false);
            periodText.setEnabled(false);
            periodUnitsStep.setEnabled(false);
        }
    }

    private void appendText(String text) {
        if (namesText.getText().isEmpty() || namesText.getText().endsWith("\r") || namesText.getText().endsWith("\n")) {
            namesText.append(text);
        } else {
            namesText.append("\n");
            namesText.append(text);
        }
    }

    private void showEndLabel() {
        try {
            OffsetDateTime from = OffsetDateTime.of(DateTimeUtil.parse(fromText.getText()), ZoneOffset.ofTotalSeconds((int) (DateTimeUtil.getOffsetHours() * 3600)));
            Integer period = Integer.valueOf(periodText.getText());
            IntType periodUnitsType = UNTIL_TYPES[periodUnitsStep.getSelectionIndex()];
            ChronoUnit chronoUnit = IntType.toChronoUnit(periodUnitsType);
            OffsetDateTime to = from.plus(period, chronoUnit);
            endLabel.setText("End: " + DateTimeUtil.format(onlyOnePoint.getSelection() ? from.toLocalDateTime() : to.toLocalDateTime()) + DateTimeUtil.getOffsetHoursString());
        } catch (Exception e) {
        }
    }

    @Override
    protected Point getInitialSize() {
        return new Point(600, 600);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }

        List<String> names;
        try {
            names = ObjectsListUtil.parseObjectsList(namesText.getText());
        } catch (Exception e) {
            LOGGER.error("Problems to parse list of names", e);
            setErrorMessage(ExceptionMessage.getCombinedMessage("Unable to parse list of names", e));
            return;
        }

        OffsetDateTime from = OffsetDateTime.of(DateTimeUtil.parse(fromText.getText()), ZoneOffset.ofTotalSeconds((int) (DateTimeUtil.getOffsetHours() * 3600)));
        Integer period = Integer.valueOf(periodText.getText());
        IntType periodUnitsType = UNTIL_TYPES[periodUnitsStep.getSelectionIndex()];
        ChronoUnit chronoUnit = IntType.toChronoUnit(periodUnitsType);
        OffsetDateTime to = from.plus(period, chronoUnit);

        IntType stepType = STEP_TYPES[step.getSelectionIndex()];
        EphRequest ephRequest;

        if (onlyOnePoint.getSelection()) {
            ephRequest = new EphRequest(names, from,
                                        from.plus(1, ChronoUnit.HOURS), 1, IntType.DAY);
            ephRequest.setForceDownload(true);
        } else {
            ephRequest = new EphRequest(names, from,
                                        to, STEP_AMOUNT[step.getSelectionIndex()], stepType);
        }

        if (!maxMagnitudeText.getText().isBlank()) {
            ephRequest.setMaxMagnitude(Double.parseDouble(maxMagnitudeText.getText().trim()));
        }

        resultEphRequest = ephRequest;

        resultShowMagCheck = showMagCheck.getSelection();

        super.buttonPressed(buttonId);
    }

    protected String valid() {
        if (namesText.getText().isBlank()) {
            return "No object to download";
        }
        if (!maxMagnitudeText.getText().isBlank()) {
            try {
                Double.parseDouble(maxMagnitudeText.getText().trim());
            } catch (NumberFormatException e) {
                return "Magnitude must be a number";
            }
        }
        if (fromText.getText().isBlank()) {
            return "Date time from must be popullated";
        }
        try {
            DateTimeUtil.parse(fromText.getText());
        } catch (Exception e) {
            return "Invalid format of date time from";
        }
        if (periodText.getText().isBlank()) {
            return "Period must be popullated";
        }
        try {
            Integer.parseInt(periodText.getText().trim());
        } catch (Exception e) {
            return "Invalid period";
        }
        if (periodUnitsStep.getSelectionIndex() == 4 && (step.getSelectionIndex() == 0 || step.getSelectionIndex() == 1)) {
            return "For year period the step may be only day, month and year";
        }

        return null;
    }

    public EphRequest getResultEphRequest() {
        return resultEphRequest;
    }

    public boolean isResultShowMagCheck() {
        return resultShowMagCheck;
    }

    public static Composite createGridCompositeIntoGridComposite(Composite parentComposite, int columns, Consumer<GridData> gdConsumer) {
        Composite panelComposite = new Composite(parentComposite, SWT.NONE);
        GridLayout gridLayout = new GridLayout(columns, false);
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        panelComposite.setLayout(gridLayout);
        GridData gridData = new GridData();
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }

    public void setCustomMessage(String message) {
        this.message = message;
    }
}
