package org.maren.spview.app.colsel;

import java.util.Comparator;

import org.maren.spview.shape.PointShape;
import org.maren.spview.tag.TagTypeUtil;

public class TagComparator implements Comparator<PointShape> {
    private String tagName;

    public TagComparator(String tagName) {
        super();
        this.tagName = tagName;
    }
    
    @Override
    public int compare(PointShape o1, PointShape o2) {
        Object val1 = o1.getTagValue(tagName);
        Object val2 = o2.getTagValue(tagName);
        if (val1 == null) {
            if (val2 != null) {
                return 1;
            }
            return 0;
        } else {
            if (val2 != null) {
                return TagTypeUtil.compare(val1, val2);
            } else {
                return -1;
            }
        }
    }
}
