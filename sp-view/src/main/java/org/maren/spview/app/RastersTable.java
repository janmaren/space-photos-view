package org.maren.spview.app;

import java.util.List;

import org.maren.spview.item.RasterItem;

public interface RastersTable {
    public void addOrUpdateRasters(List<RasterItem> rasterItems);

    public List<RasterItem> getEnabledRasters();
    
    public List<RasterItem> getAllRasters();

    public void selectRasterItem(RasterItem rasterItem, boolean additive);

    public void remove(RasterItem rasterItem);
}
