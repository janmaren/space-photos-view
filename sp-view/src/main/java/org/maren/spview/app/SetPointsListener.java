package org.maren.spview.app;

import cz.jmare.math.astro.RaDec;

import java.util.Map;

public interface SetPointsListener {
    void setPoints(Map<String, RaDec> points, String groupName);
}
