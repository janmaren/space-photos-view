package org.maren.spview.app;

import cz.jmare.math.astro.RaDec;

/**
 * Item usually from database
 */
public class CelestialObject {
    /**
     * Coordinates of star
     */
    private RaDec coords;
    
    /**
     * Set to which belong this star
     */
    private VectorSet vectorSet;

    /**
     * Magnitude
     * Nullable
     */
    private Double magnitude;

    /**
     * Constellation name
     * Nullable
     */
    private String constellationName;

    /**
     * Name
     * Nullable
     */
    private String name;
    
    private int hip;
    
    private Integer hd;
    
    private String spetralType;
    
    /**
     * Distance [pc]
     */
    private Double distance;
    
    public RaDec getCoords() {
        return coords;
    }

    public void setCoords(RaDec polar) {
        this.coords = polar;
    }

    public VectorSet getVectorSet() {
        return vectorSet;
    }

    public void setVectorSet(VectorSet vectorSet) {
        this.vectorSet = vectorSet;
    }

    public void setMagnitude(Double magnitude) {
        this.magnitude = magnitude;
    }

    public Double getMagnitude() {
        return magnitude;
    }

    public void setConstellationName(String constellationName) {
        this.constellationName = constellationName;
    }

    public String getConstellationName() {
        return constellationName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CelestialObject [coords=" + coords + ", constellationName=" + constellationName + ", name=" + name + "]";
    }

    public int getHip() {
        return hip;
    }

    public void setHip(int hip) {
        this.hip = hip;
    }

    public Integer getHd() {
        return hd;
    }

    public void setHd(Integer hd) {
        this.hd = hd;
    }

    public String getSpetralType() {
        return spetralType;
    }

    public void setSpetralType(String spetralType) {
        this.spetralType = spetralType;
    }
    
    /**
     * Distance [pc]
     * @return
     */
    public Double getDistance() {
        return distance;
    }

    /**
     * Distance [ly]
     * @return
     */
    public Double getDistanceLy() {
        return distance * 3.2615637769;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }
}
