package org.maren.spview.app;

import java.util.HashSet;
import java.util.Set;

import cz.jmare.math.geometry.entity.LineSegmentFloat;

public class Constellation {
    public String abbrev;
    
    public Set<LineSegmentFloat> lines = new HashSet<>();

    public String name;
    
    /**
     * RA of name in hours
     */
    public Double nameRa;
    
    /**
     * DEC of name in degrees
     */
    public Double nameDec;

    @Override
    public String toString() {
        return "Constellation [abbrev=" + abbrev + ", name=" + name + ", nameRa=" + nameRa + ", nameDec=" + nameDec
                + "]";
    }
    
    
}
