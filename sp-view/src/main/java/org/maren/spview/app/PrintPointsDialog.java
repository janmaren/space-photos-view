package org.maren.spview.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.StyledTextPrintOptions;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.Printer;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.maren.spview.shape.PointShape;
import org.maren.spview.util.DateTimeUtil;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.GonUtil;
import cz.jmare.data.util.HourUtil;
import cz.jmare.data.util.JulianUtil;
import cz.jmare.math.astro.RaDec;

public class PrintPointsDialog extends TitleAreaDialog {
    private final static int MAX_NUMBER = 200;

    private StyledText visibleText;

	private StringBuilder text;
    List<StyleRange> ranges;
    private List<PointShape> points;
    private Thread thread;

    private int originSize;

    private boolean truncated;


    public PrintPointsDialog(Shell shell, List<PointShape> points) {
    	super(shell);
        this.points = points;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Print Points");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
        GridLayout layout = new GridLayout();
        composite.setLayout(layout);


        visibleText = new StyledText(composite, SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER | SWT.WRAP | SWT.READ_ONLY);
        GridData textData = new GridData(SWT.FILL, SWT.FILL, true, true);
        visibleText.setLayoutData(textData);
        visibleText.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
		        if (e.stateMask == SWT.CTRL && e.keyCode == 'a') {
                    visibleText.selectAll();
		            e.doit = false;
		        }
			}
		});

        originSize = points.size();
        if (points.size() > MAX_NUMBER) {
            points = points.subList(0, MAX_NUMBER);
            truncated = true;
        }

        text = new StringBuilder();
        ranges = new ArrayList<>();

        setMessage("Please wait, generating...");
        Shell shell = getShell();
        thread = new Thread(() -> {
            populate();
            StyleRange[] rangesArr = ranges.toArray(new StyleRange[ranges.size()]);
            try {
                shell.getDisplay().asyncExec(() -> {
                    visibleText.setText(text.toString());
                    visibleText.setStyleRanges(rangesArr);
                    if (truncated) {
                        setMessage("Origin number " + originSize + " limited to " + MAX_NUMBER + " points", IMessageProvider.WARNING);
                    } else {
                        setMessage("");
                    }
                });
            } catch (Exception e) {
            }
        });
        thread.start();


        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(800, 900);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }
        thread.interrupt();

        PrintDialog dialog = new PrintDialog(getShell(), SWT.NULL);
        PrinterData printerData = dialog.open();
        Printer printer = new Printer(printerData);

        StyledTextPrintOptions styledTextPrintOptions = new StyledTextPrintOptions();
        Runnable runnable = visibleText.print(printer, styledTextPrintOptions);
        runnable.run();

        printer.dispose();

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, "Print", true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    private void populate() {
        for (PointShape point : points) {
            addTitleText("Name: ");
            add(point.getName());
            n();

            addTitleText("Group: ");
            add(point.getGroup().getName());
            n();

            addTitleText("Coordinates: ");
            add(ACooUtil.formatRaDecProfi(point.getRaDec()));
            n();

            addTitleText("Coordinates: ");
            add(format(point.getRaDec()));
            n();

            addTitleText("Description: ");
            add(point.getDescription());
            n();

            if (point.getJd() != null) {
                addTitleText("Date: ");
                add(JulianUtil.formatJulianRoundSeconds(point.getJd(), 4, DateTimeUtil.getOffsetHours()));
                n();
            }

            addTitleText("Tags: ");
            List<String> tagNames = new ArrayList<>(point.getTagNames());
            Collections.sort(tagNames);
            for (String tagName : tagNames) {
                Object tagValue = point.getTagValue(tagName);
                add(tagName + ":\u00a0" + tagValue);
                add("    ");
            }
            n();

            n();
        }
    }

    private void n() {
        add("\n");
    }
    private void add(String str) {
        if (str == null) {
            return;
        }
        text.append(str);
    }

    private void addTitleText(String str) {
        addStyledText(str, sr -> {
            sr.fontStyle = SWT.BOLD;
        });
    }

    private void addStyledText(String str, Consumer<StyleRange> consumer) {
        int start = text.length();
        text.append(str);
        StyleRange styleRange = new StyleRange();
        styleRange.start = start;
        styleRange.length = str.length();
        consumer.accept(styleRange);
        ranges.add(styleRange);
    }

    private String format(RaDec raDec) {
        double raHours = ACooUtil.degreesToHours(raDec.ra);
        String raDecStr = HourUtil.formatHoursMinutes(raHours, ":") + " " + GonUtil.formatDegreesMinutes(raDec.dec, true, ":");
        return raDecStr;
    }
}
