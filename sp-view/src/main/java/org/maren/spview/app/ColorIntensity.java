package org.maren.spview.app;

import org.eclipse.swt.graphics.RGB;

public class ColorIntensity {
    public static RGB greyIntensity(double intensity) {
        if (intensity > 1.0) {
            intensity = 1.0;
        }
        return new RGB((int) (255 * (1 - intensity)), (int) (255 * (1 - intensity)), (int) (255 * (1 - intensity)));
    }
}
