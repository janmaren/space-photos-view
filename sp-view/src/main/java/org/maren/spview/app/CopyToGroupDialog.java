package org.maren.spview.app;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.maren.spview.shape.GroupAtt;
import org.maren.spview.shape.GroupUtil;

import cz.jmare.swt.util.Create;

public class CopyToGroupDialog extends TitleAreaDialog {
    private Combo groupText;

    private Map<String, GroupAtt> groups;
    private GroupAtt currentGroup;
    private int size;

    private GroupAtt resultGroup;

    public CopyToGroupDialog(Shell parent, Map<String, GroupAtt> groups, GroupAtt currentGroup, int size) {
        super(parent);
        this.groups = groups;
        this.currentGroup = currentGroup;
        this.size = size;
    }

    protected Control createDialogArea(Composite parent) {
        setMessage("Select group to which to copy " + size + " points");
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Copy Points to Group");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Destination Group:");
        groupText = new Combo(composite, SWT.BORDER);
        List<String> itemsList = groups.entrySet().stream().map(e -> e.getValue().getName()).collect(Collectors.toList());
        Collections.sort(itemsList);
        groupText.setItems(itemsList.toArray(new String[itemsList.size()]));
        String currentGroupName = currentGroup.getName();
        for (int i = 0; i < itemsList.size(); i++) {
            if (itemsList.get(i).equals(currentGroupName)) {
                groupText.select(i);
                break;
            }
        }
        GridData grLayData = new GridData();
        grLayData.widthHint = 150;
        groupText.setLayoutData(grLayData);

        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(500, 300);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }

        String grp = groupText.getText();
        resultGroup = GroupUtil.getOrCreateGroup(groups, grp.trim());

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        return null;
    }
    
    public GroupAtt getResultGroup() {
        return resultGroup;
    }
}
