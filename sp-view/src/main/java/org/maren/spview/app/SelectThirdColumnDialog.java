package org.maren.spview.app;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.maren.spview.app.colsel.DateDescTagsFunction;
import org.maren.spview.app.colsel.RaDecComparator;
import org.maren.spview.app.colsel.SpecTypeComparator;
import org.maren.spview.app.colsel.TagComparator;
import org.maren.spview.app.colsel.TagFunction;
import org.maren.spview.shape.PointShape;
import org.maren.spview.tag.TagSupported;
import org.maren.spview.util.DateTimeUtil;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.JulianUtil;
import cz.jmare.swt.util.Create;

public class SelectThirdColumnDialog extends TitleAreaDialog {
    private Combo columnCombo;
    private Combo tagCombo;
    private final static String[] COL_SEL = {"<Tag> - predefined or custom tag", "Date + Description + Tags [unsortable]", "Description", "All Tags [unsortable]", "Date", "RA, DEC", "Spectral Type",  "Apparent Magnitude"};
    private Comparator<PointShape> resultComparator;
    private Function<PointShape, String> resultTextBuilder;
    private String resultColumnName;
    private String resultTagName;
    private Function<PointShape, String> initialTextBuilder;
    private String initialTagName;
    private final List<String> tagNames;
    private static Function<PointShape, String> DESC_TEXT_BUILDER = (ps) -> ps.getDescription() == null ? "" : ps.getDescription();
    private static Function<PointShape, String> DATE_TEXT_BUILDER = (ps) -> ps.getJd() == null ? "" : JulianUtil.formatJulianRoundSeconds(ps.getJd(), 1, DateTimeUtil.getOffsetHours());
    private static Function<PointShape, String> TAGS_TEXT_BUILDER = (ps) -> ps.getTagsString();
    private static Function<PointShape, String> DATEDESCTAGS_TEXT_BUILDER = new DateDescTagsFunction();
    private static Function<PointShape, String> RADEC_TEXT_BUILDER = (ps) -> ACooUtil.formatCoordsPublic(ps.getRaDec());

    private static Function<PointShape, String> SPECTYPE_TEXT_BUILDER =
            (ps) -> {
                Object obj = ps.getTagValue(TagSupported.SPECT.name());
                return obj != null ? obj.toString() : "";
            };

    public static Function<PointShape, String> DEFAULT_TEXT_BUILDER = DATEDESCTAGS_TEXT_BUILDER;
    public static String DEFAULT_COLUMN_NAME = COL_SEL[1];
    private static int AMAG_TAG_INDEX = COL_SEL.length - 1; // last one is apparent magnitude

    private final static List<Function<PointShape, String>> TEXT_BUILDERS = List.of(
            DEFAULT_TEXT_BUILDER,
            DESC_TEXT_BUILDER,
            TAGS_TEXT_BUILDER, 
            DATE_TEXT_BUILDER,
            RADEC_TEXT_BUILDER,
            SPECTYPE_TEXT_BUILDER
            );
    private final static List<Comparator<PointShape>> COMPARATORS = new ArrayList<>();
    static {
        COMPARATORS.add(null);
        COMPARATORS.add((ps1, ps2) -> ps1.getDescription() == null ? (ps2.getDescription() != null ? 1 : 0) : (ps2.getDescription() != null ? ps1.getDescription().compareTo(ps2.getDescription()) : -1));
        COMPARATORS.add(null);
        COMPARATORS.add((ps1, ps2) -> ps1.getJd() == null ? (ps2.getJd() != null ? 1 : 0) : (ps2.getJd() != null ? ps1.getJd().compareTo(ps2.getJd()) : -1));
        COMPARATORS.add(new RaDecComparator());
        COMPARATORS.add(new SpecTypeComparator());
    }
    
    public SelectThirdColumnDialog(Shell parent, Function<PointShape, String> initialTextBuilder, String tagName, List<String> tagNames) {
        super(parent);
        this.initialTextBuilder = initialTextBuilder;
        this.initialTagName = tagName;
        this.tagNames = tagNames;
    }

    protected Control createDialogArea(Composite parent) {
        setMessage("Select data for the third column");
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Third Column Selection");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Value for Column");
        columnCombo = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
        columnCombo.setItems(COL_SEL);
        columnCombo.addSelectionListener(SelectionListener.widgetSelectedAdapter((e) -> {
            tagCombo.setEnabled(columnCombo.getSelectionIndex() == 0); 
        }));
        GridData grLayData = new GridData();
        grLayData.widthHint = 150;
        columnCombo.setLayoutData(grLayData);
        columnCombo.select(1);
        columnCombo.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            int selectionIndex = columnCombo.getSelectionIndex();
            if (selectionIndex > 0 && selectionIndex != AMAG_TAG_INDEX) {
                Comparator<PointShape> comparator = COMPARATORS.get(selectionIndex - 1);
                if (comparator == null) {
                    setMessage("Note: such data are not sortable");
                } else {
                    setMessage("");
                }
            } else {
                setMessage("");
            }
        }));
        
        Create.label(composite, "Tag");
        tagCombo = new Combo(composite, SWT.BORDER);
        tagCombo.setItems(tagNames.toArray(new String[tagNames.size()]));
        tagCombo.setEnabled(false);
        GridData grTData = new GridData();
        grTData.widthHint = 250;
        tagCombo.setLayoutData(grTData);
        tagCombo.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            tagCombo.setSelection(new Point(0, 0));
        }));
        
        setCombos();
        
        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(500, 320);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }
        
        int selectionIndex = columnCombo.getSelectionIndex();
        if (selectionIndex == 0 || selectionIndex == AMAG_TAG_INDEX) {
            String tagName;
            if (selectionIndex == AMAG_TAG_INDEX) {
                tagName = "AMAG";
            } else {
                tagName = tagCombo.getText().toUpperCase();
                if (tagCombo.getSelectionIndex() != -1) {
                    tagName = tagNames.get(tagCombo.getSelectionIndex());
                }
            }
            resultComparator = new TagComparator(tagName);
            resultTextBuilder = new TagFunction(tagName);
            resultColumnName = "<Tag " + tagName + ">";
            this.resultTagName = tagName;
        } else {
            resultComparator = COMPARATORS.get(selectionIndex - 1);
            resultTextBuilder = TEXT_BUILDERS.get(selectionIndex - 1);
            resultColumnName = "<" + COL_SEL[selectionIndex] + ">";
        }
        
        super.buttonPressed(buttonId);
    }
    
    protected String valid() {

        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public Comparator<PointShape> getResultComparator() {
        return resultComparator;
    }

    public Function<PointShape, String> getResultTextBuilder() {
        return resultTextBuilder;
    }

    public String getResultColumnName() {
        return resultColumnName;
    }
    
    public String getResultTagName() {
        return resultTagName;
    }

    public void setCombos() {
        if (initialTextBuilder != null) {
            boolean found = false;
            for (int i = 0; i < TEXT_BUILDERS.size(); i++) {
                Function<PointShape, String> function = TEXT_BUILDERS.get(i);
                if (function == initialTextBuilder) {
                    columnCombo.select(i + 1);
                    found = true;
                    break;
                }
            }
            if (!found) {
                columnCombo.select(0);
                tagCombo.setEnabled(true);
            }
        }
        if (initialTagName != null) {
            tagCombo.setText(initialTagName);
            TagSupported[] values = TagSupported.values();
            for (int i = 0; i < values.length; i++) {
                if (initialTagName.equals(values[i].name())) {
                    tagCombo.select(i);
                    break;
                }
            }
        }
    }
}
