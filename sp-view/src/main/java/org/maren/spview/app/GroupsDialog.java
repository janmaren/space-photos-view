package org.maren.spview.app;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.maren.spview.shape.GroupAtt;
import org.maren.spview.shape.GroupUtil;
import org.maren.spview.shape.Shape;

import cz.jmare.logging.LoggerSlf4jOptional;
import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;
import cz.jmare.swt.color.ColorUtil;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.SComposite;

public class GroupsDialog extends TitleAreaDialog {
    private Map<String, GroupAtt> groups;
    private final String preselectedgroupName;
    private final List<? extends Shape> shapes;

    private Combo groupCombo;
    private Text addText;
    private Canvas canvas;

    public GroupsDialog(Shell parent, Map<String, GroupAtt> groups, String preselectedgroupName, List<? extends Shape> shapes) {
        super(parent);
        this.groups = groups;
        this.preselectedgroupName = preselectedgroupName;
        this.shapes = shapes;

        ImageUtil.put("list-add", "/image/list-add.png");
        ImageUtil.put("remove", "/image/remove.png");
        ImageUtil.put("pencil", "/image/pencil.png");
    }

    protected Control createDialogArea(Composite parent) {
        setTitle("Manage Groups");
        setMessage("You can add, delete or update a group");
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 3;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Group:");
        groupCombo = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
        List<String> itemsList = populateGroupNames();
        if (preselectedgroupName != null) {
            int i = itemsList.indexOf(preselectedgroupName);
            if (i != -1) {
                groupCombo.select(i);
            }
        } else {
            groupCombo.select(0);
        }
        GridData grData = new GridData();
        grData.widthHint = 150;
        groupCombo.setLayoutData(grData);
        groupCombo.addSelectionListener(SelectionListener.widgetSelectedAdapter((e) -> {
            applyColor();
        }));
        Composite selGroopTools = SComposite.createGridCompositeIntoGridComposite(composite, 2);

        Create.imageButton(selGroopTools, ImageUtil.get("remove"), "Remove", () -> {
            removeGroup();
        });
        Create.imageButton(selGroopTools, ImageUtil.get("pencil"), "Rename", () -> {
            renameGroup();
        });

        Create.label(composite, "New Group:");
        addText = Create.text(composite, "");
        addText.setLayoutData(grData);
        Composite neGroopTools = SComposite.createGridCompositeIntoGridComposite(composite, 2);
        Create.imageButton(neGroopTools, ImageUtil.get("list-add"), "Add", () -> {
            addGroup();
        });

        Create.label(composite, "Color:");
        canvas = new Canvas(composite, SWT.BORDER);
        GridData canvasData = new GridData();
        canvasData.horizontalSpan = 2;
        canvasData.heightHint = 50;
        canvasData.horizontalAlignment = SWT.FILL;
        canvas.setLayoutData(canvasData);
        canvas.setBackground(getShell().getDisplay().getSystemColor(SWT.COLOR_WHITE));
        canvas.addMouseListener(MouseListener.mouseUpAdapter(e -> {
            selectColor();
        }));
        Create.label(composite);

        Label label = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
        GridData layoutData = new GridData(GridData.FILL_HORIZONTAL);
        layoutData.horizontalSpan = 2;
        label.setLayoutData(layoutData);

        Create.label(composite, "Actions:");
        Link link = new Link(composite, SWT.BOLD);
        link.setText("<a>Delete All Unused</a>");
        link.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            int count = GroupUtil.removeUnusedGroups(shapes, groups);
            populateGroupNames();
            setMessage("Removed " + count + " groups");
        }));

        applyColor();

        return composite;
    }

    private List<String> populateGroupNames() {
        List<String> itemsList = groups.entrySet().stream().map(e -> e.getValue().getName()).collect(Collectors.toList());
        Collections.sort(itemsList);
        String[] groupNames = itemsList.toArray(new String[itemsList.size()]);
        groupCombo.setItems(groupNames);
        return itemsList;
    }

    private void selectColor() {
        int selectionIndex = groupCombo.getSelectionIndex();
        if (selectionIndex == -1) {
            return;
        }
        String name = groupCombo.getItem(selectionIndex);

        ColorDialog colorDialog = new ColorDialog(getShell());
        colorDialog.setText("Color for group '" + name + "'");
        GroupAtt group = groups.get(name);
        if (group == null) {
            LoggerSlf4jOptional.logError("Not found group " + name + " in groups but found in combo");
            groups.put(name, group = GroupUtil.createGroup(name, groups));
        }
        RGBEntity rgbEntity = new RGBEntity(group.getColor());
        colorDialog.setRGB(new RGB(rgbEntity.red, rgbEntity.green, rgbEntity.blue));
        RGB newRgb = colorDialog.open();
        if (newRgb != null) {
            String color = RGBEntUtil.toRGBHexString(new RGBEntity(newRgb.red, newRgb.green, newRgb.blue));
            group.setColor(color);
            applyColor();
        }
    }

    private void removeGroup() {
        int selectionIndex = groupCombo.getSelectionIndex();
        if (selectionIndex == -1) {
            return;
        }
        String name = groupCombo.getItem(selectionIndex);
        if (name.isBlank()) {
            setErrorMessage("Empty (default) group can't be deleted");
            return;
        }

        long count = shapes.stream().filter(s -> s.getGroup().getName().equals(name)).count();
        if (count > 0) {
            MessageBox messageBox = new MessageBox(getShell(), SWT.YES | SWT.NO | SWT.CANCEL| SWT.ICON_WARNING);
            messageBox.setMessage("There are " + count + " shapes using this group\nDo you want to really (YES) delete the group?\nItems will not be deleted but rather an empty group will be assigned");
            int result = messageBox.open();
            if (result == SWT.YES) {
                GroupAtt defaultGroup = groups.get(GroupUtil.DEFAULT_GROUP_NAME);
                if (defaultGroup == null) {
                    defaultGroup = GroupUtil.createGroup(GroupUtil.DEFAULT_GROUP_NAME, groups);
                    groupCombo.add("");
                    groups.put(GroupUtil.DEFAULT_GROUP_NAME, defaultGroup);
                }
                GroupAtt defaultGroupFinal = defaultGroup;
                shapes.stream().forEach((Shape sh) -> {
                    if (sh.getGroup().getName().equals(name)) {
                        sh.setGroup(defaultGroupFinal);
                    }
                });
            } else {
                return;
            }
        }

        groups.remove(name);
        groupCombo.remove(selectionIndex);
        groupCombo.select(selectionIndex < groupCombo.getItemCount() ? selectionIndex : selectionIndex - 1);
        applyColor();
    }

    private void renameGroup() {
        int selectionIndex = groupCombo.getSelectionIndex();
        if (selectionIndex == -1) {
            return;
        }
        String name = groupCombo.getItem(selectionIndex);
        if (name.isBlank()) {
            setErrorMessage("Empty (default) group can't be deleted");
            return;
        }

        long count = shapes.stream().filter(s -> s.getGroup().getName().equals(name)).count();
        if (count > 0) {
            InputDialog renameGroup = new InputDialog(getShell(), "Rename Group", "", name, null);
            if (renameGroup.open() == Window.OK) {
                String value = renameGroup.getValue();
                GroupAtt groupAtt = groups.get(name);
                groupAtt.setName(value);
                groups.remove(name);
                groups.put(value, groupAtt);
                groupCombo.remove(name);
                groupCombo.add(value);
                groupCombo.select(groupCombo.getItemCount() - 1);
            }
        }
    }

    private void addGroup() {
        String text = addText.getText();
        if (text.isBlank()) {
            setErrorMessage("Name is blank");
            return;
        }
        String name = text.trim();
        if (groups.containsKey(name)) {
            setErrorMessage("Name already exists");
            return;
        }
        GroupAtt groupAtt = GroupUtil.createGroup(name, groups);
        groupCombo.add(name);
        groups.put(name, groupAtt);
        groupCombo.select(groupCombo.getItemCount() - 1);
        addText.setText("");
        applyColor();
    }

    private void applyColor() {
        int selectionIndex = groupCombo.getSelectionIndex();
        if (selectionIndex == -1) {
            return;
        }
        String name = groupCombo.getItem(selectionIndex);
        GroupAtt groupAtt = groups.get(name);
        String color = groupAtt.getColor();
        Color color1 = ColorUtil.getColor(color);
        GC gc = new GC(canvas);
        canvas.setBackground(color1);
        canvas.drawBackground(gc, 0, 0, 10, 10);
        gc.dispose();
    }

    @Override
    protected Point getInitialSize() {
        return new Point(500, 500);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CLOSE_ID) {
            super.buttonPressed(IDialogConstants.CANCEL_ID);
            return;
        }
    }

    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.CLOSE_ID, IDialogConstants.CLOSE_LABEL, true);
    }
}
