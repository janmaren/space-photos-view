package org.maren.spview.app;

import cz.jmare.swt.util.Create;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.maren.spview.shape.GroupAtt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DeletePointsDialog extends TitleAreaDialog {
    private Combo groupToKeepCombo;

    private Combo groupToDeleteCombo;

    private Map<String, GroupAtt> groups;
    private String currentGroup;

    private GroupAtt resultGroupToKeep;

    private GroupAtt resultGroupToDelete;

    /**
     * @param groupToSelect nullable
     */
    public DeletePointsDialog(Shell parent, Map<String, GroupAtt> groups, String groupToSelect) {
        super(parent);
        this.groups = groups;
        this.currentGroup = groupToSelect;
    }

    protected Control createDialogArea(Composite parent) {
        setMessage("You can select a group to keep or a group to delete or when ignoring the combos then all points deleting");
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Deleting Points");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Keep Group (not deleting):");
        groupToKeepCombo = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
        List<String> itemsLi = groups.entrySet().stream().map(e -> e.getValue().getName()).collect(Collectors.toList());
        Collections.sort(itemsLi);
        List<String> itemsList = new ArrayList<>();
        itemsList.add("<IGNORE>");
        itemsList.addAll(itemsLi);
        groupToKeepCombo.setItems(itemsList.toArray(new String[itemsList.size()]));
        groupToKeepCombo.select(0);
        if (currentGroup != null && !"".equals(currentGroup.trim())) {
            for (int i = 1; i < itemsList.size(); i++) {
                groupToKeepCombo.select(i);
                if (itemsList.get(i).equals(currentGroup)) {
                    break;
                }
            }
        }
        GridData grLayData = new GridData();
        grLayData.widthHint = 150;
        groupToKeepCombo.setLayoutData(grLayData);


        Create.label(composite, "Group to Delete:");
        groupToDeleteCombo = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
        groupToDeleteCombo.setItems(itemsList.toArray(new String[itemsList.size()]));
        groupToDeleteCombo.select(0);

        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(580, 350);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }

        resultGroupToKeep = groups.get(groupToKeepCombo.getText());

        resultGroupToDelete = groups.get(groupToDeleteCombo.getText());

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        return null;
    }
    
    public GroupAtt getResultGroupToKeep() {
        return resultGroupToKeep;
    }

    public GroupAtt getResultGroupToDelete() {
        return resultGroupToDelete;
    }
}
