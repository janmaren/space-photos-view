package org.maren.spview.app;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.maren.spview.shape.GroupAtt;
import org.maren.spview.util.PointsIndex;

import cz.jmare.swt.util.Create;

public class MergePointsDialog extends TitleAreaDialog {
    private Combo destGroupCombo;
    private Combo sourceGroupCombo;
    private Text toleranceText;

    private Map<String, GroupAtt> groups;
    
    private GroupAtt resultSourceGroup;
    private GroupAtt resultDestinationGroup;
    private double resultTolerance;
    String[] groupNames;
    
    public MergePointsDialog(Shell parent, Map<String, GroupAtt> groups) {
        super(parent);
        this.groups = groups;
        List<String> itemsList = groups.entrySet().stream().map(e -> e.getValue().getName()).collect(Collectors.toList());
        Collections.sort(itemsList);
        groupNames = itemsList.toArray(new String[itemsList.size()]);
    }

    protected Control createDialogArea(Composite parent) {
        setMessage("Merging near source points into destination.\nIt can find for each destination point a source one and some data like tags are transferred there.");
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Merge");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);
        

        Create.label(composite, "Destination group");
        destGroupCombo = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
        destGroupCombo.setItems(groupNames);
        destGroupCombo.select(0);
        
        Create.label(composite, "Source group");
        sourceGroupCombo = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
        sourceGroupCombo.setItems(groupNames);
        sourceGroupCombo.select(0);
        
        Create.label(composite, "Tolerance [arcseconds]");
        toleranceText = Create.text(composite, "", 200);
        toleranceText.setText("10");

        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(580, 480);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }
        
        resultDestinationGroup = groups.get(groupNames[destGroupCombo.getSelectionIndex()]);
        resultSourceGroup = groups.get(groupNames[sourceGroupCombo.getSelectionIndex()]);
        resultTolerance = Double.parseDouble(toleranceText.getText().trim());

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        String toleranceStr = toleranceText.getText().trim();
        if (toleranceStr.isEmpty()) {
            return "Tolerance must be populated";
        }
        try {
            double parseDouble = Double.parseDouble(toleranceStr);
            if (parseDouble < 0) {
                return "Tolerance must be positive";
            }
            double maxAllowedTolerance = new PointsIndex().maxAllowedTolerance();
            double ars = maxAllowedTolerance * 3600;
            if (parseDouble > ars) {
                return "Maximum for tolerance may be " + ars + " arcseconds";
            }
        } catch (NumberFormatException e) {
            return "Tolerance must be a valid number";
        }
        if (destGroupCombo.getSelectionIndex() == sourceGroupCombo.getSelectionIndex()) {
            return "Source and destination groups must be different";
        }
        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public GroupAtt getResultSourceGroup() {
        return resultSourceGroup;
    }

    public GroupAtt getResultDestinationGroup() {
        return resultDestinationGroup;
    }

    public double getResultTolerance() {
        return resultTolerance;
    }
}
