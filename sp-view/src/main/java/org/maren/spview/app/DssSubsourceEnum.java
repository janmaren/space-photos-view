package org.maren.spview.app;

public enum DssSubsourceEnum {
    DSS_1(""), DSS_2_RED(""), DSS_2_BLUE(""), DSS_2_INFRARED("");
    
    private String name;

    private DssSubsourceEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
