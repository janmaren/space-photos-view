package org.maren.spview.app.colsel;

import org.maren.spview.shape.PointShape;
import org.maren.spview.tag.TagSupported;

import java.util.Comparator;

public class SpecTypeComparator implements Comparator<PointShape> {
    @Override
    public int compare(PointShape o1, PointShape o2) {
        Object tagValue1 = o1.getTagValue(TagSupported.SPECT.name());
        Object tagValue2 = o2.getTagValue(TagSupported.SPECT.name());
        if (tagValue1 == null) {
            if (tagValue2 == null) {
                return 0;
            }
            return 1;
        } else {
            if (tagValue2 == null) {
                return -1;
            }
            return tagValue1.toString().compareTo(tagValue2.toString());
        }
    }
}
