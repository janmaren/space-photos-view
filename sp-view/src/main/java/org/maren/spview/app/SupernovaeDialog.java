package org.maren.spview.app;

import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.SComposite;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.time.LocalDate;
import java.time.temporal.ChronoField;

public class SupernovaeDialog extends TitleAreaDialog {
    private Combo maxMagCombo;
    private Combo stringText;

    private Text periodText;

    private Combo periodUnitsCombo;

    private String resultString;
    private Double resultMaxMagnitude;

    private Integer resultPeriod;

    private String resultPeriodUnit;

    private boolean resultShowMagCheck;

    private String[] UNITS_TXT = {"Days", "Months", "Years"};

    private String[] UNITS_RES = {"days", "months", "years"};
    private Button showMagCheck;

    public SupernovaeDialog(Shell parent) {
        super(parent);
    }

    protected Control createDialogArea(Composite parent) {
        setMessage("Download supernovae by criteria");
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Download Supernovae");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        Create.label(composite, "Name substring:");
        stringText = Create.combo(composite, SWT.BORDER, c -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 150;
            c.setLayoutData(layoutData);
            c.setToolTipText("Name or part of name like 'SN2023' to be searched");
        });
        LocalDate now = LocalDate.now();
        int year = now.get(ChronoField.YEAR);
        stringText.setItems("SN" + year, "AT" + year);
        stringText.setText("SN" + year);
        stringText.setToolTipText("Prefix for name of supernovae. Supernovae start with SN but candidates start with AT.\nYou can also enter whole name like SN2022yau");

        Create.label(composite, "Max discovery magnitude:");
        maxMagCombo = new Combo(composite, SWT.BORDER);
        maxMagCombo.setItems(new String[] {"18", "17", "16", "15"});
        GridData dgData = new GridData();
        dgData.widthHint = 150;
        maxMagCombo.setLayoutData(dgData);

        Create.label(composite, "Last period:");
        Composite perComp = SComposite.createGridCompositeIntoGridComposite(composite, 2);
        periodText = Create.text(perComp, "1", t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 60;
            t.setLayoutData(layoutData);
            t.setToolTipText("Last period to search");
        });
        periodUnitsCombo = Create.combo(perComp, SWT.BORDER | SWT.READ_ONLY, c -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 60;
            c.setLayoutData(layoutData);
            c.setToolTipText("Units");
        });
        periodUnitsCombo.setItems(UNITS_TXT);
        periodUnitsCombo.select(1);

        StyledText info = new StyledText(composite, SWT.WRAP | SWT.READ_ONLY);
        info.setText("This is a quick way how to download supernovae from TNS.\nThey can be also loaded using import points dialog\nwhich accepts any data downloaded from tns server.");
        GridData layoutData = new GridData();
        layoutData.horizontalSpan = 2;
        layoutData.heightHint = 70;
        info.setLayoutData(layoutData);

        Create.label(composite, "Show magnitude");
        showMagCheck = Create.checkButton(composite, false);

        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(450, 450);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }
        
        resultString = stringText.getText().trim();
        if (!maxMagCombo.getText().isBlank()) {
            resultMaxMagnitude = Double.parseDouble(maxMagCombo.getText().trim());
        }

        if (!periodText.getText().isBlank()) {
            resultPeriod = Integer.parseInt(periodText.getText().trim());
            resultPeriodUnit = UNITS_RES[periodUnitsCombo.getSelectionIndex()];
        }

        resultShowMagCheck = showMagCheck.getSelection();

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        if (!maxMagCombo.getText().isBlank()) {
            try {
                Double.parseDouble(maxMagCombo.getText().trim());
            } catch (NumberFormatException e) {
                return "Invalid magnitude value";
            }
        }
        if (!periodText.getText().isBlank()) {
            try {
                Integer.parseInt(periodText.getText().trim());
            } catch (NumberFormatException e) {
                return "Invalid period value";
            }
        }
        return null;
    }
    
    public String getResultString() {
        return resultString;
    }

    public Double getResultMaxMagnitude() {
        return resultMaxMagnitude;
    }

    public Integer getResultPeriod() {
        return resultPeriod;
    }

    public String getResultPeriodUnit() {
        return resultPeriodUnit;
    }

    public boolean isResultShowMagCheck() {
        return resultShowMagCheck;
    }
}
