package org.maren.spview.app;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.http.RequestData;
import org.maren.spview.util.HttpUrlEncoderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.data.util.GonUtil;
import cz.jmare.data.util.HourUtil;
import cz.jmare.file.PathUtil;
import cz.jmare.http.HUrl;
import cz.jmare.math.astro.RahDec;

public class RetrieveDataUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(RetrieveDataUtil.class);

    private static final String[] TNS_KEYS = {"name", "discMagMax", "period", "periodUnits"};
    
    /**
     * Get fits file from eso dss or null when no such exists
     * @param destDir
     * @param replace
     * @param polarRaDec
     * @param survey 
     * @param height 
     * @param width 
     * @param suggestName 
     * @return
     * @throws IOException
     */
    public static String getEsoFitsFile(String destDir, boolean replace, RahDec polarRaDec, int width, int height, String survey, String suggestName) {
        String raHours = HourUtil.formatHours(polarRaDec.rah);
        String raDegrees = GonUtil.formatDegrees(polarRaDec.dec, true);
        String url = AppConfig.getInstance().getConfigStore().getValue("eso.dss.fits.url", null);
        url = url.replace("${ra}", URLEncoder.encode(raHours, StandardCharsets.ISO_8859_1));
        url = url.replace("${dec}", URLEncoder.encode(raDegrees, StandardCharsets.ISO_8859_1));
        url = url.replace("${width}", String.valueOf(width));
        url = url.replace("${height}", String.valueOf(height));
        url = url.replace("${survey}", String.valueOf(survey));

        String[] strCons = new String[1];
        Consumer<Map<String, String>> consumer = new Consumer<Map<String,String>>() {
            @Override
            public void accept(Map<String, String> map) {
                strCons[0] = map.get("content-disposition");
            }
        };
        
        byte[] byteArray;
        RequestData requestData = new RequestData();
        requestData.setConnectTimeout(12000);
        ConfigStore configStore = AppConfig.getInstance().getConfigStore();
        if (!configStore.getValue("proxy.host", "").isBlank()) {
            requestData.setProxy(configStore.getValue("proxy.host", null), configStore.getIntValue("proxy.port", 8080));
        }
        byteArray = HUrl.getByteArray(url, requestData, consumer);
        String contDisp = strCons[0];
        String name = extractName(contDisp, "file.fits");
        if (suggestName != null) {
            if (!suggestName.endsWith(".fits")) {
                name = suggestName + ".fits";
            } else {
                name = suggestName;
            }
        }
        Path path = Paths.get(destDir);
        path = path.resolve(name);
        if (!replace) {
            path = findUniqueName(path);
        }
        try {
            Files.write(path, byteArray);
        } catch (IOException e) {
            throw new RuntimeException("Unable to save to " + path, e);
        }
        return path.toString();
    }

    public static String extractName(String contentDisposition, String defaultName) {
        String name = defaultName;
        if (contentDisposition != null) {
            String[] split = contentDisposition.split(";");
            if (split.length > 1) {
                String filenameEq = split[1];
                String[] split2 = filenameEq.split("=");
                if (split2.length > 1) {
                    String nam = split2[1];
                    if (nam.startsWith("\"")) {
                        nam = nam.substring(1);
                    }
                    if (nam.endsWith("\"")) {
                        nam = nam.substring(0, nam.length() - 1);
                    }
                    name = nam;
                }
            }
        }
        return name;
    }

    public static Path findUniqueName(Path suggestedPath) {
        String[] parseFullPathSuffix = PathUtil.parseFullPathSuffix(suggestedPath.getFileName().toString());
        Path directory = suggestedPath.getParent();
        int i = 0;
        Path path = suggestedPath;
        while (Files.exists(path)) {
            path = directory.resolve(parseFullPathSuffix[0] + "(" + i + ")" + parseFullPathSuffix[1]);
            i++;
        }
        return path;
    }

    public static Path getTnsFile(Path destDir, boolean replace) {
        return getTnsFile(destDir, null, replace);
    }

    public static Path getTnsFile(Path destDir, Map<String, String> keyValues, boolean replace) {
        AppConfig instance = AppConfig.getInstance();
        ConfigStore configStore = instance.getConfigStore();
        String url = configStore.getValue("tns.last.novas.url", null);

        url = getUrlReplacePlaceholders(keyValues, url, TNS_KEYS);
        String[] strCons = new String[1];
        Consumer<Map<String, String>> consumer = new Consumer<Map<String,String>>() {
            @Override
            public void accept(Map<String, String> map) {
                strCons[0] = map.get("content-disposition");
            }
        };

        RequestData requestData = new RequestData();
        requestData.addHeaderItem("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:105.0) Gecko/20100101 Firefox/105.0");
        requestData.setConnectTimeout(7000);
        if (!configStore.getValue("proxy.host", "").isBlank()) {
            requestData.setProxy(configStore.getValue("proxy.host", null), configStore.getIntValue("proxy.port", 8080));
        }
        LOGGER.debug("Calling URL: {}", url);
        byte[] byteArray = HUrl.getByteArray(url, requestData, consumer);
        LOGGER.debug("Called, byteArraySize=" + (byteArray == null ? "null" : byteArray.length));
        String contDisp = strCons[0];
        String name = extractName(contDisp, "file.csv");
        Path path = destDir.resolve(name);
        if (!replace) {
            path = findUniqueName(path);
        }
        try {
            Files.write(path, byteArray);
        } catch (IOException e) {
            String[] strings = PathUtil.parseFullPathSuffix(path.toString());
            try {
                File file = File.createTempFile(strings[0], strings[1]);
                file.deleteOnExit();
                path = file.toPath();
                Files.write(path, byteArray);
            } catch (IOException ex) {
                throw new RuntimeException("Unable to save to " + path, ex);
            }
        }
        return path;
    }

    private static String getUrlReplacePlaceholders(Map<String, String> keyValues, String url, String[] keys) {
        for (String key : keys) {
            String value = "";
            String s = keyValues.get(key);
            if (s != null) {
                value = HttpUrlEncoderUtil.encodeURLComponent(s);
            }
            url = url.replace("${" + key + "}", value);
        }
        return url;
    }

    public static String getSimbadContent(String queryName) {
        RequestData requestData = new RequestData();
        requestData.addHeaderItem("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:105.0) Gecko/20100101 Firefox/105.0");
        ConfigStore configStore = AppConfig.getInstance().getConfigStore();
        if (!configStore.getValue( "proxy.host", "").isBlank()) {
            requestData.setProxy(configStore.getValue("proxy.host", null),
                    configStore.getIntValue("proxy.port", 8080));
        }
        String url = getUrlReplacePlaceholders(
                Map.of("queryName", queryName), configStore.getValue("simbad.url", null),
                new String[] {"queryName"});
        LOGGER.debug("Calling URL: {}", url);
        return HUrl.getString(url, requestData);
    }

    public static void main(String[] args) {
//        RahDec polarRaDec = new RahDec("5:40:45.52 -1:56:33.25");
//        getEsoFitsFile("C:\\Users\\marencik\\", false, polarRaDec, 25, 25, "DSS1", null);
        try {
            Locale.setDefault(Locale.ENGLISH);
            AppConfig instance = AppConfig.getInstance();
            instance.init(".sp-view");
            String simbadContent = getSimbadContent("ngc 7822");
            System.out.println(simbadContent);
        } catch (Exception e) {
            System.out.println("Vyjimka---");
            e.printStackTrace();
            System.out.println("Vyjimka####");
        }
    }
}
