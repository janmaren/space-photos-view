package org.maren.spview.app;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import cz.jmare.util.VersionFetcher;
import org.maren.spview.util.StyledUtil;

import static org.maren.spview.util.StyledUtil.addBoldText;

/**
 * This class shows an about box, based on TitleAreaDialog
 */
public class AboutDialog extends TitleAreaDialog {
    private static String version;
    private StyledText contentText;
    private StyledText coordsText;

	/**
     * MyTitleAreaDialog constructor
     *
     * @param shell
     *            the parent shell
     */
    public AboutDialog(Shell shell) {
        super(shell);
    }

    /**
     * Closes the dialog box Override so we can dispose the image we created
     */
    public boolean close() {
        return super.close();
    }

    /**
     * Creates the dialog's contents
     *
     * @param parent
     *            the parent composite
     * @return Control
     */
    protected Control createContents(Composite parent) {
        Control contents = super.createContents(parent);

        // Set the title
        setTitle("Spheric View");
        populateByManifest();
        // Set the message
        setMessage("Version " + version, IMessageProvider.NONE);

        return contents;
    }

    /**
     * Creates the gray area
     *
     * @param parent
     *            the parent composite
     * @return Control
     */
    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);

        Composite composite = titleAreaComposite;

        GridLayout layout = new GridLayout();
//        layout.numColumns = 1;
//        layout.horizontalSpacing = 40;
        layout.marginLeft = 10;
        layout.marginRight = 10;
        composite.setLayout(layout);

        Label author = new Label(composite, SWT.PUSH);
        author.setText("Author : Jan Marencik");

        Label copyright = new Label(composite, SWT.PUSH);
        copyright.setText("Copyright: Jan Marencik");
        
        Label licence = new Label(composite, SWT.PUSH);
        licence.setText("License: GPL-2.0");

        GridData gdt = new GridData();
        gdt.grabExcessVerticalSpace = true;
        gdt.verticalAlignment = SWT.FILL;
        gdt.grabExcessHorizontalSpace = true;
        gdt.horizontalAlignment = SWT.FILL;
        
        final TabFolder tabFolder = new TabFolder(composite, SWT.NONE);
        tabFolder.setLayout(new GridLayout());
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        gridData.widthHint = 200;
        tabFolder.setLayoutData(gridData);

        TabItem contentTab = new TabItem(tabFolder, SWT.NONE);
        contentTab.setText("Introduction");
        
        contentText = new StyledText(tabFolder, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
        contentText.setLayoutData(gdt);
        
        contentTab.setControl(contentText);
        
        generateContentText();
        
        // coordinates
        TabItem coordsTab = new TabItem(tabFolder, SWT.NONE);
        coordsTab.setText("Coordinates");
        coordsTab.setToolTipText("About coordinates used in SP View - the formats");
        
        coordsText = new StyledText(tabFolder, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
        GridData gdCoords = gridData;
        gdCoords.grabExcessVerticalSpace = true;
        gdCoords.verticalAlignment = SWT.FILL;
        gdCoords.grabExcessHorizontalSpace = true;
        gdCoords.horizontalAlignment = SWT.FILL;
        coordsText.setLayoutData(gdCoords);
        
        coordsTab.setControl(coordsText);
        generateCoordsText();
        
        // fits
        TabItem fitsTab = new TabItem(tabFolder, SWT.NONE);
        fitsTab.setText("Fits");
        fitsTab.setToolTipText("List of predefined functions");

        
        coordsText = new StyledText(tabFolder, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
        GridData gd = gridData;
        gd.grabExcessVerticalSpace = true;
        gd.verticalAlignment = SWT.FILL;
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = SWT.FILL;
		coordsText.setLayoutData(gd);
        
		fitsTab.setControl(coordsText);
		generateFitsText();
//        
//
//        generateFunctionsText();
//        
//        
//		// constants
//        TabItem constantsTab = new TabItem(tabFolder, SWT.NONE);
//        constantsTab.setText("Constants");
//        constantsTab.setToolTipText("List of predefined constants");
//
//        
//        constantsText = new StyledText(tabFolder, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
//        constantsText.setLayoutData(gd);
//        
//        constantsTab.setControl(constantsText);
//        
//        generateConstantsText();
//        
//		// prefixes
//        TabItem suffixesTab = new TabItem(tabFolder, SWT.NONE);
//        suffixesTab.setText("Metric prefixes");
//        suffixesTab.setToolTipText("List of predefined metric prefixes, like m, k");
//
//        
//        prefixesText = new StyledText(tabFolder, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
//        prefixesText.setLayoutData(gd);
//        
//        suffixesTab.setControl(prefixesText);
//
//		
//        generateSuffixesText();
        

        return composite;
    }

    private void generateCoordsText() {
        coordsText.append("Application can manipulate with coordinates, defined as right ascension and declination. The coordinates of the middle "
                + "cross can be picked into text field, corrected and it's possible to go to the defined "
                + "coordinates with the middle cross.\n\n");
        coordsText.append("For both coordinates there is the only text field which provide easy copying and pasting. It's not exactly defined "
                + "the format of coordinates but it supports various variants like the Simbad web,\n");
        
        StyledUtil.addUnderlineBlueText(coordsText, "http://simbad.u-strasbg.fr/simbad/sim-fcoo\n");
        addBoldText(coordsText, "Examples of coordinates:\n");
        StyledUtil.addItalicText(coordsText, " 20 54 05.689 +37 01 17.38\r\n"
                + " 10:12:45.3-45:17:50\r\n"
                + " 15h17m-11d10m\r\n"
                + " 15h17+89d15\r\n"
                + " 275d11m15.6954s+17d59m59.876s\r\n"
                + " 12.34567h-17.87654d\r\n"
                + " 350.123456d-17.33333d <=> 350.123456 -17.33333 // degrees must be enabled\n" +
                " 10.5 63.5\n" +
                " 10:30.23 63:30.42\n" +
                " 10 30.23 63 30.42\n");
        addBoldText(coordsText, "Or with a comma between RA and DEC:\n");
        StyledUtil.addItalicText(coordsText, "20 54 05.689, +37 01 17.38\r\n"
                + " 10:12:45.3, -45:17:50\r\n"
                + " 15h17m, -11d10m\r\n"
                + " 15h17, +89d15\r\n"
                + " 275d11m15.6954s, +17d59m59.876s\r\n"
                + " 12.34567h, -17.87654d\r\n"
                + " 350.123456d, -17.33333d <=> 350.123456 -17.33333 // degrees must be enabled\n" +
                " 10.5, 63.5\n" +
                " 10:30.23, 63:30.42\n" +
                " 10 30.23, 63 30.42\n");
        coordsText.append("For the case with number like 350.123456 the degrees switch button must be pressed not to suppose it's a hour number.\n");
        coordsText.append("For the case with number like 350.123456d the degrees are used regardless the degrees button.\n");
    }

    private void generateContentText() {
        contentText.append("Tool for displaying images that appear at world coordinates.\n\n");
        
        addBoldText(contentText, "Coordinates RA and DEC\n");
        contentText.append("Result coordinates is nativelly the FK5 2000 system including embedded stars ");
        contentText.append("(embedded in this version but maybe custom ones will be possible to add in a future version).\n\n");
        
        addBoldText(contentText, "Embedded Stars\n");        
        contentText.append("Embedded stars are taken from ");
        StyledUtil.addUnderlineBlueText(contentText, "https://github.com/astronexus/HYG-Database");
        contentText.append(", the hygdata_v3.csv file is used.\n\n");
        
        addBoldText(contentText, "Raster Files\n");
        contentText.append("SP View can show images which have WCS defined in a concrete way.\n\n"
                + "Format FITS supports storing both images and WCS but for example bmp, png, jpeg or tiff formats don't. But still "
                + "such common picture formats can contain WCS using appropriate text file, which has "
                + "the same extension as picture file but with letter 'w' added, for example picture1.png together with "
                + "picture1.pngw. In this case only simple affine transformation can determine coordinates.\n\n");
        
        addBoldText(contentText, "Interoperability\n");
        StyledUtil.addUnderlinedText(contentText, "Browser");
        contentText.append(" - currently only Simbad (http://simbad.u-strasbg.fr/simbad) is supported. Coordinates are passed to the browser with the Simbad web page\n");
        StyledUtil.addUnderlinedText(contentText, "ESO DSS");
        contentText.append(" - fits file is downloaded from http://archive.eso.org/dss/dss using current position (cross) and showed. Zoom as needed to see it.\n");
        StyledUtil.addUnderlinedText(contentText, "Open in Associated Application");
        contentText.append(" - clicking on 'Open' in right files manager causes to open it in a default application like DS9\n");
        StyledUtil.addUnderlinedText(contentText, "Open in Dir in File Manager");
        contentText.append(" - clicking in right files manager causes to open the directory using default OS file manager\n");

        contentText.append("\n");
        contentText.append("TIP: download fits from https://mast.stsci.edu/portal/Mashup/Clients/Mast/Portal.html\n");

//        contentText.append("Supported image types are: bmp, png, jpeg, tiff.\n\n");
//        contentText.append("Coordinates for images are stored using affine transform.\n"
//                + "Affine transform needs elements A, B, C, D, E, F.\n"
//                + "Parameters of affine transform are stored in text file, which has exactly the "
//                + "same name as image file but with the letter 'w' in the end, for example: orion.pngw. Format "
//                + "of such file is same as ESRI use for GIS georeferencing, but to calculate "
//                + "the elements a tool is needed, Universe Images View can't do it in this version.\n");
//        addUnderlineText(contentText, "https://pro.arcgis.com/en/pro-app/latest/help/data/imagery/world-files-for-raster-datasets.htm\n\n");
    }
    
    private void generateFitsText() {
        coordsText.append("The Space Photos View supports loading fits files. It can process both FK4 and FK5 "
                + "images but regarding FK4 the result will not be as precious as expected because "
                + "a conversion to FK5 must be done using an aproximation function.\n\n");
        coordsText.append("The main source of WCS algorithm is contained in AstroImageJ, ");
        StyledUtil.addUnderlineBlueText(coordsText, "https://www.astro.louisville.edu/software/astroimagej/");
        coordsText.append(" and that's why this is also under GNU license not to limit copyright of origin authors.");
    }

    /**
     * Creates the buttons for the button bar
     *
     * @param parent
     *            the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
    }

    @Override
    protected Point getInitialSize() {
        return new Point(700, 800);
    }
    
    public static void populateByManifest() {
        if (version != null) {
            return;
        }
        InputStream manifestStream = null;
        try {
            Class<?> clazz = VersionFetcher.class;
            String className = clazz.getSimpleName() + ".class";
            String classPath = clazz.getResource(className).toString();
            if (classPath.startsWith("jar")) {
                String manifestPath = classPath.substring(0, classPath.indexOf("!") + 1) + "/META-INF/MANIFEST.MF";
                manifestStream = new URL(manifestPath).openStream();
            } else {
                manifestStream = clazz.getResourceAsStream("/META-INF/MANIFEST.MF");
            }
            Manifest manifest = new Manifest(manifestStream);
            Attributes attr = manifest.getMainAttributes();
            String impVersion = attr.getValue("Implementation-Version");
            version = impVersion;
            manifestStream.close();
        } catch (Exception e1) {
            return;
        } finally {
            if (manifestStream != null) {
                try {
                    manifestStream.close();
                } catch (IOException e) {
                }
            }
        }
    }
}
