package org.maren.spview.app.colsel;

import java.util.Comparator;

import org.maren.spview.shape.PointShape;

import cz.jmare.math.astro.RaDec;

public class RaDecComparator implements Comparator<PointShape> {
    @Override
    public int compare(PointShape o1, PointShape o2) {
        RaDec raDec1 = o1.getRaDec();
        RaDec raDec2 = o2.getRaDec();
        return (int) ((raDec1.ra * 1000000 + raDec1.dec) - (raDec2.ra * 1000000 + raDec2.dec));
    }
}
