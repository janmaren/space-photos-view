package org.maren.spview.app;

import java.io.IOException;

import org.eclipse.swt.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import cz.jmare.fits.image.DataTypeNotSupportedException;
import cz.jmare.fits.image.FITSImage;
import cz.jmare.fits.image.NoImageDataFoundException;
import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.swt.image.AwtSwtUtil;
import nom.tam.fits.FitsException;

public class ImageX {
    public static void main(String[] args) throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        Display display = new Display();
        Shell shell = new Shell(display, SWT.SHELL_TRIM | SWT.DOUBLE_BUFFERED);
        shell.setLayout(new FillLayout());
        
        FITSImage fitsImage = new FITSImage("C:\\Users\\marencik\\Downloads\\dss.03.47.00.0+24.07.00.0-m42.fits");
        ImageData imageData = AwtSwtUtil.convertToSWT(fitsImage);
        imageData.setPixel(10, 0, RGBEntUtil.rLowestToRGBInt(255, 0, 0));
        imageData.setPixel(10, 1, RGBEntUtil.rLowestToRGBInt(255, 0, 0));
        imageData.setPixel(10, 2, RGBEntUtil.rLowestToRGBInt(255, 0, 0));
        imageData.setPixel(19, 8, RGBEntUtil.rLowestToRGBInt(255, 0, 0));
        imageData.setPixel(19, 9, RGBEntUtil.rLowestToRGBInt(255, 0, 0));
        imageData.setPixel(19, 10, RGBEntUtil.rLowestToRGBInt(255, 0, 0));

        final Image image = new Image(display, imageData);
        ImageLoader imageLoader = new ImageLoader();
        imageLoader.data = new ImageData[] {image.getImageData()};
        imageLoader.save("C:/temp/Idea_PureWhite.png",SWT.IMAGE_PNG); 
        shell.addListener(SWT.Paint, new Listener() {
            public void handleEvent(Event e) {
                GC gc = e.gc;
                int x = 10, y = 10;
                gc.drawImage(image, x, y);
                gc.dispose();
            }
        });
        
        shell.addListener(SWT.MouseDoubleClick, (e) -> {
            System.out.println(e.x + ", " + e.y);
        });

        shell.setSize(600, 400);
        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }

        if (image != null && !image.isDisposed())
            image.dispose();
        display.dispose();
    }

}