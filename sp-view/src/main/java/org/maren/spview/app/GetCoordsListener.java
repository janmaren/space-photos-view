package org.maren.spview.app;

import cz.jmare.math.astro.RaDec;

public interface GetCoordsListener {
    RaDec onGetCentrum();

    void goCentreTo(RaDec raDec);
}
