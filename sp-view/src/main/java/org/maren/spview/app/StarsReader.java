package org.maren.spview.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import cz.jmare.math.astro.RaDec;
import cz.jmare.math.geometry.entity.LineSegmentFloat;

public class StarsReader {
    private static List<CelestialObject> objects = null;
    private static Collection<Constellation> constellations;
    
    public synchronized static List<CelestialObject> getCelestialObjects() {
        if (objects != null) {
            return objects;
        }
        initStarsPolarsInternal(null, null, null);
        return objects;
    }
    
    private static void initStarsPolarsInternal(Double upToMagnitude, List<String> withConstelNames, String withName) {
        VectorSet vectorSet = new VectorSet();
        vectorSet.setName("Default");
        String[][] stars2 = StarsReader.getStars();
        List<CelestialObject> stars = new ArrayList<CelestialObject>();
        for (String[] ds : stars2) {
            String hip = ds[1].trim();
            if ("".equals(hip)) {
                continue;
            }
            Double ra = Double.valueOf(ds[7].trim());
            Double dec = Double.valueOf(ds[8].trim());
            Double mag = Double.valueOf(ds[13].trim());
            if (upToMagnitude != null && mag > upToMagnitude) {
                continue;
            }
            if (withConstelNames != null) {
                boolean found = false;
                for (String withConstelName : withConstelNames) {
                    if (ds[5].contains(withConstelName)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    continue;
                }
            }
            if (withName != null) {
                if (!ds[6].contains(withName)) {
                    continue;
                }
            }
            RaDec polarDeg = RaDec.ofHoursDegs(ra, dec);
            CelestialObject star = new CelestialObject();
            star.setCoords(polarDeg);
            star.setVectorSet(vectorSet);
            star.setMagnitude(mag);
            star.setConstellationName("".equals(ds[5]) ? null : ds[5]);
            star.setName("".equals(ds[6]) ? null : ds[6]);
            star.setHip(Integer.valueOf(hip));
            if (!"".equals(ds[2])) {
                star.setHd(Integer.valueOf(ds[2]));
            }
            if (!"".equals(ds[15])) {
                star.setSpetralType(ds[15]);
            }
            if (!"".equals(ds[9])) {
                Double distance = Double.valueOf(ds[9]);
                if (distance < 10000) {
                    star.setDistance(distance);
                }
            }
            stars.add(star);
        }
        objects = stars;
    }
    
    private static String[][] getStars() {
        List<String[]> stars = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(StarsReader.class.getResourceAsStream("/hygdata_v3.txt")))) {
            String line;
            int i = 0;
            while ((line = br.readLine()) != null) {
                if (i++ < 2) {
                    continue;
                }
                String[] row = line.split(",");
                for (int j = 0; j < row.length; j++) {
                    row[j] = row[j].trim();
                }
                stars.add(row);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[][] res = new String[stars.size()][];
        for (int i = 0; i < stars.size(); i++) {
            String[] strings = stars.get(i);
            res[i] = strings;
        }
        return res;
    }
    
    public static synchronized Collection<Constellation> getConstellationsLater(int sleep) {
        if (constellations != null) {
            return constellations;
        }
        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            throw new IllegalStateException("interrupted");
        }
        if (objects == null) {
            initStarsPolarsInternal(null, null, null);
        }
        initConstellationsInternal();
        return constellations;
    }
    
    private static void initConstellationsInternal() {
        Map<String, String[]> mapNameTitle = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(StarsReader.class.getResourceAsStream("/constell-labels.txt")))) {
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                String[] row = line.split(",");
                mapNameTitle.put(row[1], row);
            }
        } catch (IOException e1) {
            throw new IllegalStateException("Unable to load constellations names", e1);
        }
        
        Map<String, Constellation> map = new HashMap<String, Constellation>();
        
        try (BufferedReader br = new BufferedReader(new InputStreamReader(StarsReader.class.getResourceAsStream("/constellationship.fab")))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("#")) {
                    continue;
                }
                String[] row = line.split(" ");
                for (int j = 0; j < row.length; j++) {
                    row[j] = row[j].trim();
                }
                String abbr = row[0];
                Constellation constellation = map.get(abbr);
                if (constellation == null) {
                    constellation = new Constellation();
                    String[] titlesSplit = mapNameTitle.get(abbr);
                    if (titlesSplit != null) {
                        constellation.name = titlesSplit[0];
                        constellation.nameRa = Double.parseDouble(titlesSplit[2]);
                        constellation.nameDec = Double.parseDouble(titlesSplit[3]);
                    }
                    constellation.abbrev = abbr;
                    map.put(abbr, constellation);
                }
                int size = row.length;//.valueOf(row[1]) * 2 + 2;
                for (int j = 1; j < size; j += 2) {
                    Integer hip1 = Integer.valueOf(row[j]);
                    Integer hip2 = Integer.valueOf(row[j + 1]);
                    Optional<CelestialObject> vectorItem1Opt = objects.stream().filter(sp -> sp.getHip() == hip1).findFirst();
                    Optional<CelestialObject> vectorItem2Opt = objects.stream().filter(sp -> sp.getHip() == hip2).findFirst();
                    if (vectorItem1Opt.isEmpty()) {
                        continue;
                    }
                    if (vectorItem2Opt.isEmpty()) {
                        continue;
                    }
                    CelestialObject vectorItem1 = vectorItem1Opt.get();
                    CelestialObject vectorItem2 = vectorItem2Opt.get();
                    constellation.lines.add(new LineSegmentFloat((float) vectorItem1.getCoords().ra, (float) vectorItem1.getCoords().dec, (float) vectorItem2.getCoords().ra, (float) vectorItem2.getCoords().dec));
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException("Unable to load constellations", e);
        }
        constellations = map.values();
    }
    
    public static synchronized Collection<Constellation> getConstellations() {
        if (constellations != null) {
            return constellations;
        }
        if (objects == null) {
            initStarsPolarsInternal(null, null, null);
        }
        initConstellationsInternal();
        return constellations;
    }
    
    public static void main(String[] args) {
        Collection<Constellation> constellations = getConstellations();
        for (Constellation constellation : constellations) {
            System.out.println(constellation);
        }
    }
}
