package org.maren.spview.app.colsel;

import java.util.function.Function;

import org.maren.spview.shape.PointShape;

public class TagFunction implements Function<PointShape, String> {
    private String tagName;

    public TagFunction(String tagName) {
        super();
        this.tagName = tagName;
    }

    @Override
    public String apply(PointShape pointShape) {
        Object object = pointShape.getTagValue(tagName);
        if (object == null) {
            return "";
        }
        return object.toString();
    }
}
