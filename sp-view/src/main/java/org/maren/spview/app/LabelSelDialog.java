package org.maren.spview.app;

import java.util.function.Function;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.maren.spview.app.colsel.TagFunction;
import org.maren.spview.shape.PointShape;
import org.maren.spview.util.DateTimeUtil;
import org.maren.spview.util.LabelFunctionHolder;

import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.SComposite;

public class LabelSelDialog extends TitleAreaDialog {
    private static Function<PointShape, String> NAME_BUILDER = (ps) -> ps.getName();
    private static Function<PointShape, String> DATETIME_BUILDER = (pointShape) -> pointShape.getJd() != null ?
            DateTimeUtil.formatAsDateTimeCutOff(pointShape.getJd()) : null;
    private static Function<PointShape, String> TIME_BUILDER = (pointShape) -> pointShape.getJd() != null ? DateTimeUtil.formatAsTimeCutOff(pointShape.getJd()) : null;

    private static Function<PointShape, String> AMAG_TAG_BUILDER = new TagFunction("AMAG");

    private static Function<PointShape, String> DAY_MONTH_MAG_BUILDER = (pointShape) -> {
        Object tagValue = pointShape.getTagValue("AMAG");
        String magStr = "";
        if (tagValue != null) {
            magStr = " [" + tagValue + "]";
        }
        return pointShape.getJd() != null ?
            DateTimeUtil.formatAsDayMonth(pointShape.getJd()) + magStr : null;};

    private LabelFunctionHolder labelFunctionHolder;
    private Button nameButton;
    private Button dateTimeButton;
    private Button timeButton;
    private Button amagButton;
    private Button dayAmagButton;

    public LabelSelDialog(Shell parent, LabelFunctionHolder labelFunctionHolder) {
        super(parent);
        this.labelFunctionHolder = labelFunctionHolder;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Label Field Selection");
        setMessage("Select field to be showed on the map");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        composite.setLayout(layout);

        Create.label(composite, "Label Field:");
        Composite comp = SComposite.createGridCompositeIntoGridComposite(composite, 5);
        nameButton = Create.radioButton(comp, false, "name");
        nameButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> onSelect(e)));
        dateTimeButton = Create.radioButton(comp, false, "date time");
        dateTimeButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> onSelect(e)));
        timeButton = Create.radioButton(comp, false, "time");
        timeButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> onSelect(e)));
        amagButton = Create.radioButton(comp, false, "apparent magnitude");
        amagButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> onSelect(e)));
        dayAmagButton = Create.radioButton(comp, false, "short date and mag");
        dayAmagButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> onSelect(e)));
        Function<PointShape, String> labelOutputer = labelFunctionHolder.getLabelFunction();
        if (labelOutputer == NAME_BUILDER) {
            nameButton.setSelection(true);
        } else if (labelOutputer == DATETIME_BUILDER) {
            dateTimeButton.setSelection(true);
        } else if (labelOutputer == DAY_MONTH_MAG_BUILDER) {
            dayAmagButton.setSelection(true);
        } else if (labelOutputer == TIME_BUILDER) {
            timeButton.setSelection(true);
        } else if (labelOutputer == AMAG_TAG_BUILDER) {
            amagButton.setSelection(true);
        } else {
            nameButton.setSelection(true);
        }

        return composite;
    }


    private void onSelect(SelectionEvent e) {
        Function<PointShape, String> labelOutputer = null;
        boolean containsTime = false;
        if (nameButton.getSelection()) {
            labelOutputer = NAME_BUILDER;
        } else if (dateTimeButton.getSelection()) {
            labelOutputer = DATETIME_BUILDER;
            containsTime = true;
        } else if (dayAmagButton.getSelection()) {
            labelOutputer = DAY_MONTH_MAG_BUILDER;
            containsTime = true;
        } else if (timeButton.getSelection()) {
            labelOutputer = TIME_BUILDER;
            containsTime = true;
        } else if (amagButton.getSelection()) {
            labelOutputer = AMAG_TAG_BUILDER;
        }
        labelFunctionHolder = new LabelFunctionHolder(labelOutputer);
        labelFunctionHolder.setContainsTime(containsTime);
        close();
    }

    public LabelFunctionHolder getLabelFunctionHolder() {
        return labelFunctionHolder;
    }

    @Override
    protected Point getInitialSize() {
        return new Point(650, 300);
    }

    @Override
    protected boolean isResizable() {
        return false;
    }

    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }
}
