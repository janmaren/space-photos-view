package org.maren.spview.app;

import org.maren.spview.shape.PointShape;

public interface PointShapeEditedListener {

    void editedShape(PointShape pointShape);
}
