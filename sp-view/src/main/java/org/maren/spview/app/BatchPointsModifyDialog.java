package org.maren.spview.app;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.maren.spview.shape.GroupAtt;
import org.maren.spview.shape.GroupUtil;
import org.maren.spview.shape.PointShape;

import cz.jmare.data.util.JulianUtil;
import cz.jmare.swt.util.Create;

public class BatchPointsModifyDialog extends TitleAreaDialog {
    private Combo groupText;
    private Text dateText;
    private Text descText;
    private Text tagsText;
    
    private Button groupButton;
    private Button dateButton;
    private Button descButton;
    private Button tagsButton;
    
    private Collection<PointShape> points;

    private Map<String, GroupAtt> groups;

    public BatchPointsModifyDialog(Shell parent, Collection<PointShape> pointItems, Map<String, GroupAtt> groups) {
        super(parent);
        this.points = pointItems;
        this.groups = groups;
    }

    protected Control createDialogArea(Composite parent) {
        setMessage("Setting attributes to " + points.size() + " points.");
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Edit Point");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        composite.setLayout(layout);

        groupButton = Create.checkButton(composite, false, "Group");
        groupText = new Combo(composite, SWT.BORDER);
        List<String> itemsList = GroupUtil.getGroupsNames(groups);
        Collections.sort(itemsList);
        groupText.setItems(itemsList.toArray(new String[itemsList.size()]));
        groupText.setEnabled(false);
        groupButton.addSelectionListener(SelectionListener.widgetSelectedAdapter((e) -> {
            groupText.setEnabled(groupButton.getSelection());
        }));
        GridData grLayData = new GridData();
        grLayData.widthHint = 150;
        groupText.setLayoutData(grLayData);
        
        dateButton = Create.checkButton(composite, false, "Date");
        dateText = Create.text(composite, "", 200);
        dateText.setEnabled(false);
        dateButton.addSelectionListener(SelectionListener.widgetSelectedAdapter((e) -> {
            dateText.setEnabled(dateButton.getSelection());
        }));
        
        descButton = Create.checkButton(composite, false, "Description");
        descText = Create.text(composite, "", 300);
        descText.setEnabled(false);
        descText.setToolTipText("A description about object to add");
        descButton.addSelectionListener(SelectionListener.widgetSelectedAdapter((e) -> {
            descText.setEnabled(descButton.getSelection());
        }));
        
        tagsButton = Create.checkButton(composite, false, "Tags");
        tagsText = Create.text(composite, "", 300);
        tagsText.setEnabled(false);
        tagsText.setToolTipText("Adding tags. Tag has format key:value and multiple tags are separated with semicolon");
        tagsButton.addSelectionListener(SelectionListener.widgetSelectedAdapter((e) -> {
            tagsText.setEnabled(tagsButton.getSelection());
        }));

        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(500, 480);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }
        
        Double jd = null;
        if (dateButton.getSelection() && !dateText.getText().isBlank()) {
            String dateStr = dateText.getText().trim();
            try {
                jd = Double.valueOf(dateStr);
            } catch (NumberFormatException e) {
                jd = JulianUtil.parseToJulian(dateStr);
            }
        }
        GroupAtt group = null;
        if (groupButton.getSelection()) {
            String grp = groupText.getText();
            group = GroupUtil.getOrCreateGroup(groups, grp);
        }

        for (PointShape pointShape : points) {
            if (groupButton.getSelection()) {
                pointShape.setGroup(group);
            }
            if (dateButton.getSelection()) {
                pointShape.setJd(jd);
            }
            if (tagsButton.getSelection()) {
                pointShape.applyTagsString(tagsText.getText());
            }
            if (descButton.getSelection()) {
                String description = pointShape.getDescription();
                if (description == null || description.isBlank()) {
                    pointShape.setDescription(descText.getText());
                } else {
                    pointShape.setDescription(description + "; " + descText.getText());
                }
            }
        }
        
        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        String dateStr = dateText.getText().trim();
        if (dateButton.getSelection()) {
            if (!dateStr.isBlank()) {
    			try {
    				Double.valueOf(dateStr);
    			} catch (NumberFormatException e) {
    				try {
    				    JulianUtil.parseToJulian(dateStr);
    				} catch (Exception e1) {
    					return e1.getMessage() != null ? e1.getMessage() : "wrong date";
    				}
    			}
            }
        }

        return null;
    }
    
    /**
     * Creates the buttons for the button bar
     *
     * @param parent the parent composite
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }
}
