package org.maren.spview.app;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.maren.spview.SPView;
import org.maren.spview.imageloader.FitsHeaderItem;
import org.maren.spview.item.RasterItem;
import org.maren.spview.item.RasterSet;

import cz.jmare.file.PathUtil;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.status.StatusUtil;
import cz.jmare.swt.util.Create;
import org.maren.spview.util.StyledUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.maren.spview.util.StyledUtil.*;

public class RastersTableComposite extends Composite implements RastersTable {
    private static final Logger LOGGER = LoggerFactory.getLogger(RastersTableComposite.class);

    private Table table;
    private SPView rasterTableListener;

    public RastersTableComposite(Composite parent, int style, SPView spView) {
        super(parent, style);
        this.rasterTableListener = spView;
        ImageUtil.put("arrow-up", "/image/arrow-up.png");
        ImageUtil.put("arrow-down", "/image/arrow-down.png");

        setLayout(new FormLayout());

        Composite toolsComposite = new Composite(this, SWT.NONE); // createRowCompositeLineIntoGridComposite(this, gd -> {});
        toolsComposite.setLayout(new RowLayout());

        FormData data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        toolsComposite.setLayoutData(data);

        Create.imageButton(toolsComposite, ImageUtil.get("arrow-up"), "Move Up", () -> {
            doMoveUp();
        });
        Create.imageButton(toolsComposite, ImageUtil.get("arrow-down"), "Move Down", () -> {
            doMoveDown();
        });

        table = new Table(this, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(toolsComposite, 0, SWT.DEFAULT);
        data.bottom = new FormAttachment(100, 0);
        table.setLayoutData(data);

        table.setLinesVisible(true);
        table.setHeaderVisible(true);
        table.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                if (event.detail == SWT.CHECK) {
                    int selectionIndex = table.getSelectionIndex();
                    if (selectionIndex >= 0) {
                        // must be deselected not to scroll to selected item after checked/unchecked, swt bug?
                        table.deselect(selectionIndex);
                    }

                    TableItem item = ((TableItem) event.item);
                    if (item.getChecked()) {
                        RasterItem rasterItem = (RasterItem) item.getData();
                        if (!rasterItem.isReferenced()) {
                            item.setChecked(false);
                            StatusUtil.warnMessage("Nonreferenced image can't be showed");
                        }
                    }

                    rasterTableListener.rasterTableChanged();
                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        table.addMouseListener(new MouseListener() {
            @Override
            public void mouseUp(MouseEvent e) {
            }
            
            @Override
            public void mouseDown(MouseEvent e) {
            }
            
            @Override
            public void mouseDoubleClick(MouseEvent e) {
                int[] selectionIndices = table.getSelectionIndices();
                if (selectionIndices.length > 0) {
                    RasterItem rasterItem = (RasterItem) table.getItem(selectionIndices[0]).getData();
                    rasterTableListener.goRasterToCenter(rasterItem);
                    rasterTableListener.refresh();
                    showRasterInfo(rasterItem, rasterTableListener.getInfoStyledText());
                }
            }
        });
        
        TableColumn idColumn = new TableColumn(table, SWT.CENTER);
        idColumn.setWidth(50);
        idColumn.setResizable(true);
        idColumn.setAlignment(SWT.LEFT);

        TableColumn nameColumn = new TableColumn(table, SWT.CENTER);
        nameColumn.setText("Name");
        nameColumn.setWidth(150);
        nameColumn.setResizable(true);
        nameColumn.setAlignment(SWT.LEFT);

        TableColumn groupColumn = new TableColumn(table, SWT.CENTER);
        groupColumn.setText("Group");
        groupColumn.setWidth(150);
        groupColumn.setResizable(true);
        groupColumn.setAlignment(SWT.LEFT);

        table.setHeaderVisible(true);
//	    GridData gridDataTable = new GridData(SWT.FILL, SWT.FILL, true, true);
//
//	    gridDataTable.grabExcessVerticalSpace = true;
//	    gridDataTable.verticalAlignment = SWT.FILL;
//		table.setLayoutData(gridDataTable);


        table.addListener(SWT.Selection, new Listener() {
            @Override
            public void handleEvent(Event event) {
//        		int selectionIndex = table.getSelectionIndex();
//        		if (selectionIndex >= 0) {
//        			if ((event.stateMask & SWT.CTRL) != 0) {
//            			if (table.isSelected(selectionIndex)) {
//            				table.deselect(selectionIndex);
//            			} else {
//            				table.setSelection(selectionIndex);
//            			}
//            		} else {
//        				table.setSelection(selectionIndex);
//            		}
//        			rasterTableListener.rasterTableChanged();
//            	}
            }
        });
        
        Menu menuTable = new Menu(table);
        table.setMenu(menuTable);

        // Create menu item
        
        MenuItem miCenterInGroup = new MenuItem(menuTable, SWT.NONE);
        miCenterInGroup.setText("Center");
        Create.selectionListener(miCenterInGroup, (e) -> {
            int selectionIndex = table.getSelectionIndex();
            if (selectionIndex < 0) {
                return;
            }
            RasterItem item = (RasterItem) table.getItem(selectionIndex).getData();
            RasterSet group = item.getGroup();
            traverseTable(it -> {
                RasterItem ri2 = (RasterItem) it.getData();
                if (ri2.getGroup() == group) {
                    rasterTableListener.goRasterToCenter(ri2);
                    rasterTableListener.rasterTableChanged();
                }
            });  
        });
        
        MenuItem miOpen = new MenuItem(menuTable, SWT.NONE);
        miOpen.setText("Open");
        Create.selectionListener(miOpen, (e) -> {
            int selectionIndex = table.getSelectionIndex();
            if (selectionIndex < 0) {
                return;
            }
            RasterItem item = (RasterItem) table.getItem(selectionIndex).getData();
            String filePath = item.getFilePath();
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.open(new File(filePath));
            } catch (Exception e1) {
                LOGGER.error("Problems to open file {}", filePath, e);
                StatusUtil.errorMessage("Unable to open file");
            }
        });
        
        MenuItem miOpenDir = new MenuItem(menuTable, SWT.NONE);
        miOpenDir.setText("Show Dir in File Manager");
        Create.selectionListener(miOpenDir, (e) -> {
            int selectionIndex = table.getSelectionIndex();
            if (selectionIndex < 0) {
                return;
            }
            RasterItem item = (RasterItem) table.getItem(selectionIndex).getData();
            String filePath = item.getFilePath();
            Desktop desktop = Desktop.getDesktop();
            try {
                String[] parseFullPath = PathUtil.parseFullPath(filePath);
                desktop.open(new File(parseFullPath[0]));
            } catch (Exception e1) {
                StatusUtil.errorMessage("Unable to open file manager");
            }
        });
        

        
        MenuItem miEnableAll = new MenuItem(menuTable, SWT.NONE);
        miEnableAll.setText("Enable All");
        Create.selectionListener(miEnableAll, (e) -> {
            traverseTable((it) -> {
                RasterItem rasterItem = (RasterItem) it.getData();
                it.setChecked(rasterItem.isReferenced());
            });
            rasterTableListener.rasterTableChanged();
        });
        

        
        MenuItem miEnableInGroup = new MenuItem(menuTable, SWT.NONE);
        miEnableInGroup.setText("Enable All in Group");
        Create.selectionListener(miEnableInGroup, (e) -> {
            int selectionIndex = table.getSelectionIndex();
            if (selectionIndex < 0) {
                return;
            }
            RasterItem item = (RasterItem) table.getItem(selectionIndex).getData();
            if (!item.isReferenced()) {
                return;
            }
            RasterSet group = item.getGroup();
            traverseTable(it -> {
                RasterItem ri2 = (RasterItem) it.getData();
                if (ri2.getGroup() == group) {
                    it.setChecked(true);
                }
            });  
            rasterTableListener.rasterTableChanged();
        });
        
        MenuItem miDisableInGroup = new MenuItem(menuTable, SWT.NONE);
        miDisableInGroup.setText("Disable All in Group");
        Create.selectionListener(miDisableInGroup, (e) -> {
            int selectionIndex = table.getSelectionIndex();
            if (selectionIndex < 0) {
                return;
            }
            RasterItem item = (RasterItem) table.getItem(selectionIndex).getData();
            RasterSet group = item.getGroup();
            traverseTable(it -> {
                RasterItem ri2 = (RasterItem) it.getData();
                if (ri2.getGroup() == group) {
                    it.setChecked(false);
                }
            });  
            rasterTableListener.rasterTableChanged();
        });
        
        MenuItem miDisableOthers = new MenuItem(menuTable, SWT.NONE);
        miDisableOthers.setText("Disable Others");
        Create.selectionListener(miDisableOthers, (e) -> {
            int selectionIndex = table.getSelectionIndex();
            if (selectionIndex < 0) {
                return;
            }
            RasterItem item = (RasterItem) table.getItem(selectionIndex).getData();
            traverseTable(it -> {
                RasterItem ri2 = (RasterItem) it.getData();
                it.setChecked(ri2 == item);
            });  
            rasterTableListener.rasterTableChanged();
        });
        
        MenuItem miDisableAll = new MenuItem(menuTable, SWT.NONE);
        miDisableAll.setText("Disable All");
        Create.selectionListener(miDisableAll, (e) -> {
            traverseTable(it -> {
                it.setChecked(false);
            });  
            rasterTableListener.rasterTableChanged();
        });
        
        MenuItem miRemove = new MenuItem(menuTable, SWT.NONE);
        miRemove.setText("Remove");
        Create.selectionListener(miRemove, (e) -> {
            int[] selectionIndices = table.getSelectionIndices();
            for (int i = 0; i < selectionIndices.length; i++) {
                int selectionIndex = selectionIndices[i];
                table.remove(selectionIndex);
                for (int j = i + 1; j < selectionIndices.length; j++) {
                    selectionIndices[j]--;
                }
            }
            rasterTableListener.rasterTableChanged();            
        });
        
        MenuItem miRemoveAll = new MenuItem(menuTable, SWT.NONE);
        miRemoveAll.setText("Remove All");
        Create.selectionListener(miRemoveAll, (e) -> {
            table.removeAll();
            rasterTableListener.rasterTableChanged();
        });

        MenuItem miRemoveNonWcs = new MenuItem(menuTable, SWT.NONE);
        miRemoveNonWcs.setText("Remove All NonWcs");
        Create.selectionListener(miRemoveNonWcs, (e) -> {
            keepInTable((ri) -> ri.isReferenced());
            rasterTableListener.rasterTableChanged();
        });
        
        MenuItem miRemoveInGroup = new MenuItem(menuTable, SWT.NONE);
        miRemoveInGroup.setText("Remove All in Group");
        Create.selectionListener(miRemoveInGroup, (e) -> {
            int selectionIndex = table.getSelectionIndex();
            if (selectionIndex < 0) {
                return;
            }
            RasterItem item = (RasterItem) table.getItem(selectionIndex).getData();
            RasterSet group = item.getGroup();
            keepInTable((ri) -> ri.getGroup() != group);
            rasterTableListener.rasterTableChanged();
        });

        MenuItem miRename = new MenuItem(menuTable, SWT.NONE);
        miRename.setText("Rename/Move");
        Create.selectionListener(miRename, (e) -> {
            int[] selectionIndices = table.getSelectionIndices();
            TableItem firstTableItem = table.getItem(selectionIndices[0]);
            RasterItem firstItem = (RasterItem) firstTableItem.getData();
            String firstFilePath = firstItem.getFilePath();
            RenameDialog renameDialog = new RenameDialog(getShell(), getNamePart(firstFilePath));
            if (renameDialog.open() == Window.OK) {
                String resultName = renameDialog.getResultName();
                String resultDestinationDirectory = renameDialog.getResultDestinationDirectory();
                doRename(selectionIndices, resultName, resultDestinationDirectory);
            }
            rasterTableListener.clearInfo();
        });
        
        MenuItem miDelete = new MenuItem(menuTable, SWT.NONE);
        miDelete.setText("Delete from Disk");
        Create.selectionListener(miDelete, (e) -> {
            int[] selectionIndices = table.getSelectionIndices();
            if (!MessageDialog.openQuestion(getShell(), "Deleting", "Do you really want to delete " + selectionIndices.length + " files from disk?")) {
                return;
            }
            for (int i = 0; i < selectionIndices.length; i++) {
                int selectionIndex = selectionIndices[i];
                TableItem tableItem = table.getItem(selectionIndex);
                RasterItem item = (RasterItem) tableItem.getData();
                String filePath = item.getFilePath();
                table.remove(selectionIndex);
                for (int j = i + 1; j < selectionIndices.length; j++) {
                    selectionIndices[j]--;
                }
                try {
                    Files.delete(Paths.get(filePath));
                } catch (IOException e1) {
                    StatusUtil.errorMessage("Removed " + filePath + " from table but unable to delete from disk");
                }
            }
            rasterTableListener.rasterTableChanged();
        });

        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        fontRegistry.put("smallInfoFont", new FontData[]{new FontData("Arial", 7, SWT.NORMAL)});
    }

    /**
     * Renames/move files
     * @param tableIndexesToRename
     * @param resultName nullable but resultDestinationDirectory must be populated
     * @param resultDestinationDirectory nullable but resultName must be populated
     */
    private void doRename(int[] tableIndexesToRename, String resultName, String resultDestinationDirectory) {
        for (int i = 0; i < tableIndexesToRename.length; i++) {
            int selectionIndex = tableIndexesToRename[i];
            TableItem tableItem = table.getItem(selectionIndex);
            RasterItem item = (RasterItem) tableItem.getData();
            String filePath = item.getFilePath();
            String resultFilePath = resultDestinationDirectory != null ? buildPathKeepSuffix(resultDestinationDirectory, resultName, filePath) : replaceNameKeepSuffix(filePath, resultName);
            try {
                Path uniqueName = RetrieveDataUtil.findUniqueName(Paths.get(resultFilePath));
                Files.move(Paths.get(filePath), uniqueName);
                String uniqueNameString = uniqueName.toString();
                item.setFilePath(uniqueNameString);
                tableItem.setText(1, PathUtil.getLastPartFromPathNice(uniqueNameString));
                tableItem.setText(2, item.getGroup().getName());
            } catch (IOException ex) {
                LOGGER.error("Problems to rename {}", filePath, ex);
                StatusUtil.errorMessage("Unable to rename " + filePath);
            }
        }
    }

    /**
     * Build path
     * @param destinationDirectory
     * @param resultName nullable result name. When null use the same as original name
     * @param origFilePath
     * @return
     */
    private String buildPathKeepSuffix(String destinationDirectory, String resultName, String origFilePath) {
        if (resultName == null) {
            resultName = getNamePart(origFilePath);
        }
        String[] strings = PathUtil.parseFullPathSuffix(origFilePath);
        return Paths.get(destinationDirectory, resultName + strings[1]).toString();
    }

    private static String getNamePart(String origFilePath) {
        String lastPartFromPathNice = PathUtil.getLastPartFromPathNice(origFilePath);
        String[] strings = PathUtil.parseFullPathSuffix(lastPartFromPathNice);
        return strings[0];
    }

    public static String replaceNameKeepSuffix(String origPath, String newNameWithoutSuffix) {
        String[] parsed = PathUtil.parseFullPath(origPath);
        String[] nameAndSuffix = PathUtil.getNameAndSuffixOrEmpty(parsed[1]);
        String resultNameAndSuffix = newNameWithoutSuffix + nameAndSuffix[1];
        String resultFilePath = parsed[0] + resultNameAndSuffix;
        return resultFilePath;
    }

    /**
     * Removes all from table for which the test is false
     * @param keepRasterPred
     */
    private void keepInTable(Predicate<RasterItem> keepRasterPred) {
        int itemCount = table.getItemCount();
        for (int i = 0; i < itemCount; i++) {
            TableItem item = table.getItem(i);
            if (!keepRasterPred.test((RasterItem) item.getData())) {
                table.remove(i);
                i--;
                itemCount--;
            }
        }
    }
    
    private void traverseTable(Consumer<TableItem> consumer) {
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            consumer.accept(item);
        }
    }

    private static void switchItems(TableItem prevItem, TableItem currentItem) {
        String name = prevItem.getText(1);
        String group = prevItem.getText(2);
        Object data = prevItem.getData();

        boolean checked = prevItem.getChecked();

        prevItem.setText(1, currentItem.getText(1));
        prevItem.setText(2, currentItem.getText(2));
        prevItem.setData(currentItem.getData());
        prevItem.setChecked(currentItem.getChecked());

        currentItem.setText(1, name);
        currentItem.setText(2, group);
        currentItem.setData(data);
        currentItem.setChecked(checked);
    }

    private void doMoveUp() {
        int[] selectionIndices = table.getSelectionIndices();
        if (selectionIndices.length == 0 || selectionIndices[0] < 1) {
            return;
        }

        int from = selectionIndices[0];
        int to = selectionIndices[selectionIndices.length - 1];

        int[] sel = new int[to - from + 1];
        int ind = 0;
        for (int i = from; i <= to; i++) {
            TableItem prevItem = table.getItem(i - 1);
            TableItem currentItem = table.getItem(i);
            switchItems(prevItem, currentItem);
            sel[ind++] = i - 1;
        }

        table.setSelection(sel);

        rasterTableListener.rasterTableChanged();
    }

    private void doMoveDown() {
        int[] selectionIndices = table.getSelectionIndices();
        if (selectionIndices.length == 0 || selectionIndices[selectionIndices.length - 1] >= table.getItemCount() - 1) {
            return;
        }

        int from = selectionIndices[0];
        int to = selectionIndices[selectionIndices.length - 1];

        int[] sel = new int[to - from + 1];
        int ind = 0;
        for (int i = to; i >= from; i--) {
            TableItem currentItem = table.getItem(i);
            TableItem nextItem = table.getItem(i + 1);
            switchItems(nextItem, currentItem);
            sel[ind++] = i + 1;
        }

        table.setSelection(sel);

        rasterTableListener.rasterTableChanged();
    }
    
    public static Composite createRowCompositeLineIntoGridComposite(Composite parentComposite, Consumer<GridData> gdConsumer) {
        Composite panelComposite = new Composite(parentComposite, SWT.NONE);
        RowLayout rowLayout = new RowLayout();
        rowLayout.marginHeight = 0;
        rowLayout.marginWidth = 0;
        rowLayout.center = true;
        panelComposite.setLayout(rowLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.horizontalAlignment = SWT.FILL;
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }
    
    /**
     * Add RasterItem instances to table. It doesn't generate a RasterTableListener event
     */
    public synchronized void addOrUpdateRasters(List<RasterItem> rasterItems) {
        for (RasterItem rasterItem : rasterItems) {
            boolean foundExisting = false;
            for (int i = 0; i < table.getItemCount(); i++) {
                TableItem item = table.getItem(i);
                RasterItem existingRasterItem = (RasterItem) item.getData();
                if (existingRasterItem.getFilePath().equals(rasterItem.getFilePath())) {
                    foundExisting = true;
                    item.setData(rasterItem);
                    break;
                }
            }
            if (foundExisting) {
                continue;
            }
            TableItem tableItem = new TableItem(table, SWT.NONE);
            tableItem.setText(1, rasterItem.getName());
            tableItem.setData(rasterItem);
            tableItem.setText(2, rasterItem.getGroup().getName());
            tableItem.setChecked(rasterItem.isReferenced());
            selectRasterItem(rasterItem, false);
            //rasterTableListener.focusTab(this);
        }
    }

    @Override
    public synchronized List<RasterItem> getEnabledRasters() {
        List<RasterItem> rasters = new ArrayList<>();
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            if (item.getChecked()) {
                rasters.add((RasterItem) item.getData());
            }
        }
        return rasters;
    }

    @Override
    public List<RasterItem> getAllRasters() {
        List<RasterItem> rasters = new ArrayList<>();
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            rasters.add((RasterItem) item.getData());
        }
        return rasters;
    }

    @Override
    public void selectRasterItem(RasterItem rasterItem, boolean additive) {
        if (!additive) {
            table.setSelection(new int[] {});
        }
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            if (rasterItem == (RasterItem) item.getData()) {
                table.select(i);
                //table.showSelection();
                table.showItem(item);
                showRasterInfo(rasterItem, rasterTableListener.getInfoStyledText());
                break;
            }
        }
    }

    @Override
    public void remove(RasterItem rasterItem) {
        keepInTable((ri) -> ri != rasterItem);
        rasterTableListener.rasterTableChanged();
    }

    public void showRasterInfo(RasterItem ra, StyledText infoText) {
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        Font smallInfoFont = fontRegistry.get("smallInfoFont");
        infoText.setText("");
        if (ra != null) {
            addBoldText(infoText, ra.getName());
            addLF(infoText);
            if (!ra.getGroup().getName().isEmpty()) {
                addBoldText(infoText, "Group: ");
                addText(infoText, ra.getGroup().getName());
                addLF(infoText);
            }
            addText(infoText, "width: " + ra.getWidth() + ", height: " + ra.getHeight());
            addLF(infoText);
            addText(infoText, ra.getFilePath());
            addLF(infoText);
            if (ra.getHeaderValues() != null) {
                List<FitsHeaderItem> headerValues = ra.getHeaderValues();
                for (FitsHeaderItem item: headerValues) {
                    if (item.getValue() == null) {
                        StyledUtil.addStyledText(infoText, item.getName(), SWT.NORMAL, smallInfoFont);
                    } else {
                        StyledUtil.addStyledText(infoText, item.getName().trim() + " = " + item.getValue().trim(), SWT.NORMAL, smallInfoFont);
                    }
                    addLF(infoText);
                }
            }
        }
    }
}
