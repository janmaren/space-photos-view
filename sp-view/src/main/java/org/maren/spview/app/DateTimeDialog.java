package org.maren.spview.app;

import cz.jmare.swt.util.Create;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.maren.spview.util.DateTimeUtil;

import java.time.LocalDateTime;

public class DateTimeDialog extends TitleAreaDialog {
    private Text dateTimeText;

    private LocalDateTime resultDateTime;

    private double resultZone;

    private LocalDateTime dateTime;

    public DateTimeDialog(Shell parent, LocalDateTime dateTime, double zone) {
        super(parent);
        this.dateTime = dateTime;
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Date Time Setting");
        setMessage("Populate date and time.\nIt's needed for AZ, ALT calculations and horizon.");

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 4;
        composite.setLayout(layout);

        Create.label(composite, "Date and Time:");
        dateTimeText = Create.text(composite, "", 200);
        dateTimeText.setText(DateTimeUtil.format(dateTime));
        dateTimeText.setToolTipText("Format: year-month-day hour:minute[:second]");
        GridData dgData = new GridData();
        dgData.widthHint = 150;
        dateTimeText.setLayoutData(dgData);

        Create.label(composite, DateTimeUtil.getOffsetHoursString());

        Create.button(composite, "Now", () -> {
            LocalDateTime now = LocalDateTime.now();
            dateTimeText.setText(DateTimeUtil.format(now));
        });

        return composite;
    }
    
    @Override
    protected Point getInitialSize() {
        return new Point(550, 300);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        String message;
        if ((message = valid()) != null) {
            setErrorMessage(message);
            return; 
        }

        resultDateTime = DateTimeUtil.parse(dateTimeText.getText());

        super.buttonPressed(buttonId);
    }
    
    protected String valid() {
        if (dateTimeText.getText().isBlank()) {
            return "Date and Time must be populated";
        }
        try {
            DateTimeUtil.parse(dateTimeText.getText().trim());
        } catch (Exception e) {
            return "Invalid date and time format, it must be like 'year-month-day hour:minute:second'";
        }
        return null;
    }

    public LocalDateTime getResultDateTime() {
        return resultDateTime;
    }

    public double getResultZone() {
        return resultZone;
    }
}
