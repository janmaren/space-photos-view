package org.maren.spview.app;

public interface RasterTableListener {
    void rasterTableChanged();
}
