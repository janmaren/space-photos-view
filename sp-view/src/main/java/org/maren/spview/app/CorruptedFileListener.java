package org.maren.spview.app;

import org.maren.spview.item.RasterItem;

public interface CorruptedFileListener {
    void acceptCorrupted(RasterItem rasterItem);
}
