package org.maren.spview.horizons;

import java.time.OffsetDateTime;
import java.util.List;

public class EphRequest {
    private List<String> requiredNames;
    private OffsetDateTime from;
    private OffsetDateTime to;
    private int step;
    private IntType intType;

    private Double maxMagnitude = null;

    private NamePopulationType namePopulationType = NamePopulationType.NAME;

    private boolean forceDownload;

    public EphRequest() {
    }

    public EphRequest(List<String> requiredNames, OffsetDateTime from, OffsetDateTime to, int step, IntType intType) {
        this.requiredNames = requiredNames;
        this.from = from;
        this.to = to;
        this.step = step;
        this.intType = intType;
    }

    public List<String> getRequiredNames() {
        return requiredNames;
    }

    public EphRequest setRequiredNames(List<String> requiredNames) {
        this.requiredNames = requiredNames;
        return this;
    }

    public OffsetDateTime getFrom() {
        return from;
    }

    public EphRequest setFrom(OffsetDateTime from) {
        this.from = from;
        return this;
    }

    public OffsetDateTime getTo() {
        return to;
    }

    public EphRequest setTo(OffsetDateTime to) {
        this.to = to;
        return this;
    }

    public int getStep() {
        return step;
    }

    public EphRequest setStep(int step) {
        this.step = step;
        return this;
    }

    public IntType getIntType() {
        return intType;
    }

    public EphRequest setIntType(IntType intType) {
        this.intType = intType;
        return this;
    }

    public Double getMaxMagnitude() {
        return maxMagnitude;
    }

    public EphRequest setMaxMagnitude(Double maxMagnitude) {
        this.maxMagnitude = maxMagnitude;
        return this;
    }

    public NamePopulationType getNamePopulationType() {
        return namePopulationType;
    }

    public EphRequest setNamePopulationType(NamePopulationType namePopulationType) {
        this.namePopulationType = namePopulationType;
        return this;
    }

    public EphRequest setForceDownload(boolean forceDownload) {
        this.forceDownload = forceDownload;
        return this;
    }

    public boolean isForceDownload() {
        return forceDownload;
    }
}
