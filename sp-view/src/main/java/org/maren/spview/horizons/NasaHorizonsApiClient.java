package org.maren.spview.horizons;

import cz.jmare.http.HUrl;
import cz.jmare.http.RequestData;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class NasaHorizonsApiClient {
    public static final String NASA_HORIZONS_URL = "https://ssd.jpl.nasa.gov/api/horizons_file.api";

    public static String getData(String file, String proxyHost, Integer proxyPort) {
        RequestData requestData = new RequestData();
        requestData.setConnectTimeout(7000);
        if (proxyHost != null) {
            requestData.setProxy(proxyHost, proxyPort);
        }
        String response = HUrl.postFieldsFilesMultipartReturnString(NASA_HORIZONS_URL,
                                                                    Map.of("format", "text", "input", file), Map.of(), requestData);
        return response;
    }

    public static void main(String[] args) throws IOException {
        String string = Files.readString(Paths.get("C:\\Users\\jmarencik\\Documents\\my_input_file2.txt"));
        string = new ObsCommand("mars").toString();
        System.out.println(string);
        String data = getData(string, null, null);
        System.out.println(data);

    }
}
