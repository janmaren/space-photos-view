package org.maren.spview.horizons;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.jmare.str.LineStrUtil;

public class NasaHorizonsSelParser {
	private final static Pattern NUMBER_PATTERN = Pattern.compile("([\\+\\-]?[0-9]+)");
	public static List<NasaHorizonsSelData> parseSelections(String str) {
		int indexOfStars = str.indexOf("*******");
		int indexOfDash = str.indexOf("-------");
		if (indexOfStars >= indexOfDash) {
			throw new IllegalStateException("Not selection format");
		}
		int nextLineStart = LineStrUtil.findNextLineStart(indexOfDash, str);
		List<NasaHorizonsSelData> list = new ArrayList<>();
		while (nextLineStart < str.length()) {
			int start = nextLineStart;
			nextLineStart = LineStrUtil.findNextLineStart(nextLineStart, str);
			String line = str.substring(start, nextLineStart);
			if (line.isBlank()) {
				break;
			}
			Matcher matcher = NUMBER_PATTERN.matcher(line);
			if (matcher.find()) {
				String number = matcher.group(1);
				String rest = null;
				if (line.length() > 46) {
					rest = line.substring(11, 46);
				} else {
					rest = line.substring(11);
				}
				list.add(new NasaHorizonsSelData(number.trim(), rest.trim()));
			}
		}
		
		return list;
	}

	public static void main(String[] args) throws IOException {
		String str = Files.readString(Paths.get("C:\\Users\\jmarencik\\Documents\\sel.txt"));
		List<NasaHorizonsSelData> selections = parseSelections(str);
		System.out.println(selections);
	}
}
