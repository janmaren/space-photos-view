package org.maren.spview.horizons;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.StringJoiner;

public class ObsCommand implements Cloneable {
    public String targetName;

    /**
     * UT date time from
     */
    public OffsetDateTime from = OffsetDateTime.now();

    /**
     * UT date time to
     */
    public OffsetDateTime to = OffsetDateTime.now().plus(1, ChronoUnit.DAYS);

    public int step = 1;

    public IntType intType = IntType.HOUR;

    public String center = "500@399";

    public ObsCommand(String targetName) {
        this.targetName = targetName;
    }

    public ObsCommand targetName(String targetName) {
        this.targetName = targetName;
        return this;
    }

    /**
     * UT date time from
     * @param from
     * @return
     */
    public ObsCommand from(OffsetDateTime from) {
        this.from = from;
        return this;
    }

    /**
     * UT date time to
     * @param to
     * @return
     */
    public ObsCommand to(OffsetDateTime to) {
        this.to = to;
        return this;
    }

    /**
     * Interval between to times. Default units are hours
     * @param step
     * @return
     */
    public ObsCommand step(int step) {
        this.step = step;
        return this;
    }

    /**
     * The step units, like hours (default), days, ...
     * @param intType
     * @return
     */
    public ObsCommand intType(IntType intType) {
        this.intType = intType;
        return this;
    }

    public ObsCommand center(String center) {
        this.center = center;
        return this;
    }

    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner("\n");
        sj.add("COMMAND='" + targetName + "'");
        sj.add("OBJ_DATA='YES'");
        sj.add("MAKE_EPHEM='YES'");
        sj.add("TABLE_TYPE='OBSERVER'");
        sj.add("CENTER='" + center + "'");
        sj.add("START_TIME='" + from.withOffsetSameInstant(ZoneOffset.UTC) + "'");
        sj.add("STOP_TIME='" + to.withOffsetSameInstant(ZoneOffset.UTC) + "'");
        sj.add("STEP_SIZE='" + step + " " + intType.getStr() + "'");
        sj.add("QUANTITIES='1,9'");
        sj.add("CSV_FORMAT='NO'");
        return "!$$SOF" + "\n" + sj.toString();
    }

    @Override
    public ObsCommand clone() {
        ObsCommand obsCommand = new ObsCommand(targetName);
        obsCommand.center = center;
        obsCommand.from = from;
        obsCommand.to = to;
        obsCommand.intType = intType;
        obsCommand.step = step;
        return obsCommand;
    }

    public static void main(String[] args) {
        ObsCommand obsCommand = new ObsCommand("P/2001 Q6");
        System.out.println(obsCommand.toString());
    }
}
