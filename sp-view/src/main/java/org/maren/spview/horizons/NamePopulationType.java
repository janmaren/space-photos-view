package org.maren.spview.horizons;

public enum NamePopulationType {
    NAME, DATETIME, TIME
}
