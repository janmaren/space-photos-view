package org.maren.spview.horizons;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Duration;
import java.time.Instant;

public class HorUtil {
    public static Instant getModifTime(Path path) {
        BasicFileAttributeView view = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        BasicFileAttributes attributes = null;
        try {
            attributes = view.readAttributes();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        FileTime lastModifedTime = attributes.lastModifiedTime();
        return lastModifedTime.toInstant();
    }

    public static Duration getAge(Path path) {
        Instant modifTime = getModifTime(path);
        Duration duration = Duration.between(modifTime, Instant.now());
        return duration;
    }
}
