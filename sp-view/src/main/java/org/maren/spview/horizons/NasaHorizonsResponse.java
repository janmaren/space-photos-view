package org.maren.spview.horizons;

import org.maren.spview.shape.PointShape;

import java.util.List;

public class NasaHorizonsResponse {
    private String ephemeris;
    private String selection;
    private String requiredName;

    public NasaHorizonsResponse(String ephemeris, String selection, String ephemerisName) {
        this.ephemeris = ephemeris;
        this.selection = selection;
        this.requiredName = ephemerisName;
    }

    public boolean isEphemeris() {
        return ephemeris != null;
    }

    public boolean isSelection() {
        return selection != null;
    }

    public List<PointShape> getEphemeris() {
        return NasaHorizonsEphParser.parseFullDataPoints(ephemeris, requiredName);
    }

    public List<NasaHorizonsSelData> getSelections() {
        return NasaHorizonsSelParser.parseSelections(selection);
    }

    public String getRequiredName() {
        return requiredName;
    }

    @Override
    public String toString() {
        return ephemeris != null ? ephemeris : selection;
    }
}
