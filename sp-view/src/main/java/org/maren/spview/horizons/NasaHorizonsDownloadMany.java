package org.maren.spview.horizons;

import cz.jmare.progress.OperationInterruptedException;
import org.maren.spview.shape.PointShape;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class NasaHorizonsDownloadMany {
    private static final Logger LOGGER = LoggerFactory.getLogger(NasaHorizonsDownloadMany.class);

    public static List<NasaHorizonsEphResult> downloadEphsParalelly(List<String> names, OffsetDateTime from, OffsetDateTime to, int step, IntType intType, String proxyHost, Integer proxyPort) {
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        List<ObsCommand> obsCommands =
                names.stream().map(name -> new ObsCommand(name).from(from).to(to).intType(intType).step(step)).collect(Collectors.toList());

        List<CompletableFuture<NasaHorizonsResponse>> completableFutures =
                obsCommands.stream().map(obsCommand -> {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    return CompletableFuture.supplyAsync(() -> NasaHorizonsRequester.requestOrNull(obsCommand, proxyHost, proxyPort), executorService);}).collect(Collectors.toList());


        List<NasaHorizonsResponse> responses =
                completableFutures.stream().map(cf -> cf.join()).filter(resp -> resp != null && resp.isEphemeris()).collect(Collectors.toList());

        List<ObsCommand> notFoundObsCommands = obsCommands.stream().filter(obsCommand -> !isName(responses, obsCommand.targetName)).collect(Collectors.toList());

        List<NasaHorizonsEphResult> resultGroups = new ArrayList<>();
        for (NasaHorizonsResponse respons : responses) {
            resultGroups.add(new NasaHorizonsEphResult(respons.getEphemeris(), respons.getRequiredName(), true));
        }

        for (ObsCommand notFoundObsCommand : notFoundObsCommands) {
            NasaHorizonsResponse nasaHorizonsResponse = NasaHorizonsRequester.requestOrNull(notFoundObsCommand,
                                                                                            proxyHost, proxyPort);
            if (nasaHorizonsResponse != null && nasaHorizonsResponse.isEphemeris()) {
                List<PointShape> ephemeris = nasaHorizonsResponse.getEphemeris();
                NasaHorizonsEphResult nasaHorizonsEphResult = new NasaHorizonsEphResult(ephemeris,
                                                                                        notFoundObsCommand.targetName, true);

                resultGroups.add(nasaHorizonsEphResult);
            }
        }

        executorService.shutdownNow();
        return resultGroups;
    }

    public static List<NasaHorizonsEphResult> downloadEphsSequentially(List<String> names, OffsetDateTime from, OffsetDateTime to, int step, IntType intType, String proxyHost, Integer proxyPort) {
        List<ObsCommand> obsCommands =
                names.stream().map(name -> new ObsCommand(name).from(from).to(to).intType(intType).step(step)).collect(Collectors.toList());
        Thread currentThread = Thread.currentThread();
        List<NasaHorizonsEphResult> resultGroups = new ArrayList<>();

        for (ObsCommand obsCommand : obsCommands) {
            if (currentThread.isInterrupted()) {
                LOGGER.info("Ephemeris download interrupted");
                throw new OperationInterruptedException();
            }
            NasaHorizonsResponse nasaHorizonsResponse = NasaHorizonsRequester.requestOrNull(obsCommand,
                                                                                            proxyHost, proxyPort);
            if (nasaHorizonsResponse != null && nasaHorizonsResponse.isEphemeris()) {
                NasaHorizonsEphResult nasaHorizonsEphResult = null;
                try {
                    List<PointShape> ephemeris = nasaHorizonsResponse.getEphemeris();
                    nasaHorizonsEphResult = new NasaHorizonsEphResult(ephemeris,
                                                                      obsCommand.targetName, true);
                } catch (Exception e) {
                    LOGGER.error("Unable to download {}", obsCommand.targetName, e);
                    nasaHorizonsEphResult = new NasaHorizonsEphResult(null,
                                                                                            obsCommand.targetName, false);
                }
                resultGroups.add(nasaHorizonsEphResult);
            } else {
                NasaHorizonsEphResult nasaHorizonsEphResult = new NasaHorizonsEphResult(null,
                                                                                        obsCommand.targetName, false);
                resultGroups.add(nasaHorizonsEphResult);
            }
        }

        return resultGroups;
    }

    private static boolean isName(List<NasaHorizonsResponse> responses, String name) {
        return responses.stream().filter(resp -> resp.getRequiredName().equals(name)).findAny().isPresent();
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        List<NasaHorizonsEphResult> lists = downloadEphsSequentially(List.of("P/1994 N2", "C/1995 O1", "P/1996 R2", "P/1997 B1", "P/1998 QP54", "P/1998 VS24", "P/1999 D1"), OffsetDateTime.now(),
                                                                OffsetDateTime.now().plus(1, ChronoUnit.DAYS), 1, IntType.HOUR, null,
                                                    null);
        long end = System.currentTimeMillis();
        System.out.println("Doba " + (end- start));
        for (NasaHorizonsEphResult list : lists) {
            System.out.println("------- list -------");
            System.out.println(list.getPointShapes());
        }
    }
}
