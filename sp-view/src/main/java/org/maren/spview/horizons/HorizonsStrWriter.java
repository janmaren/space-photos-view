package org.maren.spview.horizons;

import java.util.List;
import java.util.Set;

import org.maren.spview.shape.PointShape;

import cz.jmare.collection.Colutil;
import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.JulianUtil;
import cz.jmare.math.geo.GeoLoc;
import org.maren.spview.tag.TagSupported;

public class HorizonsStrWriter {
    final static String BEGIN = "$$SOE";
    final static String END = "$$EOE";
    final static String TARGET = "Target body name:";
    final static String GEO_LOC = "Center geodetic :";
    final static String CENTER_SITE = "Center-site name: GEOCENTRIC";
    final static String CENTER_BODY_NAME = "Center body name:";
    private final static String GEOLOC = TagSupported.GEOLOC.name();

    public static String exportGroups(List<PointShape> items) {
        Set<List<PointShape>> groups = Colutil.getGroups(items, (i1, i2) -> {
            if (i1.getGroup() != i2.getGroup()) return -1;
            Object tagValue1 = i1.getTagValue(GEOLOC);
            if (tagValue1 != null) {
                Object tagValue2 = i2.getTagValue(GEOLOC);
                if (tagValue2 != null) {
                    GeoLoc loc1 = (GeoLoc) tagValue1;
                    GeoLoc loc2 = (GeoLoc) tagValue2;
                    if (!loc1.equals(loc2)) {
                        return -1;
                    }
                } else {
                    return -1;
                }
            } else {
                if (i2.getTagValue(GEOLOC) != null) {
                    return -1;
                }
            }
            return 0;
        });
        StringBuilder sb = new StringBuilder();
        for (List<PointShape> group : groups) {
            String exportGroup = exportGroup(group);
            sb.append(exportGroup).append("\n");
        }
        return sb.toString();
    }
    
    public static String exportGroup(List<PointShape> items) {
        PointShape firstPointItem = items.get(0);
        StringBuilder sb = new StringBuilder();
        sb.append(TARGET).append(" ").append(firstPointItem.getGroup().getName()).append("\n");
        if (firstPointItem.getTagValue("GEOLOC") != null) {
            GeoLoc loc = (GeoLoc) firstPointItem.getTagValue(GEOLOC);
            sb.append(GEO_LOC).append(" ").append(loc.longitude).append(",").append(loc.latitude).append(",0").append("\n");
        } else {
            sb.append(CENTER_SITE).append("\n");
        }
        sb.append(BEGIN).append("\n");
        for (PointShape pointItem : items) {
            String line = exportEph(pointItem);
            if (pointItem.getName() != null) {
                line += " \"" + pointItem.getName() + "\"";
            }
            sb.append(" ").append(line).append("\n");
        }
        sb.append(END).append("\n");
        return sb.toString();
    }

    private static String exportEph(PointShape pointItem) {
        String jdStr = "0-0-0 0:0";
        if (pointItem.getJd() != null) {
            jdStr = JulianUtil.formatJulian(pointItem.getJd());
        }
        return jdStr + " " + ACooUtil.formatCoords(pointItem.getRaDec(), " ", " ");
    }
}
