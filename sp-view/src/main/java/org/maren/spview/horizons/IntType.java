package org.maren.spview.horizons;

import java.time.temporal.ChronoUnit;

public enum IntType {
    YEAR("y"), MINUTE("m"), HOUR("h"), DAY("d"), MONTH("mo");

    private String str;

    IntType(String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }

    public static ChronoUnit toChronoUnit(IntType intType) {
        if (intType == MINUTE) {
            return ChronoUnit.MINUTES;
        }
        if (intType == HOUR) {
            return ChronoUnit.HOURS;
        }
        if (intType == DAY) {
            return ChronoUnit.DAYS;
        }
        if (intType == MONTH) {
            return ChronoUnit.MONTHS;
        }
        if (intType == YEAR) {
            return ChronoUnit.YEARS;
        }
        throw new IllegalArgumentException("Not supported IntType " + intType);
    }
}
