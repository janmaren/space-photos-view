package org.maren.spview.horizons;

import cz.jmare.http.ErrorStatusException;
import cz.jmare.logging.LoggerSlf4jOptional;
import org.maren.spview.shape.PointShape;

import java.util.List;
import java.util.regex.Pattern;

public class NasaHorizonsRequester {
    private static final Pattern EPHEMERIS = Pattern.compile("Ephemeris .* Horizons");

    private static final Pattern MULTIPLE_MATCH = Pattern.compile("Multiple .*match");

    private static final Pattern MATCHING_SMALL_BODIES = Pattern.compile("Matching small-bodies");

    public static NasaHorizonsResponse request(ObsCommand obsCommand, String proxyHost, Integer proxyPort) {
        String response = NasaHorizonsApiClient.getData(obsCommand.toString(), proxyHost, proxyPort);
        if (EPHEMERIS.matcher(response).find()) {
            return new NasaHorizonsResponse(response, null, obsCommand.targetName);
        } else if (MULTIPLE_MATCH.matcher(response).find()) {
            return new NasaHorizonsResponse(null, response, obsCommand.targetName);
        } else if (MATCHING_SMALL_BODIES.matcher(response).find()) {
            ObsCommand obsCommand2 = obsCommand.clone();
            obsCommand2.targetName = "DES= " + obsCommand.targetName + "; NOFRAG; CAP";
            response = NasaHorizonsApiClient.getData(obsCommand2.toString(), proxyHost, proxyPort);
            if (EPHEMERIS.matcher(response).find()) {
                return new NasaHorizonsResponse(response, null, obsCommand.targetName);
            }
        }
        LoggerSlf4jOptional.logWarn("Unable to download, request: " + obsCommand + "\n response: " + response);
        return new NasaHorizonsResponse(null, null, obsCommand.targetName);
    }

    public static NasaHorizonsResponse requestOrNull(ObsCommand obsCommand, String proxyHost, Integer proxyPort) {
        try {
            return request(obsCommand, proxyHost, proxyPort);
        } catch (ErrorStatusException e) {
            LoggerSlf4jOptional.logError("Unable to download {} " + obsCommand + " error status " + e.getStatus());
            return null;
        } catch (Exception e) {
            LoggerSlf4jOptional.logError("Unable to download " + obsCommand, e);
            return null;
        }
    }

    public static void main(String[] args) {
        ObsCommand obsCommand = new ObsCommand( "C/2018");
        NasaHorizonsResponse response = request(obsCommand, null, null);
        System.out.println(response);
        List<NasaHorizonsSelData> selections = null;
        if (response.isSelection()) {
            selections = response.getSelections();
            System.out.println(selections);
        }
        ObsCommand obsCommand2 = new ObsCommand(selections.get(0).getId());
        NasaHorizonsResponse response2 = request(obsCommand2, null, null);
        if (response2.isEphemeris()) {
            List<PointShape> ephemeris = response2.getEphemeris();
            System.out.println(ephemeris);
        }
    }
}
