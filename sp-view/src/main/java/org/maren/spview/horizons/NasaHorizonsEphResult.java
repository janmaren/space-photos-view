package org.maren.spview.horizons;

import org.maren.spview.shape.PointShape;

import java.util.List;

public class NasaHorizonsEphResult {
    private List<PointShape> pointShapes;

    private String queryName;

    private boolean ok;

    public NasaHorizonsEphResult(List<PointShape> pointShapes, String queryName, boolean ok) {
        this.pointShapes = pointShapes;
        this.queryName = queryName;
        this.ok = ok;
    }

    public List<PointShape> getPointShapes() {
        return pointShapes;
    }

    public NasaHorizonsEphResult setPointShapes(List<PointShape> pointShapes) {
        this.pointShapes = pointShapes;
        return this;
    }

    public String getQueryName() {
        return queryName;
    }

    public NasaHorizonsEphResult setQueryName(String queryName) {
        this.queryName = queryName;
        return this;
    }

    public boolean isOk() {
        return ok;
    }

    public NasaHorizonsEphResult setOk(boolean ok) {
        this.ok = ok;
        return this;
    }
}
