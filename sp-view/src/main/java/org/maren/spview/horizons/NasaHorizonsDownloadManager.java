package org.maren.spview.horizons;

import cz.jmare.data.util.JulianUtil;
import cz.jmare.logging.LoggerSlf4jOptional;
import org.maren.spview.shape.PointShape;
import org.maren.spview.util.AppDirsUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static org.maren.spview.horizons.NasaHorizonsDownloadMany.downloadEphsSequentially;

public class NasaHorizonsDownloadManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(NasaHorizonsDownloadManager.class);

    public static synchronized List<List<PointShape>> downloadEphsCached(EphRequest ephRequest, String proxyHost, Integer proxyPort, int maxFileValidAgeHours) {
        ArrayList<String> names = new ArrayList<>(ephRequest.getRequiredNames());

        invalidateBlacklistWhenNeeded();
        Set<String> blacklistNames = getBlacklistNames();
        names.removeAll(blacklistNames);

        LOGGER.debug("Decided what to download, number: " + names.size());

        Set<String> undownloadableNames = new HashSet<>();
        List<List<PointShape>> resultGroups = new ArrayList<>();

        if (names.size() > 0) {
            List<NasaHorizonsEphResult> lists = downloadEphsSequentially(names, ephRequest.getFrom()/*.minus(7, ChronoUnit.DAYS)*/,
                                                                         ephRequest.getTo().plus(7, ChronoUnit.DAYS),
                                                                         ephRequest.getStep(), ephRequest.getIntType(),
                                                                    proxyHost, proxyPort);
            for (NasaHorizonsEphResult nasaHorizonsEphResult : lists) {
                if (nasaHorizonsEphResult.isOk()) {
                    resultGroups.add(nasaHorizonsEphResult.getPointShapes());
                } else {
                    undownloadableNames.add(nasaHorizonsEphResult.getQueryName());
                }
            }
        }

        if (undownloadableNames.size() > 0) {
            LOGGER.info("Unable to download names {}", undownloadableNames);
        }
        if (undownloadableNames.size() < names.size() / 3) { // probably not unavailability
            addBlacklistNames(undownloadableNames);
        }

        return resultGroups;
    }

    public static List<List<PointShape>> cutFromTo(List<List<PointShape>> groups, OffsetDateTime from, OffsetDateTime to) {
        double jdFrom = JulianUtil.toJulianDate(from);
        double jdTo = JulianUtil.toJulianDate(to);
        List<List<PointShape>> result = new ArrayList<>(groups.size());
        for (List<PointShape> group : groups) {
            List<PointShape> limited = new ArrayList<>();
            for (PointShape pointShape : group) {
                if (pointShape.getJd() == null) {
                    continue;
                }
                if (pointShape.getJd() < jdFrom) {
                    continue;
                }
                if (pointShape.getJd() > jdTo) {
                    continue;
                }
                limited.add(pointShape);
            }
            result.add(limited);
        }
        return result;
    }

    public static List<List<PointShape>> applyStep(List<List<PointShape>> groups, double stepDays) {
        double toleranceDays = stepDays * 0.01;
        List<List<PointShape>> result = new ArrayList<>(groups.size());
        for (List<PointShape> group : groups) {
            List<PointShape> limited = new ArrayList<>();
            group.removeIf(s -> s.getJd() == null);
            Collections.sort(group, (c1, c2) -> c1.getJd().compareTo(c2.getJd()));
            Iterator<PointShape> iterator = group.iterator();
            Double lastJd = null;
            if (iterator.hasNext()) {
                PointShape pointShape = iterator.next();
                limited.add(pointShape);
                lastJd = pointShape.getJd();
            }
            while (iterator.hasNext()) {
                PointShape pointShape = iterator.next();
                double durDays = pointShape.getJd() - lastJd;
                if (durDays + toleranceDays > stepDays) {
                    limited.add(pointShape);
                    lastJd = pointShape.getJd();
                }
            }
            result.add(limited);
        }
        return result;
    }

    public static double getDurationDays(int step, IntType intType) {
        double durDays = 0;
        if (IntType.HOUR == intType) {
            durDays = step / 24.0;
        } else if (intType == IntType.MINUTE) {
            durDays = step / (24.0 * 60.0);
        } else if (intType == IntType.DAY) {
            durDays = step;
        } else if (intType == IntType.MONTH) {
            durDays = step * 30;
        } else if (intType == IntType.YEAR) {
            durDays = step * 365.25;
        }
        return durDays;
    }

    /**
     * Mutable set of blacklist names
     * @return
     */
    private static Set<String> getBlacklistNames() {
        File dataDir = AppDirsUtil.getDataDir("data");
        File black = new File(dataDir, "blacklist-names.txt");
        try {
            List<String> strings = Files.readAllLines(black.toPath());
            return new HashSet<>(strings.stream().map(s -> s.trim()).collect(Collectors.toSet()));
        } catch (IOException e) {
            return new HashSet<>(); // file not exists
        }
    }

    private static void addBlacklistNames(Set<String> names) {
        Set<String> blacklistNames = getBlacklistNames();
        blacklistNames.addAll(names);

        File dataDir = AppDirsUtil.getDataDir("data");
        File black = new File(dataDir, "blacklist-names.txt");
        try {
            String content = blacklistNames.stream().collect(Collectors.joining("\n"));
            Files.writeString(black.toPath(), content);
        } catch (IOException e) {
            LoggerSlf4jOptional.logError("Unable to save content to " + black);
        }
    }

    private static void invalidateBlacklistWhenNeeded() {
        File dataDir = AppDirsUtil.getDataDir("data");
        File black = new File(dataDir, "blacklist-names.txt");
        if (!black.isFile()) {
            return;
        }
        Duration age = HorUtil.getAge(black.toPath());
        if (age.toHours() > 24) {
            black.delete();
            LOGGER.info("Invalidated blacklist-names.txt");
        }
    }
}
