package org.maren.spview.horizons;


import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.data.util.JulianUtil;
import cz.jmare.exception.ExceptionMessage;
import org.eclipse.swt.widgets.Shell;
import org.maren.spview.shape.GroupAtt;
import org.maren.spview.shape.GroupUtil;
import org.maren.spview.shape.PointShape;
import org.maren.spview.table.PointsComposite;
import org.maren.spview.util.DateTimeUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

public class EphemerisRequester {
    private static volatile ConcurrentLinkedQueue<EphRequest> queue = new ConcurrentLinkedQueue<>();

    private static Thread thread;


    public synchronized static void addToQueue(EphRequest ephRequest, PointsComposite pointsComposite) {
        queue.add(ephRequest);
        if (thread == null) {
            thread = new Thread(new Fetcher(pointsComposite, pointsComposite.getShell()));
            thread.start();
            pointsComposite.setInfo("Downloading of ephemeris started");
        }
    }

    public static class Fetcher implements Runnable {
        PointsComposite pointsComposite;

        Shell shell;

        public Fetcher(PointsComposite pointsComposite, Shell shell) {
            this.pointsComposite = pointsComposite;
            this.shell = shell;
        }

        @Override
        public void run() {
            try {
                ConfigStore configStore = AppConfig.getInstance().getConfigStore();
                while (queuePopulated()) {
                    EphRequest ephRequest = queue.poll();
                    List<List<PointShape>> groups = NasaHorizonsDownloadManager.downloadEphsCached(ephRequest,
                            configStore.getValue(
                                    "proxy.host",
                                    null),
                            configStore.getIntValue(
                                    "proxy.port", null), 7 * 24);
                    List<PointShape> pointShapes = groupsToFlat(groups, ephRequest, pointsComposite.getGroups());
                    List<PointShape> unexistingShapes = shell.getDisplay().syncCall(() -> pointsComposite.getUnexistingShapes(pointShapes));
                    shell.getDisplay().syncExec(() -> {
                        pointsComposite.addShapes(unexistingShapes);
                        pointsComposite.getSPView().refresh();
                        if (groups.size() < ephRequest.getRequiredNames().size()) {
                            if (groups.isEmpty()) {
                                pointsComposite.setError("Problems to download any ephemeris");
                            } else {
                                pointsComposite.setError("Downloaded only " + groups.size() + "/" + ephRequest.getRequiredNames().size() + " objects");
                            }
                        } else {
                            pointsComposite.setInfo("Downloaded all " + ephRequest.getRequiredNames().size() + " objects");
                        }
                    });
                }
            } catch (Exception e) {
                shell.getDisplay().syncExec(() -> {
                     pointsComposite.setError(ExceptionMessage.getCombinedMessage("Problems to download any ephemeris", e), e);
                });
            }
        }
    }

    private synchronized static boolean queuePopulated() {
        if (queue.isEmpty()) {
            thread = null;
            return false;
        }
        return true;
    }

    static List<PointShape> groupsToFlat(List<List<PointShape>> lists, EphRequest ephRequest, Map<String, GroupAtt> groups) {
        List<List<PointShape>> rangedGroups = NasaHorizonsDownloadManager.cutFromTo(lists, ephRequest.getFrom(), ephRequest.getTo());
        double durationDays = NasaHorizonsDownloadManager.getDurationDays(ephRequest.getStep(), ephRequest.getIntType());
        rangedGroups = NasaHorizonsDownloadManager.applyStep(rangedGroups, durationDays);
        List<PointShape> shapes = new ArrayList<>();

        Double maxMag = ephRequest.getMaxMagnitude();

        for (List<PointShape> rangedGroup : rangedGroups) {
            if (rangedGroup.size() > 0) {
                for (PointShape pointShape : rangedGroup) {
                    Object amag = pointShape.getTagValue("AMAG");
                    if (maxMag == null || (amag != null && isLess(amag, maxMag))) {
                        GroupAtt groupAtt = GroupUtil.getOrCreateGroup(groups, pointShape.getName());
                        pointShape.setGroup(groupAtt);
                        if (ephRequest.getNamePopulationType() == NamePopulationType.DATETIME) {
                            pointShape.setName(JulianUtil.formatJulianRoundSeconds(pointShape.getJd(), 1, DateTimeUtil.getOffsetHours()));
                        } else if (ephRequest.getNamePopulationType() == NamePopulationType.TIME) {
                            pointShape.setName(JulianUtil.formatJulianTimeRoundSecondsSkipFrac(pointShape.getJd(), 1, DateTimeUtil.getOffsetHours()));
                        }
                        shapes.add(pointShape);
                    }
                }
            }
        }
        return shapes;
    }

    private static boolean isLess(Object whatLess, Double thanValue) {
        if (whatLess instanceof Integer) {
            Integer less = (Integer) whatLess;
            return less.intValue() < thanValue;
        }
        if (whatLess instanceof Double) {
            Double less = (Double) whatLess;
            return less < thanValue;
        }
        return false;
    }

    public static synchronized void interrupt() {
        if (thread != null) {
            thread.interrupt();
            try {
                thread.join(1000);
            } catch (InterruptedException e) {
            }
        }
    }
}
