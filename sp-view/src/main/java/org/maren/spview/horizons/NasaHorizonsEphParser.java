package org.maren.spview.horizons;

import cz.jmare.config.AppConfig;
import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.GlocUtil;
import cz.jmare.data.util.JulianUtil;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.geo.GeoLoc;
import cz.jmare.str.LineStrUtil;
import org.maren.spview.shape.GroupAtt;
import org.maren.spview.shape.PointShape;
import org.maren.spview.tag.TagSupported;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NasaHorizonsEphParser {
    final static String BEGIN = "$$SOE";
    final static String END = "$$EOE";
    final static String TARGET = "Target body name:";
    final static String GEO_LOC = "Center geodetic :";
    final static String CENTER_SITE = "Center-site name: GEOCENTRIC";
    final static String CENTER_BODY_NAME = "Center body name:";

    private final static Pattern TOKEN_PATTERN = Pattern.compile("[^\\s]+");

    private final static String COL_RADEC_ICRS = "R.A._____(ICRF)_____DEC";

    private final static String COL_RADEC_APPARENT = "R.A.__(a-apparent)__DEC";

    private final static String COL_DATE = "Date__(UT)__HR:MN";

    private final static String COL_DATE_TIME_SEC = "Date__(UT)__HR:MN:SS";

    private final static String COL_DATE_WITH_MILLIS = "Date__(UT)__HR:MN:SC.fff";

    private final static String RADEC = "RADEC";

    private final static String[] SUP_COLUMNS = { "APmag", "T-mag" };

    private final static String DATA_GEOLOC = "DATA_GEOLOC";

    private final static String DATA_NAME = "DATA_NAME";

    private final static String DATA_CENTER_BODY_NAME = "DATA_CENTER_BODY_NAME";

    private final static String DATA_EARTH_CENTER = "DATA_EARTH_CENTER";

    private final static String TAG_NAME_APPARENT_MAGNITUDE = "AMAG";

    public static Map<String, Object> parseData(String str) {
        Map<String, Object> map = new HashMap<>();

        String name = null;
        GeoLoc geoLoc = null;
        int indexTarget = str.indexOf(TARGET);
        if (indexTarget != -1) {
            int newLinePos = LineStrUtil.findNextLineStart(indexTarget + TARGET.length() + 1, str);
            name = str.substring(indexTarget + TARGET.length(), newLinePos).trim();
            int parIndex = name.indexOf(")");
            if (parIndex != -1) {
                name = name.substring(0, parIndex + 1);
            } else if (name.contains("{")) {
                name = name.substring(0, name.indexOf("{")).trim();
            }
            name = name.replace("\"", " ").trim();
            map.put(DATA_NAME, name);
        }

        boolean earth = false;
        int cbnNamePos = str.indexOf(CENTER_BODY_NAME);
        if (cbnNamePos != -1) {
            int nPos = LineStrUtil.findNextLineStart(cbnNamePos, str);
            if (nPos != -1) {
                String substring = str.substring(cbnNamePos, nPos);
                substring = trimRightLF(substring);
                if (substring.contains("Earth (399)")) {
                    earth = true;
                    map.put(DATA_EARTH_CENTER, true);
                }
                map.put(DATA_CENTER_BODY_NAME, substring);
            }
        } else {
            earth = true;
            map.put(DATA_EARTH_CENTER, true);
        }

        if (earth && str.indexOf(CENTER_SITE) == -1) {
            // not geocentric
            int indexGeo = str.indexOf(GEO_LOC);
            if (indexGeo != -1) {
                int newLinePos = LineStrUtil.findNextLineStart(indexGeo + GEO_LOC.length() + 1, str);
                String value = str.substring(indexGeo + GEO_LOC.length(), newLinePos).trim();
                String[] split = value.split("[, ]");
                if (split.length < 2) {
                    throw new IllegalStateException("Invalid format for geo location: " + value);
                }
                geoLoc = GlocUtil.parseCoordinatesNormalized(split[1] + ", " + split[0]);
                map.put(DATA_GEOLOC, geoLoc);
            }
        }

        return map;
    }

    public static String trimRightLF(String line) {
        if (line.length() == 0) {
            return line;
        }
        char c = line.charAt(line.length() - 1);
        if (c == '\r' || c == '\n') {
            char c2 = '\r';
            if (c == '\r') {
                c2 = '\n';
            }
            if (line.length() > 1) {
                int lastCut = 1;
                if (line.charAt(line.length() - 2) == c2) {
                    lastCut = 2;
                }
                return line.substring(0, line.length() - lastCut);
            } else {
                return "";
            }
        }
        return line;
    }

    public static Map<String, int[]> parseHeader(String string) {
        int index = string.indexOf(BEGIN);
        if (index == -1) {
            throw new IllegalStateException("No header found");
        }
        int prevStarsIndex = LineStrUtil.findPrevLineStart(index, string);
        int prevHeaderIndex = LineStrUtil.findPrevLineStart(prevStarsIndex, string);

        if (prevHeaderIndex == -1) {
            throw new IllegalStateException("No header found");
        }
        String headerLine = string.substring(prevHeaderIndex, prevStarsIndex);

        Matcher matcher = TOKEN_PATTERN.matcher(headerLine);
        Map<String, int[]> map = new HashMap<>();
        int start = 0;
        int i = 0;
        while (matcher.find()) {
            if (i > 1) {
                map.put(matcher.group(), new int[] { start, matcher.end() });
            } else {
                map.put(matcher.group(), new int[] { matcher.start(), matcher.end() });
            }
            start = matcher.end();
            i++;
        }

        return map;
    }

    private static List<PointShape> parsePoints(Map<String, int[]> header, String data) {
        List<PointShape> pointShapes = new ArrayList<>();
        int start = 0;
        int nextEnd = LineStrUtil.findNextLineStart(0, data);
        while (nextEnd != -1) {
            String line = data.substring(start, nextEnd);
            PointShape pointShape = parsePoint(line, header);
            pointShapes.add(pointShape);
            start = nextEnd;
            nextEnd = LineStrUtil.findNextLineStart(nextEnd, data);
        }
        return pointShapes;
    }

    private static PointShape parsePoint(String line, Map<String, int[]> header) {
        int[] radecRange = header.get(RADEC);
        if (radecRange == null) {
            throw new IllegalStateException("No RA DEC coordinates exist");
        }
        RaDec raDec = ACooUtil.parseCoords(line.substring(radecRange[0], radecRange[1]));
        int[] dateRange = header.get(COL_DATE);
        if (dateRange == null) {
            dateRange = header.get(COL_DATE_WITH_MILLIS);
        }
        if (dateRange == null) {
            dateRange = header.get(COL_DATE_TIME_SEC);
        }
        if (dateRange == null) {
            throw new IllegalStateException("No date time column found, line is: '" + line + "'");
        }
        double jd = JulianUtil.parseToJulian(line.substring(dateRange[0], dateRange[1]));

        PointShape pointShape = new PointShape(GroupAtt.EMPTY_GROUP, null, raDec);
        pointShape.setJd(jd);
        for (Map.Entry<String, int[]> entry : header.entrySet()) {
            for (int i = 0; i < SUP_COLUMNS.length; i++) {
                String supColumn = SUP_COLUMNS[i];
                if (entry.getKey().equals(supColumn)) {
                    int[] ints = entry.getValue();
                    String value = line.substring(ints[0], ints[1]).trim();
                    if (!"n.a.".equals(value)) {
                        pointShape.setTag(TAG_NAME_APPARENT_MAGNITUDE, value);
                    }
                }
            }
        }
        return pointShape;
    }

    private static List<PointShape> parsePoints(String str) {
        Map<String, int[]> header = parseHeader(str);
        limitOnlyOneRaDec(header);

        int beginIndex = str.indexOf(BEGIN);
        if (beginIndex == -1) {
            throw new IllegalStateException("No data found");
        }
        int endIndex = str.indexOf(END);
        if (endIndex == -1) {
            throw new IllegalStateException("No end of data found");
        }
        int dataStartIndex = LineStrUtil.findNextLineStart(beginIndex, str);
        String data = str.substring(dataStartIndex, endIndex);
        List<PointShape> pointShapes = parsePoints(header, data);
        return pointShapes;
    }

    public static List<PointShape> parseFullDataPoints(String str, String nasaHorizonsId) {
        List<PointShape> pointShapes = parsePoints(str);
        Map<String, Object> map = parseData(str);
        for (PointShape pointShape : pointShapes) {
            String name = (String) map.get(DATA_NAME);
            if (name != null) {
                pointShape.setName(name);
            }
            GeoLoc geoLoc = (GeoLoc) map.get(DATA_GEOLOC);
            if (geoLoc != null) {
                pointShape.setTag(TagSupported.GEOLOC.name(), GlocUtil.formatCoordsProfi(geoLoc));
            }
            if (nasaHorizonsId != null) {
                pointShape.setTag(TagSupported.NASA_HORIZ_ID.name(), nasaHorizonsId);
            }
        }
        return pointShapes;
    }

    private static void limitOnlyOneRaDec(Map<String, int[]> header) {
        if (header.containsKey(COL_RADEC_ICRS)) {
            int[] ints = header.get(COL_RADEC_ICRS);
            header.remove(COL_RADEC_ICRS);
            header.put(RADEC, ints);
        } else if (header.containsKey(COL_RADEC_APPARENT)) {
            int[] ints = header.get(COL_RADEC_APPARENT);
            header.remove(COL_RADEC_APPARENT);
            header.put(RADEC, ints);
        } else {
            throw new IllegalStateException("No RA DEC found");
        }
    }

    public static void main(String[] args) throws IOException {
        AppConfig instance = AppConfig.getInstance();
        instance.init(".sp-view");
        String str = Files.readString(Paths.get("C:\\Users\\jmarencik\\Downloads\\horizons_results(10).txt"));
        List<PointShape> pointShapes = parseFullDataPoints(str, null);
        for (PointShape pointShape : pointShapes) {
            System.out.println(pointShape);
        }
    }
}
