package org.maren.spview.tag;

public enum HiddenTag {

    SEEN,

    FAVOURITE;
}
