package org.maren.spview.tag;

public enum TagSupported {
    SPECT("The star's spectral type"),

    GEOLOC("Geographic location longitude and latitude"),

    NASA_HORIZ_ID("NASA Horizons ID");


    private String description;

    TagSupported(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
