package org.maren.spview.tag;

import cz.jmare.data.util.GlocUtil;
import cz.jmare.math.geo.GeoLoc;

public class TagTypeUtil {
    /**
     * Get value using the right type for a known tag or string for unknown custom type.
     * @param value
     * @return
     */
    public static Object parseValue(String value) {
        if (value == null) {
            return null;
        }
        value = value.trim();
        if (value.isEmpty()) {
            return null;
        }
        try {
            return Integer.parseInt(value);
        } catch(Exception e ){
        }

        try {
            return Double.parseDouble(value);
        } catch(Exception e ){
        }

        try {
            return parseGeoLoc(value);
        } catch(Exception e ){
        }

        return value;
    }

    public static String formatValue(Object object) {
        if (object == null) {
            return "";
        }
        if (object instanceof GeoLoc) {
            GeoLoc geoLoc = (GeoLoc) object;
            return "geoLoc(" + geoLoc.latitude + ", " + geoLoc.longitude + ")";
        }
        return object.toString();
    }

    public static int compare(Object value1, Object value2) {
        if (value1 instanceof Integer) {
            if (value2 instanceof Integer) {
                return ((Integer) value1).compareTo((Integer) value2);
            }
            if (value2 instanceof Double) {
                return Double.valueOf((Integer) value1).compareTo((Double) value2);
            }
            return -1;
        }
        if (value1 instanceof Double) {
            if (value2 instanceof Integer) {
                return ((Double) value1).compareTo(Double.valueOf((Integer) value2));
            }
            if (value2 instanceof Double) {
                return ((Double) value1).compareTo((Double) value2);
            }
            return -1;
        }
        if (value1 instanceof GeoLoc) {
            if (value2 instanceof GeoLoc) {
                GeoLoc loc1 = (GeoLoc) value1;
                GeoLoc loc2 = (GeoLoc) value2;
                if (loc1.latitude != loc2.latitude) {
                    return Double.valueOf(loc1.latitude).compareTo(Double.valueOf(loc2.latitude));
                }
                return Double.valueOf(loc1.longitude).compareTo(Double.valueOf(loc2.longitude));
            }
            return 1;
        }
        if (value1 instanceof String) {
            if (value2 instanceof GeoLoc) {
                return -1;
            }
        }

        return (value1.toString()).compareTo(value2.toString());
    }

    public static GeoLoc parseGeoLoc(String string) {
        if (string.startsWith("geoLoc(")) {
            if (!string.endsWith(")")) {
                throw new IllegalStateException("Invalid coordinated '" + string + "'");
            }
            String substring = string.substring("geoLoc(".length(), string.length() - 1);
            return GlocUtil.parseCoordinatesNormalized(substring);
        }
        try {
            return GlocUtil.parseCoordinatesNormalized(string);
        } catch (Exception e) {
            throw new IllegalArgumentException("'" + string + "' aren't geographic longitude and latitude coordinates");
        }
    }
}
