package org.maren.spview.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import cz.jmare.collection.KeepOrderSet;
import cz.jmare.file.FileUtil;

public class ObjectsListUtil {
    public static List<String> parseObjectsList(String str) {
        List<String> names = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new StringReader(str))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.isBlank()) {
                    continue;
                }

                int index = line.indexOf("#");
                if (index == -1) {
                    names.add(line.trim());
                } else {
                    names.add(line.substring(0, index).trim());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        KeepOrderSet<String> set = new KeepOrderSet<>();
        set.addAll(names);

        return new ArrayList<>(set);
    }

    public static String loadResourcesTextFile(String uri) {
        try (InputStream is = ObjectsListUtil.class.getResourceAsStream(uri)) {
            return FileUtil.getFileText(is, "ISO-8859-1");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
