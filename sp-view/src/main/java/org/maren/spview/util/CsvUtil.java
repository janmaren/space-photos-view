package org.maren.spview.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CsvUtil {
    public static char[] COLUMN_SEPARATORS = {';', ',', '|', '\t'};

    public static char detectDelimiter(String str) {
        char bestChar = ';';
        int bestCount = 0;
        for (char columnSeparator : COLUMN_SEPARATORS) {
            int count = countOf(columnSeparator, str);
            if (count > bestCount) {
                bestCount = count;
                bestChar = columnSeparator;
            }
        }
        if (bestCount < str.length() / 40) {
            int countSpace = countOf(' ', str);
            if (countSpace > str.length() / 40) {
                return ' ';
            }
        }
        return bestChar;
    }

    public static List<String[]> importCsv(String str) {
        return importCsv(str, detectDelimiter(str));
    }

    public static List<String[]> importCsv(String str, char delimiter) {
        return importCsv(str, delimiter, new HashSet<>());
    }

    public static List<String[]> importCsv(String str, HashSet<CsvWarning> warnings) {
        return importCsv(str, detectDelimiter(str), warnings);
    }

    public static List<String[]> importCsv(String str, char delimiter, HashSet<CsvWarning> warnings) {
        List<String[]> lines = new ArrayList<String[]>();
        try (BufferedReader br = new BufferedReader(new StringReader(str))) {
            String line;
            boolean wasData = false;
            boolean emptyLineAfterData = false;
            while ((line = br.readLine()) != null) {
                if (line.trim().startsWith("#")) {
                    if (wasData) {
                        warnings.add(CsvWarning.COMMENT_AFTER_DATA);
                    }
                    continue;
                }
                if (line.isBlank()) {
                    if (wasData) {
                        emptyLineAfterData = true;
                        continue;
                    } else {
                        continue;
                    }
                }
                if (emptyLineAfterData) {
                    warnings.add(CsvWarning.MULTIPLE_PARTS);
                    break;
                }
                String[] values = splitColumns(line, delimiter);
                lines.add(values);
                wasData = true;
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable read csv", e);
        }
        return lines;
    }
    
    public static String[] splitColumns(String line, char delimiter) {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (int i = 0; i < line.length(); i++) {
            char ch = line.charAt(i);
            if (ch == '"') {
                int j = i + 1;
                while (j < line.length()) {
                    if (line.charAt(j) == '"') {
                        if (j + 1 < line.length() && line.charAt(j + 1) == '"') {
                            j++;
                        } else {
                            break;
                        }
                    }
                    j++;
                }
                String substring = line.substring(i + 1, j);
                substring = substring.replace("\"\"", "\"");
                arrayList.add(substring);
                i = j + 1;
            } else {
                int j = i;
                while (j < line.length() && line.charAt(j) != delimiter) {
                    j++;
                }
                arrayList.add(line.substring(i, j));
                i = j;
            }
        }
        if (line.endsWith(String.valueOf(delimiter))) {
            arrayList.add("");
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    public static String escape(String data) {
        if (data == null) {
            return "";
        }
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'") || data.contains(";")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }
    
    private static int countOf(char ch, String str) {
        int len = str.length();
        int count = 0;
        boolean inText = false;
        for (int i = 0; i < len; i++) {
            char c = str.charAt(i);
            if (c == '"' && (i == 0 || str.charAt(i - 1) != '\\')) {
                inText = !inText;
                continue;
            }
            if (c == ch && !inText) {
                count++;
            }
        }
        return count;
    }

    public static boolean isEqual(String[] strs1, String[] strs2) {
        if (strs1 == null) {
            if (strs2 == null) {
                return true;
            }
            return false;
        } else if (strs2 == null) {
            return false;
        }

        if (strs1.length != strs2.length) {
            return false;
        }

        for (int i = 0; i < strs1.length; i++) {
            if (!strs1[i].equals(strs2[i])) {
                return false;
            }
        }
        return true;
    }
}
