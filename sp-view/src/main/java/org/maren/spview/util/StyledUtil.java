package org.maren.spview.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Font;

import java.util.function.Consumer;

public class StyledUtil {
    public static void addText(StyledText text, String str) {
        text.append(str);
    }

    public static void addLF(StyledText text) {
        text.append("\n");
    }

    public static void addInvText(StyledText text, String str) {
        addStyledText(text, str, SWT.ITALIC);
    }

    public static void addBoldText(StyledText text, String str) {
        addStyledText(text, str, SWT.BOLD);
    }

    public static void addStyledText(StyledText text, String str, int fontStyle) {
        int start = text.getText().length();
        text.append(str);
        StyleRange styleRange = new StyleRange();
        styleRange.start = start;
        styleRange.length = str.length();
        styleRange.fontStyle = fontStyle;
        text.setStyleRange(styleRange);
    }

    public static void addStyledText(StyledText text, String str, int fontStyle, Font font) {
        int start = text.getText().length();
        text.append(str);
        StyleRange styleRange = new StyleRange();
        styleRange.start = start;
        styleRange.length = str.length();
        styleRange.fontStyle = fontStyle;
        styleRange.font = font;
        text.setStyleRange(styleRange);
    }

    public static void addItalicText(StyledText text, String str) {
        addStyledText(text, str, SWT.ITALIC);
    }

    public static void addUnderlineBlueText(StyledText text, String str) {
        addStyledText(text, str, sr -> {sr.underline = true; sr.underlineStyle = SWT.UNDERLINE_LINK;});
    }

    public static void addUnderlinedText(StyledText text, String str) {
        addStyledText(text, str, sr -> {sr.underline = true;});
    }

    private static void addStyledText(StyledText text, String str, Consumer<StyleRange> consumer) {
        int start = text.getText().length();
        text.append(str);
        StyleRange styleRange = new StyleRange();
        styleRange.start = start;
        styleRange.length = str.length();
        consumer.accept(styleRange);
        text.setStyleRange(styleRange);
    }
}
