package org.maren.spview.util;

import org.maren.spview.shape.PointShape;

import java.util.Set;
import java.util.function.BiConsumer;

public class PointShapeMerger implements BiConsumer<PointShape, PointShape> {
    private int counter;
    
    @Override
    public void accept(PointShape dstShape, PointShape mrgShape) {
        Set<String> tagNames = mrgShape.getTagNames();
        for (String tagName : tagNames) {
            dstShape.setTag(tagName, mrgShape.getTagValue(tagName).toString());
        }
        counter++;
    }

    public int getCounter() {
        return counter;
    }
}
