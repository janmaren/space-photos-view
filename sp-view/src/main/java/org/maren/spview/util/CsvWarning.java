package org.maren.spview.util;

public enum CsvWarning {
    MULTIPLE_PARTS("There are multiple data sets - only first data loaded"), COMMENT_AFTER_DATA("There is a comment after data line");
    private String text;

    CsvWarning(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
