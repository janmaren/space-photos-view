package org.maren.spview.util;

import java.util.*;

import org.maren.spview.shape.PointShape;

import cz.jmare.math.astro.RaDec;

public class PointsIndex {
    private final static int DEFAULT_MERIDIANS = 360;
    private final static double DEFAULT_POLE_DEGREES = 4;
    
    private int meridiansNumber = DEFAULT_MERIDIANS;
    
    private double poleDegrees = DEFAULT_POLE_DEGREES;
    
    private List<List<PointShape>> indexed;
    private List<PointShape> nIndexed = new ArrayList<>();
    private List<PointShape> sIndexed = new ArrayList<>();
    private double northThreshold;
    private double southThreshold;
    private double poleOneRaDist;
    
    public PointsIndex() {
        indexed = new ArrayList<>(meridiansNumber);
        newIndexes();
        northThreshold = 90 - poleDegrees;
        southThreshold = -90 + poleDegrees;
        
        RaDec raDec1 = new RaDec(0, northThreshold);
        RaDec raDec2 = new RaDec(360 / meridiansNumber, northThreshold);
        poleOneRaDist = angularDistance(raDec1, raDec2);
    }
    
    public void addToIndex(Collection<PointShape> points) {
        for (PointShape p : points) {
            RaDec raDec = p.getRaDec();
            if (raDec.dec > northThreshold) {
                nIndexed.add(p);
                continue;
            } else if (raDec.dec < southThreshold) {
                sIndexed.add(p);
                continue;
            }
            
            int index = (int) (raDec.ra % meridiansNumber);
            List<PointShape> list = indexed.get(index);
            list.add(p);
        }
        
//        System.out.println("Norths: " + nIndexed.size());
//        System.out.println("Souths: " + sIndexed.size());
//        for (int i = 0; i < meridiansNumber; i++) {
//            System.out.println("Meridian " + i + ": " + indexed.get(i).size());
//        }
//        System.out.println("poleOneRaDist: " + cz.jmare.data.util.GonUtil.formatDegrees(poleOneRaDist));
    }

    public void removeFromIndex(Collection<PointShape> points) {
        for (PointShape pointShape: points){
            Set<List<PointShape>> fromIndex = getFromIndex(pointShape, 0);
            outer: for (List<PointShape> index : fromIndex) {
                for (Iterator<PointShape> iterator = index.iterator(); iterator.hasNext(); ) {
                    PointShape next = iterator.next();
                    if (next == pointShape) {
                        iterator.remove();
                        break outer;
                    }
                }
            }
        }
    }
    
    public Set<List<PointShape>> getFromIndex(PointShape pointShape, double around) {
        if (around > poleOneRaDist) {
            throw new IllegalArgumentException("Around tolerance " + around + " is greater than " + poleOneRaDist);
        }
        
        RaDec raDec = pointShape.getRaDec();
        Set<List<PointShape>> set = new HashSet<>();
        if (raDec.dec >= northThreshold) {
            set.add(nIndexed);
            if (raDec.dec - around >= northThreshold) {
                return set;
            }
        } else if (raDec.dec <= southThreshold) {
            set.add(sIndexed);
            if (raDec.dec + around <= southThreshold) {
                return set;
            }
        }
        
        int index = (int) (raDec.ra % meridiansNumber);
        set.add(indexed.get(index));
        
        int beforeIndex = (int) ((raDec.ra - around) % meridiansNumber);
        if (beforeIndex != index) {
            if (beforeIndex < 0) {
                beforeIndex = meridiansNumber - 1;
            }
            set.add(indexed.get(beforeIndex));
        }

        int afterIndex = (int) ((raDec.ra + around) % meridiansNumber);
        if (afterIndex != index) {
            if (afterIndex >= meridiansNumber) {
                afterIndex = 0;
            }
            set.add(indexed.get(afterIndex));
        }
        
        return set;
    }

    public Set<List<PointShape>> getFromIndex(double leftTopRa, double leftTopDec, double rightTopRa, double rightTopDec, double rightBottomRa, double rightBottomDec, double leftBottomRa, double leftBottomDec) {
        return null;
    }
    
    public static double angularDistance(RaDec raDec1, RaDec raDec2) {
        double beta1Rad = Math.toRadians(raDec1.dec);
        double beta2Rad = Math.toRadians(raDec2.dec);
        double lambda1Rad = Math.toRadians(raDec1.ra);
        double lambda2Rad = Math.toRadians(raDec2.ra);
        
        double sum = Math.sin(beta1Rad) * Math.sin(beta2Rad) + Math.cos(beta1Rad) * Math.cos(beta2Rad) * Math.cos(lambda1Rad - lambda2Rad);
        if (sum > 1.0) {
            if (sum > 1 + 1e-5) {
                return Double.NaN;
            } else {
                return 0;
            }
        }
        double distance = Math.acos(sum);
        
        return Math.toDegrees(distance);
    }
    
    public double maxAllowedTolerance() {
        return poleOneRaDist;
    }

    private void newIndexes() {
        for (int i = 0; i < meridiansNumber; i++) {
            indexed.add(new ArrayList<>());
        }
    }
}
