package org.maren.spview.util;

import cz.jmare.config.AppConfig;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

public class AppDirsUtil {
    public static File getDataDir(String dirRelativeToDataDir) {
        File existingDataDir = AppConfig.getInstance().getExistingDataDir();
        File dataDir = new File(existingDataDir, dirRelativeToDataDir);
        dataDir.mkdirs();
        return dataDir;
    }

    public static String escapeFileNameSpecialCharacters(String fileName) {
        StringBuilder result = new StringBuilder();
        StringCharacterIterator iterator = new StringCharacterIterator(fileName);
        char character = iterator.current();
        while (character != CharacterIterator.DONE) {
            switch (character) {
                case '/':
                case '\\':
                case ':':
                case '|':
                case '?':
                case '%':
                case '*':
                case '>':
                case '<': {
                    result.append("_").append((int) (character)).append("_");
                    break;
                }
                default:
                    // the char is not a special one
                    // add it to the result as is
                    result.append(character);
            }
            character = iterator.next();
        }
        return result.toString();
    }

    public static String getFitsDownloadPath() {
        String userHome = System.getProperty("user.home");
        Path homePath = Paths.get(userHome);
        Path downDir = homePath.resolve("Downloads");
        if (!Files.isDirectory(downDir)) {
            downDir = homePath;
        }
        return downDir.toString();
    }
}
