package org.maren.spview.util;

import java.util.StringJoiner;

public class CsvRow {
    private char delimiter = ',';
    
    private StringJoiner sj;
    
    private String delimString;
    
    public CsvRow() {
        super();
        this.delimString = String.valueOf(delimiter);
        sj = new StringJoiner(delimString);
    }

    public CsvRow(char delimiter) {
        super();
        this.delimiter = delimiter;
        this.delimString = String.valueOf(delimiter);
        sj = new StringJoiner(delimString);
    }
    
    public void add(String value) {
        if (value == null) {
            sj.add("");
        } else {
            sj.add(escape(value));
        }
    }
    
    public void add(Object value) {
        if (value == null) {
            sj.add("");
        } else {
            add(value.toString());
        }
    }
    
    public String escape(String data) {
        if (data == null) {
            return "";
        }
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'") || data.contains(";") || data.contains(delimString)) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }

    @Override
    public String toString() {
        return sj.toString();
    }
}
