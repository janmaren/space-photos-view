package org.maren.spview.util;

import org.maren.spview.app.RetrieveDataUtil;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.math.astro.RaDec;

public class SearchUtil {
    public static RaDec getSimbadContent(String queryName) {
        String content = RetrieveDataUtil.getSimbadContent(queryName);
        if (content == null) {
            return null;
        }
        String str = parseCoordsFromPlain(content);
        if (str == null) return null;
        return ACooUtil.parseCoords(str);
    }

    private static String parseCoordsFromPlain(String content) {
        int index = content.indexOf("ICRS");
        if (index == -1) {
            return null;
        }
        int ttIndex = content.indexOf(":", index + 1);
        int begin = ttIndex + ":".length();
        int end = content.indexOf("(", begin);
        String str = content.substring(begin, end).trim();
        return str;
    }
}
