package org.maren.spview.util;

import org.maren.spview.shape.PointShape;

import java.util.function.Function;

public class LabelFunctionHolder {
    private Function<PointShape, String> labelFunction;

    private boolean containsTime;

    public LabelFunctionHolder(Function<PointShape, String> labelFunction) {
        this.labelFunction = labelFunction;
    }

    public boolean isContainsTime() {
        return containsTime;
    }

    public void setContainsTime(boolean containsTime) {
        this.containsTime = containsTime;
    }

    public Function<PointShape, String> getLabelFunction() {
        return labelFunction;
    }
}
