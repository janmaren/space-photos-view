package org.maren.spview.util;

import cz.jmare.data.util.JulianUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalAccessor;

import static java.time.temporal.ChronoField.*;

public class DateTimeUtil {
    /**
     * Time offset hours
     */
    private static double timeOffset;

    private final static DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .append(DateTimeFormatter.ISO_LOCAL_DATE)
            .appendLiteral(' ')
            .appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(MINUTE_OF_HOUR, 2)
            .optionalStart()
            .appendLiteral(':')
            .appendValue(SECOND_OF_MINUTE, 2)
            .toFormatter();

    private final static DateTimeFormatter timeFormatter = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(MINUTE_OF_HOUR, 2)
            .optionalStart()
            .appendLiteral(':')
            .appendValue(SECOND_OF_MINUTE, 2)
            .toFormatter();

    private final static DateTimeFormatter dateFormatter = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .append(DateTimeFormatter.ISO_LOCAL_DATE).toFormatter();

    private final static DateTimeFormatter dayMonthFormatter = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(DAY_OF_MONTH, 2)
            .appendLiteral('.')
            .appendValue(MONTH_OF_YEAR, 2)
            .toFormatter();

    public static String format(LocalDateTime dateTime) {
        return dateTime.format(dateTimeFormatter);
    }

    public static String format(LocalDate date) {
        return dateFormatter.format(date);
    }

    public static String formatAsTime(LocalDateTime dateTime) {
        return dateTime.format(timeFormatter);
    }

    public static String formatAsDayMonth(LocalDate date) {
        return dayMonthFormatter.format(date);
    }

    public static LocalDateTime parse(String str) {
        TemporalAccessor parsed = dateTimeFormatter.parse(str.trim());
        return LocalDateTime.of(parsed.get(YEAR), parsed.get(MONTH_OF_YEAR), parsed.get(DAY_OF_MONTH), parsed.get(HOUR_OF_DAY), parsed.get(MINUTE_OF_HOUR), parsed.get(SECOND_OF_MINUTE));
    }

    public static LocalDate parseDate(String str) {
        TemporalAccessor parsed = dateFormatter.parse(str.trim());
        return LocalDate.of(parsed.get(YEAR), parsed.get(MONTH_OF_YEAR), parsed.get(DAY_OF_MONTH));
    }

    /**
     * Offset in hours
     * @return
     */
    public static double getOffsetHours() {
        return timeOffset;
    }

    public static void setOffsetHours(double timeOffset) {
        DateTimeUtil.timeOffset = timeOffset;
    }

    public static String getOffsetHoursString() {
        ZoneOffset zoneOffset = ZoneOffset.ofTotalSeconds((int) (getOffsetHours() * 3600.0));
        return zoneOffset.toString();
    }

    public static double getOffsetDays() {
        return timeOffset / 24.0;
    }

    public static String formatAsDateTimeOff(double jd) {
        return DateTimeUtil.format(JulianUtil.toOffsetDateTime(jd, getOffsetHours()).toLocalDateTime()) + " " + getOffsetHoursString();
    }

    /**
     * Return local date time but without offset part (showed on another place)
     * @param jd
     * @return
     */
    public static String formatAsDateTimeCutOff(double jd) {
        return DateTimeUtil.format(JulianUtil.toOffsetDateTime(jd, getOffsetHours()).toLocalDateTime());
    }

    public static String formatAsTimeOff(double jd) {
        return DateTimeUtil.formatAsTime(JulianUtil.toOffsetDateTime(jd, getOffsetHours()).toLocalDateTime()) + " " + getOffsetHoursString();
    }

    /**
     * Return local time but without offset part (showed on another place)
     * @param jd
     * @return
     */
    public static String formatAsTimeCutOff(double jd) {
        return DateTimeUtil.formatAsTime(JulianUtil.toOffsetDateTime(jd, getOffsetHours()).toLocalDateTime());
    }

    public static String formatAsDayMonth(double jd) {
        return DateTimeUtil.formatAsDayMonth(JulianUtil.toOffsetDateTime(jd, getOffsetHours()).toLocalDate());
    }
}
