package org.maren.spview.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StackUtil {
    public static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    public static void printStack() {
        System.out.println("====== Stack trace begin ======");
        System.out.println(dateFormat.format(new Date()));
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        for (int i = 2; i < elements.length; i++) {
          StackTraceElement s = elements[i];
          System.out.println("\tat " + s.getClassName() + "." + s.getMethodName()
              + "(" + s.getFileName() + ":" + s.getLineNumber() + ")");
        }
        System.out.println("====== Stack trace end ======");
    }
}
