package org.maren.spview.util;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

import org.maren.gis.util.GonUtil;
import org.maren.spview.shape.PointShape;

public class MergeUtil {
    public static void merge(Collection<PointShape> destShapes, Collection<PointShape> mergeShapes, BiConsumer<PointShape, PointShape> mergeOneShape, double maxDistance) {
        for (PointShape dstShape : destShapes) {
            double minDegrees = Double.MAX_VALUE;
            PointShape minShape = null;
            for (PointShape mrgShape : mergeShapes) {
                double beta1Rad = Math.toRadians(dstShape.getRaDec().dec);
                double beta2Rad = Math.toRadians(mrgShape.getRaDec().dec);
                double lambda1Rad = Math.toRadians(dstShape.getRaDec().ra);
                double lambda2Rad = Math.toRadians(mrgShape.getRaDec().ra);
                double angularDistanceRad = GonUtil.angularDistanceRad(lambda1Rad, beta1Rad, lambda2Rad, beta2Rad);
                double degrees = Math.toDegrees(angularDistanceRad);
                if (degrees < minDegrees) {
                    minDegrees = degrees;
                    minShape = mrgShape;
                }
            }
            if (minShape != null && minDegrees < maxDistance) {
                mergeOneShape.accept(dstShape, minShape);
            }
        }
    }
    
    public static void mergeOptimized(Collection<PointShape> destShapes, Collection<PointShape> mergeShapes, BiConsumer<PointShape, PointShape> mergeOneShape, double maxDistance) {
        PointsIndex pointsIndex = new PointsIndex();
        pointsIndex.addToIndex(mergeShapes);
        
        for (PointShape dstShape : destShapes) {
            double minDegrees = Double.MAX_VALUE;
            PointShape minShape = null;
            
            Set<List<PointShape>> set = pointsIndex.getFromIndex(dstShape, maxDistance);
            for (List<PointShape> list : set) {
                for (PointShape mrgShape : list) {
                    double beta1Rad = Math.toRadians(dstShape.getRaDec().dec);
                    double beta2Rad = Math.toRadians(mrgShape.getRaDec().dec);
                    double lambda1Rad = Math.toRadians(dstShape.getRaDec().ra);
                    double lambda2Rad = Math.toRadians(mrgShape.getRaDec().ra);
                    double angularDistanceRad = GonUtil.angularDistanceRad(lambda1Rad, beta1Rad, lambda2Rad, beta2Rad);
                    double degrees = Math.toDegrees(angularDistanceRad);
                    if (degrees < minDegrees) {
                        minDegrees = degrees;
                        minShape = mrgShape;
                    }
                }
            }
            if (minShape != null && minDegrees < maxDistance) {
                mergeOneShape.accept(dstShape, minShape);
            }
        }
    }
}
