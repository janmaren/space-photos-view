package org.maren.spview;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.data.util.GlocUtil;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.file.DirUtil;
import cz.jmare.file.PathUtil;
import cz.jmare.swt.color.ColorUtil;
import cz.jmare.graphfw.labeler.Labeler;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.geo.GeoLoc;
import cz.jmare.os.AppHelper;
import cz.jmare.os.PlatformType;
import cz.jmare.swt.action.ActionEventOperation;
import cz.jmare.swt.action.ActionOperation;
import cz.jmare.swt.action.SwitchOperation;
import cz.jmare.swt.color.ColorManager;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.key.MouseLogger;
import cz.jmare.swt.status.StatusUtil;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.Printer;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.sphericcanvas.FindSelectionListener;
import org.maren.gis.sphericcanvas.ToolListener;
import org.maren.gis.type.Spheric;
import org.maren.spview.app.*;
import org.maren.spview.guiutil.DateContribution;
import org.maren.spview.horizons.EphemerisRequester;
import org.maren.spview.item.RasterItem;
import org.maren.spview.item.RasterSet;
import org.maren.spview.prolink.CzskyProlink;
import org.maren.spview.prolink.Prolink;
import org.maren.spview.prolink.SimbadProlink;
import org.maren.spview.prolink.SkyserverProlink;
import org.maren.spview.shape.PointShape;
import org.maren.spview.shape.Shape;
import org.maren.spview.table.AbstractShapesComposite;
import org.maren.spview.table.CirclesComposite;
import org.maren.spview.table.PointsComposite;
import org.maren.spview.table.TextFilesLoader;
import org.maren.spview.util.AppDirsUtil;
import org.maren.spview.util.SearchUtil;
import org.maren.spview.wcstransform.ImageRegistrationApp;
import org.maren.spview.wcstransform.SPCaller;
import org.maren.swt.util.ConvTypesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.File;
import java.lang.ref.SoftReference;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

import static org.maren.spview.ConfigKeys.DEFAULT_MAP_BACKGROUND_COLOR;
import static org.maren.spview.ConfigKeys.KEY_CENTER_ADDED_RASTER;
import static org.maren.spview.ConfigKeys.KEY_MAP_BACKGROUND_COLOR;

public class SPView extends ApplicationWindow implements RasterTableListener, FindSelectionListener, GetCoordsListener, SetPointsListener, SPCaller, DrawShapesListener, NearSelectionClickedListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(SPView.class);
    public static final int SMALLER_WIDTH_FROM_RIGHT = -550;
    public static final int LARGER_WIDTH_FROM_RIGHT = -720;

    public static final int SMALLER_GTK_WIDTH_FROM_RIGHT = -500;

    public static final int LARGER_GTK_WIDTH_FROM_RIGHT = -580;

    private static final Prolink[] PROLINKS = {new SimbadProlink(), new SkyserverProlink(),
            new CzskyProlink()};

    public static List<String> PURE_IMAGE_SUFFIXES = List.of(".jpg", ".jpeg", ".tif", ".tiff", ".png", ".bmp", ".gif");

    public static List<String> IMAGE_SUFFIXES = new ArrayList<>();

    static {
        IMAGE_SUFFIXES.add(".fit");
        IMAGE_SUFFIXES.add(".fits");
        IMAGE_SUFFIXES.addAll(PURE_IMAGE_SUFFIXES);
    }

    public static List<String> TEXT_SUFFIXES = List.of(".csv", ".tsv", ".txt");

    private static String FITS_SUFFIX = ".fits";
    
    public static String WLD_SUFFIX = ".wld";
    
    public static List<String> ALL_SUPPORTED_SUFFIXES = new ArrayList<String>();
    static {
        ALL_SUPPORTED_SUFFIXES.addAll(IMAGE_SUFFIXES);
        ALL_SUPPORTED_SUFFIXES.add(FITS_SUFFIX);
        ALL_SUPPORTED_SUFFIXES.add(WLD_SUFFIX);
        ALL_SUPPORTED_SUFFIXES.addAll(TEXT_SUFFIXES);
    }

    private static final List<String> STAR_COLORS = List.of("#EF2CF0", "#2CF084", "#1F9FD6", "#F0EB2C");
    private static final List<String> STAR_COLORS_ICONS = List.of("color-violet", "color-green", "color-blue", "color-yellow");
    
    private static final List<Double> STAR_MAG = List.of(-1.0, 9.0, 6.0, 4.5, 3.0);
    private static final List<String> STAR_MAG_ICONS = List.of("mag-unlimited", "mag-9", "mag-6", "mag-4.5", "mag-3");
  
    StatusLineManager slm = new StatusLineManager();
    MenuManager menuManager = new MenuManager();
    ToolBarManager toolbarManager = new ToolBarManager();
    
    private MenuManager menuFile;
    private MenuManager menuView;
    @SuppressWarnings("unused")
    private MenuManager menuOperation;
    @SuppressWarnings("unused")
    private MenuManager menuTool;
    private MenuManager menuHelp;
    private SPViewDeskComposite spViewComposite;
    private ActionOperation openFileOperation;
    private ActionOperation openDirOperation;
    private ActionOperation openImageRegistration;
    private SwitchOperation showDefaultStarsOperation;
    private SwitchOperation showCrossOperation;
    private List<String> passedFiles;
    private boolean passedRecursive;
    private RastersTable rastersTable;

    private SwitchOperation gridOperation;

    private ActionOperation changeColorOperation;
    private ActionOperation changeMagOperation;

    private boolean addFileCenter;

    private ActionOperation showInBrowserOperation;
    
    private List<CelestialObject> celestialObjects;
    
    private ActionOperation aboutOperation;
    
    private RastersLoader rastersLoader;

    private Clipboard clipboard;
    
    private int starColorIndex;
    
    private int starMagIndex;

    private SwitchOperation showConstellationsOperation;

    private ActionOperation downloadFitsOperation;

    private PointsComposite pointsComposite;

    private CirclesComposite circlesComposite;

    private List<AbstractShapesComposite<?>> shapeTables = new ArrayList<>();

    private TabFolder tabFolder;
    
    private Color crossColor = null;
    private ActionOperation configOperation;

    private ActionOperation printOperation;
    private SwitchOperation underAltOperation;
    private DateContribution dateContribution;

    private ActionEventOperation timeBackOperation;

    private ActionEventOperation timeForwOperation;

    private List<SoftReference<ImageRegistrationApp>> editPointListeners = new ArrayList<>();
    private ActionOperation saveImageOperation;
    private ActionOperation polarisOperation;
    private ActionOperation findOperation;
    private Menu prolinksMenu;


    public SPView(List<String> files, boolean recursive) {
        super(null);
        this.passedFiles = files;
        this.passedRecursive = recursive;
        StatusUtil.setStatusLineManager(slm);
        menuManager.add(menuFile = new MenuManager("File"));
        menuManager.add(menuView = new MenuManager("View"));
        menuManager.add(menuOperation = new MenuManager("Operation"));
        menuManager.add(menuTool = new MenuManager("Tool"));
        menuManager.add(menuHelp = new MenuManager("Help"));
        addMenuBar();
        addToolBar(SWT.FLAT | SWT.WRAP);
        addStatusLine();
    }
    
    private void initImages() {
        ImageUtil.put("undo", "/image/edit-undo.png");
        ImageUtil.put("redo", "/image/edit-redo.png");
        ImageUtil.put("new", "/image/new.png");
        ImageUtil.put("import", "/image/import.png");
        ImageUtil.put("left", "/image/1leftarrow.png");
        ImageUtil.put("right", "/image/1rightarrow.png");
        ImageUtil.put("registration", "/image/registration.png");
        ImageUtil.put("up", "/image/1uparrow.png");
        ImageUtil.put("down", "/image/1downarrow.png");
        ImageUtil.put("color-violet", "/image/color-violet.png");
        ImageUtil.put("color-green", "/image/color-green.png");
        ImageUtil.put("color-yellow", "/image/color-yellow.png");
        ImageUtil.put("color-blue", "/image/color-blue.png");
        ImageUtil.put("mag-unlimited", "/image/m-unlimited.png");
        ImageUtil.put("mag-3", "/image/m3.png");
        ImageUtil.put("mag-4.5", "/image/m45.png");
        ImageUtil.put("mag-6", "/image/m6.png");
        ImageUtil.put("mag-9", "/image/m9.png");
        ImageUtil.put("globe", "/image/globe-with-meridians.png");
        ImageUtil.put("underalt", "/image/underalt.png");
        ImageUtil.put("star", "/image/star.png");
        ImageUtil.put("go", "/image/cross-go.png");
        ImageUtil.put("dropper", "/image/cross-grab.png");
        ImageUtil.put("cross", "/image/cross.png");
        ImageUtil.put("folder", "/image/folder.png");
        ImageUtil.put("folderd", "/image/folderd.png");
        ImageUtil.put("addfilecenter", "/image/add-file-center.png");
        ImageUtil.put("constellations", "/image/lines16.png");
        ImageUtil.put("raster", "/image/download-fits.png");
        ImageUtil.put("raster-points", "/image/download-fits-question2.png");
        ImageUtil.put("browser", "/image/www-icon.png");
        ImageUtil.put("cross-cursor", "/image/cross-cursor-16.png");
        ImageUtil.put("shift-left", "/image/shift-left.png");
        ImageUtil.put("shift-right", "/image/shift-right.png");
        ImageUtil.put("settings", "/image/configure.png");
        ImageUtil.put("print", "/image/printer.png");
        ImageUtil.put("back", "/image/goback.png");
        ImageUtil.put("forward", "/image/goforw.png");
        ImageUtil.put("save-image", "/image/document-save.png");
        ImageUtil.put("find", "/image/find.png");
    }
   
    private void initDrop() {
        DropTarget dropTarget = new DropTarget(getShell(), DND.DROP_MOVE | DND.DROP_COPY);
        dropTarget.setTransfer(new Transfer[] { FileTransfer.getInstance() });
        dropTarget.addDropListener(new DropTargetAdapter() {
            public void drop(DropTargetEvent event) {
                Object data = event.data;
                if (data instanceof String[]) {
                    String[] strs = (String[]) data;
                    if (strs.length > 0) {
                        List<String> files = Arrays.asList(strs);
                        addFiles(files);
                    }
                }
            }
        });
    }

    @Override
    protected Control createContents(Composite parent) {
        initImages();
        initActions();

        KeyLogger.init(this);
        MouseLogger.initWithStatus(slm);
        
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new FillLayout());

        SashForm leftRightSash = new SashForm(composite, SWT.NONE);

        spViewComposite = new SPViewDeskComposite(leftRightSash, SWT.NONE);
        spViewComposite.setPointClickedListener(this);
        spViewComposite.setDrawShapesListener(this);
        spViewComposite.setNearSelectionClickedListener(this);
        AppConfig appConfig = AppConfig.getInstance();
        ConfigStore configStore = appConfig.getConfigStore();
        boolean underHorizon = configStore.getBoolValue("show.horizon", false);
        if (underHorizon) {
            spViewComposite.setShowUnderAlt(underHorizon);
            underAltOperation.setChecked(true);
        }

        populateMenuFile();
        
        toolbarManager.add(new Separator());
        
        populateMenuView();
        
        toolbarManager.add(new Separator());
        
        populateMenuOperation();
        
        toolbarManager.add(new Separator());
        
        populateMenuTool();

        populateMenuHelp();

        updateControls();
        
        createRightPanel(leftRightSash);
        leftRightSash.setWeights(56, 44);
        menuManager.update(true);
        toolbarManager.update(true);
        menuFile.update(true);
        menuView.update(true);
        menuHelp.update(true);
        
        spViewComposite.setStarsColor(ColorUtil.getColor(STAR_COLORS.get(0)));

        StatusUtil.infoMessage("Loading files...");
        try {
            loadPassedFiles();
        } catch (Exception e) {
            LOGGER.error("Problems to load passed files", e);
        }
        initDrop();

        clipboard = new Clipboard(getShell().getDisplay());
        spViewComposite.setClipboard(clipboard);
        
        showObjects();
        crossColor = ColorUtil.getColorByRGB(255, 128, 0);

        String geoLocStr = configStore.getValue(ConfigKeys.GEO_LOCATION, null);
        if (geoLocStr != null) {
            GeoLoc geoLoc = GlocUtil.parseCoordinatesNormalized(geoLocStr);
            spViewComposite.setGeoLoc(geoLoc);
            if (geoLoc.latitude < 0) {
                spViewComposite.setCenter(new Spheric(Math.PI, -Math.PI));
            }
        }
        addFileCenter = configStore.getBoolValue(KEY_CENTER_ADDED_RASTER, false);

        Color mapBackColor = ColorManager.getColor(configStore.getValue(KEY_MAP_BACKGROUND_COLOR, DEFAULT_MAP_BACKGROUND_COLOR));
        spViewComposite.setMapBackgroundColor(mapBackColor);

        return composite;
    }
    
    private void loadPassedFiles() {
        addFiles(passedFiles);
    }
    
    private String findUniqueGroupName(String requiredName) {
        List<RasterItem> allRasters = rastersTable.getAllRasters();
        Set<String> groupNames = new HashSet<>();
        for (RasterItem rasterItem : allRasters) {
            groupNames.add(rasterItem.getGroup().getName());
        }
        if (!groupNames.contains(requiredName)) {
            return requiredName;
        }
        int i = 0;
        while (groupNames.contains(requiredName + "(" + i + ")")) {
            i++;
        }
        return requiredName + "(" + i + ")";
    }
    
    private void addFiles(List<String> files) {
        List<AddFileRequest> addRasterFileRequestList = new ArrayList<AddFileRequest>();
        List<File> addTextFileRequestList = new ArrayList<File>();
        Map<String, RasterSet> rasterSets = new HashMap<>();
        for (String string : files) {
            LOGGER.debug("Adding " + string);
            List<Path> filesInDir;

            String detectedGroupName = null;
            File file = new File(string);
            if (file.isFile()) {
                String[] fullPath = PathUtil.parseFullPath(string);
                detectedGroupName = PathUtil.getLastPartFromPathNice(fullPath[0]);
                filesInDir = List.of(Paths.get(string));
            } else if (file.isDirectory()) {
                detectedGroupName = PathUtil.getLastPartFromPathNice(string);
                filesInDir = getFilesInDir(Paths.get(string), passedRecursive);
            } else {
                continue;
            }
            detectedGroupName = findUniqueGroupName(detectedGroupName);
            String finalName = detectedGroupName;
            RasterSet rasterSet = rasterSets.computeIfAbsent(finalName, key -> new RasterSet(finalName));

            for (Path path : filesInDir) {
                String[] fullPath = PathUtil.parseFullPath(path.toString());
                String suffix = PathUtil.getSuffixOrEmpty(path.toString()).toLowerCase();
                if (!ALL_SUPPORTED_SUFFIXES.contains(suffix)) {
                    continue;
                }
                
                String[] pathSuffix = PathUtil.parseFullPathSuffix(path.toString());

                if (TEXT_SUFFIXES.contains(suffix)) {
                    addTextFileRequestList.add(path.toFile());
                    continue;
                }

                if (pathSuffix[1].equals(".wld")) {
                    String imageName = findImageName(pathSuffix[0]);
                    if (imageName == null) {
                        throw new IllegalStateException("There is not image file for " + path);
                    }
                    path = Paths.get(imageName);
                    fullPath[1] = new File(imageName).getName();
                }
                
                try {
                    addRasterFileRequestList.add(new AddFileRequest(path.toFile(), fullPath[1], rasterSet));
                } catch (Exception e) {
                    LOGGER.error("Unable to add file {}", path, e);
                    StatusUtil.errorMessage(ExceptionMessage.getCombinedMessage("Unable to read " + path, e));
                }
            }
        }
        
        rastersLoader.addFiles(addRasterFileRequestList);
        if (!addTextFileRequestList.isEmpty()) {
            new Thread(() -> {
                TextFilesLoader textFilesLoader = new TextFilesLoader(getShell(), shapeTables, b -> {
                    getShell().getDisplay().asyncExec(() -> spViewComposite.refresh());
                });
                List<File> errs = textFilesLoader.loadFiles(addTextFileRequestList, true, true);
                getShell().getDisplay().asyncExec(() -> {
                    if (errs.isEmpty()) {
                        String warnings = textFilesLoader.getWarnings();
                        if (warnings != null) {
                            setWarn(warnings);
                        } else {
                            setInfo("All text files loaded");
                        }
                    } else {
                        setError("Not all text files loaded, like " + errs.get(0).getName());
                    }
                });
            }).start();
        }
    }

    private List<Path> getFilesInDir(Path dir, boolean recursive) {
        return DirUtil.listEntities(dir, recursive ? 100 : 1, (p, a) -> {
            String pathFile = p.getFileName().toString();
            String[] nameAndSuffixOrEmpty = PathUtil.getNameAndSuffixOrEmpty(pathFile);
            return ALL_SUPPORTED_SUFFIXES.contains(nameAndSuffixOrEmpty[1].toLowerCase());
        }, true);
    }
    
    private void createRightPanel(Composite panelComposite) {
        tabFolder = new TabFolder(panelComposite, SWT.NONE);
        //tabFolder.setLayout(new FillLayout());
        tabFolder.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));

        TabItem points2TabItem = new TabItem(tabFolder, SWT.NONE);
        points2TabItem.setText("Points");
        pointsComposite = new org.maren.spview.table.PointsComposite(tabFolder, SWT.NONE, this);
        points2TabItem.setControl(pointsComposite);
        
        TabItem circlesTabItem = new TabItem(tabFolder, SWT.NONE);
        circlesTabItem.setText("Circles");
        circlesComposite = new CirclesComposite(tabFolder, SWT.NONE, this);
        circlesTabItem.setControl(circlesComposite);

        TabItem rastersTabItem = new TabItem(tabFolder, SWT.NONE);
        rastersTabItem.setText("Rasters");
        rastersTable = new RastersTableComposite(tabFolder, SWT.NONE, this);
        rastersTabItem.setControl((RastersTableComposite) rastersTable);
        spViewComposite.setRastersTable(rastersTable);
        rastersLoader = new RastersLoader(this, rastersTable);

        shapeTables.add(pointsComposite);
        shapeTables.add(circlesComposite);


//        EphRequest ephRequest = new EphRequest(List.of("2I Borisov", "P/1994 N2", "C/1995 O1", "P/1996 R2", "P/1997 " +
//                "B1", "P/1998 QP54", "P/1998 VS24", "P/1999 D1"), OffsetDateTime.now(),
//                                               OffsetDateTime.now().plus(1, ChronoUnit.DAYS), 1, IntType.HOUR);
//
//        EphemerisRequester.addToQueue(ephRequest, pointsComposite);
    }

    @Override
    public boolean close() {
        LOGGER.info("shutdown");
        boolean close = super.close();
        EphemerisRequester.interrupt();
        rastersLoader.destroy();
        clipboard.dispose();
        return close;
    }
    
    protected void initializeBounds() {
        getShell().setText("SPView");
        getShell().setSize(1600, 1200);
        getShell().setMinimumSize(1150, 750);
    }

    @Override
    protected StatusLineManager createStatusLineManager() {
        return slm;
    }

    @Override
    protected MenuManager createMenuManager() {
        return menuManager;
    }

    @Override
    protected ToolBarManager createToolBarManager(int style) {
        return toolbarManager;
    }
    
    public void updateControls() {
        
    }

    public void populateMenuFile() {
        menuFile.add(openFileOperation);
        menuFile.add(openDirOperation);
        menuFile.add(saveImageOperation);
        menuFile.add(openImageRegistration);
        toolbarManager.add(openFileOperation);
        toolbarManager.add(openDirOperation);
        toolbarManager.add(saveImageOperation);
        toolbarManager.add(printOperation);
    }

    public void populateMenuView() {
        menuView.add(findOperation);
        menuView.add(gridOperation);
        menuView.add(showCrossOperation);
        menuView.add(showDefaultStarsOperation);
        menuView.add(showConstellationsOperation);
        menuView.add(changeColorOperation);
        menuView.add(changeMagOperation);
        menuView.add(showInBrowserOperation);
        menuView.add(downloadFitsOperation);
        menuView.add(configOperation);
        toolbarManager.add(findOperation);
        toolbarManager.add(gridOperation);
        toolbarManager.add(showCrossOperation);
        toolbarManager.add(underAltOperation);
        toolbarManager.add(showDefaultStarsOperation);
        toolbarManager.add(showConstellationsOperation);
        toolbarManager.add(changeColorOperation);
        toolbarManager.add(changeMagOperation);
        toolbarManager.add(showInBrowserOperation);
        toolbarManager.add(downloadFitsOperation);
    }

    public void populateMenuOperation() {
        toolbarManager.add(timeBackOperation);
        toolbarManager.add(dateContribution);
        toolbarManager.add(timeForwOperation);
        spViewComposite.setJulianDate(dateContribution.getJulianDate());
    }

    public void populateMenuTool() {
        menuHelp.add(printOperation);
        menuHelp.add(polarisOperation);
        menuHelp.add(aboutOperation);
    }

    public void populateMenuHelp() {
    }
    
 
    private void initActions() {
        openFileOperation = new ActionOperation("Open File", "folder", SWT.CTRL | 'L', () -> {
            FileDialog fd = new FileDialog(getShell(), SWT.OPEN | SWT.MULTI);
            fd.setText("Image Files to Open");
            StringBuilder sb = new StringBuilder();
            ArrayList<String> suffixes = new ArrayList<>();
            for (String supportedSuffix: ALL_SUPPORTED_SUFFIXES) {
                if (sb.length() > 0) {
                    sb.append(";");
                }
                sb.append("*" + supportedSuffix + (AppHelper.getPlatform() == PlatformType.LINUX ? ";*" + supportedSuffix.toUpperCase() : ""));
                suffixes.add("*" + supportedSuffix  + (AppHelper.getPlatform() == PlatformType.LINUX ? ";*" + supportedSuffix.toUpperCase() : ""));
            }
            suffixes.add(0, sb.toString());
            String[] filterExt = suffixes.toArray(new String[suffixes.size()]);
            fd.setFilterExtensions(filterExt);
            String path = null;
            if ((path = fd.open()) != null) {
                File dir = new File(path).getParentFile();
                List<String> paths = Arrays.asList(fd.getFileNames()).stream().map(n -> (new File(dir, n)).toString()).collect(Collectors.toList());
                addFiles(paths);
            }
        });
        openDirOperation = new ActionOperation("Open Directory", "folderd", SWT.CTRL | 'D', () -> {
            DirectoryDialog dlg = new DirectoryDialog(getShell());
            dlg.setText("Directory Containing Images to Open");
            dlg.setMessage("Select a directory from which to load images");
            String dir = dlg.open();
            if (dir != null) {
                addFiles(List.of(dir));
            }
        });
        openImageRegistration = new ActionOperation("WCS Solving", "registration", SWT.NONE, () -> {
            ImageRegistrationApp imageRegistering = new ImageRegistrationApp();
            imageRegistering.setSpCaller(this);
            imageRegistering.setGetCoordsListener(this);
            imageRegistering.setSetPointsListener(this);
            imageRegistering.open();
            editPointListeners.add(new SoftReference<>(imageRegistering));
        });
        findOperation = new ActionOperation("Find", "find", SWT.CTRL | 'F', () -> {
            InputDialog inputDialog = new InputDialog(getShell(), "Find Object",
                    "Name (like sirius, NGC 7822, IC 4665, x Cygni, M31, Sh2-88, 52 Ori)", "",
                    v -> v.length() > 0 && v.trim().length() == 0 ? "Not valid name" : null);
            if (inputDialog.open() == Window.OK) {
                String value = inputDialog.getValue();
                if (value != null) {
                    try {
                        RaDec raDec = SearchUtil.getSimbadContent(value);
                        if (raDec != null) {
                            spViewComposite.setCenter(ConvTypesUtil.toSpheric(raDec));
                            spViewComposite.refresh();
                        } else {
                            setWarn("Not found '" + value + "'");
                        }
                    } catch (Exception e) {
                        setError(ExceptionMessage.getCombinedMessage("Unable to search for '" + value + "'", e), e);
                    }
                }
            }
        });
        showDefaultStarsOperation = new SwitchOperation("Show Default Stars", "star", SWT.ALT | 'S', (b) -> {
            if (b) {
                doLoadDefaultStars();
            } else {
                disableStars();
            }
        }, true);
        showConstellationsOperation = new SwitchOperation("Show Constellations", "constellations", SWT.ALT | 'C', (b) -> {
            if (b) {
                spViewComposite.setConstellations(StarsReader.getConstellations());
            } else {
                spViewComposite.setConstellations(null);
            }
            spViewComposite.getSphericCanvas().refresh();
        }, true);
        showCrossOperation = new SwitchOperation("Show Cross", "cross", SWT.ALT | 'C', (b) -> {
            spViewComposite.showCross(b);
        }, true);
        
        gridOperation = new SwitchOperation("Show Meridians and Parallels", "globe", SWT.ALT | 'G', (b) -> {
            spViewComposite.setGrid(b);
        }, true);

        AppConfig appConfig = AppConfig.getInstance();
        ConfigStore configStore = appConfig.getConfigStore();
        underAltOperation = new SwitchOperation("Show Area under Horizon", "underalt", SWT.ALT | 'U', (b) -> {
            if (b) {
                String geoLoc = configStore.getValue(ConfigKeys.GEO_LOCATION, null);
                if (geoLoc == null) {
                    setInfo("Please, populate geo location");
                    SettingsDialog settingsDialog = new SettingsDialog(getShell());
                    settingsDialog.setInitialMessage("Set geo location");
                    settingsDialog.setFocusField(SettingsDialog.FocusField.GEO_LOC);
                    settingsDialog.open();
                    dateContribution.updateDateTimeZoneText();
                    addFileCenter = configStore.getBoolValue(KEY_CENTER_ADDED_RASTER, false);
                }
                geoLoc = configStore.getValue(ConfigKeys.GEO_LOCATION, null);
                if (geoLoc == null) {
                    setWarn("Geo location is not populated");
                    underAltOperation.setChecked(false);
                    return;
                }
                spViewComposite.setGeoLoc(GlocUtil.parseCoordinatesNormalized(geoLoc));
            }
            spViewComposite.setShowUnderAlt(b);
            spViewComposite.refresh();
            configStore.putBoolValue("show.horizon", b);
        }, false);

        changeColorOperation = new ActionOperation("Stars Color", "color-violet", () -> {
            starColorIndex++;
            if (starColorIndex >= STAR_COLORS.size()) {
                starColorIndex = 0;
            }
            String color = STAR_COLORS.get(starColorIndex);
            String iconName = STAR_COLORS_ICONS.get(starColorIndex);
            changeColorOperation.setImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get(iconName)));
            spViewComposite.setStarsColor(ColorUtil.getColor(color));
        });
        
        changeMagOperation = new ActionOperation("Maximal Magnitude", "mag-unlimited", () -> {
            starMagIndex++;
            if (starMagIndex >= STAR_MAG.size()) {
                starMagIndex = 0;
            }
            Double mag = STAR_MAG.get(starMagIndex);
            if (mag == -1.0) {
                mag = null;
            }
            String iconName = STAR_MAG_ICONS.get(starMagIndex);
            changeMagOperation.setImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get(iconName)));
            spViewComposite.setMagnitudeLimit(mag);
        });

        prolinksMenu = new Menu(getShell(), SWT.POP_UP);
        showInBrowserOperation = new ActionOperation("Show in Browser", "browser", SWT.CTRL | 'B', () -> {
            MenuItem defaultItem = prolinksMenu.getDefaultItem();
            if (defaultItem != null) {
                doProlink((Prolink) defaultItem.getData());
            } else {
                prolinksMenu.setVisible(true);
            }
        });
        for (int i = 0; i < PROLINKS.length; i++) {
            Prolink prolink = PROLINKS[i];
            MenuItem item = new MenuItem(prolinksMenu, SWT.RADIO);
            item.setText(prolink.getName());
            item.setData(prolink);
            item.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    MenuItem item = (MenuItem) e.widget;
                    prolinksMenu.setDefaultItem(item);
                    doProlink((Prolink) item.getData());
                }
            });
        }
        showInBrowserOperation.setMenuCreator(new IMenuCreator() {
            @Override
            public void dispose() {
            }

            @Override
            public Menu getMenu(Control parent) {
                return prolinksMenu;
            }

            @Override
            public Menu getMenu(Menu parent) {
                return prolinksMenu;
            }
        });

        downloadFitsOperation = new ActionOperation("Download Fits", "raster", SWT.ALT | 'E', () -> {
        }){
            @Override
            public void runWithEvent(Event event) {
                if ((event.stateMask & SWT.CTRL) != 0) {
                    DownloadFitsDialog downloadFitsDialog = new DownloadFitsDialog(getShell());
                    if (downloadFitsDialog.open() != Window.OK) {
                        return;
                    }
                    String suggestName = downloadFitsDialog.getResultName();
                    downloadEso(suggestName);
                } else {
                    downloadEso(null);
                }
            }
        };
        downloadFitsOperation.setToolTipText("Download fits raster using last configuration.\nUse CTRL for configuration");
        
        aboutOperation = new ActionOperation("About", SWT.CTRL | SWT.SHIFT | 'H', () -> {
            new AboutDialog(getShell()).open();
        });
        polarisOperation = new ActionOperation("Polaris", SWT.CTRL | SWT.SHIFT | 'P', () -> {
            new PolarisDialog(getShell()).open();
        });
        
        configOperation = new ActionOperation("Settings", "settings", () -> {
            if (new SettingsDialog(getShell()).open() == Window.OK) {
                dateContribution.updateDateTimeZoneText();
                String geoLoc = configStore.getValue(ConfigKeys.GEO_LOCATION, null);
                if (geoLoc != null) {
                    spViewComposite.setGeoLoc(GlocUtil.parseCoordinatesNormalized(geoLoc));
                } else {
                    spViewComposite.setGeoLoc(null);
                    underAltOperation.setChecked(false);
                }
                addFileCenter = configStore.getBoolValue(KEY_CENTER_ADDED_RASTER, false);
                Color mapBackColor = ColorManager.getColor(configStore.getValue(KEY_MAP_BACKGROUND_COLOR, DEFAULT_MAP_BACKGROUND_COLOR));
                spViewComposite.setMapBackgroundColor(mapBackColor);
                refresh();
            }
        });

        printOperation = new ActionOperation("Print", "print", () -> {
            PrintDialog dialog = new PrintDialog(getShell(), SWT.NULL);
            PrinterData printerData = dialog.open();

            Printer printer = new Printer(printerData);

            Point printerDPI = printer.getDPI();
            if (printer.startJob("spview")) {
                if (printer.startPage()) {
                    Rectangle rect = printer.getClientArea(); // Get the printable area
                    Rectangle trim = printer.computeTrim(0, 0, 0, 0); // Get the whole page
                    int left = (int)(trim.x + printerDPI.x * 0.5d); // margin half inch from left
                    int top = (int)(trim.y + printerDPI.y * 0.5d); // margin half inch from top

                    GC gc = new GC(printer);
                    spViewComposite.getSphericCanvas().print(gc, left, top, rect.width);
                    gc.dispose();
                    printer.endPage();
                }
            }
            // End the job and dispose the printer
            printer.endJob();
            printer.dispose();
        });

        timeBackOperation = new ActionEventOperation("Time Back", "back", (e) -> {
            LocalDateTime dateTime = dateContribution.getDateTime();
            LocalDateTime decreased;

            if ((e.stateMask & SWT.SHIFT) != 0 && (e.stateMask & SWT.CTRL) != 0) {
                decreased = dateTime.minus(1, ChronoUnit.YEARS);
            } else if ((e.stateMask & SWT.CTRL) != 0) {
                decreased = dateTime.minus(1, ChronoUnit.DAYS);
            } else if ((e.stateMask & SWT.ALT) != 0) {
                decreased = dateTime.minus(1, ChronoUnit.MINUTES);
            } else if ((e.stateMask & SWT.SHIFT) != 0) {
                decreased = dateTime.minus(1, ChronoUnit.MONTHS);
            } else {
                decreased = dateTime.minus(1, ChronoUnit.HOURS);
            }
            dateContribution.setDateTime(decreased);

            updateDateTimeZone();
        });
        timeBackOperation.setToolTipText("Shift time back. Use modification keys:\nSHIFT-by months\nCTRL-by days\nALT-by minutes\nCTRL+SHIFT-by years\notherwise hours");
        dateContribution = new DateContribution(this);
        timeForwOperation = new ActionEventOperation("Time Forward", "forward", (e) -> {
            LocalDateTime dateTime = dateContribution.getDateTime();
            LocalDateTime increased;
            if ((e.stateMask & SWT.SHIFT) != 0 && (e.stateMask & SWT.CTRL) != 0) {
                increased = dateTime.plus(1, ChronoUnit.YEARS);
            } else if ((e.stateMask & SWT.CTRL) != 0) {
                increased = dateTime.plus(1, ChronoUnit.DAYS);
            } else if ((e.stateMask & SWT.ALT) != 0) {
                increased = dateTime.plus(1, ChronoUnit.MINUTES);
            } else if ((e.stateMask & SWT.SHIFT) != 0) {
                increased = dateTime.plus(1, ChronoUnit.MONTHS);
            } else {
                increased = dateTime.plus(1, ChronoUnit.HOURS);
            }
            dateContribution.setDateTime(increased);

            updateDateTimeZone();
        });
        timeForwOperation.setToolTipText("Shift time forward. Use modification keys:\nSHIFT-by months\nCTRL-by days\nALT-by minutes\nCTRL+SHIFT-by years\notherwise hours");

        saveImageOperation = new ActionOperation("Save Image", "save-image", SWT.CTRL | 'S', () -> {
            FileDialog fd = new FileDialog(getShell(), SWT.SAVE);
            fd.setText("Save current image as");
            String[] filterExt = {"*.png", "*.jpeg", "*.bmp", "*.ico"};
            fd.setFilterExtensions(filterExt);
            fd.setOverwrite(true);
            String savePath = fd.open();
            if (savePath != null) {
                try {
                    String[] strings = PathUtil.parseFullPathSuffix(savePath);
                    int lastIndexOf = strings[0].lastIndexOf("_");
                    Integer width = null;
                    if (lastIndexOf != -1) {
                        String number = strings[0].substring(lastIndexOf + 1);
                        try {
                            width = Integer.parseInt(number);
                        } catch (NumberFormatException e) {
                        }
                    }
                    if (width != null) {
                        spViewComposite.saveImageToFile(savePath, width);
                    } else {
                        spViewComposite.saveImageToFile(savePath);
                    }
                } catch (Exception e) {
                    setError(ExceptionMessage.getCombinedMessage("Unable to save to " + savePath, e), e);
                }
            }
        });
    }

    private void doProlink(Prolink prolink) {
        try {
            RaDec raDec0 = spViewComposite.getRaDec0();
            Desktop desktop = Desktop.getDesktop();
            String url = prolink.getUrl(raDec0, spViewComposite.getArcsecondsPerPixel());
            URI oURL = new URI(url);
            desktop.browse(oURL);
        } catch (Exception e) {
            LOGGER.error("Unable to show in browser", e);
            StatusUtil.errorMessage(ExceptionMessage.getCombinedMessage("Unable to open browser", e));
        }
    }

    private void showObjects() {
        new Thread(() -> {
            StarsReader.getConstellationsLater(800);
            Shell shell = getShell();
            if (shell == null) {
                return;
            }
            Display display = shell.getDisplay();
            if (display != null) {
                getShell().getDisplay().syncExec(() -> {
                    doLoadDefaultStars();
                    Collection<Constellation> constellations = StarsReader.getConstellations();
                    if (showConstellationsOperation.isChecked()) {
                        spViewComposite.setConstellations(constellations);
                    }
                    spViewComposite.getSphericCanvas().refresh();
                });
            }
        }).start();
    }

    private void downloadEso(String suggestName) {
        StatusUtil.infoMessage("Loading FITS...");
        AppConfig appConfig = AppConfig.getInstance();
        ConfigStore configStore = appConfig.getConfigStore();
        Display display = getShell().getDisplay();
        RaDec raDec0 = spViewComposite.getRaDec0();
        Thread thread = new Thread(() -> {
            String downDir = configStore.getValue("download.fits.directory", AppDirsUtil.getFitsDownloadPath());
            Integer width = configStore.getIntValue("downloading.width", 25);
            Integer height = configStore.getIntValue("downloading.height", 25);
            DssSubsourceEnum dssSubsourceEnum = (DssSubsourceEnum) configStore.getEnumValue("downloading.subsource", DssSubsourceEnum.DSS_1);
            String code = null;
            switch (dssSubsourceEnum) {
                case DSS_1:
                    code = "DSS1";
                    break;
                case DSS_2_RED:
                    code = "DSS2-red";
                    break;
                case DSS_2_BLUE:
                    code = "DSS2-blue";
                    break;
                case DSS_2_INFRARED:
                    code = "DSS2-infrared";
                    break;
            }
            String esoFitsFile = null;
            try {
                esoFitsFile = RetrieveDataUtil.getEsoFitsFile(downDir.toString(), false, raDec0.toNormalizedRahDec(), width, height, code, suggestName);
            } catch (Exception e) {
                LOGGER.error("Problems to download fits", e);
                display.asyncExec(() -> {
                    setError(ExceptionMessage.getCombinedMessage("Unable to load fits", e), e);
                });
                return;
            }
            File file = new File(esoFitsFile);
            setInfo("Downloaded " + file);
            String name = file.getName();
            String group = file.getParentFile().getName();
            display.asyncExec(() -> {
                List<RasterItem> allRasters = rastersTable.getAllRasters();
                Optional<RasterSet> grOpt = allRasters.stream().filter(ri -> ri.getGroup().getName().equals(group)).map(ri -> ri.getGroup()).findFirst();
                RasterSet rasterSet = grOpt.orElseGet(() -> {
                    return new RasterSet(group);
                });
                AddFileRequest addFileRequest = new AddFileRequest(file, name, rasterSet);
                rastersLoader.addFiles(List.of(addFileRequest));
                refresh();
            });
        });
        thread.start();
    }

    private void doLoadDefaultStars() {
        if (celestialObjects == null) {
            celestialObjects = StarsReader.getCelestialObjects();
        }
        spViewComposite.setStars(celestialObjects);
    }
    
    private void disableStars() {
        spViewComposite.setStars(new ArrayList<>());
    }

    @Override
    public void rasterTableChanged() {
        List<RasterItem> rasters = rastersTable.getEnabledRasters();
        spViewComposite.setRasters(rasters);
    }

    public void shapesChanged() {
//        List<PointShape> points = pointsTableComposite.getEnabledPoints();
//        spViewComposite.setPoints(points);
    }
    
    public void goRasterToCenter(RasterItem rasterItem) {
        spViewComposite.moveRasterToCenter(rasterItem);
    }
    
    public void setCentre(Spheric spheric0) {
        spViewComposite.setCenter(spheric0);
    }
    
    public double setZoom(double zoom) {
        return spViewComposite.setZoom(zoom);
    }
    
    public void setCenterAndRefresh(Spheric spheric0) {
        spViewComposite.setCenterAndRefresh(spheric0);
        spViewComposite.onCursorChanged(spheric0, 0);
    }
    
    public void refresh() {
        spViewComposite.refresh();
    }
    
    public RaDec getCenter() {
        return spViewComposite.getRaDec0();
    }
    
    public boolean isCenterAddedFile() {
        return addFileCenter;
    }

    @Override
    public boolean onFindingClick(int screenX, int screenY, CoordState coordState, int stateMask) {
        spViewComposite.clearInfo();
        Spheric clickSpheric = coordState.toSpheric(screenX, screenY);
        if (clickSpheric == null) {
            return false;
        }
        Spheric clickSpheric2 = coordState.toSpheric(screenX + 6, screenY);
        if (clickSpheric2 == null) {
            clickSpheric2 = coordState.toSpheric(screenX - 6, screenY);
        }
        if (clickSpheric2 == null) {
            return false;
        }
        double distRad = ConvTypesUtil.angularDistanceRad(clickSpheric, clickSpheric2);
        double distDeg = Math.toDegrees(distRad);
        List<NearestSelectionObject> allNearestShapes = new ArrayList<>();
        for (int i = 0; i < shapeTables.size(); i++) {
            AbstractShapesComposite<?> abstractShapesComposite = shapeTables.get(i);
            List<NearestSelectionObject> shapes = abstractShapesComposite.findShapes(clickSpheric, distDeg);
            for (NearestSelectionObject shape : shapes) {
                shape.tableIndex = i;
            }
            allNearestShapes.addAll(shapes);
        }

        List<NearestSelectionObject> stars = spViewComposite.findStars(coordState, clickSpheric, distDeg);
        allNearestShapes.addAll(stars);

        Collections.sort(allNearestShapes, Comparator.comparing(NearestSelectionObject::getDistanceDegs));

        List<NearestSelectionObject> rasters = spViewComposite.findRasters(screenX, screenY, coordState);
        Collections.sort(rasters, (s1, s2) -> s1.getDistanceDegs().compareTo(s2.getDistanceDegs()));
        allNearestShapes.addAll(rasters);

        if (allNearestShapes.size() > 0) {
            NearestSelectionObject nearestSelectionObject = allNearestShapes.get(0);
            if (nearestSelectionObject.getObjectType() == NearestSelectionObject.ObjectType.STAR) {
                CelestialObject celestialObject = (CelestialObject) nearestSelectionObject.getObject();
                Spheric centre = ConvTypesUtil.toSpheric(celestialObject.getCoords());
                ScreenPoint centreCoordinates = coordState.toScreenCoordinates(centre);
                spViewComposite.getSphericCanvas().forceDraw(gc -> {
                    gc.setForeground(crossColor);
                    gc.drawOval(centreCoordinates.point.x - 6, centreCoordinates.point.y - 6, 11, 11);
                });
                spViewComposite.showStarInfo(celestialObject);
                if ((stateMask & SWT.SHIFT) == SWT.SHIFT) {
                    spViewComposite.getSphericCanvas().setSpheric0(ConvTypesUtil.toSpheric(celestialObject.getCoords()));
                    spViewComposite.refresh();
                }
            } else if (nearestSelectionObject.getObjectType() == NearestSelectionObject.ObjectType.RASTER) {
                RasterItem rasterItem = (RasterItem) nearestSelectionObject.getObject();
                spViewComposite.selectRasterItem(rasterItem, (stateMask & SWT.CONTROL) != 0);
                tabFolder.setSelection(tabFolder.getItemCount() - 1);
            } else {
                tabFolder.setSelection(nearestSelectionObject.tableIndex);
                shapeTables.get(nearestSelectionObject.tableIndex).selectShape(nearestSelectionObject.getObject(), (stateMask & SWT.CTRL) != 0);
                Spheric centre = ((Shape) (nearestSelectionObject.getObject())).getCentre();
                ScreenPoint centreCoordinates = coordState.toScreenCoordinates(centre);
                spViewComposite.getSphericCanvas().forceDraw(gc -> {
                    gc.setForeground(crossColor);
                    gc.drawOval(centreCoordinates.point.x - 6, centreCoordinates.point.y - 6, 11, 11);
                });
                if ((stateMask & SWT.SHIFT) == SWT.SHIFT) {
                    spViewComposite.getSphericCanvas().setSpheric0(centre);
                }
            }
        }

        spViewComposite.setNearestSelections(allNearestShapes);

        return allNearestShapes.size() > 0;
    }

    private static String findImageName(String nameWithoutSuffix) {
        for (String imageSuffix: IMAGE_SUFFIXES) {
            String file = nameWithoutSuffix + imageSuffix;
            if (new File(file).isFile()) {
                return file;
            }
            file = nameWithoutSuffix + imageSuffix.toUpperCase();
            if (new File(file).isFile()) {
                return file;
            }
        }
        return null;
    }

    @Override
    public void openRegistered(String path) {
        addFiles(List.of(path));
    }

    @Override
    public void drawShapes(GC gc, CoordState coordState, Labeler labeler) {
        pointsComposite.drawShapes(gc, coordState, labeler);
        circlesComposite.drawShapes(gc, coordState, labeler);
    }

    public void setToolListener(ToolListener newTool, AbstractShapesComposite<?> toolInComposite) {
        if (pointsComposite != toolInComposite) {
            pointsComposite.unpressAddByMapButton();
        }
        if (circlesComposite != toolInComposite) {
            circlesComposite.unpressAddByMapButton();
        }
        spViewComposite.setToolListener(newTool);
    }
    
    public void setInfo(String message) {
        getStatusLineManager().setMessage(message);
    }
    
    public void setError(String str, Exception e) {
        LOGGER.error(str, e);
        getStatusLineManager().setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_ERROR), str != null ? str : "Operation failed");
    }
    
    public void setError(String message) {
        getStatusLineManager().setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_ERROR), message);
    }
    
    public void setWarn(String message) {
        getStatusLineManager().setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_WARNING), message);
    }
    
    public StyledText getInfoStyledText() {
        return spViewComposite.getInfoStyledText();
    }

    @Override
    public void onNearSelectionClicked(NearestSelectionObject nearestSelectionObject, int stateMask) {
        if (nearestSelectionObject.getObjectType() == NearestSelectionObject.ObjectType.STAR) {
            CelestialObject celestialObject = (CelestialObject) nearestSelectionObject.getObject();
            spViewComposite.showStarInfo(celestialObject);
            if ((stateMask & SWT.SHIFT) == SWT.SHIFT) {
                spViewComposite.getSphericCanvas().setSpheric0(ConvTypesUtil.toSpheric(celestialObject.getCoords()));
                spViewComposite.refresh();
            }
        } else if (nearestSelectionObject.getObjectType() == NearestSelectionObject.ObjectType.RASTER) {
            RasterItem rasterItem = (RasterItem) nearestSelectionObject.getObject();
            spViewComposite.selectRasterItem(rasterItem, (stateMask & SWT.CONTROL) != 0);
            tabFolder.setSelection(tabFolder.getItemCount() - 1); // last is raster
            if ((stateMask & SWT.SHIFT) == SWT.SHIFT) {
                goRasterToCenter(rasterItem);
                spViewComposite.refresh();
            }
        } else if (nearestSelectionObject.getObjectType() == NearestSelectionObject.ObjectType.POINT || nearestSelectionObject.getObjectType() == NearestSelectionObject.ObjectType.CIRCLE) {
            tabFolder.setSelection(nearestSelectionObject.tableIndex);
            shapeTables.get(nearestSelectionObject.tableIndex).selectShape(nearestSelectionObject.getObject(), (stateMask & SWT.CTRL) != 0);
            Spheric centre = ((Shape) (nearestSelectionObject.getObject())).getCentre();
            if ((stateMask & SWT.SHIFT) == SWT.SHIFT) {
                spViewComposite.getSphericCanvas().setSpheric0(centre);
                spViewComposite.refresh();
            }
        }
    }

    @Override
    public RaDec onGetCentrum() {
        return spViewComposite.getRaDec0();
    }

    @Override
    public void goCentreTo(RaDec raDec) {
        setCenterAndRefresh(ConvTypesUtil.toSpheric(raDec));
    }

    @Override
    public void setPoints(Map<String, RaDec> points, String groupName) {
        pointsComposite.setPoints(points, groupName);
    }

    public void updateDateTimeZone() {
        double jd = dateContribution.getJulianDate();
        spViewComposite.setJulianDate(jd);
        spViewComposite.refresh();
    }

    public GeoLoc getGeoloc() {
        return spViewComposite.getGeoLoc();
    }

    public Double getJulianDate() {
        return spViewComposite.getJulianDate();
    }

    public void focusTab(Object itemControl) {
        for (int i = 0; i < tabFolder.getItemCount(); i++) {
            TabItem item = tabFolder.getItem(i);
            if (itemControl == item.getControl()) {
                tabFolder.setSelection(i);
                return;
            }
        }
    }

    public void onEscape() {
        setToolListener(null, null);
        refresh();
    }

    public void shapeEdited(Shape shape) {
        if (shape instanceof PointShape) {
            PointShape pointShape = (PointShape) shape;
            for (SoftReference<ImageRegistrationApp> editPointListener : editPointListeners) {
                ImageRegistrationApp imageRegistrationApp = editPointListener.get();
                if (imageRegistrationApp != null) {
                    imageRegistrationApp.editedShape(pointShape);
                }
            }
        }
    }

    public void clearInfo() {
        spViewComposite.clearInfo();
    }
}
