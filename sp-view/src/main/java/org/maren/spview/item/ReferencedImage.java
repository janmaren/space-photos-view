package org.maren.spview.item;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.maren.spview.app.ImageToSphericCoords;

import cz.jmare.math.util.ImgDimUtil;
import cz.jmare.swt.image.ImageUtil;

public class ReferencedImage {
    private ImageData imageData;
    
    private ImageToSphericCoords imageToRealCoords;

    public ReferencedImage(ImageData imageData, ImageToSphericCoords imageToRealCoords) {
        this.imageData = imageData;
        this.imageToRealCoords = imageToRealCoords;
    }

    public ReferencedImage(ImageData imageData) {
        this.imageData = imageData;
    }
    
    public ReferencedImage createResized(int imageWidth) {
        ImageToSphericCoords transScreenToPolarCoords = null;
        double div = ImgDimUtil.bestDividerForResize(imageData.width, imageData.height, imageWidth);
        if (imageToRealCoords != null) {
            transScreenToPolarCoords = new ImageToSphericCoords(imageToRealCoords.getPixelToSpheric().toPixelToSpheric(div));
        }
        
        Image createResized = ImageUtil.createResized(imageData, (int) (imageData.width / div), (int) (imageData.height / div));
        ReferencedImage referencedImage = new ReferencedImage(createResized.getImageData(), transScreenToPolarCoords);
        createResized.dispose();
        
        return referencedImage;
    }

    public ImageData getImageData() {
        return imageData;
    }

    public ImageToSphericCoords getImageToRealCoords() {
        return imageToRealCoords;
    }
}
