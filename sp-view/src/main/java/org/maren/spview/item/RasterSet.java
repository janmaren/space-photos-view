package org.maren.spview.item;

public class RasterSet {
    private static int grpSeq;
    
    private String name;

    public RasterSet(String name) {
        this.name = name;
    }

    public RasterSet() {
        this.name = "grp-" + (grpSeq++);
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
