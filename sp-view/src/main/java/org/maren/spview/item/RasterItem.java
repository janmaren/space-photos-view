package org.maren.spview.item;

import java.io.File;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.ImageData;
import org.maren.spview.app.ImageToSphericCoords;
import org.maren.spview.imageloader.FitsHeaderItem;
import org.maren.spview.imageloader.ImageLoader;

import cz.jmare.file.PathUtil;

public class RasterItem {
    /**
     * Path to image file - mandatory
     */
    protected String filePath;
    
    /**
     * Referencing of full image
     * It should contain 3 coordinates. When null the image will not be referenced
     */
    protected ImageToSphericCoords screenToRealCoords;
    
    /**
     * Parent group
     */
    protected RasterSet rasterSet;
    
    protected String name;
    
    /**
     * Full ReferencedImage
     */
    protected SoftReference<ReferencedImage> fullImageWeak = new SoftReference<>(null);
    
    /**
     * Width of full image
     */
    protected Integer width;
    
    /**
     * Height of full image
     */
    protected Integer height;
    
    protected List<ReferencedImage> cachedResizedImages = new ArrayList<ReferencedImage>();
    
    /**
     * Load full image for cases the cache is cleared
     */
    private ImageLoader imageLoader;
    private List<FitsHeaderItem> headerValues;

    public RasterItem(String filePath, String name, RasterSet group, ImageLoader imageLoader, ImageToSphericCoords screenToPolarCoords) {
        this.name = name;
        this.filePath = filePath;
        this.rasterSet = group;
        this.screenToRealCoords = screenToPolarCoords;
        this.imageLoader = imageLoader;
        ImageData imageData = loadImage();
        ReferencedImage referencedImage = new ReferencedImage(imageData, screenToRealCoords);
        width = imageData.width;
        height = imageData.height;
        fullImageWeak = new SoftReference<ReferencedImage>(referencedImage);
    }
    
    public RasterItem(String filePath, String name, RasterSet group, ImageLoader imageLoader, ImageToSphericCoords screenToPolarCoords, ImageData imageData) {
        this.name = name;
        this.filePath = filePath;
        this.rasterSet = group;
        this.imageLoader = imageLoader;
        this.screenToRealCoords = screenToPolarCoords;
        width = imageData.width;
        height = imageData.height;
        ReferencedImage referencedImage = new ReferencedImage(imageData, screenToRealCoords);
        fullImageWeak = new SoftReference<ReferencedImage>(referencedImage);
    }

    public static boolean isDouble(String str) {
        try {
            Double.parseDouble(str.trim());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    
    public String getFilePath() {
        return filePath;
    }

    public ImageToSphericCoords getImageToSphericCoords() {
        return screenToRealCoords;
    }

    public void setRasterSet(RasterSet rasterSet) {
        this.rasterSet = rasterSet;
    }

    public ReferencedImage getFullReferencedImage() {
        ReferencedImage referencedImage = fullImageWeak.get();
        if (referencedImage == null) {
            ImageData imageData = loadImage();
            width = imageData.width;
            height = imageData.height;
            referencedImage = new ReferencedImage(imageData, screenToRealCoords);
            fullImageWeak = new SoftReference<ReferencedImage>(referencedImage);
        }
        return referencedImage;
    }
    
    public ImageData getFullImageData() {
        ReferencedImage referencedImage = getFullReferencedImage();
        return referencedImage.getImageData();
    }
    
    public ReferencedImage getReferencedImage(double zoom) {
        int reqHorizPixels = (int) (zoom / 3.5);
        if (reqHorizPixels > getWidth()) {
            return getFullReferencedImage();
        }
        for (ReferencedImage referencedImage : cachedResizedImages) {
            ImageData imageData = referencedImage.getImageData();
            int imageWidth = imageData.width;
            if (imageWidth >= reqHorizPixels && imageWidth < reqHorizPixels * 2) {
                return referencedImage;
            }
        }
        ReferencedImage referencedImage = getFullReferencedImage();
        ReferencedImage createResized = referencedImage.createResized(reqHorizPixels * 2);
        cachedResizedImages.add(createResized);
        return createResized;
    }

    protected ImageData loadImage() {
        return imageLoader.loadImageData().imageData;
    }
    
    public boolean isReferenced() {
        return screenToRealCoords != null;
    }

    public String getName() {
        return name;
    }
    
    public RasterSet getGroup() {
        return rasterSet;
    }

    public int getWidth() {
        if (width == null) {
            getFullReferencedImage();
        }
        return width;
    }

    public int getHeight() {
        if (height == null) {
            getFullReferencedImage();
        }
        return height;
    }

    public List<FitsHeaderItem> getHeaderValues() {
        return headerValues;
    }

    public void setHeaderValues(List<FitsHeaderItem> headerValues) {
        this.headerValues = headerValues;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
        imageLoader.setFullPath(filePath);
        String[] strings = PathUtil.parseFullPath(filePath);
        this.name = strings[1];
        this.rasterSet = new RasterSet(new File(strings[0]).getName());
    }
}
