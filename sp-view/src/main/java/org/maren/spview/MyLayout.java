package org.maren.spview;

import org.eclipse.swt.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

public class MyLayout {
    public static void main(String[] args) {
        Display display = new Display();
        Shell composite = new Shell(display);
        FormLayout formLayout = new FormLayout();
        composite.setLayout(formLayout);


        Group leftRightSash = new Group(composite, SWT.NONE);
        Composite separatorComposite = new Composite(composite, SWT.BORDER);
        leftRightSash.setText("group0");
        FormData leftData = new FormData();
        leftData.left = new FormAttachment(0, 0);
        leftData.right = new FormAttachment(separatorComposite, 0, SWT.DEFAULT);
        leftData.top = new FormAttachment(0, 0);
        leftData.bottom = new FormAttachment(100, 0);
        leftRightSash.setLayoutData(leftData);

        FormData separatorData = new FormData();
        separatorData.width = 10;
        separatorData.left = new FormAttachment(100, -400);
        separatorData.top = new FormAttachment(0, 0);
        separatorData.bottom = new FormAttachment(100, 0);
        separatorComposite.setLayoutData(separatorData);

        Group rightTabFolder = new Group(composite, SWT.NONE);
        rightTabFolder.setText("group2");
        FormData rightData = new FormData();
        rightData.left = new FormAttachment(separatorComposite, 0, SWT.DEFAULT);
        rightData.right = new FormAttachment(100, 0);
        rightData.top = new FormAttachment(0, 0);
        rightData.bottom = new FormAttachment(100, 0);
        rightTabFolder.setLayoutData(rightData);

        composite.pack();
        composite.open();

        while (!composite.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        display.dispose();
    }
}