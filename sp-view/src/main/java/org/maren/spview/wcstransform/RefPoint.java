package org.maren.spview.wcstransform;

import org.maren.statis.entity.Coordinate;

import cz.jmare.math.astro.RaDec;

public class RefPoint {
    public Coordinate imgPoint;
    public RaDec raDec;
    public boolean enabled;
}
