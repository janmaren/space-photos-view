package org.maren.spview.wcstransform.saver;

import java.io.File;
import java.util.List;

import org.maren.spview.imageloader.FitsHeaderItem;
import org.maren.statis.affine.AffineElements;

import cz.jmare.fits.util.FitsUtil;
import cz.jmare.math.geometry.entity.Point;
import nom.tam.fits.Fits;
import nom.tam.fits.Header;
import nom.tam.fits.HeaderCardException;
import nom.tam.fits.ImageHDU;

public class FitsHeaderSaveUtil {
    public static void putWcsToFitsFile(File inputFile, File outputFile, AffineElements elements, Point refPix, int height) throws Exception {
        Fits fits = new Fits(inputFile);
        ImageHDU imageHDU = FitsUtil.imageHDU(fits);
        Header header = imageHDU.getHeader();
        setWcs(elements, refPix, height, header);
        fits.write(outputFile);
    }

    public static void putWcsToFitsFile(File inputFile, File outputFile, List<FitsHeaderItem> valueComments) throws Exception {
        Fits fits = new Fits(inputFile);
        ImageHDU imageHDU = FitsUtil.imageHDU(fits);
        Header header = imageHDU.getHeader();
        setWcs(valueComments, header);
        fits.write(outputFile);
    }

    public static void setWcs(AffineElements elements, Point refPix, int height, Header header) throws HeaderCardException {
        header.addValue("CTYPE1", "RA---SIN", "RA coordinate degrees");
        header.addValue("CTYPE2", "DEC--SIN", "DEC coordinate degrees");
        header.addValue("CRPIX1", refPix.x + 0.5, "Refpix of first axis");
        header.addValue("CRPIX2", height - refPix.y + 0.5, "Refpix of second axis");
        header.addValue("CRVAL1", elements.c, "RA at Ref pix in decimal degrees");
        header.addValue("CRVAL2", elements.f, "DEC at Ref pix in decimal degrees");
        header.addValue("CD1_1", elements.a, "CD matrix - the a");
        header.addValue("CD1_2", -elements.b, "CD matrix - the b");
        header.addValue("CD2_1", elements.d, "CD matrix - the d");
        header.addValue("CD2_2", -elements.e, "CD matrix - the e");
    }

    public static void setWcs(List<FitsHeaderItem> headerValues, Header header) throws HeaderCardException {
        for (FitsHeaderItem headerValue : headerValues) {
            header.addValue(headerValue.getName(), headerValue.getValue(),
                    headerValue.getComment());
        }
    }
}
