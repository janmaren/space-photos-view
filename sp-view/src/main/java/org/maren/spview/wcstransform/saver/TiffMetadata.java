package org.maren.spview.wcstransform.saver;

import javax.imageio.metadata.IIOMetadataNode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TiffMetadata {

    private final List<IIOMetadataNode> addList = new ArrayList<>();
    private final Map<String, String> fileAtt = new TreeMap<>();

    public TiffMetadata() {}

    public List<IIOMetadataNode> get() {
        return addList;
    }

    public Map<String, String> getFileAtt() {
        return fileAtt;
    }

    public TiffMetadata add(int exifTag, String exifTagName, Object val) {
        IIOMetadataNode md;
        if (val instanceof byte[]) {
            md = createBytesField(exifTag, exifTagName, (byte[]) val);
        } else if (val instanceof String) {
            md = createAsciiField(exifTag, exifTagName, (String) val);
            fileAtt.put(exifTagName, String.valueOf(val));
        } else if (val instanceof Short) {
            md = createShortField(exifTag, exifTagName, ((Short) val).intValue());
            fileAtt.put(exifTagName, String.valueOf(val));
        } else if (val instanceof Integer) {
            md = createShortField(exifTag, exifTagName, ((Integer) val).intValue());
            fileAtt.put(exifTagName, String.valueOf(val));
        } else if (val instanceof Long) {
            md = createLongField(exifTag, exifTagName, ((Long) val).longValue());
            fileAtt.put(exifTagName, String.valueOf(val));
        } else if (val instanceof Float) {
            md = createFloatField(exifTag, exifTagName, ((Float) val).floatValue());
            fileAtt.put(exifTagName, String.valueOf(val));
        } else if (val instanceof Double) {
            md = createDoubleField(exifTag, exifTagName, ((Double) val).doubleValue());
            fileAtt.put(exifTagName, String.valueOf(val));
        } else {
            throw new IllegalArgumentException("unsupported value class: " + val.getClass().getName());
        }

        addList.add(md);
        return this;
    }

    private static IIOMetadataNode createBytesField(int number, String name, byte[] bytes) {
        IIOMetadataNode field = new IIOMetadataNode("TIFFField");
        field.setAttribute("number", Integer.toString(number));
        field.setAttribute("name", name);
        IIOMetadataNode arrayNode = new IIOMetadataNode("TIFFBytes");
        field.appendChild(arrayNode);

        for (byte b : bytes) {
            IIOMetadataNode valueNode = new IIOMetadataNode("TIFFByte");
            valueNode.setAttribute("value", Integer.toString(b));
            arrayNode.appendChild(valueNode);
        }
        return field;
    }

    private static IIOMetadataNode createShortField(int number, String name, int val) {
        IIOMetadataNode field, arrayNode, valueNode;
        field = new IIOMetadataNode("TIFFField");
        field.setAttribute("number", Integer.toString(number));
        field.setAttribute("name", name);
        arrayNode = new IIOMetadataNode("TIFFShorts");
        field.appendChild(arrayNode);
        valueNode = new IIOMetadataNode("TIFFShort");
        arrayNode.appendChild(valueNode);
        valueNode.setAttribute("value", Integer.toString(val));
        return field;
    }

    private static IIOMetadataNode createAsciiField(int number, String name, String val) {
        IIOMetadataNode field, arrayNode, valueNode;
        field = new IIOMetadataNode("TIFFField");
        field.setAttribute("number", Integer.toString(number));
        field.setAttribute("name", name);
        arrayNode = new IIOMetadataNode("TIFFAsciis");
        field.appendChild(arrayNode);
        valueNode = new IIOMetadataNode("TIFFAscii");
        arrayNode.appendChild(valueNode);
        valueNode.setAttribute("value", val);
        return field;
    }

    private static IIOMetadataNode createLongField(int number, String name, long val) {
        IIOMetadataNode field, arrayNode, valueNode;
        field = new IIOMetadataNode("TIFFField");
        field.setAttribute("number", Integer.toString(number));
        field.setAttribute("name", name);
        arrayNode = new IIOMetadataNode("TIFFLongs");
        field.appendChild(arrayNode);
        valueNode = new IIOMetadataNode("TIFFLong");
        arrayNode.appendChild(valueNode);
        valueNode.setAttribute("value", Long.toString(val));
        return field;
    }

    private static IIOMetadataNode createFloatField(int number, String name, float val) {
        IIOMetadataNode field, arrayNode, valueNode;
        field = new IIOMetadataNode("TIFFField");
        field.setAttribute("number", Integer.toString(number));
        field.setAttribute("name", name);
        arrayNode = new IIOMetadataNode("TIFFFloats");
        field.appendChild(arrayNode);
        valueNode = new IIOMetadataNode("TIFFFloat");
        arrayNode.appendChild(valueNode);
        valueNode.setAttribute("value", Float.toString(val));
        return field;
    }

    private static IIOMetadataNode createDoubleField(int number, String name, double val) {
        IIOMetadataNode field, arrayNode, valueNode;
        field = new IIOMetadataNode("TIFFField");
        field.setAttribute("number", Integer.toString(number));
        field.setAttribute("name", name);
        arrayNode = new IIOMetadataNode("TIFFDoubles");
        field.appendChild(arrayNode);
        valueNode = new IIOMetadataNode("TIFFDouble");
        arrayNode.appendChild(valueNode);
        valueNode.setAttribute("value", Double.toString(val));
        return field;
    }

    @SuppressWarnings("unused")
	private static IIOMetadataNode createRationalField(int number, String name, long numerator,
                                                       long denominator) {
        IIOMetadataNode field, arrayNode, valueNode;
        field = new IIOMetadataNode("TIFFField");
        field.setAttribute("number", Integer.toString(number));
        field.setAttribute("name", name);
        arrayNode = new IIOMetadataNode("TIFFRationals");
        field.appendChild(arrayNode);
        valueNode = new IIOMetadataNode("TIFFRational");
        arrayNode.appendChild(valueNode);
        valueNode.setAttribute("value", numerator + "/" + denominator);
        return field;
    }

}
