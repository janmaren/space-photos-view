package org.maren.spview.wcstransform;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.*;
import org.maren.spview.affin.AffElementsParser;
import org.maren.spview.app.GetCoordsListener;
import org.maren.spview.app.SetPointsListener;
import org.maren.spview.shape.PointShape;
import org.maren.spview.wcstransform.util.RecalculResult;
import org.maren.spview.wcstransform.util.WcsLeftTopAbstraction;
import org.maren.statis.affine.AffineElements;
import org.maren.statis.affine.AffineTransCoefsCalculator;
import org.maren.statis.conformal.ConformalElements;
import org.maren.statis.conformal.ConformalTransCoefsCalculator;
import org.maren.statis.entity.ControlPoint;
import org.maren.statis.entity.Coordinate;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.GonUtil;
import cz.jmare.data.util.HourUtil;
import cz.jmare.file.PathUtil;
import cz.jmare.pureraster.PositionData;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.astro.RahDec;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.TwoPoints;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.util.Create;

public class RefPointsTableComposite extends Composite {

    private final Button pickRaDec;
    private volatile Table table;
    private Color okTextColor;
    private Label pointsStatus;
    private RefTableChangedListener refTableChangedListener;
    private String path;
    @SuppressWarnings("unused")
    private StatusLineManager slm;
    private org.maren.statis.affine.AffineElements affineElements;
    private boolean dirty;
    @SuppressWarnings("unused")
    private ImageRegistrationApp imageRegistrationApp;

    private SetPointsListener setPointsListener;

    private GetCoordsListener getCoordsListener;

    private String groupName;
    private WcsLeftTopAbstraction wcsLeftTopAbstraction;

    private Map<Integer, Boolean> editedByHandMap = new HashMap<>();

    public RefPointsTableComposite(Composite parent, int style, StatusLineManager slm, ImageRegistrationApp imageRegistrationApp) {
        super(parent, style);
        this.slm = slm;
        this.imageRegistrationApp = imageRegistrationApp;
        ImageUtil.put("pick-radec", "/image/pick-radec.png");
        ImageUtil.put("export", "/image/export.png");
        ImageUtil.put("color", "/image/color-dial.png");
        ImageUtil.put("folder", "/image/folder.png");
        ImageUtil.put("save", "/image/document-save.png");
        ImageUtil.put("trash", "/image/trash.png");

        setLayout(new GridLayout());

        Composite toolsComposite = createRowCompositeLineIntoGridComposite(this, gd -> {});

        Create.imageButton(toolsComposite, ImageUtil.get("folder"), "Import Points", () -> {
            FileDialog fd = new FileDialog(getShell(), SWT.OPEN);
            fd.setText("Text File to Open");
            fd.setFilterExtensions(new String[] {"*.rfpoint"});
            String path;
            if ((path = fd.open()) != null) {
                List<String> lines = Files.readAllLines(Paths.get(path));
                List<TwoPoints> twoPoints = AffElementsParser.parseRefDoubleAndDegPoints(lines);
                deleteEmptyRow();
                for (TwoPoints twoPoints2 : twoPoints) {
                    add(twoPoints2.getPoint1(), new RahDec(ACooUtil.degreesToHours(twoPoints2.getPoint2().x), twoPoints2.getPoint2().y));
                }
                calcAffineElements();
                slm.setMessage("Imported " + twoPoints.size() + " points");
                refTableChangedListener.refTableChanged();
            }
        });
        
        Create.imageButton(toolsComposite, ImageUtil.get("save"), "Export Checked Points", () -> {
            List<ControlPointRow> controlPoints = getControlPoints();
            List<TwoPoints> refPoints
                    = controlPoints.stream().filter(cp -> cp.isValid() && cp.isEnabled())
                                   .map(cp -> new TwoPoints(cp.getImgPoint().x, cp.getImgPoint().y, cp.getSphPoint().x, cp.getSphPoint().y))
                                   .collect(Collectors.toList());
            String str = "RF_POINT=";
            for (int i = 0; i < refPoints.size(); i++) {
                str += refPoints.get(i).x1 + ", " + refPoints.get(i).y1 + ", " + HourUtil.formatHours(ACooUtil.degreesToHours(refPoints.get(i).x2)) + ", " + GonUtil.formatDegrees(refPoints.get(i).y2) + (i != refPoints.size() - 1 ? ";" : "");
            }
            
            FileDialog fileDialog = new FileDialog(getShell(), SWT.SAVE);
            fileDialog.setText("Exporting ref points");
            fileDialog.setFilterExtensions(new String[] {"*.rfpoint"});
            fileDialog.setOverwrite(true);
            if (path != null) {
                String[] nameAndSuffix = PathUtil.parseFullPathSuffix(path);
                fileDialog.setFileName(nameAndSuffix[0] + ".rfpoint");
            }
            String filename = fileDialog.open();
            if (filename != null) {
                 Files.writeString(Paths.get(filename), str);
                 slm.setMessage("Written to " + filename);
                 dirty = false;
            }
        });

        pickRaDec = Create.imageButton(toolsComposite, ImageUtil.get("trash"), "Delete All Points", ()-> {
            MessageBox messageBox = new MessageBox(getShell(), SWT.YES | SWT.NO | SWT.ICON_WARNING);
            messageBox.setMessage("Do you really want to delete all points?");
            int result = messageBox.open();
            if (result != SWT.YES) {
                return;
            }
            clearPoints();
            refTableChangedListener.refTableChanged();
            table.setSelection(0);
        });

        table = new Table(this, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.FULL_SELECTION);
        table.setLinesVisible(true);
        table.setHeaderVisible(true);
        table.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                if (event.detail == SWT.CHECK) {
                    int selectionIndex = table.getSelectionIndex();
                    if (selectionIndex >= 0) {
                        // must be deselected not to scroll to selected item after checked/unchecked, swt bug?
                        table.deselect(selectionIndex);
                    }

                    TableItem item = ((TableItem) event.item);
                    if (item.getChecked()) {
                        //PointItem pointItem = (PointItem) item.getData();
                    }
                    //spView.pointsTableChanged();
                    calcAffineElements();

                    if (refTableChangedListener != null) {
                        refTableChangedListener.refTableChanged();
                    }
                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        table.addMouseListener(new MouseListener() {
            @Override
            public void mouseUp(MouseEvent e) {
            }
            
            @Override
            public void mouseDown(MouseEvent e) {
            }
            
            @Override
            public void mouseDoubleClick(MouseEvent e) {
                int[] selectionIndices = table.getSelectionIndices();
                if (selectionIndices.length > 0) {
                    //
                }
            }
        });
        
        TableColumn idColumn = new TableColumn(table, SWT.CENTER);
        idColumn.setText("\u25A1");
        idColumn.setWidth(65);
        idColumn.setResizable(true);
        idColumn.setAlignment(SWT.LEFT);
        idColumn.setToolTipText("Enable or disable point");

        TableColumn groupColumn = new TableColumn(table, SWT.CENTER);
        groupColumn.setText("SRC Image X, Y");
        groupColumn.setWidth(120);
        groupColumn.setResizable(true);
        groupColumn.setAlignment(SWT.LEFT);

        TableColumn nameColumn = new TableColumn(table, SWT.CENTER);
        nameColumn.setText("DST WCS RA, DE");
        nameColumn.setWidth(185);
        nameColumn.setResizable(true);
        nameColumn.setAlignment(SWT.LEFT);

        TableColumn dateColumn = new TableColumn(table, SWT.CENTER);
        dateColumn.setText("Error(deg)");
        dateColumn.setWidth(150);
        dateColumn.setResizable(true);
        dateColumn.setAlignment(SWT.LEFT);

        table.setHeaderVisible(true);
        GridData gridDataTable = new GridData(SWT.FILL, SWT.FILL, true, true);

        gridDataTable.grabExcessVerticalSpace = true;
        gridDataTable.verticalAlignment = SWT.FILL;
        table.setLayoutData(gridDataTable);
        
        Menu menuTable = new Menu(table);
        table.setMenu(menuTable);
        applyEditor();

        MenuItem miPick = new MenuItem(menuTable, SWT.NONE);
        miPick.setText("Pick DST from centre of SPView");
        Create.selectionListener(miPick, (e) -> {
            RaDec raDec = getCoordsListener.onGetCentrum();
            setRaDecToRow(raDec);
        });

        MenuItem miCalcDst = new MenuItem(menuTable, SWT.NONE);
        miCalcDst.setText("Calculate DST point");
        Create.selectionListener(miCalcDst, (e) -> {
            int selectionIndex = table.getSelectionIndex();
            if (selectionIndex == -1) {
                return;
            }
            doCalcSphericCoord(selectionIndex);
        });

        MenuItem miRemove = new MenuItem(menuTable, SWT.NONE);
        miRemove.setText("Remove");
        Create.selectionListener(miRemove, (e) -> {
            int selectionIndex = table.getSelectionIndex();
            if (selectionIndex < 0) {
                return;
            }
            removeEditedRahdecByHand(selectionIndex);
            table.remove(selectionIndex);
            calcAffineElements();
            refTableChangedListener.refTableChanged();
            table.setSelection(table.getItemCount() - 1);
        });

        MenuItem miImageCenter = new MenuItem(menuTable, SWT.NONE);
        miImageCenter.setText("Center SRC");
        Create.selectionListener(miImageCenter, (e) -> {
            doImageCenter();
        });

        ensureEmptyRow();
        table.setSelection(0);
        table.addMouseListener(MouseListener.mouseDoubleClickAdapter(e -> {
            doImageCenter();
        }));

        pointsStatus = Create.label(this, "", lab -> {
            GridData gridData = new GridData();
            gridData.widthHint = 500;
            lab.setLayoutData(gridData);
        });
        
        okTextColor = pointsStatus.getForeground();

        groupName = "wcsref-" + Integer.toHexString(ThreadLocalRandom.current().nextInt(100000, Integer.MAX_VALUE));
    }

    private void doImageCenter() {
        int selectionIndex = table.getSelectionIndex();
        if (selectionIndex < 0) {
            return;
        }
        List<ControlPointRow> controlPoints = getControlPoints();
        if (selectionIndex < controlPoints.size() && controlPoints.get(selectionIndex) != null) {
            ControlPointRow controlPointRow = controlPoints.get(selectionIndex);
            Coordinate imgPoint = controlPointRow.getImgPoint();
            imageRegistrationApp.centerToCoordinate(imgPoint);
        }
    }

    private RecalculResult doCalcSphericCoord(int selectionIndex) {
        RecalculResult recalculResult = recalculByImgPointChanged(selectionIndex);
        if (recalculResult == RecalculResult.RECALCULATED) {
            imageRegistrationApp.setInfo("DST point populated - please, correct position");
        } else if (recalculResult == RecalculResult.NOT_RECALCULATED_NO_DATA) {
            imageRegistrationApp.setWarn("Unable to calculate DST because there is not enough data");
        }
        return recalculResult;
    }


    public static Composite createRowCompositeLineIntoGridComposite(Composite parentComposite, Consumer<GridData> gdConsumer) {
        Composite panelComposite = new Composite(parentComposite, SWT.NONE);
        RowLayout rowLayout = new RowLayout();
        rowLayout.marginHeight = 0;
        rowLayout.marginWidth = 0;
        rowLayout.center = true;
        panelComposite.setLayout(rowLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.horizontalAlignment = SWT.FILL;
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }

    public static Group createRowGroupLineIntoGridComposite(Composite parentComposite, Consumer<GridData> gdConsumer, String text) {
        Group panelComposite = new Group(parentComposite, SWT.NONE);
        RowLayout rowLayout = new RowLayout();
        rowLayout.marginHeight = 0;
        rowLayout.marginWidth = 0;
        rowLayout.center = true;
        panelComposite.setLayout(rowLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.horizontalAlignment = SWT.FILL;
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        panelComposite.setText(text);
        return panelComposite;
    }

    private int findMaxIdNumber() {
        int itemCount = table.getItemCount();
        int last = 0;
        for (int i = 0; i < itemCount; i++) {
            TableItem item = table.getItem(i);
            String text = item.getText(0);
            if (text != null && !text.isBlank()) {
                int integer = Integer.valueOf(text);
                if (integer > last) {
                    last = integer;
                }
            }
        }
        return last;
    }

    private void add(Point src, RahDec dst) {
        TableItem tableItem = new TableItem(table, SWT.NONE);
        tableItem.setText(0, String.valueOf(findMaxIdNumber() + 1));
        tableItem.setText(1, src.x + ", " + src.y);
        tableItem.setText(2, HourUtil.formatHours(dst.rah) + ", " + GonUtil.formatDegrees(dst.dec, true));
        tableItem.setChecked(true);
    }

    private void ensureEmptyRow() {
        boolean emptyRow = false;
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            if (item.getText(1).isBlank() && item.getText(2).isBlank()) {
                emptyRow = true;
                break;
            }
        }
        if (!emptyRow) {
            TableItem tableItem = new TableItem(table, SWT.NONE);
            tableItem.setText(0, String.valueOf(findMaxIdNumber() + 1));
            tableItem.setText(1, "");
            tableItem.setText(2, "");
            tableItem.setChecked(true);
        }
    }

    private void deleteEmptyRow() {
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            if (item.getText(1).isBlank() && item.getText(2).isBlank()) {
                table.remove(i);
                i--;
            }
        }
    }

    public void applyEditor() {
        final TableEditor editor = new TableEditor(table);
        editor.horizontalAlignment = SWT.LEFT;
        editor.grabHorizontal = true;
        table.addListener(SWT.MouseDown, new Listener() {
            public void handleEvent(Event event) {
                if (event.button != 1) return;
                Rectangle clientArea = table.getClientArea();
                org.eclipse.swt.graphics.Point pt = new org.eclipse.swt.graphics.Point(event.x, event.y);
                int index = table.getTopIndex();
                while (index < table.getItemCount()) {
                    boolean visible = false;
                    final TableItem item = table.getItem(index);
                    for (int i = 0; i < table.getColumnCount(); i++) {
                        Rectangle rect = item.getBounds(i);
                        if (rect.contains(pt)) {
                            if (i == 0 || i == 3) {
                                break;
                            }
                            final int column = i;
                            final Text text = new Text(table, SWT.NONE);
                            final int finalIndex = index;
                            Listener textListener = new Listener() {
                                public void handleEvent(final Event e) {
                                    switch (e.type) {
                                    case SWT.FocusOut:
                                        if (column == 2) {
                                            editedRahdecByHandWhenTextOk(finalIndex, text.getText());
                                        }
                                        setText(finalIndex, column, text.getText());
                                        text.dispose();
                                        dirty = true;
                                        break;
                                    case SWT.Traverse:
                                        switch (e.detail) {
                                        case SWT.TRAVERSE_RETURN:
                                            if (column == 2) {
                                                editedRahdecByHandWhenTextOk(finalIndex, text.getText());
                                            }
                                            setText(finalIndex, column, text.getText());
                                            dirty = true;
                                        case SWT.TRAVERSE_ESCAPE:
                                            text.dispose();
                                            e.doit = false;
                                        }
                                        break;
                                    }
                                }

                            };
                            text.addListener(SWT.FocusOut, textListener);
                            text.addListener(SWT.Traverse, textListener);
                            editor.setEditor(text, item, i);
                            text.setText(item.getText(i));
                            text.selectAll();
                            text.setFocus();
                            return;
                        }
                        if (!visible && rect.intersects(clientArea)) {
                            visible = true;
                        }
                    }
                    if (!visible)
                        return;
                    index++;
                }
            }
        });
    }

    public void setImagePoint(PositionData positionData, int stateMask) {
        int selectionIndex = table.getSelectionIndex();
        if (selectionIndex != -1) {
            TableItem item = table.getItem(selectionIndex);
            String imgCoordsStr = positionData.imageX + ", " + positionData.imageY;
            setText(selectionIndex, 1, imgCoordsStr);
            item.setText(1, imgCoordsStr);
            if (!isEditedRahdecByHand(selectionIndex) && doCalcSphericCoord(selectionIndex) == RecalculResult.RECALCULATED) {
                selectFirstEmptyRowBehindIndex(selectionIndex);
            }
        } else {
            imageRegistrationApp.setWarn("No row in table selected");
        }
    }

    public void setRaDecToRow(RaDec raDec) {
        RahDec rahDec = raDec.toRahDec();
        int selectionIndex = table.getSelectionIndex();
        if (selectionIndex != -1) {
            TableItem item = table.getItem(selectionIndex);
            editedRahdecByHand(selectionIndex);
            String rahDecStr = setRahdecToTable(rahDec, selectionIndex);
            imageRegistrationApp.setInfo("Spheric coordinate " + rahDecStr + " set for point " + item.getText(0));
            selectFirstEmptyRowBehindIndex(selectionIndex);
        } else {
            imageRegistrationApp.setWarn("No row in table selected");
        }
    }

    private String setRahdecToTable(RahDec rahDec, int selectionIndex) {
        String rahDecStr = HourUtil.formatHours(rahDec.rah) + ", " + GonUtil.formatDegrees(rahDec.dec, true);
        setText(selectionIndex, 2, rahDecStr);
        return rahDecStr;
    }

    private void selectFirstEmptyRowBehindIndex(int index) {
        List<ControlPointRow> controlPoints = getControlPoints();
        ControlPointRow controlPointRow = controlPoints.get(index);
        if (controlPointRow.isValid()) {
            for (int i = index + 1; i < table.getItemCount(); i++) {
                controlPointRow = controlPoints.get(i);
                if (controlPointRow.getImgPoint() == null && controlPointRow.getSphPoint() == null) {
                    table.setSelection(i);
                    break;
                }
            }
        }
    }

    private Coordinate parsePoint(String str) {
        String[] split = str.trim().split("[\\s,]+");
        if (split.length != 2) {
            throw new IllegalArgumentException("No 2 coordinates");
        }
        return new Coordinate(Double.parseDouble(split[0].trim()), Double.parseDouble(split[1].trim()));
    }

    public Map<String, RefPoint> getRefPoints() {
        Map<String, RefPoint> refPoints = new HashMap<String, RefPoint>();
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            if (item.getText(1).isBlank() && item.getText(2).isBlank()) {
                continue;
            }
            Coordinate imgPoint;
            try {
                imgPoint = parsePoint(item.getText(1));
            } catch (Exception e1) {
                continue;
            }
            RaDec raDec = null;
            try {
                raDec = ACooUtil.parseCoords(item.getText(2));
            } catch (Exception e) {
            }
            RefPoint refPoint = new RefPoint();
            refPoint.enabled = item.getChecked();
            refPoint.raDec = raDec;
            refPoint.imgPoint = imgPoint;
            refPoints.put(item.getText(0), refPoint);
        }
        return refPoints;
    }
    
    public AffineElements calcAffineElements() {
        return this.affineElements = calcAffineElements(new Point(0, 0));
    }

    public AffineElements calcAffineElements(List<ControlPointRow> controlPointRows) {
        return this.affineElements = calcAffineElements(new Point(0, 0), controlPointRows);
    }
    
    public AffineElements getAffineElements() {
        return affineElements;
    }

    public List<ControlPointRow> getControlPoints() {
        List<ControlPointRow> controlPoints = new ArrayList<>();
        Map<String, RaDec> validRealPoints = new TreeMap<>();

        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            ControlPointRow controlPointRow = new ControlPointRow();
            controlPointRow.setEnabled(item.getChecked());
            try {
                controlPointRow.setImgPoint(parsePoint(item.getText(1)));
            } catch (Exception e) {
                // not handling
            }
            try {
                RaDec raDec = ACooUtil.parseCoords(item.getText(2));
                controlPointRow.setSphPoint(new Coordinate(raDec.ra, raDec.dec));
                validRealPoints.put(item.getText(0), raDec);
            } catch (Exception e) {
                // not handling
            }
            if (!item.getText(1).isBlank() && !item.getText(2).isBlank()) {
                if (!controlPointRow.isValid()) {
                    item.setForeground(3, getShell().getDisplay().getSystemColor(SWT.COLOR_RED));
                    item.setText(3, "Invalid");
                } else {
                    item.setForeground(3, okTextColor);
                }
            }
            controlPoints.add(controlPointRow);
        }

        if (setPointsListener != null) {
            setPointsListener.setPoints(validRealPoints, groupName);
        }

        return controlPoints;
    }

    public AffineElements calcAffineElements(Point centerImgPixel) {
        List<ControlPointRow> controlPoints = getControlPoints();
        return calcAffineElements(centerImgPixel, controlPoints);
    }

    /**
     * Calculate affine elements
     * @param centerImgPixel point considered to be beginning. From this point to the right the x increases and to the down the y increases
     * @return affine elements
     */
    public AffineElements calcAffineElements(Point centerImgPixel, List<ControlPointRow> controlPoints) {
        int validPoints = 0;
        int enabledValidPoints = 0;
        AffineElements affineElements = null;
        List<ControlPoint> refPoints = controlPoints.stream().filter(cp -> cp.isValid() && cp.isEnabled()).map(cp -> new ControlPoint(subtractPoint(cp.getImgPoint(), centerImgPixel), cp.getSphPoint())).collect(Collectors.toList());
        if (refPoints.size() >= 3) {
            affineElements = AffineTransCoefsCalculator.calcCoefs(refPoints);

            double sumPowerErrors = 0;
            int count = 0;
            for (int i = 0; i < controlPoints.size(); i++) {
                ControlPointRow controlPointRow = controlPoints.get(i);
                TableItem item = table.getItem(i);
                if (controlPointRow.isEnabled() && controlPointRow.isValid()) {
                    Coordinate imPoint = subtractPoint(controlPointRow.getImgPoint(), centerImgPixel);
                    RaDec expectedRaDec = new RaDec(controlPointRow.getSphPoint().x, controlPointRow.getSphPoint().y);

                    Coordinate raDecPoint = affineElements.transform(imPoint);

                    RaDec calcRaDec = new RaDec(raDecPoint.x, raDecPoint.y);
                    double angDeg = GonUtil.angularDistanceDeg(expectedRaDec, calcRaDec);

                    item.setText(3, GonUtil.formatDegrees(angDeg));
                    sumPowerErrors += angDeg * angDeg;
                    count++;
                } else {
                    item.setText(3, "");
                }
            }
            String errorAll = GonUtil.formatDegrees(Math.sqrt(sumPowerErrors / count), false);
            String text = "Points: " + count + ", RMS: " + errorAll;
            pointsStatus.setText(text);
        } else {
            pointsStatus.setText("Too little points: " + enabledValidPoints + "/" + validPoints);
        }
        ensureEmptyRow();
        
        return affineElements;
    }

    private static Coordinate subtractPoint(Coordinate imgPoint, Point centerImgPixel) {
        return new Coordinate(imgPoint.x - centerImgPixel.x, imgPoint.y - centerImgPixel.y);
    }

    public void setRefTableChangedListener(RefTableChangedListener refTableChangedListener) {
        this.refTableChangedListener = refTableChangedListener;
    }
    
    public void setFilename(String path) {
        this.path = path;
    }

    public String getFilename() {
        return this.path;
    }


    public boolean isDirty() {
        return dirty;
    }


    public void clearPoints() {
        table.removeAll();
        getControlPoints(); // clear points from spheric view
        calcAffineElements();
        ensureEmptyRow();
        dirty = false;
    }

    public void setSetPointsListener(SetPointsListener setPointsListener) {
        this.setPointsListener = setPointsListener;
    }

    public void setGetCoordsListener(GetCoordsListener getCoordsListener) {
        this.getCoordsListener = getCoordsListener;
        pickRaDec.setEnabled(true);
    }

    RecalculResult recalculByImgPointChanged(int rowIndex) {
        List<ControlPointRow> controlPoints = getControlPoints();
        ControlPointRow controlPointRow = controlPoints.get(rowIndex);
        Coordinate transformed = null;
        if (controlPointRow.getImgPoint() != null) {
            List<ControlPoint> refPoints = controlPoints.stream().filter(cp -> cp.isValid() && cp.isEnabled()).map(cp -> new ControlPoint(cp.getImgPoint(), cp.getSphPoint())).collect(Collectors.toList());
            if (refPoints.size() >= 3) {
                affineElements = AffineTransCoefsCalculator.calcCoefs(refPoints);
                transformed = affineElements.transform(controlPointRow.getImgPoint());
            } else if (refPoints.size() >= 2) {
                ConformalElements conformalElements = ConformalTransCoefsCalculator.calcCoefs(refPoints);
                transformed = conformalElements.transform(controlPointRow.getImgPoint());
            } else if (wcsLeftTopAbstraction != null) {
                RaDec raDec = wcsLeftTopAbstraction.pixelToFK5(controlPointRow.getImgPoint().x, controlPointRow.getImgPoint().y);
                transformed = new Coordinate(raDec.ra, raDec.dec);
            }
            if (transformed != null) {
                RaDec raDec = new RaDec(transformed.x, transformed.y);
                RahDec rahDec = raDec.toRahDec();
                String rahDecStr = setRahdecToTable(rahDec, rowIndex);
                getCoordsListener.goCentreTo(raDec);
                imageRegistrationApp.setInfo("Automatically added spheric point " + rahDecStr);
            }
        }
        if (transformed != null) {
            return RecalculResult.RECALCULATED;
        }
        return RecalculResult.NOT_RECALCULATED_NO_DATA;
    }

    void recalculBySphPointChanged(int rowIndex) {
        List<ControlPointRow> controlPoints = getControlPoints();
        calcAffineElements(controlPoints);
    }

    public void setWcsLeftTopAbstraction(WcsLeftTopAbstraction wcsLeftTopAbstraction) {
        this.wcsLeftTopAbstraction = wcsLeftTopAbstraction;
    }

    private void setText(int row, int column, String str) {
        imageRegistrationApp.clearMessage();
        TableItem item = table.getItem(row);
        item.setText(column, str);
        item.setText(3, "");
        refTableChangedListener.refTableChanged();
        if (column == 1) {
            try {
                parsePoint(item.getText(1));
                item.setForeground(1, okTextColor);
                item.setText(1, item.getText(1));
            } catch (Exception e) {
                item.setForeground(1, getShell().getDisplay().getSystemColor(SWT.COLOR_RED));
                item.setText(1, item.getText(1));
            }
        } else if (column == 2) {
            try {
                ACooUtil.parseCoords(item.getText(2));
                item.setForeground(2, okTextColor);
                item.setText(2, item.getText(2));
            } catch (Exception e) {
                item.setForeground(2, getShell().getDisplay().getSystemColor(SWT.COLOR_RED));
                item.setText(2, item.getText(2));
            }
        }
        ensureEmptyRow();
        calcAffineElements();
    }

    public void editedShape(PointShape pointShape) {
        if (!pointShape.getGroup().getName().equals(groupName)) {
            return;
        }
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            String text = item.getText(0);
            if (text.equals(pointShape.getName())) {
                editedRahdecByHand(i);
                setRahdecToTable(pointShape.getRaDec().toRahDec(), i);
            }
        }
    }

    private void editedRahdecByHandWhenTextOk(int row, String text) {
        try {
            ACooUtil.parseCoords(text);
            TableItem item = table.getItem(row);
            String nameText = item.getText(0);
            editedByHandMap.put(Integer.valueOf(nameText), true);
        } catch (Exception e) {
        }
    }

    private void editedRahdecByHand(int row) {
        TableItem item = table.getItem(row);
        String nameText = item.getText(0);
        editedByHandMap.put(Integer.valueOf(nameText), true);
    }

    private boolean isEditedRahdecByHand(int row) {
        TableItem item = table.getItem(row);
        String nameText = item.getText(0);
        return editedByHandMap.getOrDefault(Integer.valueOf(nameText), false);
    }

    private void removeEditedRahdecByHand(int row) {
        TableItem item = table.getItem(row);
        String nameText = item.getText(0);
        editedByHandMap.remove(Integer.valueOf(nameText));
    }
}
