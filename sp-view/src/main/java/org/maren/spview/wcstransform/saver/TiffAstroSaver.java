package org.maren.spview.wcstransform.saver;

import cz.jmare.file.PathUtil;
import cz.jmare.math.geometry.entity.Point;
import org.maren.spview.imageloader.FitsHeaderItem;
import org.maren.statis.affine.AffineElements;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;

public class TiffAstroSaver implements ReferencedSaver {

    @Override
    public boolean isApplicable(String inputFilename, String filename) {
        return filename.toLowerCase().endsWith(".tif") || filename.toLowerCase().endsWith(".tiff");
    }

    @Override
    public void save(BufferedImage bufferedImage, String outputFilename, AffineElements elements, Point refPix) throws IOException {
        Raster raster = bufferedImage.getData();
        String wcsHeader = genHeader(elements, refPix, raster.getWidth(), raster.getHeight());
        saveWcsTiff(bufferedImage, outputFilename, wcsHeader);
    }

    public void save(BufferedImage bufferedImage, String outputFilename, List<FitsHeaderItem> header) throws IOException {
        saveWcsTiff(bufferedImage, outputFilename, genHeader(header));
    }

    public void saveWcsTiff(BufferedImage bufferedImage, String outputFilename, String wcsHeader) throws IOException {
        TiffMetadataWriteUtil.saveTiffWithMetadata(bufferedImage, new File(outputFilename), new TiffMetadata().add(270, "Image Description", wcsHeader).get());
    }

    public void saveWcsTxt(String outputFilename, String wcsHeader) throws IOException {
        String[] strings = PathUtil.parseFullPathSuffix(outputFilename);
        Files.writeString(Paths.get(strings[0] + ".wcs"), wcsHeader);
    }

    private String genHeader(List<FitsHeaderItem> header) {
        StringBuilder sb = new StringBuilder();
        for (FitsHeaderItem item : header) {
            String value = item.getValue();
            if (value == null) {
                addFullLineHeader(sb, item.getName());
            } else {
                addHeader(sb, item.getName(), item.getValue(), item.getComment());
            }
        }
        sb.append("END");
        return sb.toString();
    }

    private String genHeader(AffineElements elements, Point refPix, int width, int height) {
        StringBuilder sb = new StringBuilder();
        addHeader(sb, "SIMPLE", true, null);
        addHeader(sb, "NAXIS", 2, "Number od data axes");
        addHeader(sb, "NAXIS1", width, "Length of data axis 2");
        addHeader(sb, "NAXIS2", height, "Length of data axis 2");
        addHeader(sb, "CTYPE1", "RA---SIN", "RA coordinate degrees");
        addHeader(sb, "CTYPE2", "DEC--SIN", "DEC coordinate degrees");
        addHeader(sb, "CRPIX1", refPix.x + 0.5, "Refpix of first axis");
        addHeader(sb, "CRPIX2", height - refPix.y + 0.5, "Refpix of second axis");
        addHeader(sb, "CRVAL1", elements.c, "RA at Ref pix in decimal degrees");
        addHeader(sb, "CRVAL2", elements.f, "DEC at Ref pix in decimal degrees");
        addHeader(sb, "CD1_1", elements.a, "CD matrix - the a");
        addHeader(sb, "CD1_2", -elements.b, "CD matrix - the b");
        addHeader(sb, "CD2_1", elements.d, "CD matrix - the d");
        addHeader(sb, "CD2_2", -elements.e, "CD matrix - the e");
        addHeader(sb, "EQUINOX", (int) 2000, "Equinox of coordinates");
        addHeader(sb, "CUNIT1", "deg     ", "Unit of coordinates");
        sb.append("END");
        return sb.toString();
    }

    private void addHeader(StringBuilder sb, String key, double value, String comment) {
        sb.append(rightPad(key, 8));
        sb.append("= ");
        String formVal = String.format(Locale.ENGLISH, "%1.12E", value);
        sb.append(leftPad(formVal, 20));
        sb.append(" /");
        sb.append(comment);
        sb.append("\n");
    }

    private void addHeader(StringBuilder sb, String key, int value, String comment) {
        sb.append(rightPad(key, 8));
        sb.append("= ");
        sb.append(leftPad(String.valueOf(value), 20));
        sb.append(" /");
        sb.append(comment);
        sb.append("\n");
    }

    private void addHeader(StringBuilder sb, String key, String value, String comment) {
        sb.append(rightPad(key, 8));
        sb.append("= ");
        sb.append(rightPad("'" + value + "'", 20));
        sb.append(" /");
        if (comment == null) {
            comment = rightPad("", 72);
        }
        sb.append(comment);
        sb.append("\n");
    }

    private void addHeader(StringBuilder sb, String key, boolean value, String comment) {
        sb.append(rightPad(key, 8));
        sb.append("= ");
        sb.append(value ? leftPad("T", 20) : leftPad("F", 20));
        if (comment != null) {
            sb.append(" /");
            sb.append(comment);
        }
        sb.append("\n");
    }

    private void addFullLineHeader(StringBuilder sb, String name) {
        sb.append(name);
        sb.append("\n");
    }

    private static String rightPad(String str, int number) {
        StringBuilder sb = new StringBuilder();
        int num = number - str.length();
        for (int i = 0; i < num; i++) {
            sb.append(" ");
        }
        return str + sb.toString();
    }

    private static String leftPad(String str, int number) {
        StringBuilder sb = new StringBuilder();
        int num = number - str.length();
        for (int i = 0; i < num; i++) {
            sb.append(" ");
        }
        return sb.toString() + str;
    }
}
