package org.maren.spview.wcstransform.util;

import cz.jmare.file.FileUtil;
import cz.jmare.fits.header.WCS2d;
import nom.tam.fits.Header;

public class WcsUtil {
    public static WCS2d createWCS2d(String headerLinesString) {
        String[] strings = FileUtil.splitToLinesQuick(headerLinesString);
        WCS2d wcs2d = new WCS2d(new Header(strings));
        return wcs2d;
    }
}
