package org.maren.spview.wcstransform.image;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;

import cz.jmare.image.ras.RGBPix;
import cz.jmare.image.ras.RasImageRead;

public abstract class RasImageData implements RasImageRead {
    protected ImageData imageData;
    
    @Override
    public abstract RGBPix getRGB(int x, int y, RGBPix rgbPix);

    @Override
    public int getWidth() {
        return imageData.width;
    }

    @Override
    public int getHeight() {
        return imageData.height;
    }
    
    public static RasImageRead getInstance(ImageData imageData) throws IllegalArgumentException {
        RasImageData rasImage = null;
        PaletteData paletteData = imageData.palette;
        if (paletteData.redMask > 240 && paletteData.blueMask > 16710000) {
            rasImage = new BGRImageData();
        } else if (paletteData.blueMask > 240 && paletteData.redMask > 16710000) {
            rasImage = new RGBImageData();
        } else {
            throw new IllegalArgumentException("Unsupported type of image");
        }
        rasImage.imageData = imageData;
        return rasImage;
    }
}
