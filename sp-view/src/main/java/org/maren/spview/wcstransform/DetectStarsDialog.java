package org.maren.spview.wcstransform;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import cz.jmare.swt.util.Create;

public class DetectStarsDialog extends TitleAreaDialog {

    private boolean resultRed;
    private boolean resultGreen;
    private boolean resultBlue;
    private Button redCheck;
    private Button greenCheck;
    private Button blueCheck;
    private Text pixelsText;
    private int resultPixels;
    private Text pixelScoreText;
    private int resultPixelScore;
    private Text maxStarsText;
    private int resultMaxStars;
    
    static private Boolean lastResultRed;
    static private Boolean lastResultGreen;
    static private Boolean lastResultBlue;
    static private Integer lastResultPixels;
    static private Integer lastResultMaxStars;
    static private Integer lastResultPixelScore;
    
    public DetectStarsDialog(Shell parent) {
        super(parent);
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Detect Stars");
        setMessage("Select parameters by which the stars will be detected", IMessageProvider.INFORMATION);

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        layout.marginLeft = 30;
        composite.setLayout(layout);

        Create.label(composite, "Use Red Channel:");
        redCheck = Create.button(composite, SWT.CHECK, "", () -> {
        });
        redCheck.setToolTipText("Include red channel into calculation of score");
        redCheck.setSelection(lastResultRed != null ? lastResultRed : true);
        
        Create.label(composite, "Use Green Channel:");
        greenCheck = Create.button(composite, SWT.CHECK, "", () -> {
        });
        greenCheck.setToolTipText("Include green channel into calculation of score");
        greenCheck.setSelection(lastResultGreen != null ? lastResultGreen : true);

        Create.label(composite, "Use Blue Channel:");
        blueCheck = Create.button(composite, SWT.CHECK, "", () -> {
        });
        blueCheck.setToolTipText("Include blue channel into calculation of score");
        blueCheck.setSelection(lastResultBlue != null ? lastResultBlue : true);

        Create.label(composite, "At Least Pixels:");
        pixelsText = Create.text(composite, lastResultPixels != null ? String.valueOf(lastResultPixels) : "2", t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 30;
            t.setLayoutData(layoutData);
        });
        
        Create.label(composite, "Pixel Score:");
        pixelScoreText = Create.text(composite, lastResultPixelScore != null ? String.valueOf(lastResultPixelScore) : "450", t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 30;
            t.setLayoutData(layoutData);
            t.setToolTipText("How much score the pixel must have not to be ignored. For R, G and B checked the max value is 765");
        });
        
        Create.label(composite, "Max Stars Number:");
        maxStarsText = Create.text(composite, lastResultMaxStars != null ? String.valueOf(lastResultMaxStars) : "10", t -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 30;
            t.setLayoutData(layoutData);
            t.setToolTipText("The maximum how much stars to detect");
        });
        
        Create.button(composite, "Set Defaults", () -> {
            redCheck.setSelection(true);
            greenCheck.setSelection(true);
            blueCheck.setSelection(true);
            pixelsText.setText("2");
            pixelScoreText.setText("450");
            maxStarsText.setText("10");
        });
        
        return composite;
    }

    @Override
    protected Point getInitialSize() {
        return new Point(450, 350);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        if (buttonId == IDialogConstants.OK_ID) {
            String message;
            if ((message = valid()) != null) {
                setErrorMessage(message);
                return;
            }

            resultRed = redCheck.getSelection();
            resultGreen = greenCheck.getSelection();
            resultBlue = blueCheck.getSelection();
            
            resultPixels = Integer.parseInt(pixelsText.getText().trim());
            resultPixelScore = Integer.parseInt(pixelScoreText.getText().trim());
            resultMaxStars = Integer.parseInt(maxStarsText.getText().trim());
            
            lastResultRed = resultRed;
            lastResultGreen = resultGreen;
            lastResultBlue = resultBlue;
            lastResultPixels = resultPixels;
            lastResultPixelScore = resultPixelScore;
            lastResultMaxStars = resultMaxStars;
        }
        super.buttonPressed(buttonId);
    }

    private String valid() {
        try {
            int parseInt = Integer.parseInt(pixelsText.getText().trim());
            if (parseInt < 1) {
                return "No pixels";
            }
        } catch (NumberFormatException e) {
            return "Pixels must be a number";
        }
        
        try {
            int parseInt = Integer.parseInt(pixelScoreText.getText().trim());
            if (parseInt < 1) {
                return "No pixel score - nothing would be detected";
            }
        } catch (NumberFormatException e) {
            return "Pixels score must be a number (0 - 765)";
        }
        
        try {
            int parseInt = Integer.parseInt(maxStarsText.getText().trim());
            if (parseInt < 1) {
                return "Max stars is too low";
            }
        } catch (NumberFormatException e) {
            return "Max stars must be a number";
        }
        return null;
    }

    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public boolean isResultRed() {
        return resultRed;
    }

    public boolean isResultGreen() {
        return resultGreen;
    }

    public boolean isResultBlue() {
        return resultBlue;
    }

    public int getResultPixels() {
        return resultPixels;
    }

    public int getResultPixelScore() {
        return resultPixelScore;
    }

    public int getResultMaxStars() {
        return resultMaxStars;
    }
}
