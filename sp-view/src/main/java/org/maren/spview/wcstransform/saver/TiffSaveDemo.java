package org.maren.spview.wcstransform.saver;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.ImageOutputStream;

import org.w3c.dom.Node;

public class TiffSaveDemo {
    public static void main(String[] args) throws IOException, IOException {
        BufferedImage original = ImageIO.read(new File("d:\\Pictures\\hvezdy-20221022\\m34\\nef\\tif\\result\\output-stacked.tif"));

        ImageWriter writer = ImageIO.getImageWritersByFormatName("TIFF").next();

        try (ImageOutputStream stream = ImageIO.createImageOutputStream(new File("out.tif"))) {
            // You may use the param to control compression
            ImageWriteParam param = writer.getDefaultWriteParam();

            IIOMetadata metadata = writer.getDefaultImageMetadata(ImageTypeSpecifier.createFromRenderedImage(original), param);
            Node root = metadata.getAsTree("com_sun_media_imageio_plugins_tiff_image_1.0"); // "javax_imageio_tiff_image_1.0" will work in later versions
            Node ifd = root.getFirstChild();
            // Add X and Y resolution tags
            ifd.appendChild(createResTag("282", "XResolution", "204/1"));
            ifd.appendChild(createResTag("283", "YResolution", "196/1"));
            ifd.appendChild(createAsciiField(270, "Image Description", "Muj popis"));

            // Merge changes back to metadata
            metadata.mergeTree("com_sun_media_imageio_plugins_tiff_image_1.0", root);

            // Write full image, with metadata
            writer.setOutput(stream);
            writer.write(null, new IIOImage(original, null, metadata), param);
        }
        finally {
            writer.dispose();
        }
    }

    private static IIOMetadataNode createResTag(String tagNumber, String tagName, String tagValue) {
        IIOMetadataNode res = new IIOMetadataNode("TIFFField");
        res.setAttribute("number", tagNumber);
        res.setAttribute("name", tagName); // Tag name is optional

        IIOMetadataNode value = new IIOMetadataNode("TIFFRational");
        value.setAttribute("value", tagValue);

        IIOMetadataNode rationals = new IIOMetadataNode("TIFFRationals");
        rationals.appendChild(value);
        res.appendChild(rationals);

        return res;
    }

    private static IIOMetadataNode createAsciiField(int number, String name, String val) {
        IIOMetadataNode field, arrayNode, valueNode;
        field = new IIOMetadataNode("TIFFField");
        field.setAttribute("number", Integer.toString(number));
        field.setAttribute("name", name);
        arrayNode = new IIOMetadataNode("TIFFAsciis");
        field.appendChild(arrayNode);
        valueNode = new IIOMetadataNode("TIFFAscii");
        arrayNode.appendChild(valueNode);
        valueNode.setAttribute("value", val);
        return field;
    }
}
