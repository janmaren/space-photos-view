package org.maren.spview.wcstransform.util;

import cz.jmare.fits.header.WCS2d;
import cz.jmare.math.astro.RaDec;

public class PicWcsLeftTopAbstraction implements WcsLeftTopAbstraction {
    private WCS2d wcs2d;

    public PicWcsLeftTopAbstraction(WCS2d wcs2d) {
        this.wcs2d = wcs2d;
    }

    @Override
    public RaDec pixelToFK5(double xFromLeft, double yFromTop) {
        return wcs2d.pixelToFK5(xFromLeft, yFromTop);
    }
}
