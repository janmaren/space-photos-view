package org.maren.spview.wcstransform.saver;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.maren.statis.affine.AffineElements;

import cz.jmare.math.geometry.entity.Point;
import nom.tam.fits.BasicHDU;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.FitsFactory;
import nom.tam.fits.Header;
import nom.tam.util.FitsOutputStream;

public abstract class FitsReferencedSaver implements ReferencedSaver {

    @Override
    public boolean isApplicable(String inputFilename, String filename) {
        return filename.toLowerCase().endsWith(".fits");
    }

    @Override
    public void save(BufferedImage bufferedImage, String outputFilename, AffineElements elements, Point refPix) throws IOException {
        Raster raster = bufferedImage.getData();
        
        Object imageData = genImage(bufferedImage);
        int height = raster.getHeight();
        Fits f = new Fits();
        try (
            FitsOutputStream out = new FitsOutputStream(new FileOutputStream(new File(outputFilename)))) {     
            BasicHDU<?> hdu = FitsFactory.hduFactory(imageData);
            Header header = hdu.getHeader();
            header.addValue("CTYPE1", "RA---SIN", "RA coordinate degrees");
            header.addValue("CTYPE2", "DEC--SIN", "DEC coordinate degrees");
            header.addValue("CRPIX1", refPix.x + 0.5, "Refpix of first axis");
            header.addValue("CRPIX2", height - refPix.y + 0.5, "Refpix of second axis");
            header.addValue("CRVAL1", elements.c, "RA at Ref pix in decimal degrees");
            header.addValue("CRVAL2", elements.f, "DEC at Ref pix in decimal degrees");
            header.addValue("CD1_1", elements.a, "CD matrix - the a");
            header.addValue("CD1_2", -elements.b, "CD matrix - the b");
            header.addValue("CD2_1", elements.d, "CD matrix - the d");
            header.addValue("CD2_2", -elements.e, "CD matrix - the e");
            f.addHDU(hdu);
            f.write(out);
        } catch (FitsException e) {
            throw new IllegalStateException("Unable to create fits", e);
        } finally {
            f.close();
        }
    }
    
    public abstract Object genImage(BufferedImage bufferedImage);

    public static FitsReferencedSaver getSaverByData(BufferedImage bufferedImage) {
        DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();
        if (dataBuffer instanceof DataBufferUShort) {
            return new FitsRGBShortsToFloatsChannelsSaver(); // FitsRGBShortsChannelsSaver short[] doesn't work with FitsFactory.hduFactory, use float[]
        }
        if (dataBuffer instanceof DataBufferFloat) {
            return new FitsRGBFloatsChannelsSaver();
        }
        if (dataBuffer instanceof DataBufferByte) {
            return new FitsRGB255ChannelsSaver();
        }
        throw new IllegalStateException("Unsupported data buffer " + dataBuffer.getClass());
    }
}
