package org.maren.spview.wcstransform;

public interface RefTableChangedListener {
    void refTableChanged();
}
