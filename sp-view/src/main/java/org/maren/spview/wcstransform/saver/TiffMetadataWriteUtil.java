package org.maren.spview.wcstransform.saver;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.plugins.tiff.TIFFDirectory;
import javax.imageio.plugins.tiff.TIFFField;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import org.w3c.dom.Node;

public class TiffMetadataWriteUtil {

    public static void saveTiffWithMetadata(File inputFile, File outputFile, TiffMetadata tiffMetadata) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(inputFile);
        saveTiffWithMetadata(bufferedImage, outputFile, tiffMetadata.get());
    }

//    public static void saveTiffWithMetadata(BufferedImage bufferedImage, File outputFile, TiffMetadata tiffMetadata) throws IOException {
//        saveTiffWithMetadata(bufferedImage, outputFile, tiffMetadata.get());
//    }

    public static void saveTiffWithMetadata(File inputFile, File outputFile, Collection<IIOMetadataNode> metadataChilds) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(inputFile);
        saveTiffWithMetadata(bufferedImage, outputFile, metadataChilds);
    }

    public static void saveTiffWithMetadata(BufferedImage bufferedImage, File outputFile, Collection<IIOMetadataNode> metadataChilds) throws IOException {
        ImageWriter writer = ImageIO.getImageWritersByFormatName("TIFF").next();

        try (ImageOutputStream stream = ImageIO.createImageOutputStream(outputFile)) {
            // You may use the param to control compression
            ImageWriteParam param = writer.getDefaultWriteParam();

            IIOMetadata metadata = writer.getDefaultImageMetadata(ImageTypeSpecifier.createFromRenderedImage(bufferedImage), param);
            Node root = metadata.getAsTree("javax_imageio_tiff_image_1.0"); // "javax_imageio_tiff_image_1.0" will work in later versions
            Node ifd = root.getFirstChild();

            // add metadata
            for (IIOMetadataNode node: metadataChilds) {
                ifd.appendChild(node);
            }

            // Merge changes back to metadata
            metadata.mergeTree("javax_imageio_tiff_image_1.0", root);

            // Write full image, with metadata
            writer.setOutput(stream);
            writer.write(null, new IIOImage(bufferedImage, null, metadata), param);
        }
        finally {
            writer.dispose();
        }
    }

    public static Map<Integer, TIFFField> getTiffMetadata(File inputFile, Collection<Integer> keys) throws IOException {
        Map<Integer, TIFFField> map = new HashMap<>();
        try (ImageInputStream input = ImageIO.createImageInputStream(inputFile)) {
            ImageReader reader = ImageIO.getImageReaders(input).next();
            reader.setInput(input);
            IIOMetadata metadata = reader.getImageMetadata(0);
            TIFFDirectory ifd = TIFFDirectory.createFromMetadata(metadata);
            for (Integer key: keys) {
                TIFFField field = ifd.getTIFFField(key);
                map.put(key, field);
            }
        }
        return map;
    }
}
