package org.maren.spview.wcstransform.util;

import cz.jmare.fits.header.InconsistentFitsException;
import nom.tam.fits.*;

import java.io.File;
import java.io.IOException;

public class AddHeaderTest {
    public static void main(String[] args) throws FitsException, IOException {
        Fits fits = new Fits(new File("C:\\temp\\test\\test-wcs.fits"));
        int i = imageHDU(fits);
        BasicHDU<?> hdu = fits.getHDU(i);
        Header header = hdu.getHeader();
        header.addValue("TELESCOP", "SW100", "telescope name");
        fits.write(new File("C:\\temp\\test\\test-wcsx.fits"));
    }

    public static int imageHDU(Fits fits) {
        boolean noNaxis = false;
        int i = 0;
        try {
            BasicHDU<?> hdu = null;
            while ((hdu = fits.getHDU(i)) != null) {
                if (hdu instanceof ImageHDU) {
                    ImageHDU imageHDU = (ImageHDU) hdu;
                    int[] axes = imageHDU.getAxes();
                    if (axes == null) {
                        noNaxis = true;
                        continue;
                    }
                    return i;
                }
                i++;
            }
        } catch (FitsException | IOException e) {
            throw new IllegalArgumentException("Unable to find image hdu in file", e);
        }
        if (noNaxis) {
            throw new InconsistentFitsException("Found image hdu in file but no NAXIS exists");
        }
        throw new IllegalArgumentException("Unable to find image hdu in file");
    }
}
