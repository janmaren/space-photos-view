package org.maren.spview.wcstransform.saver;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferFloat;
import java.awt.image.Raster;

public class FitsRGBFloatsChannelsSaver extends FitsReferencedSaver {
    public Object genImage(BufferedImage bufferedImage) {
        Raster raster = bufferedImage.getData();
        
        int height = raster.getHeight();
        int width = raster.getWidth();
        float[][][] data = new float[3][height][width];
        float[] pixels = ((DataBufferFloat) bufferedImage.getRaster().getDataBuffer()).getData();
        int numForPixel = pixels.length / (width * height);
        if (numForPixel != 3) {
            throw new IllegalStateException("Not RGB data");
        }
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int index = (width * i + j) * 3;
                float pixelR = pixels[index++];
                float pixelG = pixels[index++];
                float pixelB = pixels[index];
                data[0][height - i - 1][j] = pixelR;
                data[1][height - i - 1][j] = pixelG;
                data[2][height - i - 1][j] = pixelB;
            }
        }
        return data;
    }
}
