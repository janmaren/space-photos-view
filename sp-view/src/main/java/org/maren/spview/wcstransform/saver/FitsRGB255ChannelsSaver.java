package org.maren.spview.wcstransform.saver;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;

import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;

public class FitsRGB255ChannelsSaver extends FitsReferencedSaver {
    public byte[][][] genImage(BufferedImage bufferedImage) {
        Raster raster = bufferedImage.getData();
        
        int height = raster.getHeight();
        int width = raster.getWidth();
        byte[][][] data = new byte[3][height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgb = bufferedImage.getRGB(j, i);
                RGBEntity rHighestToRGB = RGBEntUtil.rHighestToRGB(rgb);
                if (rHighestToRGB.red > 255 || rHighestToRGB.green > 255 || rHighestToRGB.blue > 255) {
                    throw new IllegalStateException("Image is not RGB [0-255], [0-255], [0-255]");
                }
                data[0][height - i - 1][j] = (byte) rHighestToRGB.red;
                data[1][height - i - 1][j] = (byte) rHighestToRGB.green;
                data[2][height - i - 1][j] = (byte) rHighestToRGB.blue;
            }
        }
        return data;
    }
}
