package org.maren.spview.wcstransform;

import static cz.jmare.rastercomposite.RasterComposite.FEATURE_DRAW_CURSOR;
import static cz.jmare.rastercomposite.RasterComposite.FEATURE_SHOW_FOV;
import static cz.jmare.rastercomposite.RasterComposite.FEATURE_SHOW_ZOOM;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.plugins.tiff.TIFFField;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.maren.spview.SPView;
import org.maren.spview.app.GetCoordsListener;
import org.maren.spview.app.PointShapeEditedListener;
import org.maren.spview.app.SetPointsListener;
import org.maren.spview.astrometry.AstrometryDownloadRunnable;
import org.maren.spview.astrometry.AstrometryResultConsumer;
import org.maren.spview.imageloader.FitsHeaderItem;
import org.maren.spview.imageloader.FitsHeaderUtil;
import org.maren.spview.imageloader.FitsImageLoader;
import org.maren.spview.imageloader.ImageAndMetadata;
import org.maren.spview.imageloader.LoadBufferedImageUtil;
import org.maren.spview.imageloader.WcsMetadataUtil;
import org.maren.spview.shape.PointShape;
import org.maren.spview.wcstransform.SaveWcsDialog.Format;
import org.maren.spview.wcstransform.saver.AffineFileSaver;
import org.maren.spview.wcstransform.saver.FitsHeaderSaveUtil;
import org.maren.spview.wcstransform.saver.FitsReferencedSaver;
import org.maren.spview.wcstransform.saver.TiffAstroSaver;
import org.maren.spview.wcstransform.util.PicWcsLeftTopAbstraction;
import org.maren.spview.wcstransform.util.WcsUtil;
import org.maren.statis.entity.Coordinate;
import org.maren.swt.util.BufImageSWT;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.data.util.GonUtil;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.fits.header.WCS2d;
import cz.jmare.fits.util.FitsUtil;
import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.centroid.PixelScore;
import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.image.ras.RasImagePack;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.PathUtil;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.os.AppHelper;
import cz.jmare.os.PlatformType;
import cz.jmare.pureraster.DrawScreenInfo;
import cz.jmare.pureraster.PositionData;
import cz.jmare.pureraster.RasterToolListener;
import cz.jmare.rastercomposite.RasterComposite;
import cz.jmare.swt.action.ActionOperation;
import cz.jmare.swt.action.SwitchOperation;
import cz.jmare.swt.image.AwtSwtUtil;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.key.KeyLogger;
import cz.jmare.swt.key.MouseLogger;
import cz.jmare.swt.util.SMessage;
import nom.tam.fits.Fits;
import nom.tam.fits.ImageHDU;

public class ImageRegistrationApp extends ApplicationWindow implements RasterToolListener,
        RefTableChangedListener, PointShapeEditedListener, AstrometryResultConsumer {

    StatusLineManager slm = new StatusLineManager();
    ToolBarManager toolbarManager = new ToolBarManager();
    private MenuManager menuFile;
    MenuManager menuManager = new MenuManager();
    private ActionOperation openFileOperation;
    private ActionOperation astrometryOperation;
    private ActionOperation toWcsOperation;
    private RasterComposite rasterComposite;
    private RefPointsTableComposite refPointsTableComposite;
    private String imagePath;
    private Integer imgHeight;
    private Integer imgWidth;
    private SPCaller spCaller;
    private Image image;
    private GetCoordsListener getCoordsListener;

    private SetPointsListener setPointsListener;

    private AstrometryDownloadRunnable astrometryDownloadRunnable;
    private Thread astrometryDownloadThread;
    private SwitchOperation fitsStateColorOperation;
    private File astrometryNewFitsFile;

    public ImageRegistrationApp() {
        super(null);
        menuManager.add(menuFile = new MenuManager("File"));
        addMenuBar();
        addToolBar(SWT.FLAT | SWT.WRAP);
        addStatusLine();
    }

    protected Control createContents(Composite parent) {
        initImages();
        initActions();

        KeyLogger.init();
        MouseLogger.initWithStatus(slm);
        
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new FillLayout());
        
        SashForm leftRightSash = new SashForm(composite, SWT.NONE);

        rasterComposite = new RasterComposite(leftRightSash, SWT.NONE,  FEATURE_SHOW_FOV | FEATURE_SHOW_ZOOM | FEATURE_DRAW_CURSOR);
        rasterComposite.setDragButton(3);
        rasterComposite.setDrawObjectsListener((GC gc, DrawScreenInfo drawScreenInfo) -> {
            gc.setForeground(getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_GREEN));
            Map<String, RefPoint> refPoints = refPointsTableComposite.getRefPoints();
            for (Map.Entry<String, RefPoint> entry : refPoints.entrySet()) {
                RefPoint refPoint = entry.getValue();
                String number = entry.getKey();
                gc.drawOval((int) drawScreenInfo.toScreenX(refPoint.imgPoint.x) - 3, (int) drawScreenInfo.toScreenY(refPoint.imgPoint.y) - 3, 6, 6);
                gc.drawString(number, drawScreenInfo.toScreenX(refPoint.imgPoint.x) + 6, drawScreenInfo.toScreenY(refPoint.imgPoint.y), true);
                if (refPoint.enabled) {
                    gc.setForeground(getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_YELLOW));
                    gc.drawOval((int) drawScreenInfo.toScreenX(refPoint.imgPoint.x) - 5, (int) drawScreenInfo.toScreenY(refPoint.imgPoint.y) - 5, 10, 10);
                    gc.setForeground(getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_GREEN));
                }
            }
            org.maren.statis.affine.AffineElements affineElements = refPointsTableComposite.getAffineElements();
            if (affineElements != null) {
                int centerX = imgWidth / 2;
                int centerY = imgHeight / 2;
                int centerX2 = centerX + imgWidth / 2;
                Coordinate raDecC = affineElements.transform(new Coordinate(centerX, centerY));
                Coordinate raDec2 = affineElements.transform(new Coordinate(centerX2, centerY));
                int pixels = centerX2 - centerX;
                double angularDistance = GonUtil.angularDistanceDeg(new RaDec(raDecC.x, raDecC.y), new RaDec(raDec2.x, raDec2.y));
                gc.setForeground(getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_GREEN));
                gc.drawString(GonUtil.formatDegreesForLength(angularDistance / pixels) + "/px", 10, 10, true);

                Rectangle canvasDim = rasterComposite.getCanvasDim();
                double zoom = rasterComposite.getZoom();

                rasterComposite.setFovText("W: " + GonUtil.formatDegreesForLength(angularDistance * canvasDim.width / zoom / pixels) +
                        ", H:" + GonUtil.formatDegreesForLength(angularDistance * canvasDim.height / zoom / pixels));
            } else {
                rasterComposite.setFovText(null);
            }
        });

        rasterComposite.setRasterToolListener(this);
        
        populateMenuFile();
        menuManager.update(true);
        menuFile.update(true);
        createRightPanel(leftRightSash);
        leftRightSash.setWeights(60, 40);
        toolbarManager.update(true);
        
        initDrop();

        return composite;
    }

    private void initImages() {
        ImageUtil.put("color-red", "/image/color-red.png");
        ImageUtil.put("color-greend", "/image/color-greend.png");
        ImageUtil.put("color-orange", "/image/color-orange.png");
    }

    private void createRightPanel(SashForm leftRightSash) {
        refPointsTableComposite = new RefPointsTableComposite(leftRightSash, SWT.BORDER, slm, this);
        refPointsTableComposite.setRefTableChangedListener(this);
        refPointsTableComposite.setSetPointsListener(setPointsListener);
        refPointsTableComposite.setGetCoordsListener(getCoordsListener);
    }

    private void populateMenuFile() {
        menuFile.add(openFileOperation);
        menuFile.add(astrometryOperation);
        menuFile.add(toWcsOperation);
        toolbarManager.add(openFileOperation);
        toolbarManager.add(astrometryOperation);
        toolbarManager.add(toWcsOperation);
        toolbarManager.add(new Separator());
        toolbarManager.add(fitsStateColorOperation);
    }
    
    private Boolean discardRefPointsDialog() {
        MessageBox messageBox = new MessageBox(getShell(), SWT.YES | SWT.NO | SWT.CANCEL| SWT.ICON_WARNING);
        messageBox.setMessage("You have unsaved ref. points.\nDo you want to discard (YES) the changes or not (NO) to discard or cancel loading?");
        int result = messageBox.open();
        if (result == SWT.YES) {
            return true;
        }
        if (result == SWT.NO) {
            return false;
        }
        return null;
    }
    
    private void initActions() {
        ImageUtil.put("folder", "/image/folder.png");
        ImageUtil.put("save", "/image/document-save.png");
        ImageUtil.put("astrometry", "/image/astrometry.png");
        
        openFileOperation = new ActionOperation("Open File", "folder", SWT.CTRL | 'L', () -> {
            FileDialog fd = new FileDialog(getShell(), SWT.OPEN | SWT.MULTI);
            fd.setText("Image Files to Open");
            ArrayList<String> suffixes = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            for (String supportedSuffix: SPView.IMAGE_SUFFIXES) {
                if (sb.length() > 0) {
                    sb.append(";");
                }
                sb.append("*" + supportedSuffix + (AppHelper.getPlatform() == PlatformType.LINUX ? ";*" + supportedSuffix.toUpperCase() : ""));
                suffixes.add("*" + supportedSuffix  + (AppHelper.getPlatform() == PlatformType.LINUX ? ";*" + supportedSuffix.toUpperCase() : ""));
            }
            suffixes.add(0, sb.toString());
            String[] filterExt = suffixes.toArray(new String[suffixes.size()]);
            fd.setFilterExtensions(filterExt);
            String path = null;
            if ((path = fd.open()) != null) {
                Boolean discard = null;
                if (refPointsTableComposite.isDirty()) {
                    discard = discardRefPointsDialog();
                    if (discard == null) {
                        return;
                    }
                }
                String finalPath = path;
                Boolean finalDiscard = discard;
                SMessage.withErrorStatus(slm, () -> {
                    loadImage(finalPath);
                    if (finalDiscard == null || finalDiscard == Boolean.TRUE) {
                        refPointsTableComposite.clearPoints();
                    }
                });
            }
        });

        astrometryOperation = new ActionOperation("Astrometry", "astrometry", SWT.CTRL | 'A', () -> {
            if (imagePath == null) {
                setWarn("No file opened");
                return;
            }
            ConfigStore configStore = AppConfig.getInstance().getConfigStore();
            String astrometryApiKey = configStore.getValue("astrometry.api.key", null);
            if (astrometryApiKey == null) {
                setWarn("No astrometry api key in settings");
                return;
            }
            AstrometryDownloadRunnable runnable = astrometryDownloadRunnable;
            if (runnable != null) {
                runnable.setAstrometryResultConsumer(null);
            }
            Thread thread = astrometryDownloadThread;
            if (thread != null) {
                thread.interrupt();
            }
            fitsStateColorOperation.setImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get("color-orange")));
            fitsStateColorOperation.setDisabledImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get("color-orange")));
            astrometryDownloadRunnable = new AstrometryDownloadRunnable(imagePath, astrometryApiKey, true, true, this);
            astrometryDownloadThread = new Thread(astrometryDownloadRunnable);
            astrometryDownloadThread.start();
            setInfo("Astrometry plate solving in progress...");
        });

        toWcsOperation = new ActionOperation("To WCS", "save", SWT.CTRL | 'S', () -> {
            if (imagePath == null) {
                slm.setErrorMessage("No Image Opened");
                return;
            }

            Point centerImgPixel = new Point(imgWidth / 2.0, imgHeight / 2.0);
            org.maren.statis.affine.AffineElements affineElementsCentered = refPointsTableComposite.calcAffineElements(centerImgPixel);

            if (affineElementsCentered == null && astrometryNewFitsFile == null) {
                slm.setErrorMessage("Unable to calculate tranformation");
                return;
            }

            SaveWcsDialog saveWcsDialog = new SaveWcsDialog(getShell(), imagePath, astrometryNewFitsFile != null, affineElementsCentered != null);
            if (saveWcsDialog.open() == IDialogConstants.OK_ID) {
                String outputFilename = saveWcsDialog.getResultOutputFilename();
                Format resultFormat = saveWcsDialog.getResultFormat();
                if (resultFormat == Format.FITS_BY_ASTROMETRY) {
                    try {
                        Files.copy(astrometryNewFitsFile.toPath(), Paths.get(outputFilename));
                    } catch (IOException e) {
                        slm.setErrorMessage(ExceptionMessage.getCombinedMessage("Writting error", e));
                    }
                } else if (resultFormat == Format.TIFF) {
                    TiffAstroSaver tiffAstroSaver = new TiffAstroSaver();
                    try {
                        BufferedImage bufferedImage = LoadBufferedImageUtil.load(refPointsTableComposite.getFilename());
                        tiffAstroSaver.save(bufferedImage, outputFilename, affineElementsCentered, centerImgPixel);
                    } catch (IOException e) {
                        slm.setErrorMessage(ExceptionMessage.getCombinedMessage("Writting error", e));
                    }
                } else if (resultFormat == Format.TIFF_BY_ASTROMETRY) {
                    TiffAstroSaver tiffAstroSaver = new TiffAstroSaver();
                    try {
                        List<FitsHeaderItem> headerValuesComments = FitsHeaderUtil.getHeaderValuesComments(astrometryNewFitsFile);
                        BufferedImage bufferedImage = LoadBufferedImageUtil.load(refPointsTableComposite.getFilename());
                        tiffAstroSaver.save(bufferedImage, outputFilename, headerValuesComments);
                    } catch (IOException e) {
                        slm.setErrorMessage(ExceptionMessage.getCombinedMessage("Writting error", e));
                    }
                } else if (resultFormat == Format.FITS_AS_IS) {
                    try {
                        FitsHeaderSaveUtil.putWcsToFitsFile(new File(refPointsTableComposite.getFilename()), new File(outputFilename), affineElementsCentered, centerImgPixel, imgHeight);
                        slm.setMessage("Saved to " + outputFilename);
                    } catch (Exception e) {
                        slm.setErrorMessage(ExceptionMessage.getCombinedMessage("Writting error", e));
                    }
                } else if (resultFormat == Format.FITS_3_CHANNELS) {
                    BufferedImage bufferedImage;
                    try {
                        bufferedImage = LoadBufferedImageUtil.load(refPointsTableComposite.getFilename());
                    } catch (IOException e) {
                        slm.setErrorMessage(ExceptionMessage.getCombinedMessage("loading error", e));
                        return;
                    }
                    FitsReferencedSaver fitsReferencedSaver = FitsReferencedSaver.getSaverByData(bufferedImage);
                    try {
                        fitsReferencedSaver.save(bufferedImage, outputFilename, affineElementsCentered, centerImgPixel);
                        slm.setMessage("Saved to " + outputFilename);
                    } catch (IOException e) {
                        slm.setErrorMessage(ExceptionMessage.getCombinedMessage("Writting error", e));
                    }
                } else if (resultFormat == Format.WLD) {
                    Point imgPixel = new Point(0, 0);
                    org.maren.statis.affine.AffineElements affineElements = refPointsTableComposite.calcAffineElements(imgPixel);

                    AffineFileSaver affineFileSaver = new AffineFileSaver();
                    try {
                        affineFileSaver.save(null, outputFilename, affineElements, imgPixel);
                        slm.setMessage("Saved to " + outputFilename);
                    } catch (IOException e) {
                        slm.setErrorMessage(ExceptionMessage.getCombinedMessage("Writting error", e));
                    }
                }
                if (saveWcsDialog.isResultOpen()) {
                    if (spCaller != null) {
                        spCaller.openRegistered(saveWcsDialog.getResultOutputFilename());
                    }
                }
            }
        });

        fitsStateColorOperation = new SwitchOperation("Plate solve state", "color-red", SWT.CTRL + 'R', (b) -> {
        });
        fitsStateColorOperation.setEnabled(false);
        fitsStateColorOperation.setDisabledImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get("color-red")));
    }

    private void loadImage(String path) throws IOException {
        ImageData imageData;
        File file = new File(path);
        try {
            RasImagePack rasImagePack = RasImageFactory.get(file);
            imageData = BufImageSWT.toImageData(rasImagePack.getRasImage());
        } catch (Exception e) {
            if (path.toLowerCase().endsWith(".fits") || path.toLowerCase().endsWith(".fit")) {
                try {
                    FitsImageLoader fitsImageLoader = new FitsImageLoader(path);
                    ImageAndMetadata imageAndMetadata = fitsImageLoader.loadImageData();
                    imageData = imageAndMetadata.imageData;
                    AwtSwtUtil.revertRows(imageData);
                } catch (Exception ex) {
                    rasterComposite.setImageAndRedraw(null);
                    slm.setErrorMessage("Unable to load and show " + path);
                    return;
                }
            } else {
                rasterComposite.setImageAndRedraw(null);
                slm.setErrorMessage("Unable to load and show " + path);
                return;
            }
        }

        Image image = new Image(getShell().getDisplay(), imageData);
        this.image = image;
        rasterComposite.setImageAndCenterFitRedraw(image);
        imagePath = path;
        imgWidth = image.getImageData().width;
        imgHeight = image.getImageData().height;
        refPointsTableComposite.setFilename(path);
        boolean wcsLoaded = loadWcs(path);
        getShell().setText(path + (wcsLoaded ? " WCS" : ""));
    }

    private boolean loadWcs(String file) {
        String[] fullPathAndSuffix = PathUtil.toFullPathAndSuffix(file);
        if (fullPathAndSuffix[1].equalsIgnoreCase(".tif") || fullPathAndSuffix[1].equalsIgnoreCase(".tiff")) {
            Map<Integer, TIFFField> tiffMetadata = null;
            try {
                tiffMetadata = WcsMetadataUtil.getTiffMetadata(new File(file), List.of(270));
                TIFFField tiffField = tiffMetadata.get(270);
                if (tiffField == null) {
                    return false;
                }
                String fitsHeader = tiffField.getValueAsString(0);
                WCS2d wcs2d = WcsUtil.createWCS2d(fitsHeader);
                refPointsTableComposite.setWcsLeftTopAbstraction(new PicWcsLeftTopAbstraction(wcs2d));
                return true;
            } catch (IOException e) {
                // no handling
            }
        } else if (fullPathAndSuffix[1].equalsIgnoreCase(".fits") || fullPathAndSuffix[1].equalsIgnoreCase(".fit"))  {
            try (Fits fits = new Fits(file)) {
                ImageHDU hdu = FitsUtil.imageHDU(fits);
                WCS2d wcs2d = new WCS2d(hdu);
                refPointsTableComposite.setWcsLeftTopAbstraction(new PicWcsLeftTopAbstraction(wcs2d));
                return true;
            } catch (Exception e) {
                // no handling
            }
        }
        refPointsTableComposite.setWcsLeftTopAbstraction(null);
        return false;
    }
    
    
    private void initDrop() {
        DropTarget dropTarget = new DropTarget(getShell(), DND.DROP_MOVE | DND.DROP_COPY);
        dropTarget.setTransfer(FileTransfer.getInstance());
        dropTarget.addDropListener(new DropTargetAdapter() {
            public void drop(DropTargetEvent event) {
                Object data = event.data;
                if (data instanceof String[]) {
                    String[] strs = (String[]) data;
                    if (strs.length == 1) {
                        SMessage.withErrorStatus(slm, () -> {
                            loadImage(strs[0]);
                        });
                    }
                }
            }
        });
    }
    
    @Override
    protected StatusLineManager createStatusLineManager() {
        return slm;
    }
    
    @Override
    protected ToolBarManager createToolBarManager(int style) {
        return toolbarManager;
    }
    
    @Override
    protected MenuManager createMenuManager() {
        return menuManager;
    }
    
    protected void initializeBounds() {
        getShell().setText("WCS Solving");
        getShell().setSize(1130, 750);
        getShell().setMinimumSize(900, 700);
    }
    
    public static void main(String[] args) {
        ImageRegistrationApp awin = new ImageRegistrationApp();
        awin.setBlockOnOpen(true);
        awin.open();
        Display.getCurrent().dispose();
    }

    @Override
    public void refTableChanged() {
        rasterComposite.refresh();
    }

    public void setSpCaller(SPCaller spCaller) {
        this.spCaller = spCaller;
    }

    @Override
    public boolean close() {
        if (refPointsTableComposite.isDirty()) {
            if (!discardRefPointsYesNoDialog()) {
                return false;
            }
        }
        if (astrometryDownloadThread != null) {
            astrometryDownloadThread.interrupt();
        }
        return super.close();
    }

    private boolean discardRefPointsYesNoDialog() {
        MessageBox messageBox = new MessageBox(getShell(), SWT.YES | SWT.NO | SWT.ICON_WARNING);
        messageBox.setMessage("You have unsaved ref. points.\nDo you want to discard the changes?");
        int result = messageBox.open();
        return result == SWT.YES;
    }

    public Set<Centroid> detectStars(Float clusterScoreGreater, int pixelScoreThreshold, PixelScore pixelScore, int pixelsMakingCluster) {
        if (image == null) {
            Set.of();
        }
        CentroidsAnalyzer centroidsAnalyzer = new CentroidsAnalyzer(new CachedImage(new File(imagePath)), pixelScoreThreshold, pixelsMakingCluster);
        centroidsAnalyzer.setPixelScore(pixelScore);
        centroidsAnalyzer.setMinClusterScore(clusterScoreGreater);
        return centroidsAnalyzer.centroids();
    }

    public void setGetCoordsListener(GetCoordsListener getCoordsListener) {
        this.getCoordsListener = getCoordsListener;
    }

    public GetCoordsListener getGetCoordsListener() {
        return getCoordsListener;
    }

    public void setSetPointsListener(SetPointsListener setPointsListener) {
        this.setPointsListener = setPointsListener;
    }

    @Override
    protected void handleShellCloseEvent() {
        refPointsTableComposite.clearPoints();
        super.handleShellCloseEvent();
    }

    public void clearMessage() {
        getStatusLineManager().setErrorMessage(null);
    }

    public void setInfo(String message) {
        getStatusLineManager().setMessage(message);
    }

    public void setWarn(String message) {
        getStatusLineManager().setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_WARNING), message);
    }

    public void setError(String message) {
        getStatusLineManager().setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_ERROR), message);
    }

    @Override
    public void editedShape(PointShape pointShape) {
        refPointsTableComposite.editedShape(pointShape);
    }

    public void centerToCoordinate(Coordinate imgPoint) {
        rasterComposite.centerToCoordinateAndRedraw(imgPoint.x, imgPoint.y);
    }

    @Override
    public void astrometryResultFiles(File wcsFile, File newFitsFile) {
        getShell().getDisplay().asyncExec(() -> {
            if (newFitsFile != null) {
                fitsStateColorOperation.setImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get("color-greend")));
                fitsStateColorOperation.setDisabledImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get("color-greend")));
                this.astrometryNewFitsFile = newFitsFile;
                setInfo("Astrometry plate solving successfuly finished");
                loadWcs(newFitsFile.getPath());
            } else {
                fitsStateColorOperation.setImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get("color-red")));
                fitsStateColorOperation.setDisabledImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get("color-red")));
                this.astrometryNewFitsFile = null;
                setError("Astrometry plate solving failed");
            }
        });
    }

    @Override
    public boolean mouseClickEvent(PositionData positionData, int button, int stateMask, boolean up) {
        if (button == 1 && !up) {
            refPointsTableComposite.setImagePoint(positionData, stateMask);
            rasterComposite.refresh();
        }
        return false;
    }

    @Override
    public void drawEvent(GC gc, DrawScreenInfo drawScreenInfo, PositionData positionData) {

    }
}
