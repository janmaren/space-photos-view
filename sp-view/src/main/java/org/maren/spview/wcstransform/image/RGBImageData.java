package org.maren.spview.wcstransform.image;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RGBPix;
import cz.jmare.image.util.RGBPixUtil;

public class RGBImageData extends RasImageData {

    @Override
    public RGBPix getRGB(int x, int y, RGBPix rgbPix) {
        int pixel = imageData.getPixel(x, y);
        return RGBPixUtil.rHighestToRGB(pixel, rgbPix);
    }
    
    @Override
    public RGBFloat getRGBFloat(int x, int y, RGBFloat rgbFloat, RGBPix rgbPix) {
        return RGBPixUtil.toRGBFloat(getRGB(x, y, rgbPix), rgbFloat);
    }
}
