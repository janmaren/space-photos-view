package org.maren.spview.wcstransform.saver;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.maren.statis.affine.AffineElements;

import cz.jmare.file.PathUtil;
import cz.jmare.math.geometry.entity.Point;

public class AffineFileSaver implements ReferencedSaver {
    private static final List<String> SUPPORTED_IMAGES_SUFFIXES = List.of(".pngw", ".bmpw", ".gifw", ".tifw", ".tiffw", ".jpgw", ".jpegw", ".wld"); 

    @Override
    public boolean isApplicable(String inputFilename, String outputFilename) {
        String[] nameAndSuffixOrEmpty = PathUtil.getNameAndSuffixOrEmpty(outputFilename);
        return SUPPORTED_IMAGES_SUFFIXES.contains(nameAndSuffixOrEmpty[1].toLowerCase());
    }
    
    @Override
    public void save(BufferedImage bufferedImage, String outputFilename, AffineElements elements, Point refPix) throws IOException {
        if (refPix.x != 0 || refPix.y != 0) {
            throw new IllegalArgumentException("Ref pixel must be 0, 0");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(elements.a).append("\n");
        sb.append(elements.d).append("\n");
        sb.append(elements.b).append("\n");
        sb.append(elements.e).append("\n");
        sb.append(elements.c).append("\n");
        sb.append(elements.f).append("\n");
        Files.writeString(Paths.get(outputFilename), sb.toString());
    }
}
