package org.maren.spview.wcstransform;

import org.maren.statis.entity.Coordinate;

public class ControlPointRow {
    private Coordinate imgPoint;

    private Coordinate sphPoint;

    private boolean enabled;

    public Coordinate getImgPoint() {
        return imgPoint;
    }

    public void setImgPoint(Coordinate imgPoint) {
        this.imgPoint = imgPoint;
    }

    public Coordinate getSphPoint() {
        return sphPoint;
    }

    public void setSphPoint(Coordinate sphPoint) {
        this.sphPoint = sphPoint;
    }

    public boolean isValid() {
        return sphPoint != null && imgPoint != null;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
