package org.maren.spview.wcstransform.util;

import cz.jmare.math.astro.RaDec;

public interface WcsLeftTopAbstraction {
    RaDec pixelToFK5(double xFromLeft, double yFromTop);
}
