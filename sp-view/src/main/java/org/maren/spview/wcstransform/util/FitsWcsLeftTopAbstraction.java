package org.maren.spview.wcstransform.util;

import cz.jmare.fits.header.WCS2d;
import cz.jmare.math.astro.RaDec;

public class FitsWcsLeftTopAbstraction implements WcsLeftTopAbstraction {
    private final WCS2d wcs2d;
    private final int height;

    public FitsWcsLeftTopAbstraction(WCS2d wcs2d, int height) {
        this.wcs2d = wcs2d;
        this.height = height;
    }

    @Override
    public RaDec pixelToFK5(double xFromLeft, double yFromTop) {
        return wcs2d.pixelToFK5(xFromLeft, height - yFromTop);
    }
}
