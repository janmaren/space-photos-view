package org.maren.spview.wcstransform.saver;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.maren.statis.affine.AffineElements;

import cz.jmare.math.geometry.entity.Point;

public interface ReferencedSaver {
    boolean isApplicable(String inputFilename, String filename);
    
    void save(BufferedImage bufferedImage, String outputFilename, AffineElements elements, Point refPix) throws IOException;
}
