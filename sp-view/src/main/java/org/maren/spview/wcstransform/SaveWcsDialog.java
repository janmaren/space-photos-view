package org.maren.spview.wcstransform;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import cz.jmare.file.PathUtil;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.SComposite;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SaveWcsDialog extends TitleAreaDialog {
    private String sourceFilename;
    private final boolean astrometryFits;
    private final boolean manualSolving;
    private Text outputFileText;

    private String resultOutputFilename;
    private Format resultFormat;
    private boolean resultOpen;
    private Button openCheck;

    private Combo outFormatCombo;

    private List<String> comboItems = new ArrayList<>();

    private List<Format> comboFormats = new ArrayList<>();

    private List<String> comboSuffixes = new ArrayList<>();

    public SaveWcsDialog(Shell parent, String sourceFilename, boolean astrometryFits, boolean manualSolving) {
        super(parent);
        this.sourceFilename = sourceFilename;
        this.astrometryFits = astrometryFits;
        this.manualSolving = manualSolving;
        ImageUtil.put("folder", "/image/folder.png");
        makeItems();
    }

    private void makeItems() {
        String[] strings = PathUtil.parseFullPathSuffix(this.sourceFilename);
        String suffix = strings[1].toLowerCase();

        if (astrometryFits) {
            comboItems.add("Fits by Astrometry");
            comboFormats.add(Format.FITS_BY_ASTROMETRY);
            comboSuffixes.add(".fits");
        }

        if (manualSolving) {
            comboItems.add("Fits by manual solving");
            comboFormats.add(suffix.equals(".fits") || suffix.equals(".fit") ? Format.FITS_AS_IS : Format.FITS_3_CHANNELS);
            comboSuffixes.add(".fits");
        }

        if (astrometryFits) {
            comboItems.add("Tif by Astrometry");
            comboFormats.add(Format.TIFF_BY_ASTROMETRY);
            comboSuffixes.add(".tif");
        }

        if (manualSolving) {
            comboItems.add("Tif by manual solving");
            comboFormats.add(Format.TIFF);
            comboSuffixes.add(".tif");
        }
    }

    protected Control createDialogArea(Composite parent) {
        Composite titleAreaComposite = (Composite) super.createDialogArea(parent);
        setTitle("Save");
        setMessage("Choose directory to which to save frames", IMessageProvider.INFORMATION);

        /* --- area --- */
        final Composite composite = new Composite(titleAreaComposite, SWT.NULL);

        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.horizontalSpacing = 40;
        layout.marginLeft = 30;
        composite.setLayout(layout);

        Create.label(composite, "Output Format:", (lab) -> {
            GridData layoutData = new GridData();
            layoutData.verticalAlignment = SWT.TOP;
            lab.setLayoutData(layoutData);
        });

        outFormatCombo = Create.combo(composite, SWT.READ_ONLY, c -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 250;
            c.setLayoutData(layoutData);
        });

        outFormatCombo.setItems(comboItems.toArray(new String[comboItems.size()]));
        outFormatCombo.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            if (outFormatCombo.getSelectionIndex() == -1) return;
            updateOutputFileText(comboSuffixes.get(outFormatCombo.getSelectionIndex()));
        }));
        
        Create.label(composite, "Output file:",
                l -> l.setToolTipText("Output file in accordance with format"));
        Composite filesComposite = SComposite.createGridCompositeIntoGridComposite(composite, 2, (gd) -> {
        });
        outputFileText = Create.text(filesComposite, "", t -> {
            Create.gridData(t, c -> {c.widthHint = 500;});
        });
        outputFileText.setText(sourceFilename);
        outputFileText.addModifyListener(e -> {updateOverwriteWarn();});
        Create.imageButton(filesComposite, ImageUtil.get("folder"), "Select file", () -> {
            FileDialog  fd = new FileDialog(getShell(), SWT.SAVE);
            fd.setFilterPath(outputFileText.getText());
            fd.setText("Output to File");
            fd.setFileName(outputFileText.getText());
            String path = fd.open();
            if (path != null) {
                outputFileText.setText(path);
            }
        });
        
        Create.label(composite, "Open:");
        openCheck = Create.button(composite, SWT.CHECK, "", () -> {});
        openCheck.setToolTipText("Open saved file in SPView");

        if (comboItems.size() > 0) {
            outFormatCombo.select(0);
            updateOutputFileText(comboSuffixes.get(0));
        }

        return composite;
    }

    private void updateOutputFileText(String suffix) {
        String text = outputFileText.getText();
        String[] nameSuff = PathUtil.parseFullPathSuffix(text);
        outputFileText.setText(nameSuff[0] + suffix);
    }

    private void updateOverwriteWarn() {
        File file = new File(sourceFilename);
        try {
            if (Files.isSameFile(file.toPath(), Paths.get(outputFileText.getText().trim()))) {
                setMessage("Overwriting source file", IMessageProvider.WARNING);
                return;
            }
        } catch (Exception e) {
        }

        try {
            if (new File(outputFileText.getText().trim()).isFile()) {
                setMessage("Overwriting destination file", IMessageProvider.WARNING);
                return;
            }
        } catch (Exception e) {
        }

        setMessage("");
    }

    @Override
    protected Point getInitialSize() {
        return new Point(850, 450);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            super.buttonPressed(buttonId);
            return;
        }
        if (buttonId == IDialogConstants.OK_ID) {
            String message;
            if ((message = valid()) != null) {
                setErrorMessage(message);
                return; 
            }
            
            resultOutputFilename = outputFileText.getText();

            resultFormat = comboFormats.get(outFormatCombo.getSelectionIndex());

            resultOpen = openCheck.getSelection(); 
        }
        super.buttonPressed(buttonId);
    }

    private String valid() {
        if (outFormatCombo.getSelectionIndex() == -1) {
            return "No format chosen";
        }
        if (outputFileText.getText().trim().equals("")) {
            return "No file selected";
        }
        
        return null;
    }

    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    public String getResultOutputFilename() {
        return resultOutputFilename;
    }

    public Format getResultFormat() {
        return resultFormat;
    }

    public boolean isResultOpen() {
        return resultOpen;
    }

    public static enum Format {
        FITS_AS_IS, FITS_1_CHANNEL, FITS_3_CHANNELS, WLD, TIFF, FITS_BY_ASTROMETRY, TIFF_BY_ASTROMETRY
    }
}
