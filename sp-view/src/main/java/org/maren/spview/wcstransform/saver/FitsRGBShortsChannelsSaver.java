package org.maren.spview.wcstransform.saver;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;

@Deprecated // short[][][] doesn't work with Fits framework, don't use this
public class FitsRGBShortsChannelsSaver extends FitsReferencedSaver {
    public short[][][] genImage(BufferedImage bufferedImage) {
        Raster raster = bufferedImage.getData();
        
        int height = raster.getHeight();
        int width = raster.getWidth();
        short[][][] data = new short[3][height][width];

        short[] pixels = ((DataBufferUShort) bufferedImage.getRaster().getDataBuffer()).getData();
        int numForPixel = pixels.length / (width * height);
        if (numForPixel != 3) {
            throw new IllegalStateException("Not RGB data");
        }
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int index = (width * i + j) * 3;
                short pixelR = pixels[index++];
                short pixelG = pixels[index++];
                short pixelB = pixels[index];
                data[0][height - i - 1][j] = pixelR;
                data[1][height - i - 1][j] = pixelG;
                data[2][height - i - 1][j] = pixelB;
            }
        }
        return data;
    }
}
