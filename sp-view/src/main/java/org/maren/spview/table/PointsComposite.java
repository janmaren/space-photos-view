package org.maren.spview.table;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.data.util.*;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.swt.color.ColorUtil;
import cz.jmare.graphfw.labeler.Labeler;
import cz.jmare.math.astro.AzAlt;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.astro.RahDec;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.status.StatusUtil;
import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.RGBSwtUtil;
import cz.jmare.swt.util.SMessage;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.sphericcanvas.ToolListener;
import org.maren.gis.type.Spheric;
import org.maren.spview.ConfigKeys;
import org.maren.spview.NearestSelectionObject;
import org.maren.spview.SPView;
import org.maren.spview.app.*;
import org.maren.spview.app.colsel.TagComparator;
import org.maren.spview.app.colsel.TagFunction;
import org.maren.spview.horizons.EphRequest;
import org.maren.spview.horizons.EphemerisRequester;
import org.maren.spview.horizons.HorizonsStrWriter;
import org.maren.spview.shape.*;
import org.maren.spview.tag.TagSupported;
import org.maren.spview.util.*;
import org.maren.swt.util.ConvTypesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.maren.spview.util.StyledUtil.*;

public class PointsComposite extends AbstractShapesComposite<PointShape> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PointsComposite.class);

    private static final String TAG_MAG_NAME = "AMAG";

    private MenuItem interpolateMenuItem;

    private MenuItem miBatchModify;
    
    private MenuItem miClone;

    private MenuItem miMoveLocation;
    
    private Function<PointShape, String> thirdColumnBuilder = SelectThirdColumnDialog.DEFAULT_TEXT_BUILDER;
    
    private String sortingTagName;
    
    private ColumnSorter<PointShape> thirdColumnSorter;
    private MenuItem miCopyToGroup;
    private Button showLabelRaDecButton;
    private Button printerButton;
    private MenuItem miNextEphs;

    protected LabelFunctionHolder labelFunctionHolder = new LabelFunctionHolder((ps) -> ps.getName());

    public PointsComposite(Composite parent, int style, SPView spView) {
        super(parent, style, spView);
    }

    @Override
    protected void doLabelDialog() {
        LabelSelDialog labelSelDialog = new LabelSelDialog(getShell(), labelFunctionHolder);
        labelSelDialog.open();
        labelFunctionHolder = labelSelDialog.getLabelFunctionHolder();
    }

    @Override
    public String[] shapeToColumns(PointShape pointItem) {
        String[] cols = new String[4];
        cols[0] = "";
        if (pointItem.getName() != null) {
            cols[1] = pointItem.getName();
        } else {
            cols[1] = "";
        }
        cols[2] = pointItem.getGroup().getName();
        cols[3] = thirdColumnBuilder.apply(pointItem);
        return cols;
    }

    protected void addColumns(Table table) {
        TableColumn nameColumn = new TableColumn(table, SWT.CENTER);
        nameColumn.setText("Name");
        nameColumn.setWidth(150);
        nameColumn.setResizable(true);
        nameColumn.setAlignment(SWT.LEFT);
        ColumnSorter<PointShape> nameColumnSorter = new ColumnSorter<>(table, this);
        nameColumnSorter.setComparator((PointShape ps1, PointShape ps2) -> ps1.getName() == null ? (ps2.getName() != null ? Integer.MAX_VALUE : 0)
            : (ps2.getName() != null ? ps1.getName().compareTo(ps2.getName()) : Integer.MIN_VALUE));
        nameColumn.addSelectionListener(nameColumnSorter);
        
        TableColumn groupColumn = new TableColumn(table, SWT.CENTER);
        groupColumn.setText("Group");
        groupColumn.setWidth(80);
        groupColumn.setResizable(true);
        groupColumn.setAlignment(SWT.LEFT);
        ColumnSorter<PointShape> groupColumnSorter = new ColumnSorter<>(table, this);
        groupColumnSorter.setComparator((ps1, ps2) -> {
            GroupAtt gr1 = ps1.getGroup();
            GroupAtt gr2 = ps2.getGroup();
            String g1 = null;
            String g2 = null;
            if (gr1 != null) g1 = gr1.getName(); 
            if (gr2 != null) g2 = gr2.getName();
            if ("".equals(g1)) g1 = null;
            if ("".equals(g2)) g2 = null;
            return g1 == null ? (g2 != null ? Integer.MAX_VALUE : 0)
                : (g2 != null ? g1.compareTo(g2) : Integer.MIN_VALUE);});
        groupColumn.addSelectionListener(groupColumnSorter);

        TableColumn thirdColumn = new TableColumn(table, SWT.CENTER);
        thirdColumn.setText(SelectThirdColumnDialog.DEFAULT_COLUMN_NAME);
        thirdColumn.setWidth(350);
        thirdColumn.setResizable(true);
        thirdColumn.setAlignment(SWT.LEFT);
        thirdColumnSorter = new ColumnSorter<>(table, this);
        thirdColumn.addSelectionListener(thirdColumnSorter);
        thirdColumn.setToolTipText("Variable column. Please click on the third column selection to choose different data");
        
        table.setSortColumn(nameColumn);
    }

    private void repopulateThirdColumn() {
        if (thirdColumnBuilder == null) {
            return; // unsortable column
        }
        int itemCount = table.getItemCount();
        for (int i = 0; i < itemCount; i++) {
            TableItem item = table.getItem(i);
            PointShape pointShape = (PointShape) item.getData();
            item.setText(3, thirdColumnBuilder.apply(pointShape));
        }
    }
    
    @Override
    protected PointShape editNewShape(GroupAtt pointSet, Map<String, GroupAtt> groups) {
        PointShape pointShape = new PointShape(pointSet, "", spView.getCenter());
        EditPointDialog editPointDialog = new EditPointDialog(getShell(), pointShape, groups);
        int result = editPointDialog.open();
        if (result != Window.OK) {
            if (result == EditPointDialog.DRAW_BUTTON) {
                ToolResultLisSetter<PointShape> newTool = newTool(editPointDialog.getResultPointShape(), pointShape);
                newTool.setDrawListener(this);
                spView.setToolListener((ToolListener) newTool, this);
            }
            return null;
        }
        pointShape = editPointDialog.getResultPointShape();
        return pointShape;
    }
    
    @Override
    protected boolean editUpdatingShape(PointShape pointItem, Map<String, GroupAtt> groups) {
        EditPointDialog editPointDialog = new EditPointDialog(getShell(), pointItem, groups);
        int result = editPointDialog.open();
        if (result == EditCircleDialog.DRAW_BUTTON) {
            ToolResultLisSetter<PointShape> newTool = newTool(editPointDialog.getResultPointShape(), pointItem);
            newTool.setDrawListener(this);
            spView.setToolListener((ToolListener) newTool, this);
            return false;
        } else if (result == Window.OK) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void export(List<PointShape> items) {
        Set<String> groupNames = items.stream().map(it -> it.getGroup().getName()).collect(Collectors.toSet());
        String suggestName = groupNames.stream().collect(Collectors.joining("_"));
        FileDialog fileDialog = new FileDialog(getShell(), SWT.SAVE);
        fileDialog.setFilterExtensions(new String[] {"*.csv", "*.txt"});
        fileDialog.setFileName(suggestName);
        if (TextFilesLoader.lastFile != null) {
            fileDialog.setFilterPath(TextFilesLoader.lastFile.getParent());
        }
        fileDialog.setOverwrite(true);
        String filename = fileDialog.open();
        if (filename != null) {
            if (filename.endsWith(".csv")) {
                SMessage.withErrorStatus(() -> {
                    CsvShapesUtil.exportPointsToCsv(new File(filename), items);
                });
            } else if (filename.endsWith(".txt")) {
                String str = HorizonsStrWriter.exportGroups((List<PointShape>) items);
                SMessage.withErrorStatus(() -> {
                    Files.writeString(Paths.get(filename), str);
                    StatusUtil.infoMessage("Saved to " + filename);
                });
            }
        }
    }

    @Override
    protected void imports(boolean forceRebuild) {
        FileDialog fd = new FileDialog(getShell(), SWT.OPEN | SWT.MULTI);
        fd.setText("Image Files to Open");
        fd.setFilterExtensions(new String[] {"*.csv;*.tsv;*.txt", "*.csv", "*.tsv", "*.txt"});
        if (fd.open() != null) {
            String[] fileNames = fd.getFileNames();
            TextFilesLoader textFilesLoader = new TextFilesLoader(getShell(), List.of(this), b -> {
                getShell().getDisplay().asyncExec(() -> spView.refresh());
            });
            textFilesLoader.setForceRebuild(forceRebuild);
            List<File> errs = textFilesLoader.loadFiles(Arrays.stream(fileNames).map(fn -> new File(fd.getFilterPath(), fn)).collect(Collectors.toList()), true, false);
            if (errs.isEmpty()) {
                String warnings = textFilesLoader.getWarnings();
                if (warnings != null) {
                    setWarn(warnings);
                } else {
                    setInfo("Text file" + (fileNames.length > 1 ? "s" : "") + " loaded");
                }
            } else {
                setError("Not all files loaded, like " + errs.get(0).getName());
            }
        }
    }

    @Override
    public void drawShapes(GC gc, CoordState coordState, boolean withLabel, Labeler labeler) {
        List<PointShape> points = getEnabledShapes();
        Color origColor = gc.getForeground();
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        Font origFont = gc.getFont();
        gc.setFont(fontRegistry.get(coordState.getZoom() < 250 ? "smallest-font" : "smaller-font"));
        Function<PointShape, String> labelOutputer = labelFunctionHolder.getLabelFunction();
        if (labelFunctionHolder.isContainsTime()) {
            gc.drawText(DateTimeUtil.getOffsetHoursString(), coordState.getScreenWidth() - (coordState.getZoom() < 250 ? 24 : 40), 0);
        }
        Set<String> activeGroups = new HashSet<>();
        for (PointShape pointShape : points) {
            gc.setForeground(ColorUtil.getColor(pointShape.getGroup().getColor()));
            String name = labelOutputer.apply(pointShape);
            if (name != null && name.length() > 18) {
                name = name.substring(0, 16) + "...";
            }
            String raDecStr = null;
            if (showLabelRaDecButton.getSelection()) {
                RaDec raDec = pointShape.getRaDec();
                double raHours = ACooUtil.degreesToHours(raDec.ra);
                raDecStr = HourUtil.formatHoursMinutes(raHours, ":") + " " + GonUtil.formatDegreesMinutes(raDec.dec, true, ":");
            }
            Spheric spheric = ConvTypesUtil.toSpheric(pointShape.getRaDec());
            if (pointShape.isFavourite()) {
                DrawUtil.drawFavourite(gc, spheric, coordState);
            }
            if (pointShape.isSeen()) {
                DrawUtil.drawSeen(gc, spheric, coordState);
            }
            DrawUtil.drawPoint(gc, spheric, coordState, withLabel ? name : null, raDecStr, labeler, false);

            activeGroups.add(pointShape.getGroup().getName());
        }

        List<String> activeGroupsSorted = new ArrayList<>(activeGroups);
        Collections.sort(activeGroupsSorted);

        ConfigStore configStore = AppConfig.getInstance().getConfigStore();
        if (configStore.getBoolValue(ConfigKeys.KEY_SHOW_LEGEND, true)) {
            int y = 1;
            gc.setFont(fontRegistry.get(coordState.getZoom() < 250 ? "smallest-font" : "smaller-font"));
            FontMetrics fontMetrics = gc.getFontMetrics();
            int height = fontMetrics.getHeight();
            for (String name : activeGroupsSorted) {
                GroupAtt value = groups.get(name);
                drawLegendItem(gc, y, value.getColor(), "".equals(name) ? "<default group>" : name);
                y += height;
            }
        }

        gc.setForeground(origColor);
        gc.setFont(origFont);
    }

    private static void drawLegendItem(GC gc, int y, String colorName, String label) {
        RGB rgb = RGBSwtUtil.createRGB(colorName);
        Color color = ColorUtil.getColor(rgb);
        gc.setForeground(color);
        gc.drawString(label, 4, y, true);
    }

    @Override
    protected ToolResultLisSetter<PointShape> newTool(PointShape shape, PointShape origShape) {
        return new PointTool(shape, origShape);
    }

    @Override
    protected PointShape newEmptyShape(GroupAtt group) {
        PointShape pointShape = new PointShape(group, null, null);
        return pointShape;
    }

    @Override
    public List<NearestSelectionObject> findShapes(Spheric polar, double maxDistanceDegs) {
        List<NearestSelectionObject> nearest = new ArrayList<>();

        RaDec normalizedPolarDeg = ConvTypesUtil.toNormalizedRaDec(polar);
        List<PointShape> points = getEnabledShapes();
        for (PointShape pointShape : points) {
            double angularDistance = GonUtil.angularDistanceDeg(normalizedPolarDeg, pointShape.getRaDec());
            if (angularDistance < maxDistanceDegs) {
                String name = pointShape.getName();
                if (name == null) {
                    name = ACooUtil.formatRaDecProfi(pointShape.getRaDec());
                }
                if (!pointShape.getGroup().getName().isEmpty()) {
                    name += " (" + pointShape.getGroup().getName() + ")";
                }
                if (pointShape.getJd() != null) {
                    name += ", " + JulianUtil.formatJulianRoundSecondsSkipFrac(pointShape.getJd(), 2);
                } else if (pointShape.getDescription() != null) {
                    name += ", " + pointShape.getDescription();
                }
                nearest.add(new NearestSelectionObject(NearestSelectionObject.ObjectType.POINT, pointShape, name, angularDistance));
            }
        }

        return nearest;
    }

    @Override
    protected void addCustomPopMenuItems(Table table, Menu menuTable) {
        miBatchModify = new MenuItem(menuTable, SWT.NONE);
        miBatchModify.setText("Batch Modify for Selected");
        Create.selectionListener(miBatchModify, (e) -> {
            List<PointShape> selections = new ArrayList<>();
            int[] selectionIndices = table.getSelectionIndices();
            for (int i = 0; i < selectionIndices.length; i++) {
                int selectionIndex = selectionIndices[i];
                TableItem item = table.getItem(selectionIndex);
                PointShape pointShape = (PointShape) item.getData();
                selections.add(pointShape);
            }
            BatchPointsModifyDialog batchPointsModifyDialog = new BatchPointsModifyDialog(getShell(), selections, groups);
            if (batchPointsModifyDialog.open() == Window.OK) {
                TableItem[] tableItems = table.getItems();
                for (int i = 0; i < selectionIndices.length; i++) {
                    int selectionIndex = selectionIndices[i];
                    PointShape pointShape = selections.get(i);
                    setDataToTableItem(pointShape, tableItems[selectionIndex]);
                }
                spView.refresh();
                setInfo("Batch modify finished");
            }
        });
        
        miClone = new MenuItem(menuTable, SWT.NONE);
        miClone.setText("Duplicate");
        Create.selectionListener(miClone, (e) -> {
            int[] selectionIndices = table.getSelectionIndices();
            if (selectionIndices.length != 1) {
                return;
            }
            TableItem tableItem = table.getItem(selectionIndices[0]);
            PointShape pointShape = (PointShape) tableItem.getData();
            PointShape clonedShape = pointShape.clone();
            clonedShape.setGroup(getCurrentGroupOrCreateDefault());
            EditPointDialog editPointDialog = new EditPointDialog(getShell(), clonedShape, groups, "Cloning Point");
            int result = editPointDialog.open();
            if (result == EditCircleDialog.DRAW_BUTTON) {
                ToolResultLisSetter<PointShape> newTool = newTool(editPointDialog.getResultPointShape(), clonedShape);
                newTool.setDrawListener(this);
                spView.setToolListener((ToolListener) newTool, this);
            } else if (result == Window.OK) {
                PointShape newPointShape = editPointDialog.getResultPointShape();
                addShapes(List.of(newPointShape));
                showItem(newPointShape);
                spView.refresh();
            }
        });

        miCopyToGroup = new MenuItem(menuTable, SWT.NONE);
        miCopyToGroup.setText("Copy to Group");
        miCopyToGroup.setToolTipText("Hold CTRL not to show dialog and use current group");
        Create.selectionListener(miCopyToGroup, (e) -> {
            int[] selectionIndices = table.getSelectionIndices();

            GroupAtt group = getCurrentGroupOrCreateDefault();
            if ((e.stateMask & SWT.CTRL) == 0) {
                CopyToGroupDialog copyToGroupDialog = new CopyToGroupDialog(getShell(), groups, group, selectionIndices.length);
                if (copyToGroupDialog.open() == Window.OK) {
                    group = copyToGroupDialog.getResultGroup();
                } else {
                    return;
                }
            }

            List<PointShape> newShapes = new ArrayList<>();
            TableItem[] tableItems = table.getItems();
            Set<PointShape> all = new HashSet<>();
            int ignored = 0;
            for (int i = 0; i < tableItems.length; i++) {
                PointShape item = (PointShape) table.getItem(i).getData();
                all.add(item);
            }
            for (int i = 0; i < selectionIndices.length; i++) {
                int selectionIndex = selectionIndices[i];
                TableItem item = table.getItem(selectionIndex);
                PointShape pointShape = (PointShape) item.getData();
                PointShape cloned = pointShape.clone();
                cloned.setGroup(group);
                if (!all.contains(cloned)) {
                    newShapes.add(cloned);
                } else {
                    ignored++;
                }
            }
            if (newShapes.isEmpty()) {
                setInfo("Nothing copied (maybe already exists)");
                return;
            }
            addShapes(newShapes);
            if (newShapes.size() > 0) {
                showItem(newShapes.get(0));
            }
            spView.refresh();
            setInfo("Copied " + (selectionIndices.length - ignored) + " points" + (ignored > 0 ? " but " + ignored + " points ignored because already exist" : ""));
        });

        miNextEphs = new MenuItem(menuTable, SWT.NONE);
        miNextEphs.setText("Download Next Ephemeris");
        miNextEphs.setToolTipText("Add next ephemeris for this object");
        Create.selectionListener(miNextEphs, (e) -> {
            int[] selectionIndices = table.getSelectionIndices();
            Double greatestDate = null;
            List<String> names = new ArrayList<>();
            int noNasaHorizNumber = 0;
            for (int i = 0; i < selectionIndices.length; i++) {
                int selectionIndex = selectionIndices[i];
                TableItem item = table.getItem(selectionIndex);
                PointShape pointShape = (PointShape) item.getData();
                String nasHorizId = getNasHorizId(pointShape);
                if (nasHorizId != null && !names.contains(nasHorizId)) {
                    names.add(nasHorizId + " #" + pointShape.getGroup().getName());
                }
                if (nasHorizId == null) {
                    noNasaHorizNumber++;
                }
                if (pointShape.getJd() != null) {
                    if (greatestDate == null) {
                        greatestDate = pointShape.getJd();
                    } else if (greatestDate < pointShape.getJd()) {
                        greatestDate = pointShape.getJd();
                    }
                }
            }
            CustomEphDialog quickEphDialog = new CustomEphDialog(getShell(), names, greatestDate);
            if (noNasaHorizNumber > 0) {
                quickEphDialog.setCustomMessage(selectionIndices.length == 1 ? "Unable to detect NASA Horizons ID - no ephemeris point" : "Unable to detect NASA Horizons IDs - no ephemeris points");
            }
            if (quickEphDialog.open() == Window.OK) {
                EphRequest resultEphRequest = quickEphDialog.getResultEphRequest();
                EphemerisRequester.addToQueue(resultEphRequest, this);
                if (quickEphDialog.isResultShowMagCheck()) {
                    setThirdColumnTag(new TagFunction(TAG_MAG_NAME), new TagComparator(TAG_MAG_NAME), TAG_MAG_NAME);
                }
            }
        });

        miMoveLocation = new MenuItem(menuTable, SWT.NONE);
        miMoveLocation.setText("Move Location");
        Create.selectionListener(miMoveLocation, (e) -> {
            int[] selectionIndices = table.getSelectionIndices();
            if (selectionIndices.length != 1) {
                return;
            }
            TableItem tableItem = table.getItem(selectionIndices[0]);
            PointShape pointShape = (PointShape) tableItem.getData();
            PointShape clonedShape = pointShape.clone();

            ToolResultLisSetter<PointShape> newTool = newTool(clonedShape, pointShape);
            newTool.setDrawListener(this);
            spView.setToolListener((ToolListener) newTool, this);
        });
    }

    private String getNasHorizId(PointShape pointShape) {
        Object nameByTag = pointShape.getTagValue(TagSupported.NASA_HORIZ_ID.name());
        if (nameByTag != null) {
            return nameByTag.toString();
        }
        String name = pointShape.getGroup().getName();
        if (name.length() > 0) {
            int index = name.indexOf("(");
            if (index == -1) {
                return null;
            }
            int indexTo = name.indexOf(")");
            if (indexTo < index) {
                return null;
            }
            return name.substring(index + 1, indexTo);
        }
        return null;
    }

    @Override
    protected void resolveCustomPopMenuEnabled(MenuItem menuItem) {
        if (menuItem == interpolateMenuItem) {
            menuItem.setEnabled(false);
        } else if (menuItem == miBatchModify) {
            menuItem.setEnabled(table.getSelectionIndices().length > 0);
        } else if (menuItem == miClone) {
            menuItem.setEnabled(table.getSelectionIndices().length == 1);
        } else if (menuItem == miCopyToGroup) {
            menuItem.setEnabled(table.getSelectionIndices().length > 0);
        } else if (menuItem == miNextEphs) {
            menuItem.setEnabled(table.getSelectionIndices().length > 0);
        } else if (menuItem == miMoveLocation) {
            menuItem.setEnabled(table.getSelectionIndices().length == 1);
        }
    }

    @Override
    protected void addCustomToolbarButtons(Composite toolsComposite) {
        Shell shell = getShell();

        ImageUtil.put("label-radec", "/image/label-radec.png");
        showLabelRaDecButton = Create.button(toolsComposite, SWT.TOGGLE, button -> {
            button.setToolTipText("Show also RA and DEC as label");
            button.setImage(ImageUtil.get("label-radec"));
        });
        showLabelRaDecButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            spView.refresh();
        }));

        ImageUtil.put("selColumn", "/image/sel_column.png");
        Create.button(toolsComposite, button -> {
            button.setToolTipText("Third Column Selection");
            button.setImage(ImageUtil.get("selColumn"));
            button.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
                if (thirdColumnSorter == null) {
                    return;
                }
                TreeSet<String> tagNames = new TreeSet<>();
                List<PointShape> allItems = getAllItems();
                for (PointShape pointShape: allItems) {
                    tagNames.addAll(pointShape.getTagNames());
                }
                List<String> tagNamesList = tagNames.stream().collect(Collectors.toList());
                Collections.sort(tagNamesList);
                SelectThirdColumnDialog dialog = new SelectThirdColumnDialog(shell, thirdColumnBuilder, sortingTagName, tagNamesList);
                if (dialog.open() == Window.OK) {
                    setThirdColumn(dialog.getResultTextBuilder(), dialog.getResultComparator(),
                            dialog.getResultTagName(), dialog.getResultColumnName());
                }
            }));
        });
        
        ImageUtil.put("box", "/image/select-check.png");
        Create.button(toolsComposite, button -> {
            button.setToolTipText("Enabling and Selecting");
            button.setImage(ImageUtil.get("box"));
            button.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
                List<PointShape> allItems = getAllItems();
                Map<String, Set<String>> tagValues = new HashMap<>();
                for (PointShape shape : allItems) {
                    Set<String> tagNames = shape.getTagNames();
                    for (String tagName : tagNames) {
                        Set<String> values = tagValues.get(tagName);
                        if (values == null) {
                            values = new TreeSet<>();
                            tagValues.put(tagName, values);
                        }
                        values.add(shape.getTagValue(tagName).toString());
                    }
                }
                SwitchItemsDialog dialog = new SwitchItemsDialog(shell, groups, tagValues);
                if (dialog.open() == Window.OK) {
                    GroupAtt onlyGroup = dialog.getResultOnlyGroup();
                    boolean groupBesides = dialog.isResultGroupBeside();
                    boolean onlyEnabled = dialog.isResultOnlyEnabled();
                    boolean onlySelected = dialog.isResultOnlySelected();
                    boolean onlyFavourites = dialog.isResultOnlyFavourites();
                    boolean onlySeen = dialog.isResultOnlySeen();
                    boolean select = dialog.isResultSelect();
                    boolean enable = dialog.isResultEnable();
                    boolean disable = dialog.isResultDisable();
                    boolean remove = dialog.isResultRemove();
                    boolean markFavourite = dialog.isResultMarkFavourite();
                    boolean unmarkFavourite = dialog.isResultUnmarkFavourite();
                    boolean markSeen = dialog.isResultMarkSeen();
                    boolean unmarkSeen = dialog.isResultUnmarkSeen();
                    String resultUpdateGroup = dialog.getResultUpdateGroup();
                    GroupAtt updateGroup = null;
                    if (resultUpdateGroup != null) {
                        updateGroup = GroupUtil.getOrCreateGroup(groups, resultUpdateGroup);
                    }
                    String resultTagName = dialog.getResultTagName();
                    String resultTagValue = dialog.getResultTagValue();
                    boolean besidesTag = dialog.isBesidesTag();

                    int[] selectionIndices = table.getSelectionIndices();
                    Set<Integer> selList = Arrays.stream(selectionIndices).boxed().collect(Collectors.toSet());
                    List<Integer> removeIndexes = new ArrayList<>();
                    for (int i = 0; i < table.getItemCount(); i++) {
                        TableItem item = table.getItem(i);
                        PointShape shape = (PointShape) item.getData();
                        if (groupBesides) {
                            if (onlyGroup != null && shape.getGroup() == onlyGroup) {
                                continue;
                            }
                        } else {
                            if (onlyGroup != null && shape.getGroup() != onlyGroup) {
                                continue;
                            }
                        }
                        if (onlyEnabled && !item.getChecked()) {
                            continue;
                        }
                        if (onlySelected && !selList.contains(i)) {
                            continue;
                        }
                        if (onlyFavourites && !shape.isFavourite()) {
                            continue;
                        }
                        if (onlySeen && !shape.isSeen()) {
                            continue;
                        }
                        if (resultTagName != null) {
                            Object tagValue = shape.getTagValue(resultTagName);
                            if (tagValue == null) {
                                continue;
                            }
                            if (besidesTag ^ !tagValue.toString().equals(resultTagValue)) {
                                continue;
                            }
                        }

                        if (remove) {
                            removeIndexes.add(i);
                        }
                        if (enable) {
                            item.setChecked(true);
                        }
                        if (disable) {
                            item.setChecked(false);
                        }
                        if (select) {
                            table.select(i);
                        }
                        boolean setData = false;
                        if (markFavourite) {
                            shape.setFavourite(true);
                            setData = true;
                        }
                        if (unmarkFavourite) {
                            shape.setFavourite(false);
                            setData = true;
                        }
                        if (markSeen) {
                            shape.setSeen(true);
                            setData = true;
                        }
                        if (unmarkSeen) {
                            shape.setSeen(false);
                            setData = true;
                        }
                        if (updateGroup != null) {
                            shape.setGroup(updateGroup);
                            setData = true;
                        }
                        if (setData) {
                            setDataToTableItem(shape, item);
                        }
                    }
                    if (remove) {
                        removeFromTable(removeIndexes.stream()
                                .mapToInt(Integer::intValue)
                                .toArray());
                    }
                    if (select) {
                        resolvePopupEnablement();
                    }
                    if (enable || disable || markFavourite || unmarkFavourite || markSeen || unmarkSeen || updateGroup != null || resultTagName != null) {
                        spView.refresh();
                    }
                    setInfo("Enabling and selecting finished");
                }
            }));
        });
        
        ImageUtil.put("merge", "/image/merge.png");
        Create.button(toolsComposite, button -> {
            button.setToolTipText("Merge");
            button.setImage(ImageUtil.get("merge"));
            button.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
                MergePointsDialog mergePointsDialog = new MergePointsDialog(getShell(), groups);
                if (mergePointsDialog.open() == Window.OK) {
                    GroupAtt dstGroupAtt = mergePointsDialog.getResultDestinationGroup();
                    GroupAtt mrgGroupAtt = mergePointsDialog.getResultSourceGroup();
                    double tolerance = mergePointsDialog.getResultTolerance() / 3600.0; // in degrees
                    
                    List<PointShape> dstShapes = new ArrayList<>();
                    List<PointShape> mrgShapes = new ArrayList<>();
                    for (int i = 0; i < table.getItemCount(); i++) {
                        TableItem item = table.getItem(i);
                        PointShape shape = (PointShape) item.getData();
                        if (shape.getGroup() == dstGroupAtt) {
                            dstShapes.add(shape);
                        } else if (shape.getGroup() == mrgGroupAtt) {
                            mrgShapes.add(shape);
                        }
                    }
                    PointShapeMerger mergeOneShape = new PointShapeMerger();
                    long start = System.currentTimeMillis();
                    MergeUtil.mergeOptimized(dstShapes, mrgShapes, mergeOneShape, tolerance);
                    long end = System.currentTimeMillis();
                    int count = mergeOneShape.getCounter();

                    updateTableItems();

                    setInfo("Merged with count: " + count + ", duration: " + ((end - start) / 1000) + "s");
                }
            }));
        });

        ImageUtil.put("supernova", "/image/krabi2_16c.png");
        Create.button(toolsComposite, button -> {
            button.setToolTipText("Import supernovas from TNS");
            button.setImage(ImageUtil.get("supernova"));
            button.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
                SupernovaeDialog supernovaeDialog = new SupernovaeDialog(shell);
                if (supernovaeDialog.open() != Window.OK) {
                    return;
                }
                GroupAtt destinationGroup = GroupUtil.getOrCreateDefaultGroup(groups);
                ExecutorService executor = Executors.newSingleThreadExecutor();
                executor.submit(() -> {
                    String userHome = System.getProperty("user.home");
                    Path homePath = Paths.get(userHome);
                    Path downDir = homePath.resolve("Downloads");
                    if (!Files.isDirectory(downDir)) {
                        downDir = homePath;
                    }
                    String csvText;
                    try {
                        Map<String, String> data = new HashMap<>();
                        if (supernovaeDialog.getResultString() != null) {
                            data.put("name", supernovaeDialog.getResultString());
                        }
                        if (supernovaeDialog.getResultMaxMagnitude() != null) {
                            data.put("discMagMax", supernovaeDialog.getResultMaxMagnitude().toString());
                        }
                        if (supernovaeDialog.getResultPeriod() != null) {
                            data.put("period", supernovaeDialog.getResultPeriod().toString());
                            data.put("periodUnits", supernovaeDialog.getResultPeriodUnit());
                        }
                        Path tnsFile = RetrieveDataUtil.getTnsFile(downDir, data, false);
                        csvText = Files.readString(tnsFile, StandardCharsets.ISO_8859_1);
                        LOGGER.debug("TNS file has length " + (csvText == null ? "null" : csvText.length()));
                    } catch (Exception ex) {
                        shell.getDisplay().asyncExec(() -> {
                            setError(ExceptionMessage.getCombinedMessage("Unable to download supernovae", ex), ex);
                        });
                        return;
                    }

                    String[] directives = {"2", "3, \" \", 4", "datespacetime 21", "5", "\"" + TAG_MAG_NAME + ":\", 19"};
                    List<String[]> lines = CsvUtil.importCsv(csvText, ',');
                    List<String[]> nativeCsv = CsvShapesUtil.rebuildCsvToNative(lines, directives, true);
                    List<PointShape> pointShapes = CsvShapesUtil.importNativePointsCsv(nativeCsv);
                    LOGGER.debug("loaded " + pointShapes.size() + " shapes");
                    shell.getDisplay().syncExec(() -> {
                        pointShapes.forEach(p -> p.setGroup(destinationGroup));
                        addShapes(pointShapes);
                        spView.refresh();
                        if (pointShapes.isEmpty()) {
                            setWarn("Loading of supernovae finished" + (pointShapes.isEmpty() ? " but nothing downloaded" : ""));
                        } else if (pointShapes.size() == 50) {
                            setWarn("Loading of supernovae finished but reached the maximum limit 50");
                        } else {
                            setInfo("Loading of supernovae finished");
                        }
                        if (supernovaeDialog.isResultShowMagCheck()) {
                            setThirdColumnTag(new TagFunction(TAG_MAG_NAME), new TagComparator(TAG_MAG_NAME), TAG_MAG_NAME);
                        }
                    });
                });
                setInfo("Loading supernovae...");
            }));
        });

        ImageUtil.put("comet", "/image/kometa.png");
        Create.button(toolsComposite, button -> {
            button.setToolTipText("Import Comets");
            button.setImage(ImageUtil.get("comet"));
            button.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
                CometsEphDialog cometsEphDialog = new CometsEphDialog(getShell());
                if (cometsEphDialog.open() == Window.OK) {
                    EphRequest resultEphRequest = cometsEphDialog.getResultEphRequest();
                    EphemerisRequester.addToQueue(resultEphRequest, this);
                    if (cometsEphDialog.isResultShowMagCheck()) {
                        setThirdColumnTag(new TagFunction(TAG_MAG_NAME), new TagComparator(TAG_MAG_NAME), TAG_MAG_NAME);
                    }
                }
            }));
        });

        ImageUtil.put("trajectory", "/image/ephs.png");
        Create.button(toolsComposite, button -> {
            button.setToolTipText("Import Ephemeris");
            button.setImage(ImageUtil.get("trajectory"));
            button.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
                CustomEphDialog quickEphDialog = new CustomEphDialog(getShell());
                if (quickEphDialog.open() == Window.OK) {
                    EphRequest resultEphRequest = quickEphDialog.getResultEphRequest();
                    EphemerisRequester.addToQueue(resultEphRequest, this);
                    if (quickEphDialog.isResultShowMagCheck()) {
                        setThirdColumnTag(new TagFunction(TAG_MAG_NAME), new TagComparator(TAG_MAG_NAME), TAG_MAG_NAME);
                    }
                }
            }));
        });

        ImageUtil.put("print", "/image/printer.png");
        printerButton = Create.button(toolsComposite, SWT.TOGGLE, button -> {
            button.setToolTipText("Print");
            button.setImage(ImageUtil.get("print"));
        });
        printerButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
            PrintPointsDialog printPointsDialog = new PrintPointsDialog(shell, getEnabledShapes());
            printPointsDialog.open();
        }));

        ImageUtil.put("jupiter", "/image/jupiter.png");
        ImageUtil.put("dwarf-planet", "/image/dwarf-planet.png");
        ImageUtil.put("asteroid-brown", "/image/asteroid.png");
    }

    private void setThirdColumnTag(Function<PointShape, String> thirdColumnBuilder, Comparator<PointShape> comparator, String tagName) {
        setThirdColumn(thirdColumnBuilder, comparator, tagName, "<Tag " + tagName + ">");
    }

    private void setThirdColumn(Function<PointShape, String> thirdColumnBuilder, Comparator<PointShape> comparator, String tagName, String columnName) {
        this.thirdColumnBuilder = thirdColumnBuilder;
        thirdColumnSorter.setComparator(comparator);
        sortingTagName = tagName;
        table.getColumn(3).setText(columnName);
        repopulateThirdColumn();
    }

    protected void fillInfo(PointShape shape, StyledText infoStyledText) {
        infoStyledText.setText("");
        String name = null;
        if (shape.getName() != null) {
            name = shape.getName().trim();
            StyledUtil.addBoldText(infoStyledText, name);
            infoStyledText.append("\n");
        }
        addText(infoStyledText, "RA, DEC: ");
        StyledUtil.addInvText(infoStyledText, ACooUtil.formatCoordsProfi(shape.getRaDec()));
        if (spView.getGeoloc() != null && spView.getJulianDate() != null) {
            RahDec rahDec = shape.getRaDec().toNormalizedRahDec();
            AzAlt azAlt = AstroConversionUtil.toAzAlt(rahDec, spView.getGeoloc(), spView.getJulianDate());
            addLF(infoStyledText);
            addText(infoStyledText, "AZ, ALT: ");
            addInvText(infoStyledText, " " + ACooUtil.formatAzAlt(azAlt, false));
        }

        infoStyledText.append("\n");
        StyledUtil.addBoldText(infoStyledText, "Group: ");
        infoStyledText.append(shape.getGroup().getName());
        if (shape.getJd() != null) {
            infoStyledText.append(", ");
            StyledUtil.addBoldText(infoStyledText, "Date: ");
            infoStyledText.append(JulianUtil.formatJulianRoundSeconds(shape.getJd(), 4, DateTimeUtil.getOffsetHours()));
        }
        if (shape.getDescription() != null) {
            infoStyledText.append(", ");
            StyledUtil.addBoldText(infoStyledText, "Description: ");
            infoStyledText.append(shape.getDescription());
        }
        String tagsString = shape.getTagsString();
        if (!tagsString.isBlank()) {
            infoStyledText.append(", ");
            StyledUtil.addBoldText(infoStyledText, "Tags: ");
            infoStyledText.append(tagsString);
        }
    }

    @Override
    public boolean same(PointShape t1, PointShape that) {
        RaDec t1RaDec = t1.getRaDec();
        RaDec thatRaDec = that.getRaDec();
        return t1.getGroup().equals(that.getGroup()) && Objects.equals(t1.getName(), that.getName()) && Objects.equals(t1.getJd(), that.getJd()) && t1RaDec.ra == thatRaDec.ra && t1RaDec.dec == thatRaDec.dec && Objects.equals(t1.getDescription(), that.getDescription());
    }

    public Map<String, GroupAtt> getGroups() {
        return groups;
    }

    public void setPoints(Map<String, RaDec> points, String groupName) {
        TableItem[] items = table.getItems();
        Set<Integer> selList = new HashSet<>();
        for (int i = 0; i < items.length; i++) {
            TableItem itemI = items[i];
            PointShape pointShape = (PointShape) itemI.getData();
            if (pointShape.getGroup().getName().equals(groupName)) {
                selList.add(i);
            }
        }
        removeFromTable(selList.stream().mapToInt(i -> i).toArray());

        if (!points.isEmpty()) {
            GroupAtt group = GroupUtil.getOrCreateGroup(groups, groupName);
            List<PointShape> pointShapes = new ArrayList<>();
            for (Map.Entry<String, RaDec> entry : points.entrySet()) {
                PointShape pointShape = new PointShape(group, entry.getKey(), entry.getValue());
                pointShapes.add(pointShape);
            }

            addShapes(pointShapes);
        } else {
            groups.remove(groupName);
        }
        spView.refresh();
    }
}
