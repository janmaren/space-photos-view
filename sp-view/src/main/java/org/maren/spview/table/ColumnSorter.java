package org.maren.spview.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.maren.spview.shape.Shape;

public class ColumnSorter<T extends Shape> implements SelectionListener {
    private Comparator<T> comparator = null;
    private Table table;
    private AbstractShapesComposite<T> shapesComposite;
    
    public ColumnSorter(Table table, AbstractShapesComposite<T> pointsComposite) {
        super();
        this.table = table;
        this.shapesComposite = pointsComposite;
    }

    @Override
    public void widgetSelected(SelectionEvent e) {
        TableColumn column = (TableColumn) e.widget;
        if (comparator == null) {
            shapesComposite.setWarn("Sorting of this column is not allowed for combination of multiple data - use column setup to choose simple data");
            table.setSortDirection(SWT.NONE);
            return;
        }
        TableColumn sortColumn = table.getSortColumn();
        int sortDirection = table.getSortDirection();
        if (sortColumn != column) {
            sortDirection  = SWT.NONE;
        }
        if (sortDirection == SWT.NONE || sortDirection == SWT.UP) {
            sortDirection = SWT.DOWN;
        } else {
            sortDirection = SWT.UP;
        }

        table.setSortDirection(sortDirection);

        sortItems();

        table.setSortColumn(column);

        shapesComposite.setCurrentColumnSorter(this);
    }

    @SuppressWarnings("unchecked")
	public void sortItems() {
        int sortDirection = table.getSortDirection();
        TableItem[] items = table.getItems();

        List<T> shapes = new ArrayList<>(items.length);
        int[] selectionIndices = table.getSelectionIndices();
        List<Integer> selList = new ArrayList<>();
        for (int i : selectionIndices) {
            selList.add(i);
        }
        for (int i = 0; i < items.length; i++) {
            TableItem itemI = items[i];
            T shape = (T) itemI.getData();
            shapes.add(shape);
            shape.selected = selList.contains(i);
            shape.enabled = itemI.getChecked();
        }

        Comparator<T> useComp = comparator;
        if (sortDirection == SWT.UP) {
            useComp = comparator.reversed();
        }
        Collections.sort(shapes, useComp);

        List<Integer> selections = new ArrayList<>();
        for (int i = 0; i < items.length; i++) {
            T shape = shapes.get(i);
            shapesComposite.setDataToTableItem(shape, items[i]);
            if (shape.selected) {
                selections.add(i);
            }
        }
        table.setSelection(selections.stream()
                .mapToInt(Integer::intValue)
                .toArray());
    }

    @Override
    public void widgetDefaultSelected(SelectionEvent e) {
    }

    public void setComparator(Comparator<T> comparator) {
        this.comparator = comparator;
    }
}
