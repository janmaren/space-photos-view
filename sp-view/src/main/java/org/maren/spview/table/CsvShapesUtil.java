package org.maren.spview.table;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

import cz.jmare.data.util.GonUtil;
import cz.jmare.graphfw.csv.RowRebuilder;
import org.maren.spview.shape.CircleShape;
import org.maren.spview.shape.GroupAtt;
import org.maren.spview.shape.PointShape;
import org.maren.spview.tag.HiddenTag;
import org.maren.spview.util.CsvRow;
import org.maren.spview.util.CsvUtil;

import cz.jmare.data.util.ACooUtil;

public class CsvShapesUtil {
    public static final String CIRCLE_HEADER = "name,centre,radius,jd,desc";
    public static final String POINTS_HEADER = "name,coords,jd,desc,tags";

    public static final String[] POINTS_HEADER_ARRAY = POINTS_HEADER.split(",");

    public static final String[] CIRCLES_HEADER_ARRAY = CIRCLE_HEADER.split(",");

    public static final RaDecColumn[] VIZIER_COLUMN_NAMES = {new RaDecColumn("_RAJ2000", "_DEJ2000"),
            new RaDecColumn("_RA", "_DE"), new RaDecColumn("RA_ICRS", "DE_ICRS"),
            new RaDecColumn("RAJ2000", "DEJ2000"), new RaDecColumn("_RA.icrs", "_DE.icrs"),
            new RaDecColumn("RAB1950", "DEB1950")};
    
    private static char DEFAULT_DELIMITER = ',';
    
    public static void exportPointsToCsv(File file, List<PointShape> shapes) throws IOException {
        try (PrintWriter pw = new PrintWriter(file, Charset.forName("UTF-8"))) {
            pw.println(POINTS_HEADER);
            shapes.stream()
              .map(CsvShapesUtil::convertPointToCSV)
              .forEach(pw::println);
        }
    }

    private static String convertPointToCSV(PointShape pointShape) {
        CsvRow sj = new CsvRow(DEFAULT_DELIMITER);
        sj.add(pointShape.getName());
        sj.add(ACooUtil.formatCoords(pointShape.getRaDec(), ":", " "));
        sj.add(pointShape.getJd());
        sj.add(pointShape.getDescription());

        if (pointShape.isFavourite()) {
            pointShape.setTag(HiddenTag.FAVOURITE.name(), "1");
        }
        if (pointShape.isSeen()) {
            pointShape.setTag(HiddenTag.SEEN.name(), "1");
        }

        sj.add(pointShape.getTagsString());

        if (pointShape.isFavourite()) {
            pointShape.setTag(HiddenTag.FAVOURITE.name(), null);
        }
        if (pointShape.isSeen()) {
            pointShape.setTag(HiddenTag.SEEN.name(), null);
        }

        return sj.toString();
    }

    /**
     * Rebuild preparsed csv. If first row is header the hasHeader flag must be set to true
     * @param origCsv
     * @param directives
     * @param hasHeader
     * @return
     */
    public static List<String[]> rebuildCsvToNative(List<String[]> origCsv, String[] directives, boolean hasHeader) {
        List<String[]> lines = new ArrayList<String[]>();
        Iterator<String[]> iterator = origCsv.iterator();
        if (!iterator.hasNext()) {
            return List.of();
        }

        if (hasHeader) {
            iterator.next();
        }

        while (iterator.hasNext()) {
            String[] values = iterator.next();
            if (values == null) {
                break;
            }
            String[] row = RowRebuilder.buildResultRow(values, directives);
            lines.add(row);
        }
        return lines;
    }

    public static List<PointShape> importNativePointsCsv(String fileText) {
        return importNativePointsCsv(fileText, ',');
    }

    public static List<PointShape> importNativePointsCsv(String fileText, char delimiter) {
        List<String[]> lines = CsvUtil.importCsv(fileText, delimiter);
        String[] header = lines.remove(0);
        if (!CsvUtil.isEqual(POINTS_HEADER_ARRAY, header)) {
            throw new IllegalArgumentException("Not the native points format");
        }
        return importNativePointsCsv(lines);
    }

    /**
     * Import native file with parsed lines or after {@link #rebuildCsvToNative(List, String[], boolean)}
     * @param lines lines with columns, without header
     * @return
     */
    public static List<PointShape> importNativePointsCsv(List<String[]> lines) {
        List<PointShape> records = new ArrayList<>();
        for (String[] values : lines) {
            String name = values[0].trim();
            if (name.length() == 0) {
                name = null;
            }
            if (values[1].isBlank()) {
                throw new IllegalStateException("The ra, dec field is mandatory but it's empty for row " + String.join(", ", values));
            }
            PointShape pointShape = new PointShape(GroupAtt.EMPTY_GROUP, name, ACooUtil.parseCoords(values[1].replace("\u00a0", " ")));
            String jdStr = values[2].trim();
            if (jdStr.length() != 0) {
                try {
                    Double jd = Double.valueOf(jdStr);
                    pointShape.setJd(jd);
                } catch (NumberFormatException e) {
                    throw new IllegalStateException("The '" + jdStr + "' string is not a julian date number or not used datespacetime directive to convert gregorian date");
                }
            }
            String desc = values[3].trim();
            if (desc.length() != 0) {
                pointShape.setDescription(desc);
            }
            if (values[4].length() != 0) {
                pointShape.setTagsString(values[4]);

                Object favValue = pointShape.getTagValue(HiddenTag.FAVOURITE.name());
                if (favValue instanceof Integer) {
                    Integer value = (Integer) favValue;
                    pointShape.removeTag(HiddenTag.FAVOURITE.name());
                    pointShape.setFavourite(value == 1);
                }

                Object seenValue = pointShape.getTagValue(HiddenTag.SEEN.name());
                if (seenValue instanceof Integer) {
                    Integer value = (Integer) seenValue;
                    pointShape.removeTag(HiddenTag.SEEN.name());
                    pointShape.setSeen(value == 1);
                }
            }
            records.add(pointShape);
        }
        return records;
    }

    public static List<CircleShape> importNativeCirclesCsv(List<String[]> lines) {
        List<CircleShape> records = new ArrayList<>();
        for (String[] values : lines) {
            CircleShape circleShape = new CircleShape(GroupAtt.EMPTY_GROUP);
            circleShape.setName(values[0].isBlank() ? null : values[0]);
            circleShape.setCentre(ACooUtil.parseHCoords(values[1]).toRaDec());
            circleShape.setRadiusDeg(GonUtil.parseDegrees(values[2]));
            if (values[3] != null && !"".equals(values[3])) {
                circleShape.setJd(Double.valueOf(values[3]));
            }
            if (values[4] != null && !"".equals(values[4])) {
                circleShape.setDescription(values[4]);
            }
            records.add(circleShape);
        }
        return records;
    }

    public static void exportCirclesToCsv(File file, List<CircleShape> shapes) throws IOException {
        try (PrintWriter pw = new PrintWriter(file, Charset.forName("UTF-8"))) {
            pw.println(CIRCLE_HEADER);
            shapes.stream()
              .map(CsvShapesUtil::convertCircleToCSV)
              .forEach(pw::println);
        }
    }

    private static String convertCircleToCSV(CircleShape circleshape) {
        CsvRow sj = new CsvRow(DEFAULT_DELIMITER);
        sj.add(circleshape.getName());
        sj.add(ACooUtil.formatCoords(circleshape.getCentreRaDec(), ":", " "));
        sj.add(GonUtil.formatDegrees(circleshape.getRadiusDeg()));
        sj.add(circleshape.getJd());
        sj.add(circleshape.getDescription());
        return sj.toString();
    }

    public static int[] findVizierRaDecIndexes(String[] columnNames) {
        for (RaDecColumn vizierColumnName : VIZIER_COLUMN_NAMES) {
            int raIndex = find(vizierColumnName.raName, columnNames);
            int decIndex = find(vizierColumnName.decName, columnNames);
            if (raIndex != -1 && decIndex != -1) {
                return new int[] {raIndex, decIndex};
            }
        }
        return null;
    }

    public static int find(String text, String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(text)) {
                return i;
            }
        }
        return -1;
    }
}
