package org.maren.spview.table;

import cz.jmare.graphfw.labeler.Labeler;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.util.Create;
import cz.jmare.swt.util.SMessage;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.sphericcanvas.ToolListener;
import org.maren.gis.type.Spheric;
import org.maren.spview.NearestSelectionObject;
import org.maren.spview.SPView;
import org.maren.spview.app.GroupsDialog;
import org.maren.spview.app.SaveItemsDialog;
import org.maren.spview.shape.*;

import java.util.List;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public abstract class AbstractShapesComposite<T extends Shape> extends Composite implements ShapeDrawnListener<T> {

    private final MenuItem miEnableAll;
    private final MenuItem miDisableAll;
    private final MenuItem miDisableOthers;
    private final MenuItem miInvertSelection;
    protected Table table;
    protected SPView spView;

    protected Map<String, GroupAtt> groups = new HashMap<>();
    private Button addByMapButton;
    private Button showLabelButton;
    private List<MenuItem> popMenuItems = new ArrayList<>();
    private MenuItem miRemove;
    private MenuItem miRemoveAll;
    private Menu menuTable;
    private MenuItem miDisableSelected;

    private MenuItem miMarkFavourite;

    private MenuItem miUnmarkFavourite;

    private Display display;

    private boolean persistentTool;
    private ColumnSorter<T> currentColumnSorter;

    public AbstractShapesComposite(Composite parent, int style, SPView spView) {
        super(parent, style);
        this.spView = spView;
        this.display = getShell().getDisplay();
        GroupUtil.getOrCreateDefaultGroup(groups);
        ImageUtil.put("add", "/image/list-add.png");
        ImageUtil.put("add-cursor", "/image/add-cursor.png");
        ImageUtil.put("add-cursor-many", "/image/add-cursor-many.png");
        ImageUtil.put("open", "/image/folder.png");
        ImageUtil.put("export", "/image/document-save.png");
        ImageUtil.put("group", "/image/user-group-icon.png");
        ImageUtil.put("label", "/image/label.png");
        ImageUtil.put("star", "/image/star.png");
        ImageUtil.put("star-eye", "/image/star-eye.png");
        ImageUtil.put("eye", "/image/eye.png");

        setLayout(new FormLayout());

        Composite toolsComposite = new Composite(this, SWT.NONE);
        toolsComposite.setLayout(new GridLayout(14, false));

        FormData data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.top = new FormAttachment(0, 0);
        data.top.alignment = SWT.TOP;
        data.right = new FormAttachment(100, 0);
        data.bottom = new FormAttachment(0, 40);
        toolsComposite.setLayoutData(data);

        Create.button(toolsComposite, button -> {
            button.setToolTipText("Import");
            button.setImage(ImageUtil.get("open"));
            button.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    boolean forceRebuild = (e.stateMask & SWT.CTRL) != 0;
                    imports(forceRebuild);
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {
                }
            });
        });

        Create.button(toolsComposite, button -> {
            button.setToolTipText("Export");
            button.setImage(ImageUtil.get("export"));
            button.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    SaveItemsDialog saveItemsDialog = new SaveItemsDialog(getShell(), GroupUtil.getGroupsNames(groups));
                    if (saveItemsDialog.open() == Window.OK) {
                        String onlyGroupStr = saveItemsDialog.getResultOnlyGroup();
                        GroupAtt onlyGroup = groups.get(onlyGroupStr);
                        boolean onlyEnabled = saveItemsDialog.isResultOnlyEnabled();
                        boolean onlySelected = saveItemsDialog.isResultOnlySelected();
                        boolean onlyFavourites = saveItemsDialog.isResultOnlyFavourites();
                        boolean onlySeen = saveItemsDialog.isResultOnlySeen();

                        List<T> items = new ArrayList<>();
                        int[] selectionIndices = table.getSelectionIndices();
                        Set<Integer> selList = onlySelected ? Arrays.stream(selectionIndices).boxed().collect(Collectors.toSet()) : new HashSet<>();
                        for (int i = 0; i < table.getItemCount(); i++) {
                            TableItem item = table.getItem(i);
                            T shape = (T) item.getData();
                            if (onlyGroup != null && shape.getGroup() != onlyGroup) {
                                continue;
                            }
                            if (onlyEnabled && !item.getChecked()) {
                                continue;
                            }
                            if (onlySelected && !selList.contains(i)) {
                                continue;
                            }
                            if (onlyFavourites && !shape.isFavourite()) {
                                continue;
                            }
                            if (onlySeen && !shape.isSeen()) {
                                continue;
                            }
                            items.add(shape);
                        }
                        export(items);
                        setInfo("Exported");
                    }
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {
                }
            });
        });

        Create.button(toolsComposite, button -> {
            button.setToolTipText("Add");
            button.setImage(ImageUtil.get("add"));
            button.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    SMessage.withErrorDialog(() -> {
                        doAddShapeByEdit();
                        setInfo("Added");
                    });
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {
                }
            });
        });

        addByMapButton = Create.button(toolsComposite, SWT.TOGGLE, button -> {
            button.setToolTipText("Add by drawing\nPress with ctrl key to make the tool persistent allowing to make more shapes");
            button.setImage(ImageUtil.get("add-cursor"));
            button.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    if (button.getSelection()) {
                        persistentTool = (e.stateMask & SWT.CTRL) != 0;
                        if (persistentTool) {
                            setInfo("Tool enabled using ctrl key - persists until manually disabled");
                            addByMapButton.setImage(ImageUtil.get("add-cursor-many"));
                        }
                        prepareAddShapeByMap();
                    } else {
                        addByMapButton.setImage(ImageUtil.get("add-cursor"));
                        AbstractShapesComposite.this.spView.setToolListener(null, AbstractShapesComposite.this);
                        AbstractShapesComposite.this.spView.refresh();
                    }
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {
                }
            });
        });

        Create.imageButton(toolsComposite, ImageUtil.get("group"), "Manage Groups", () -> {
            List<T> allItems = getAllItems();
            GroupsDialog groupsDialog = new GroupsDialog(getShell(), groups, getCurrentGroupNameOrNull(), allItems);
                groupsDialog.open();
                updateTableItems();
                spView.shapesChanged();
                this.spView.refresh();
            });

        showLabelButton = Create.button(toolsComposite, SWT.TOGGLE, button -> {
            button.setToolTipText("Show labels\nPress CTRL for field selection dialog");
            button.setImage(ImageUtil.get("label"));
            button.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
                        button.setSelection(!button.getSelection());
                        doLabelDialog();
                    }
                    AbstractShapesComposite.this.spView.refresh();
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {
                }
            });
            button.setSelection(true);
        });

        addCustomToolbarButtons(toolsComposite);

        table = new Table(this, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI | SWT.VIRTUAL);
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(toolsComposite, 0, SWT.DEFAULT);
        data.bottom = new FormAttachment(100, 0);
        table.setLayoutData(data);
        
        table.setLinesVisible(true);
        table.setHeaderVisible(true);
        table.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                if (event.detail == SWT.CHECK) {
                    spView.refresh();
                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        table.addMouseListener(new MouseListener() {
            @Override
            public void mouseUp(MouseEvent e) {
            }
            
            @Override
            public void mouseDown(MouseEvent e) {
            }
            
            @Override
            public void mouseDoubleClick(MouseEvent e) {
                int[] selectionIndices = table.getSelectionIndices();
                if (selectionIndices.length > 0) {
                    T shape = (T) table.getItem(selectionIndices[0]).getData();
                    centerShape(shape);
                }
            }
        });

        table.addKeyListener(KeyListener.keyPressedAdapter(e -> {
            if ((e.keyCode == 'a' || e.keyCode == 'A') && (e.stateMask & SWT.CTRL) != 0) {
                table.selectAll();
            }
            if (e.keyCode == 127) {
                doRemoveSelected();
            }
        }));
        
        
        TableColumn idColumn = new TableColumn(table, SWT.CENTER);
        idColumn.setText("\u25A1");
        idColumn.setWidth(50);
        idColumn.setResizable(true);
        idColumn.setAlignment(SWT.LEFT);
        ColumnSorter<T> idColumnSorter = new ColumnSorter<T>(table, this);
        idColumnSorter.setComparator(getIdComparator());
        idColumn.addSelectionListener(idColumnSorter);
        addColumns(table);

        table.setSortDirection(SWT.DOWN);
        table.setSortColumn(idColumn);
        currentColumnSorter = idColumnSorter;

        table.setHeaderVisible(true);

        table.addListener(SWT.Selection, new Listener() {
            @Override
            public void handleEvent(Event event) {
                resolvePopupEnablement();
            }
        });
        
        menuTable = new Menu(table);
        table.setMenu(menuTable);

        // Create menu item
        
        MenuItem miCenterInGroup = new MenuItem(menuTable, SWT.NONE);
        miCenterInGroup.setText("Center");
        Create.selectionListener(miCenterInGroup, (e) -> {
            int selectionIndex = table.getSelectionIndex();
            if (selectionIndex < 0) {
                return;
            }
            T item = (T) table.getItem(selectionIndex).getData();
            centerShape(item);
        });
        
        MenuItem miEditInGroup = new MenuItem(menuTable, SWT.NONE);
        miEditInGroup.setText("Edit");
        Create.selectionListener(miEditInGroup, (e) -> {
            doUpdateShapeByEdit();
        });

        miMarkFavourite = new MenuItem(menuTable, SWT.NONE);
        miMarkFavourite.setText("Mark Favourite");
        Create.selectionListener(miMarkFavourite, (e) -> {
            traverseSelectedTable((it) -> {
                Shape shape = (Shape) it.getData();
                shape.setFavourite(true);
                setDataToTableItem((T) shape, it);
            });
            if (currentColumnSorter != null) {
                currentColumnSorter.sortItems();
            }
            spView.refresh();
        });

        miUnmarkFavourite = new MenuItem(menuTable, SWT.NONE);
        miUnmarkFavourite.setText("Unmark Favourite");
        Create.selectionListener(miUnmarkFavourite, (e) -> {
            traverseSelectedTable((it) -> {
                Shape shape = (Shape) it.getData();
                shape.setFavourite(false);
                setDataToTableItem((T) shape, it);
            });
            if (currentColumnSorter != null) {
                currentColumnSorter.sortItems();
            }
            spView.refresh();
        });

        miEnableAll = new MenuItem(menuTable, SWT.NONE);
        miEnableAll.setText("Enable All");
        Create.selectionListener(miEnableAll, (e) -> {
            traverseTable((it) -> {
                it.setChecked(true);
            });
            spView.shapesChanged();
            spView.refresh();
        });

        
        miDisableAll = new MenuItem(menuTable, SWT.NONE);
        miDisableAll.setText("Disable All");
        Create.selectionListener(miDisableAll, (e) -> {
            traverseTable(it -> {
                it.setChecked(false);
            });  
            spView.shapesChanged();
            spView.refresh();
        });

        miDisableSelected = new MenuItem(menuTable, SWT.NONE);
        miDisableSelected.setText("Disable Selected");
        Create.selectionListener(miDisableSelected, (e) -> {
            int[] selectionIndices = table.getSelectionIndices();
            TableItem[] items = table.getItems();
                
            Set<Integer> selList = new HashSet<>();
            for (int i : selectionIndices) {
                selList.add(i);
            }
            for (int i = 0; i < items.length; i++) {
                TableItem itemI = items[i];
                if (selList.contains(i)) {
                    itemI.setChecked(false);
                }
            }
            spView.refresh(); 
        });

        miDisableOthers = new MenuItem(menuTable, SWT.NONE);
        miDisableOthers.setText("Only Selection Enabled");
        Create.selectionListener(miDisableOthers, (e) -> {
            int[] selectionIndices = table.getSelectionIndices();
            if (selectionIndices.length == 0) {
                return;
            }
            Set<T> selections = new HashSet<>();
            for (int i = 0; i < selectionIndices.length; i++) {
                T item = (T) table.getItem(selectionIndices[i]).getData();
                selections.add(item);
            }
            traverseTable(it -> {
                T ri2 = (T) it.getData();
                it.setChecked(selections.contains(ri2));
            });
            spView.shapesChanged();
            spView.refresh();
        });

        miInvertSelection = new MenuItem(menuTable, SWT.NONE);
        miInvertSelection.setText("Invert Selection");
        Create.selectionListener(miInvertSelection, (e) -> {
            int[] selectionIndices = table.getSelectionIndices();
            TableItem[] items = table.getItems();
            table.setSelection(new int[] {});
            for (int i = 0; i < items.length; i++) {
                boolean found = false;
                for (int j = 0; j < selectionIndices.length; j++) {
                    if (selectionIndices[j] == i) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    table.select(i);
                }
            }
        });

        miRemove = new MenuItem(menuTable, SWT.NONE);
        miRemove.setText("Remove Selected");
        Create.selectionListener(miRemove, (e) -> {
            doRemoveSelected();
        });
        
        miRemoveAll = new MenuItem(menuTable, SWT.NONE);
        miRemoveAll.setText("Remove All");
        Create.selectionListener(miRemoveAll, (e) -> {
            table.removeAll();
            spView.refresh();
        });

        popMenuItems = List.of(miCenterInGroup, miDisableAll, miDisableOthers, miEditInGroup, miEnableAll, miInvertSelection, miRemove, miRemoveAll, miDisableSelected, miDisableOthers);
        
        addCustomPopMenuItems(table, menuTable);
        
        popWhenNone();
    }

    private Comparator<T> getIdComparator() {
        return new Comparator<T>() {
            @Override
            public int compare(T ps1, T ps2) {
                int order = 0;
                if (ps1.isFavourite()) {
                    order -= 10;
                }
                if (ps2.isFavourite()) {
                    order += 10;
                }
                if (ps1.isSeen()) {
                    order--;
                }
                if (ps2.isSeen()) {
                    order++;
                }
                return order;
            }
        };
    }

    private void doRemoveSelected() {
        int[] selectionIndices = table.getSelectionIndices();
        removeFromTable(selectionIndices);
        spView.refresh();
    }

    protected abstract void doLabelDialog();

    protected void putItemsToTable(List<T> items) {
        Display display = getShell().getDisplay();
        executeTask(() -> {
                display.asyncExec(() -> {
                        if (items.size() > 10000) {
                            spView.setZoom(items.size() / 100);
                        }
                    });
                display.syncExec(() -> {
                    addShapes(items);
                });
                display.syncExec(() -> {
                    spView.refresh();
                    setInfo("Imported " + items.size() + " items");
                });
            });
    }

    protected void removeFromTable(int[] selectionIndices) {
        TableItem[] items = table.getItems();

        List<T> shapes = new ArrayList<>(items.length);
        Set<Integer> selList = new HashSet<>();
        for (int i : selectionIndices) {
            selList.add(i);
        }
        for (int i = 0; i < items.length; i++) {
            TableItem itemI = items[i];
            T pointShape = (T) itemI.getData();
            if (selList.contains(i)) {
                continue; // deleting
            }
            shapes.add(pointShape);
        }

        table.setItemCount(shapes.size());
        table.setSelection(new int[] {});

        for (int i = 0; i < shapes.size(); i++) {
            T pointShape = shapes.get(i);
            setDataToTableItem(pointShape, items[i]);
            items[i].setData(pointShape);
        }
    }

    protected void addCustomToolbarButtons(Composite toolsComposite) {
    }

    protected void addCustomPopMenuItems(Table table, Menu menuTable) {
    }
    
    protected void resolveCustomPopMenuEnabled(MenuItem menuItem) {
    }
    
    protected void popWhenMultiple() {
        for (MenuItem menuItem : popMenuItems) {
            menuItem.setEnabled(false);
        }
        miRemove.setEnabled(true);
        miRemoveAll.setEnabled(true);
        miDisableSelected.setEnabled(true);
        miEnableAll.setEnabled(true);
        miDisableAll.setEnabled(true);
        miDisableOthers.setEnabled(true);
        miInvertSelection.setEnabled(true);
        miMarkFavourite.setEnabled(true);
        miUnmarkFavourite.setEnabled(true);
    }


    protected void popWhenNone() {
        for (MenuItem menuItem : popMenuItems) {
            menuItem.setEnabled(false);
        }
        miRemoveAll.setEnabled(true);
        miEnableAll.setEnabled(true);
        miDisableAll.setEnabled(true);
        miInvertSelection.setEnabled(true);
    }


    protected void popWhenOne() {
        for (MenuItem menuItem : popMenuItems) {
            menuItem.setEnabled(true);
        }
    }
    
    private void popCustoms() {
        MenuItem[] items = menuTable.getItems();
        for (MenuItem menuItem : items) {
            if (!popMenuItems.contains(menuItem)) {
                resolveCustomPopMenuEnabled(menuItem);
            }
        }
    }

    private void traverseTable(Consumer<TableItem> consumer) {
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            consumer.accept(item);
        }
    }

    private void traverseSelectedTable(Consumer<TableItem> consumer) {
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            if (table.isSelected(i)) {
                consumer.accept(item);
            }
        }
    }
    
    private void doAddShapeByEdit() {
        T item = editNewShape(GroupUtil.getOrCreateDefaultGroup(groups), groups);
        if (item != null) {
            display.syncExec(() -> {
                addShapes(List.of(item));
            });
            spView.setCenterAndRefresh(item.getCentre());
            table.select(table.getItemCount() - 1);
            table.showItem(table.getItem(table.getItemCount() - 1));
        }
    }


    protected GroupAtt getCurrentGroupOrCreateDefault() {
        return GroupUtil.getOrCreateGroup(groups, GroupUtil.DEFAULT_GROUP_NAME);
    }

    protected String getCurrentGroupNameOrNull() {
        int[] selectionIndices = table.getSelectionIndices();
        if (selectionIndices.length > 0) {
            T item = (T) table.getItem(selectionIndices[0]).getData();
            return item.getGroup().getName();
        }
        return null;
    }
    
    private void prepareAddShapeByMap() {
        T shape = newEmptyShape(getCurrentGroupOrCreateDefault());
        ToolResultLisSetter<T> newTool = newTool(shape, null);
        newTool.setDrawListener(this);
        spView.setToolListener((ToolListener) newTool, this);
        setInfo("Click to map to create a shape. Use the CTRL button to add point exactly at the centre.");
    }
    
    private void doUpdateShapeByEdit() {
        int selectionIndex = table.getSelectionIndex();
        if (selectionIndex < 0) {
            return;
        }
        TableItem selectedItem = table.getItem(selectionIndex);
        T item = (T) selectedItem.getData();
        if (editUpdatingShape(item, groups)) {
            setDataToTableItem(item, table.getItem(selectionIndex));
            spView.refresh();
            if (currentColumnSorter != null) {
                currentColumnSorter.sortItems();
            }
            spView.shapeEdited(item);
        }
    }

    public static Composite createRowCompositeLineIntoGridComposite(Composite parentComposite, Consumer<GridData> gdConsumer) {
        Composite panelComposite = new Composite(parentComposite, SWT.NONE);
        RowLayout rowLayout = new RowLayout();
        rowLayout.marginHeight = 0;
        rowLayout.marginWidth = 0;
        rowLayout.center = true;
        panelComposite.setLayout(rowLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.horizontalAlignment = SWT.FILL;
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }
    
    protected void centerShape(T shape) {
        spView.setCenterAndRefresh(shape.getCentre());
        StyledText infoStyledText = spView.getInfoStyledText();
        fillInfo(shape, infoStyledText);
    }

    protected void fillInfo(T shape, StyledText infoStyledText) {
    }

    public List<T> getEnabledShapes() {
        List<TableItem> enabledItems = getEnabledItems();
        return enabledItems.stream().map(item -> (T) item.getData()).collect(Collectors.toList());
    }

    public List<TableItem> getEnabledItems() {
        List<TableItem> list = new ArrayList<TableItem>();
        traverseTable((TableItem item) -> {
            if (item.getChecked()) {
                list.add(item);
            }
        });
        return list;
    }

    public List<T> getAllItems() {
        List<T> list = new ArrayList<>();
        traverseTable((TableItem item) -> {
            list.add((T) item.getData());
        });
        return list;
    }

    public void selectShape(Object shape, boolean additive) {
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            Object shape2 = item.getData();
            if (shape == shape2) {
                if (additive) {
                    table.select(i);
                } else {
                    table.setSelection(i);
                }
                table.showItem(item);
            }
        }
        fillInfo((T) shape, spView.getInfoStyledText());
    }

    /**
     * Add shapes to table and update groups combo when missing such group
     * @param shapes
     */
    public void addShapes(List<T> shapes) {
        if (shapes.size() == 0) {
            return;
        }
        int origCount = table.getItemCount();
        int newCount = origCount + shapes.size();
        table.setItemCount(newCount);

        TableItem[] items = table.getItems();
        int is = 0;
        for (int i = origCount; i < newCount; i++) {
            T shape = shapes.get(is++);
            TableItem item = items[i];
            setDataToTableItem(shape, item);
            item.setChecked(true);
        }

        if (currentColumnSorter != null) {
            currentColumnSorter.sortItems();
        }
    }

    public void addShapes(List<T> shapes, String groupName) {
        GroupAtt group = GroupUtil.getOrCreateGroup(groups, groupName);
        for (T shape : shapes) {
            shape.setGroup(group);
        }
        addShapes(shapes);
    }


    /**
     * Get shapes which don't exist. Using {@link #same(Shape, Shape)} for comparison
     * @param shapesAdd
     * @return
     */
    public List<T> getUnexistingShapes(List<T> shapesAdd) {
        if (shapesAdd.size() == 0) {
            return shapesAdd;
        }

        List<T> allItems = getAllItems();
        List<T> shapes = new ArrayList<>();
        for (T t : shapesAdd) {
            boolean exists = false;
            for (T item : allItems) {
                if (same(t, item)) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                shapes.add(t);
            }
        }
        return shapes;
    }

    public abstract boolean same(T t1, T t2);

    protected void showItem(T shape) {
        traverseTable(item -> {
            if (item.getData() == shape) {
                table.setSelection(item);
                table.showItem(item);
            }
        });
    }

    protected void setDataToTableItem(T data, TableItem tableItem) {
        String[] columnsStrings = shapeToColumns(data);
        tableItem.setData(data);
        tableItem.setText(columnsStrings);
        if (data.isFavourite() || data.isSeen()) {
            if (data.isFavourite()) {
                if (data.isSeen()) {
                    tableItem.setImage(ImageUtil.get("star-eye"));
                } else {
                    tableItem.setImage(ImageUtil.get("star"));
                }
            } else {
                tableItem.setImage(ImageUtil.get("eye"));
            }
        } else {
            tableItem.setImage((Image) null);
        }
    }
    
    /**
     * Update existing shape by edit dialog
     *
     * @param shape
     * @param groups
     */
    protected abstract boolean editUpdatingShape(T shape, Map<String, GroupAtt> groups);
    
    /**
     * Create new shape by edit dialog
     * @param group
     * @param groups
     * @return
     */
    protected abstract T editNewShape(GroupAtt group, Map<String, GroupAtt> groups);
    
    /**
     * Return instance of new shape
     * @param group
     * @return
     */
    protected abstract T newEmptyShape(GroupAtt group);
    
    /**
     * Return instance of new tool which contains a shape (existing or new)
     * @param shape
     * @return
     */
    protected abstract ToolResultLisSetter<T> newTool(T shape, T origShape);

    /**
     * Add columns to table. The size must be same as {@link #shapeToColumns(Shape)}
     * @param table
     */
    protected abstract void addColumns(Table table);

    /**
     * Return texts for table
     * @param shape
     * @return
     */
    protected abstract String[] shapeToColumns(T shape);
    
    protected abstract void export(List<T> items);
    
    protected abstract void imports(boolean forceRebuild);
    
    /**
     * Draw all checked shapes. Emphasize selected shapes. 
     * @param gc
     * @param coordState
     */
    public abstract void drawShapes(GC gc, CoordState coordState, boolean withLabel, Labeler labeler);

    @Override
    public void onShapeComplete(T shape, T origShape) {
        if (origShape != null) {
            for (int i = 0; i < table.getItemCount(); i++) {
                TableItem item = table.getItem(i);
                Shape oshape = (Shape) item.getData();
                if (oshape == origShape) {
                    spView.setToolListener(null, null);
                    setDataToTableItem(shape, item);
                    table.setSelection(item);
                    spView.setCenterAndRefresh(shape.getCentre());
                    spView.focusTab(this);
                    spView.shapeEdited(shape);
                    return;
                }
            }
        }
        GroupAtt currentGroup = getCurrentGroupOrCreateDefault();
        shape.setGroup(currentGroup);
        display.syncExec(() -> {
            addShapes(List.of(shape));
        });
        //parentListener.setCenterAndRefresh(shape.getCentre());
        spView.refresh();
        spView.focusTab(this);
        showItem(shape);

        prepareAddShapeByMap(); // add next shape

        if (!persistentTool) {
            spView.setToolListener(null, AbstractShapesComposite.this);
            addByMapButton.setSelection(false);
            addByMapButton.setImage(ImageUtil.get("add-cursor"));
        }
    }
    
    public void unpressAddByMapButton() {
        addByMapButton.setImage(ImageUtil.get("add-cursor"));
        addByMapButton.setSelection(false);
    }

    public void drawShapes(GC gc, CoordState coordState, Labeler labeler) {
        drawShapes(gc, coordState, showLabelButton.getSelection(), labeler);
    }

    public abstract List<NearestSelectionObject> findShapes(Spheric polar, double maxDistanceDegs);
    
    public void setInfo(String message) {
        spView.setInfo(message);
    }
    
    public void setError(String message) {
        spView.setError(message);
    }
    
    public void setError(String message, Exception e) {
        spView.setError(message, e);
    }
    
    protected void setWarn(String message) {
        spView.setWarn(message);
    }

    protected void resolvePopupEnablement() {
        int[] selectionIndices = table.getSelectionIndices();
        if (selectionIndices.length == 1) {
            popWhenOne();
        } else if (selectionIndices.length == 0) {
            popWhenNone();
        } else {
            popWhenMultiple();
        }
        popCustoms();
    }

    protected void executeTask(final Runnable runnable) {
        ShapesTask shapesTask = new ShapesTask(runnable, getShell(), this);
        new Thread(shapesTask).start();
    }

    public SPView getSPView() {
        return spView;
    }


    protected void updateTableItems() {
        TableItem[] tableItems = table.getItems();
        for (int i = 0; i < tableItems.length; i++) {
            TableItem tableItem = tableItems[i];
            T shape = (T) tableItem.getData();
            setDataToTableItem(shape, tableItem);
        }
    }

    public void setCurrentColumnSorter(ColumnSorter<T> columnSorter) {
        this.currentColumnSorter = columnSorter;
    }
}
