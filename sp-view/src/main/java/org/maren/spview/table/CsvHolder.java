package org.maren.spview.table;

import cz.jmare.file.PathUtil;
import org.maren.spview.util.CsvUtil;
import org.maren.spview.util.CsvWarning;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.List;

public class CsvHolder {
    private File file;

    private List<String[]> lines;

    private List<String[]> linesComma;

    private String fileString;

    private boolean tried;
    private boolean triedLinesComma;

    private HashSet<CsvWarning> warnings = new HashSet<>();

    public CsvHolder(File file) {
        this.file = file;
    }

    public List<String[]> getLines() {
        if (tried) {
            return lines;
        }
        tried = true;
        try {
            String fileString = getFileString();
            if (fileString != null) {
                return lines = CsvUtil.importCsv(fileString, warnings);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public List<String[]> getLinesComma() {
        if (triedLinesComma) {
            return linesComma;
        }
        triedLinesComma = true;
        try {
            String fileString = getFileString();
            if (fileString != null) {
                return linesComma = CsvUtil.importCsv(fileString, ',');
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public String getFileString() {
        if (fileString == null) {
            try {
                fileString = Files.readString(file.toPath());
            } catch (IOException e) {
            }
        }
        return fileString;
    }

    public String getFileSuffix() {
        return PathUtil.getSuffix(file.getName());
    }

    public HashSet<CsvWarning> getWarnings() {
        return warnings;
    }
}
