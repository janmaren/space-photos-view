package org.maren.spview.table;

import org.eclipse.swt.widgets.Shell;

public class ShapesTask implements Runnable {
    private Runnable runnable;

    private Shell shell;
    private AbstractShapesComposite<?> abstractShapesComposite;

    public ShapesTask(Runnable runnable, Shell shell, AbstractShapesComposite<?> abstractShapesComposite) {
        this.runnable = runnable;
        this.shell = shell;
        this.abstractShapesComposite = abstractShapesComposite;
    }

    @Override
    public void run() {
        try {
            runnable.run();
        } catch (Exception e) {
            shell.getDisplay().asyncExec(() -> {
                e.printStackTrace();
                abstractShapesComposite.setError("Not success", e);
            });
        }
    }
}
