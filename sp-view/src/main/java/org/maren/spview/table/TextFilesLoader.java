package org.maren.spview.table;

import static org.maren.spview.table.CsvShapesUtil.CIRCLES_HEADER_ARRAY;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.maren.spview.horizons.NasaHorizonsEphParser;
import org.maren.spview.shape.CircleShape;
import org.maren.spview.shape.PointShape;
import org.maren.spview.util.CsvUtil;
import org.maren.spview.util.CsvWarning;

import cz.jmare.file.PathUtil;
import cz.jmare.graphfw.csv.ColsAdvicer;
import cz.jmare.graphfw.csv.CsvRedesignDialog;
import cz.jmare.graphfw.csv.PointColsAdvicer;
import cz.jmare.graphfw.csv.RowRebuilder;
import cz.jmare.graphfw.csv.VizierPointColsAdvicer;

public class TextFilesLoader {
    private final Shell shell;
    private List<AbstractShapesComposite<?>> shapesCompositeList;
    private final Consumer<Boolean> refresher;

    private boolean forceRebuild;

    public static File lastFile;

    private StringJoiner warnings = new StringJoiner(", ");

    public TextFilesLoader(Shell shell, List<AbstractShapesComposite<?>> shapesCompositeList, Consumer<Boolean> refresher) {
        this.shell = shell;
        this.shapesCompositeList = shapesCompositeList;
        this.refresher = refresher;
    }

    public List<File> loadFiles(List<File> files, boolean doPoints, boolean doCircles) {
        PointsComposite pointsComposite = doPoints ? findFirstPointsComposite() : null;
        CirclesComposite circlesComposite = doCircles ? findFirstCirclesComposite() : null;

        List<File> errorFiles = new ArrayList<>();

        for (File file : files) {
            String groupName = PathUtil.getNameAndSuffixOrEmpty(file.toString())[0];
            if (loadFile(file, groupName, pointsComposite, circlesComposite)) {
                lastFile = file;
            } else {
                errorFiles.add(file);
            }
        }

        refresher.accept(true);

        return errorFiles;
    }

    private boolean loadFile(File file, String groupName, PointsComposite pointsComposite, CirclesComposite circlesComposite) {
        CsvHolder csvHolder = new CsvHolder(file);

        if (!forceRebuild) {
            if (pointsComposite != null) {
                List<PointShape> points = loadNativePoints(csvHolder);
                if (points != null) {
                    addPointsSync(groupName, pointsComposite, points);
                    processWarnings(csvHolder, file);
                    return true;
                }

                points = loadVizierPoints(csvHolder);
                if (points != null) {
                    addPointsSync(groupName, pointsComposite, points);
                    processWarnings(csvHolder, file);
                    return true;
                }

                points = loadTnsPoints(csvHolder);
                if (points != null) {
                    addPointsSync(groupName, pointsComposite, points);
                    processWarnings(csvHolder, file);
                    return true;
                }

                points = loadNasaHorizonPoints(csvHolder);
                if (points != null) {
                    addPointsSync(groupName, pointsComposite, points);
                    processWarnings(csvHolder, file);
                    return true;
                }
            }

            if (circlesComposite != null) {
                List<CircleShape> circles = loadNativeCircles(csvHolder);
                if (circles != null) {
                    addCirclesSync(groupName, circlesComposite, circles);
                    processWarnings(csvHolder, file);
                    return true;
                }
            }
        }

        if (pointsComposite == null && circlesComposite != null) {
            String[] circlesNativeFields = {"Name*", "Centre coordinates", "Radius*", "Date [Julian day]*", "Description*"};
            return shell.getDisplay().syncCall(() -> {
                CsvRedesignDialog csvRedesignDialog = new CsvRedesignDialog(shell, csvHolder.getLines(), circlesNativeFields, file.getName());
                if (csvRedesignDialog.open() == Window.OK) {
                    try {
                        List<String[]> rebuildedCsv = csvRedesignDialog.getResultRebuildedCsv();
                        List<CircleShape> circleShapes = CsvShapesUtil.importNativeCirclesCsv(rebuildedCsv);
                        circlesComposite.addShapes(circleShapes, groupName);
                        refresher.accept(true);
                        processWarnings(csvHolder, file);
                        return true;
                    } catch (Exception e1) {
                        return false;
                    }
                }
                return false;
            });
        } else {
            String[] pointsNativeFields = {"Name*", "Equatorial Coordinates [ra, dec]", "Date [Julian day]*", "Description*", "Tags*"};
            ColsAdvicer adviser = file.getName().endsWith(".tsv") ? new VizierPointColsAdvicer(csvHolder.getLines(), pointsNativeFields.length) : new PointColsAdvicer(csvHolder.getLines(), pointsNativeFields.length);
            List<PointShape> pointShapes = shell.getDisplay().syncCall(() -> CsvRedesignDialog.rebuildCsv(shell, csvHolder.getLines(), adviser, file.getName()));
            if (pointShapes != null) {
                addPointsSync(groupName, pointsComposite, pointShapes);
                processWarnings(csvHolder, file);
                return true;
            }
        }

        return false;
    }

    private void processWarnings(CsvHolder csvHolder, File file) {
        HashSet<CsvWarning> warnings1 = csvHolder.getWarnings();
        if (warnings1.isEmpty()) {
            return;
        }
        StringJoiner sj = new StringJoiner(", ");
        for (CsvWarning csvWarning : warnings1) {
            sj.add(csvWarning.getText());
        }
        warnings.add(file.getName() + ": " + sj);
    }

    private List<PointShape> loadNasaHorizonPoints(CsvHolder csvHolder) {
        try {
            return NasaHorizonsEphParser.parseFullDataPoints(csvHolder.getFileString(), null);
        } catch (Exception e) {
            return null; // not nasa horizons format
        }
    }

    private List<PointShape> loadTnsPoints(CsvHolder csvHolder) {
        try {
            String[] directives = {"2", "3, \" \", 4", "datespacetime 21", "", "\"AMAG:\", 19"};
            List<String[]> nativeCsv = csvHolder.getFileSuffix().equals(".csv") ?
                    CsvShapesUtil.rebuildCsvToNative(csvHolder.getLinesComma(), directives, true) :
                    CsvShapesUtil.rebuildCsvToNative(csvHolder.getLines(), directives, true);
            return CsvShapesUtil.importNativePointsCsv(nativeCsv);
        } catch (Exception e) {
            return null; // not tns format
        }
    }

    private void addPointsSync(String groupName, PointsComposite pointsComposite, List<PointShape> points) {
        shell.getDisplay().asyncExec(() -> {
            pointsComposite.addShapes(points, groupName);
        });
    }

    private void addCirclesSync(String groupName, CirclesComposite circlesComposite, List<CircleShape> circles) {
        shell.getDisplay().asyncExec(() -> {
            circlesComposite.addShapes(circles, groupName);
        });
    }

    private List<PointShape> loadNativePoints(CsvHolder csvHolder) {
        List<String[]> lines = csvHolder.getLines();
        if (lines == null || lines.size() < 2) {
            return null;
        }
        if (!CsvUtil.isEqual(CsvShapesUtil.POINTS_HEADER_ARRAY, lines.get(0))) {
            return null;
        }
        try {
            return CsvShapesUtil.importNativePointsCsv(lines.subList(1, lines.size()));
        } catch (Exception e) {
            // no handling
        }
        return null;
    }

    private List<CircleShape> loadNativeCircles(CsvHolder csvHolder) {
        List<String[]> lines = csvHolder.getLines();
        if (lines == null || lines.size() < 2) {
            return null;
        }
        if (!CsvUtil.isEqual(CIRCLES_HEADER_ARRAY, lines.get(0))) {
            return null;
        }
        try {
            return CsvShapesUtil.importNativeCirclesCsv(lines.subList(1, lines.size()));
        } catch (Exception e) {
            // no handling
        }
        return null;
    }

    private List<PointShape> loadVizierPoints(CsvHolder csvHolder) {
        try {
            Pattern vizPattern = Pattern.compile("#Column");
            String fileString = csvHolder.getFileString();
            if (fileString == null || !(vizPattern.matcher(fileString).find() || !".tsv".equals(csvHolder.getFileSuffix()))) {
                return null;
            }
            List<String[]> lines = csvHolder.getLines();
            if (lines == null || lines.size() < 4) {
                return null;
            }
            lines = new ArrayList<>(lines);

            VizierPointColsAdvicer vizierPointColsAdvicer = new VizierPointColsAdvicer(lines, 5);
            String[] directives = vizierPointColsAdvicer.getDirectives();
            Integer raIndex = vizierPointColsAdvicer.getRaIndex();
            Integer decIndex = vizierPointColsAdvicer.getDecIndex();
            if (raIndex == null || decIndex == null) {
                return null;
            }

            List<String[]> csvLines = new ArrayList<>();
            lines = lines.subList(3, lines.size());
            for (String[] line : lines) {
                String[] columns = RowRebuilder.buildResultRow(line, directives);
                csvLines.add(columns);
            }

            return CsvShapesUtil.importNativePointsCsv(csvLines);
        } catch (Exception e) {
            return null;
        }
    }

    private PointsComposite findFirstPointsComposite() {
        return shapesCompositeList.stream()
                .filter(s -> s instanceof PointsComposite).map(s -> (PointsComposite) s).findFirst().get();
    }

    private CirclesComposite findFirstCirclesComposite() {
        return shapesCompositeList.stream()
                .filter(s -> s instanceof CirclesComposite).map(s -> (CirclesComposite) s).findFirst().get();
    }

    public void setForceRebuild(boolean forceRebuild) {
        this.forceRebuild = forceRebuild;
    }

    public String getWarnings() {
        return warnings.length() == 0 ? null : warnings.toString();
    }
}
