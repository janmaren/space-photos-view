package org.maren.spview.table;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.graphfw.labeler.Labeler;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.sphericcanvas.ToolListener;
import org.maren.gis.type.Spheric;
import org.maren.spview.NearestSelectionObject;
import org.maren.spview.SPView;
import org.maren.spview.shape.CircleShape;
import org.maren.spview.shape.CircleTool;
import org.maren.spview.shape.DrawUtil;
import org.maren.spview.shape.EditCircleDialog;
import org.maren.spview.shape.GroupAtt;
import org.maren.spview.shape.ToolResultLisSetter;
import org.maren.spview.util.DateTimeUtil;
import org.maren.swt.util.ConvTypesUtil;

import cz.jmare.data.util.GonUtil;
import cz.jmare.data.util.JulianUtil;
import cz.jmare.swt.color.ColorUtil;
import cz.jmare.math.astro.RaDec;
import cz.jmare.swt.util.SMessage;

public class CirclesComposite extends AbstractShapesComposite<CircleShape> {
    public CirclesComposite(Composite parent, int style, SPView spView) {
        super(parent, style, spView);
    }

    @Override
    protected void doLabelDialog() {

    }

    @Override
    public boolean same(CircleShape t1, CircleShape that) {
        return t1.getGroup().equals(that.getGroup()) && t1.getCentre().equals(that.getCentre()) && t1.getRadiusDeg().equals(that.getRadiusDeg()) && Objects.equals(t1.getName(), that.getName()) && Objects.equals(t1.getJd(), that.getJd());
    }

    @Override
    protected String[] shapeToColumns(CircleShape pointItem) {
        String[] cols = new String[4];
        cols[0] = "";
        if (pointItem.getName() != null) {
            cols[1] = pointItem.getName();
        } else {
            cols[1] = "";
        }
        cols[2] = pointItem.getGroup().getName();
        if (pointItem.getJd() != null) {
            cols[3] = JulianUtil.formatJulianRoundSeconds(pointItem.getJd(), 1, DateTimeUtil.getOffsetHours());
        } else {
            cols[3] = "";
        }
        return cols;
    }

    protected void addColumns(Table table) {
        TableColumn nameColumn = new TableColumn(table, SWT.CENTER);
        nameColumn.setText("Name");
        nameColumn.setWidth(150);
        nameColumn.setResizable(true);
        nameColumn.setAlignment(SWT.LEFT);

        TableColumn groupColumn = new TableColumn(table, SWT.CENTER);
        groupColumn.setText("Group");
        groupColumn.setWidth(80);
        groupColumn.setResizable(true);
        groupColumn.setAlignment(SWT.LEFT);
        
        TableColumn dateColumn = new TableColumn(table, SWT.CENTER);
        dateColumn.setText("Date");
        dateColumn.setWidth(150);
        dateColumn.setResizable(true);
        dateColumn.setAlignment(SWT.LEFT);
    }

    @Override
    protected CircleShape editNewShape(GroupAtt pointSet, Map<String, GroupAtt> groups) {
        CircleShape circleShape = new CircleShape(pointSet);
        circleShape.setCentre(spView.getCenter());
        EditCircleDialog editCircleDialog = new EditCircleDialog(getShell(), circleShape, groups);
        int open = editCircleDialog.open();
        if (open != Window.OK) {
            if (open == EditCircleDialog.DRAW_BUTTON) {
                ToolResultLisSetter<CircleShape> newTool = newTool(editCircleDialog.getResultCircleShape(), circleShape);
                newTool.setDrawListener(this);
                spView.setToolListener((ToolListener) newTool, this);
            }
            return null;
        }
        circleShape = editCircleDialog.getResultShape();
        return circleShape;
    }
    
    @Override
    protected boolean editUpdatingShape(CircleShape circleShape, Map<String, GroupAtt> groups) {
        EditCircleDialog editCircleDialog = new EditCircleDialog(getShell(), circleShape, groups);
        int result = editCircleDialog.open();
        if (result == EditCircleDialog.DRAW_BUTTON) {
            ToolResultLisSetter<CircleShape> newTool = newTool(editCircleDialog.getResultCircleShape(), circleShape);
            newTool.setDrawListener(this);
            spView.setToolListener((ToolListener) newTool, this);
            return false;
        } else if (result == Window.OK) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void export(List<CircleShape> items) {
        FileDialog fileDialog = new FileDialog(getShell(), SWT.SAVE);
        fileDialog.setFilterExtensions(new String[] {"*.csv"});
        String filename = fileDialog.open();
        if (filename != null) {
            SMessage.withErrorStatus(() -> {
                CsvShapesUtil.exportCirclesToCsv(new File(filename), items);
            });
        }
    }

    @Override
    protected void imports(boolean forceRebuild) {
        FileDialog fd = new FileDialog(getShell(), SWT.OPEN | SWT.MULTI);
        fd.setText("Image Files to Open");
        fd.setFilterExtensions(new String[] {"*.csv", "*.tsv", "*.txt"});
        if (fd.open() != null) {
            String[] fileNames = fd.getFileNames();
            TextFilesLoader textFilesLoader = new TextFilesLoader(getShell(), List.of(this), b -> {
                getShell().getDisplay().asyncExec(() -> spView.refresh());
            });
            textFilesLoader.setForceRebuild(forceRebuild);
            List<File> errs = textFilesLoader.loadFiles(Arrays.stream(fileNames).map(fn -> new File(fd.getFilterPath(), fn)).collect(Collectors.toList()), false, true);
            if (errs.isEmpty()) {
                setInfo("All files loaded");
            } else {
                setError("Not all files loaded, like " + errs.get(0).getName());
            }
        }
    }

    @Override
    public void drawShapes(GC gc, CoordState coordState, boolean withLabel, Labeler labeler) {
        List<CircleShape> circles = getEnabledShapes();
        Color origColor = gc.getForeground();
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        Font origFont = gc.getFont();
        gc.setFont(fontRegistry.get(coordState.getZoom() < 250 ? "smallest-font" : "smaller-font"));
        for (CircleShape circleShape : circles) {
            if (circleShape.getRadiusDeg() == null || circleShape.centre == null) {
                continue;
            }
            Color color = ColorUtil.getColor(circleShape.getGroup().getColor());
            gc.setForeground(color);
            String name = circleShape.getName();
            if (name != null && name.length() > 10) {
                name = name.substring(0, 10) + "...";
            }
            DrawUtil.drawSphCircle(gc, circleShape.getCentre(), Math.toRadians(circleShape.getRadiusDeg()), coordState, withLabel ? name : null, true, labeler);
        }
        gc.setForeground(origColor);
        gc.setFont(origFont);
    }

    @Override
    protected ToolResultLisSetter<CircleShape> newTool(CircleShape shape, CircleShape origShape) {
        return new CircleTool(shape, origShape);
    }

    @Override
    protected CircleShape newEmptyShape(GroupAtt group) {
        return new CircleShape(group);
    }


    @Override
    public List<NearestSelectionObject> findShapes(Spheric polar, double maxDistanceDegs) {
        List<NearestSelectionObject> nearest = new ArrayList<>();
        RaDec normalizedPolarDeg = ConvTypesUtil.toNormalizedRaDec(polar);
        List<CircleShape> shapes = getEnabledShapes();
        for (CircleShape shape : shapes) {
            RaDec centreRaDec = shape.getCentreRaDec();
            double angDistance = GonUtil.angularDistanceDeg(normalizedPolarDeg, centreRaDec);
            double distance = Math.abs(angDistance - shape.getRadiusDeg());
            double distCenter = GonUtil.angularDistanceDeg(normalizedPolarDeg, centreRaDec);
            if (distCenter < distance) {
                distance = distCenter;
            }
            if (distance < maxDistanceDegs) {
                String name = shape.getName();
                if (name == null) {
                    name = ACooUtil.formatRaDecProfi(shape.getCentreRaDec());
                }
                if (!shape.getGroup().getName().isEmpty()) {
                    name += " (" + shape.getGroup().getName() + ")";
                }
                if (shape.getJd() != null) {
                    name += ", " + JulianUtil.formatJulianRoundSecondsSkipFrac(shape.getJd(), 2);
                } else if (shape.getDescription() != null) {
                    name += ", " + shape.getDescription();
                }
                nearest.add(new NearestSelectionObject(NearestSelectionObject.ObjectType.CIRCLE, shape, name, distance));
            }
        }

        return nearest;
    }
}
