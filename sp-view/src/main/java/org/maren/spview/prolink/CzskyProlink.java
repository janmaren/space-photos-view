package org.maren.spview.prolink;

import static java.lang.Math.toRadians;

import cz.jmare.math.astro.RaDec;

public class CzskyProlink extends Prolink {
    @Override
    public String getUrl(RaDec raDec0, double arcsecondsPerPixel) {
        return String.format("https://www.czsky.cz/star/156/descr-chart?ra=%s&dec=%s&fsz=%s",
                toRadians(raDec0.ra), toRadians(raDec0.dec), arcsecondsPerPixel / 2.5);
    }

    @Override
    public String getName() {
        return "CZsky";
    }
}
