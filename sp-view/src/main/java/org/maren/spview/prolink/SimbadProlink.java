package org.maren.spview.prolink;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.math.astro.RaDec;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class SimbadProlink extends Prolink {
    @Override
    public String getUrl(RaDec raDec0, double arcsecondsPerPixel) {
        String formatedRaDec = ACooUtil.formatRaDec(raDec0, false, false);
        String coordStr = URLEncoder.encode(formatedRaDec, StandardCharsets.ISO_8859_1);
        return String.format("http://simbad.u-strasbg.fr/simbad/sim-coo?Coord=%s&CooFrame=FK5&CooEpoch=2000&CooEqui=2000&CooDefinedFrames=none&Radius=2&Radius.unit=arcmin&submit=submit+query&CoordList=", coordStr);
    }

    @Override
    public String getName() {
        return "Simbad";
    }
}
