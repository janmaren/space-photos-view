package org.maren.spview.prolink;

import cz.jmare.math.astro.RaDec;
import cz.jmare.math.astro.RahDec;

public class SkyserverProlink extends Prolink {
    @Override
    public String getUrl(RaDec raDec0, double arcsecondsPerPixel) {
        RahDec normalizedRahDec = raDec0.toNormalizedRahDec();
        return String.format("https://skyserver.sdss.org/dr18/VisualTools/navi?opt=G&ra=%s&dec=%s&scale=%s",
                normalizedRahDec.rah * 15, normalizedRahDec.dec, arcsecondsPerPixel);
    }

    @Override
    public String getName() {
        return "Skyserver";
    }
}
