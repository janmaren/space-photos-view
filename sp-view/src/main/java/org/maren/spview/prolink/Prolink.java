package org.maren.spview.prolink;

import cz.jmare.math.astro.RaDec;

public abstract class Prolink {
    public abstract String getUrl(RaDec raDec0, double arcsecondsPerPixel);

    public abstract String getName();
}
