package org.maren.spview;

public class ConfigKeys {
    public static final String KEY_MAP_BACKGROUND_COLOR = "map.background.color";

    public static final String DEFAULT_MAP_BACKGROUND_COLOR = "#FFFFFF";

    public static final String KEY_CENTER_ADDED_RASTER = "center.added.raster";

    public static final String KEY_SHOW_LEGEND = "show.legend";

    public static final String GEO_LOCATION = "geo.location";
}
