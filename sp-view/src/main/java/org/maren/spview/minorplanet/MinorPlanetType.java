package org.maren.spview.minorplanet;

public enum MinorPlanetType {
    ALL_ASTEROIDS_MPC("/Extended_Files/mpcorb_extended.dat.gz"),
    CURRENT_COMETS_MPC("/iau/MPCORB/CometEls.txt"), LATEST_DOU_MPEC("/Extended_Files/daily_extended.dat.gz");
    private String uri;

    MinorPlanetType(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }
}
