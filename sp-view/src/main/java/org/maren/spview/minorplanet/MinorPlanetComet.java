package org.maren.spview.minorplanet;

import java.time.LocalDate;

public class MinorPlanetComet {
    public String name;

    public LocalDate perihelionDate;

    public MinorPlanetComet(String name, LocalDate perihelionDate) {
        this.name = name;
        this.perihelionDate = perihelionDate;
    }
}
