package org.maren.spview.minorplanet;

import cz.jmare.config.AppConfig;
import cz.jmare.config.ConfigStore;
import cz.jmare.http.HUrl;
import cz.jmare.http.RequestData;
import org.maren.spview.util.AppDirsUtil;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

public class SmallObjectsFetcher {
    /**
     * After how many hours to download fresh data again. The database changes 2 times per day and they don't want us to donload too often
     */
    public static final int REFRESH_PERIOD_HOURS = 12;

    public static final Pattern FORMAT_NEW = Pattern.compile("^[A-Z]\\/[0-9]{4}");


    public static List<MinorPlanetComet> parseSmallObjectComets(String str) {
        List<MinorPlanetComet> comets = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new StringReader(str))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                if (line.length() < 57) {
                    break;
                }
                String name = line.substring(0, 47).trim();
                String dateStr = line.substring(47, 57);
                LocalDate date = LocalDate.of(Integer.parseInt(dateStr.substring(0, 4)), Integer.parseInt(dateStr.substring(5, 7)), Integer.parseInt(dateStr.substring(8, 10)));
                comets.add(new MinorPlanetComet(name, date));
            }
        } catch (Exception e) {
            throw new IllegalStateException("Unable to parse small objects", e);
        }
        return comets;
    }

    public static boolean youngFileExists(File file) {
        if (!file.isFile()) {
            return false;
        }
        try {
            FileTime modifTime = Files.getLastModifiedTime(file.toPath());
            LocalDateTime ldt =  LocalDateTime.ofInstant(modifTime.toInstant(), ZoneId.systemDefault());
            return ldt.plus(REFRESH_PERIOD_HOURS, ChronoUnit.HOURS).isAfter(LocalDateTime.now());
        } catch (IOException e) {
            return false;
        }
    }

    private static RequestData createRequestData() {
        ConfigStore configStore = AppConfig.getInstance().getConfigStore();
        String proxyHost = configStore.getValue("proxy.host",null);
        Integer proxyPort = configStore.getIntValue("proxy.port", null);
        RequestData requestData = new RequestData();
        requestData.setConnectTimeout(7000);
        if (proxyHost != null) {
            requestData.setProxy(proxyHost, proxyPort);
        }
        return requestData;
    }

    /**
     * Download comets from minorplanetcenter
     * @return
     * @throws IOException
     */
    public static List<MinorPlanetComet> getComets() throws IOException {
        String url = AppConfig.getInstance().getConfigStore().getValue("minor.planet.url", null);
        File dataDir = AppDirsUtil.getDataDir("data");
        File file = new File(dataDir, "Soft01Cmt.txt");
        String str = null;
        if (youngFileExists(file)) {
            str = Files.readString(file.toPath());
        } else {
            RequestData requestData = createRequestData();
            str = HUrl.getString(url + "/iau/Ephemerides/Comets/Soft01Cmt.txt", requestData);
            Files.writeString(file.toPath(), str);
        }
        return parseSmallObjectComets(str);
    }

    public static List<String[]> shortSmallObjectNames(List<MinorPlanetComet> names) {
        List<String[]> resultNames = new ArrayList<>();
        for (MinorPlanetComet comet : names) {
            String name = comet.name;
            String shortName = null;
            if (FORMAT_NEW.matcher(name).find()) {
                int index = name.indexOf(" ");
                if (index != -1) {
                    int index1 = name.indexOf(" ", index + 1);
                    if (index1 != -1) {
                        shortName = name.substring(0, index1);
                    } else {
                        shortName = name.trim();
                    }
                }
            } else {
                int index = name.indexOf(" ");
                if (index != -1) {
                    shortName = name.substring(0, index);
                } else {
                    shortName = name.trim();
                }
            }

            resultNames.add(new String[] {name, shortName});
        }
        return resultNames;
    }

    public static String gunzip(byte[] byteArray, String uri) throws IOException {
        ByteArrayOutputStream out = null;
        try (InputStream in = new ByteArrayInputStream(byteArray); GZIPInputStream gzipInputStream = new GZIPInputStream(in)) {
            out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int len;
            while ((len = gzipInputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        }
        return new String(out.toByteArray());
    }

    public static String downloadMinorPlanetData(MinorPlanetType minorPlanetType) {
        String url = AppConfig.getInstance().getConfigStore().getValue("minor.planet.url", null);
        RequestData requestData = createRequestData();
        String uri = minorPlanetType.getUri();
        String data = null;
        if (uri.endsWith(".gz")) {
            byte[] byteArray = HUrl.getByteArray(url + uri, requestData);
            try {
                data = gunzip(byteArray, uri);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            data = HUrl.getString(url + uri, requestData);
        }
        return data;
    }
}
