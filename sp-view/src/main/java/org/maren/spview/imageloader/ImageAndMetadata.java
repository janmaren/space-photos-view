package org.maren.spview.imageloader;

import org.eclipse.swt.graphics.ImageData;

public class ImageAndMetadata {
    public ImageData imageData;

    public ImageAndMetadata(ImageData imageData) {
        this.imageData = imageData;
    }
}
