package org.maren.spview.imageloader;

import java.io.File;
import java.lang.ref.SoftReference;

import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.ImageData;
import org.maren.swt.util.BufImageSWT;

import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.image.ras.RasImagePack;

/**
 * Loader for formats like png, jpeg, bmp... 
 */
public class PictureImageLoader implements ImageLoader {
    private String filename;
    
    private SoftReference<ImageData> imageDataRef;
    
    public PictureImageLoader(String filename) {
        super();
        this.filename = filename;
    }

    @Override
    public ImageAndMetadata loadImageData() {
        ImageData imageData = imageDataRef != null ? imageDataRef.get() : null;
        if (imageData == null) {
            try {
                imageData = new ImageData(filename);
                this.imageDataRef = new SoftReference<ImageData>(imageData);
            } catch (SWTException e) {
                try {
                    RasImagePack rasImagePack = RasImageFactory.get(new File(filename));
                    imageData = BufImageSWT.toImageData(rasImagePack.getRasImage());
                    this.imageDataRef = new SoftReference<ImageData>(imageData);
                } catch (Exception ex) {
                    throw new IllegalStateException("Unable to load " + filename, e);
                }
            }
        }
        return new ImageAndMetadata(imageData);
    }

    @Override
    public void setFullPath(String fullPath) {
        this.filename = fullPath;
    }
}
