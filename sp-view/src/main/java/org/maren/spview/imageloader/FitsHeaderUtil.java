package org.maren.spview.imageloader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.jmare.file.FileUtil;
import cz.jmare.fits.util.FitsUtil;
import nom.tam.fits.Fits;
import nom.tam.fits.HeaderCard;
import nom.tam.fits.ImageHDU;
import nom.tam.util.Cursor;

public class FitsHeaderUtil {

    public static List<FitsHeaderItem> getHeaderValues(ImageHDU hdu) {
        List<FitsHeaderItem> list = new ArrayList<>();
        Cursor<String, HeaderCard> iterator = hdu.getHeader().iterator();
        while (iterator.hasNext()) {
            HeaderCard headerCard = iterator.next();
            list.add(new FitsHeaderItem(headerCard.getKey(), headerCard.getValue(), headerCard.getComment()));
        }
        return list;
    }

    public static List<FitsHeaderItem> getHeaderValuesComments(String fitsHeader) {
        List<FitsHeaderItem> list = new ArrayList<>();
        String[] strings = FileUtil.splitToLinesQuick(fitsHeader);
        for (String str: strings) {
            int eqIndex = str.indexOf("=");
            if (eqIndex != -1) {
                String name = str.substring(0, eqIndex);
                String right = str.substring(eqIndex + 1);
                String value = right;
                list.add(new FitsHeaderItem(name, value, null));
            } else {
                list.add(new FitsHeaderItem(str, null, null));
            }
        }
        return list;
    }

    public static List<FitsHeaderItem> getHeaderValuesComments(ImageHDU hdu) {
        List<FitsHeaderItem> list = new ArrayList<>();
        Cursor<String, HeaderCard> iterator = hdu.getHeader().iterator();
        while (iterator.hasNext()) {
            HeaderCard headerCard = iterator.next();
            String value = headerCard.getValue();
            if (value == null) {
                continue;
            }
            list.add(new FitsHeaderItem(headerCard.getKey(), value, headerCard.getComment()));
        }
        return list;
    }

    public static List<FitsHeaderItem> getHeaderValuesComments(File wcsFitsFile) {
        try (Fits fits = new Fits(wcsFitsFile)) {
            ImageHDU hdu = FitsUtil.imageHDU(fits);
            return getHeaderValuesComments(hdu);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
}
