package org.maren.spview.imageloader;

public interface ImageLoader {
    ImageAndMetadata loadImageData();

    void setFullPath(String fullPath);
}
