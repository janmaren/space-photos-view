package org.maren.spview.imageloader;

import cz.jmare.image.ras.RasBufImage;
import cz.jmare.image.ras.RasImage;
import org.maren.imageprocess.util.FitsRasImageLoader;

import javax.imageio.ImageIO;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;

public class LoadBufferedImageUtil {
    public static BufferedImage load(String path) throws IOException {
        if (path.toLowerCase().endsWith(".fit") || path.toLowerCase().endsWith(".fits")) {
            FitsRasImageLoader fitsRasImageLoader = new FitsRasImageLoader();
            RasImage rasImage = fitsRasImageLoader.getRasImage(new File(path));
            if (rasImage instanceof RasBufImage) {
                RasBufImage rasBufImage = (RasBufImage) rasImage;
                return rasBufImage.getBufferedImage();
            } else {
                throw new IOException("Unsupported type of file " + path);
            }
        } else {
            return ImageIO.read(new File(path));
        }
    }
}
