package org.maren.spview.imageloader;

import cz.jmare.fits.header.InconsistentFitsException;
import cz.jmare.fits.header.WCS2d;
import org.maren.spview.app.ImageToSphericCoords;
import org.maren.spview.rasterpos.pixeltospheric.PixelToSpheric;
import org.maren.spview.rasterpos.pixeltospheric.WCS2dPixelToSpheric;
import org.maren.spview.rasterpos.pixeltospheric.WCS2dRevertedPixelToSpheric;
import org.maren.spview.wcstransform.util.WcsUtil;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.plugins.tiff.TIFFDirectory;
import javax.imageio.plugins.tiff.TIFFField;
import javax.imageio.stream.ImageInputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class WcsMetadataUtil {
    public static ImageToSphericCoordsAndHeader getWcsAffinPoints(String filePath, PictureImageLoader pictureImageLoader) throws InconsistentFitsException {
        String fitsHeader;
        List<FitsHeaderItem> headerValues;
        try {
            Map<Integer, TIFFField> tiffMetadata = getTiffMetadata(new File(filePath), List.of(270));
            TIFFField tiffField = tiffMetadata.get(270);
            if (tiffField == null) {
                return null;
            }
            fitsHeader = tiffField.getValueAsString(0);
            if (fitsHeader == null) {
                return null;
            }
            headerValues = FitsHeaderUtil.getHeaderValuesComments(fitsHeader);
        } catch (IOException e) {
            return null;
        }
        int width;
        int height;
        WCS2d wcs2d;
        try {
            wcs2d = WcsUtil.createWCS2d(fitsHeader);
        } catch (Exception e) {
            return null;
        }
        try {
            int[] naxis = wcs2d.getNAXIS();
            width = naxis[0];
            height = naxis[1];
        } catch (Exception e) {
            try {
                ImageAndMetadata imageAndMetadata = pictureImageLoader.loadImageData();
                width = imageAndMetadata.imageData.width;
                height = imageAndMetadata.imageData.height;
            } catch (Exception e2) {
                return null;
            }
        }

        ImageToSphericCoords imageToSphericCoords = createImageToSphericCoords(wcs2d, width, height, false);
        return new ImageToSphericCoordsAndHeader(imageToSphericCoords, headerValues);
    }

    public static Map<Integer, TIFFField> getTiffMetadata(File inputFile, Collection<Integer> keys) throws IOException {
        Map<Integer, TIFFField> map = new HashMap<>();
        try (ImageInputStream input = ImageIO.createImageInputStream(inputFile)) {
            ImageReader reader = ImageIO.getImageReaders(input).next();
            reader.setInput(input);
            IIOMetadata metadata = reader.getImageMetadata(0);
            TIFFDirectory ifd = TIFFDirectory.createFromMetadata(metadata);
            for (Integer key: keys) {
                TIFFField field = ifd.getTIFFField(key);
                map.put(key, field);
            }
        }
        return map;
    }

    /**
     * Return screen and spheric coordinates based on various places of image
     * @param wcs wcs to return coordinate
     * @param imageWidth image width
     * @param imageHeight image height
     * @param rowsFromBottomToTop true when first stored the bottom row and the top row as last (fits)
     * @return screen and spheric coordinates
     */
    public static ImageToSphericCoords createImageToSphericCoords(WCS2d wcs, int imageWidth, int imageHeight, boolean rowsFromBottomToTop) {
        PixelToSpheric pixelToSpheric = rowsFromBottomToTop ? new WCS2dRevertedPixelToSpheric(wcs, imageHeight)
                : new WCS2dPixelToSpheric(wcs);
        ImageToSphericCoords screenToPolarCoords = new ImageToSphericCoords(pixelToSpheric);
        return screenToPolarCoords;
    }

    public static class ImageToSphericCoordsAndHeader {
        public ImageToSphericCoords imageToSphericCoords;

        public List<FitsHeaderItem> headerValues;

        public ImageToSphericCoordsAndHeader(ImageToSphericCoords imageToSphericCoords, List<FitsHeaderItem> headerValues) {
            this.imageToSphericCoords = imageToSphericCoords;
            this.headerValues = headerValues;
        }
    }
}
