package org.maren.spview.imageloader;

public class FitsHeaderItem {
    /**
     * Left part of item up to '=' ecluding, or whole line when COMMENT or HISTORY
     */
    private String name;

    /**
     * It can be null when name is COMMENT or HISTORY
     */
    private String value;

    /**
     * It can be null when name is COMMENT or HISTORY but otherwise it shouldn't
     */
    private String comment;

    public FitsHeaderItem(String name, String value, String comment) {
        this.name = name.trim();
        this.value = value;
        this.comment = comment;
        if (this.value != null) {
            this.value = this.value.trim();
        }
        if (this.comment != null) {
            this.comment = this.comment.trim();
        }
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FitsHeaderItem{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
