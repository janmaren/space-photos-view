package org.maren.spview.imageloader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.maren.spview.affin.AffElementsParser;
import org.maren.spview.app.ImageToSphericCoords;
import org.maren.spview.rasterpos.pixeltospheric.AffElPixelToSpheric;
import org.maren.statis.affine.AffineElements;

import cz.jmare.file.PathUtil;
import cz.jmare.fits.header.InconsistentFitsException;

public class TxtAffinMetadataUtil {

    public static ImageToSphericCoords parseAffinPoints(String filePath) throws InconsistentFitsException {
        String[] pathSuff = PathUtil.parseFullPathSuffix(filePath);
        File affinFilePath = new File(pathSuff[0] + ".wld");
        if (!affinFilePath.isFile()) {
            return null;
        }
        
        List<String> lines = null;
        try {
            lines = Files.readAllLines(affinFilePath.toPath());
            if (lines.size() == 0) {
                return null;
            }
            AffineElements elements = AffElementsParser.parseLines(lines);
            AffElPixelToSpheric affElPixelToSpheric = new AffElPixelToSpheric(elements);
            return new ImageToSphericCoords(affElPixelToSpheric);
        } catch (IOException e) {
            throw new InconsistentFitsException("Unable to read " + affinFilePath);
        }
    }

}
