package org.maren.spview.imageloader;

import java.io.IOException;
import java.lang.ref.SoftReference;

import org.eclipse.swt.graphics.ImageData;

import cz.jmare.fits.image.DataTypeNotSupportedException;
import cz.jmare.fits.image.FITSImage;
import cz.jmare.fits.image.NoImageDataFoundException;
import cz.jmare.fits.util.FitsUtil;
import cz.jmare.swt.image.AwtSwtUtil;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.ImageHDU;

public class FitsImageLoader implements ImageLoader {
    private String filename;
    
    private int[] frame;
    
    private SoftReference<ImageData> imageDataRef;
    
    public FitsImageLoader(String filename) {
        super();
        this.filename = filename;
    }
    
    public FitsImageLoader(String filename, int[] frame) {
        super();
        this.filename = filename;
        this.frame = frame;
    }
    
    public FitsImageLoader(String filename, Fits fits, int[] frame) {
        super();
        this.filename = filename;
        this.frame = frame;
        this.imageDataRef = new SoftReference<ImageData>(loadScaled(fits, filename, frame));
    }

    @Override
    public ImageAndMetadata loadImageData() {
        try {
            ImageData imageData = imageDataRef != null ? imageDataRef.get() : null;
            if (imageData == null) {
                try (Fits fits = new Fits(filename)) {
                    imageData = loadScaled(fits, filename, frame);
                    this.imageDataRef = new SoftReference<ImageData>(imageData);
                }
            }
            return new ImageAndMetadata(imageData);
        } catch (FitsException | DataTypeNotSupportedException | NoImageDataFoundException | IOException e) {
            throw new IllegalStateException("Unable to load " + filename, e);
        }
    }

    private static ImageData loadScaled(Fits fits, String filename, int[] frame) {
        try {
            ImageHDU imageHDU = FitsUtil.imageHDU(fits);
            if (frame == null && FitsUtil.getAxesCount(imageHDU) == 3) {
                if (FitsUtil.getAxeDim(imageHDU, 2) == 3) {
                    FITSImage fitsImageR = new FITSImage(fits, 0, new int[] {0});
                    FITSImage fitsImageG = new FITSImage(fits, 0, new int[] {1});
                    FITSImage fitsImageB = new FITSImage(fits, 0, new int[] {2});
                    return AwtSwtUtil.convertToSWT(fitsImageR, fitsImageG, fitsImageB);
                }
            }
        } catch (Exception e) {
            // let's do fallback bellow
            e.printStackTrace();
        }
        try {
            FITSImage fitsImage = new FITSImage(fits, 0, frame);
            return AwtSwtUtil.convertToSWT(fitsImage);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to load " + filename, e);
        }
    }

    @Override
    public void setFullPath(String fullPath) {
        this.filename = fullPath;
    }
}
