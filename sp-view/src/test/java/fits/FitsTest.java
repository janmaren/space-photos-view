package fits;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.junit.jupiter.api.Test;

import cz.jmare.fits.image.DataTypeNotSupportedException;
import cz.jmare.fits.image.FITSImage;
import cz.jmare.fits.image.NoImageDataFoundException;
import cz.jmare.swt.image.AwtSwtUtil;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;

public class FitsTest {
    @Test
    public void fits() throws FitsException, IOException, DataTypeNotSupportedException, NoImageDataFoundException {
        File file = classpathFile("/dss.05.40.45.5-01.56.33.3-alnitak.fits");
        Fits fits = new Fits(file);
        FITSImage fitsImage = new FITSImage(fits);
        ImageIO.write(fitsImage, "PNG", new File("target/dss.05.40.45.5-01.56.33.3-alnitak.png")); 
        ImageData swtImage = AwtSwtUtil.convertToSWT(fitsImage);
        ImageLoader imageLoader = new ImageLoader();
        imageLoader.data = new ImageData[] { swtImage };
        imageLoader.save("target/dss.05.40.45.5-01.56.33.3-alnitak-swt.png", SWT.IMAGE_PNG);
        AwtSwtUtil.revertRows(swtImage);
        imageLoader.data = new ImageData[] { swtImage };
        imageLoader.save("target/dss.05.40.45.5-01.56.33.3-alnitak-swt-reverted.png", SWT.IMAGE_PNG);
    }
    
    private File classpathFile(String path) {
        URL url = FitsTest.class.getResource(path);
        File file = null;
        try {
            file = Paths.get(url.toURI()).toFile();
            return file;
        } catch (URISyntaxException e) {
            throw new IllegalStateException("unable to load " + path);
        }
    }
}
