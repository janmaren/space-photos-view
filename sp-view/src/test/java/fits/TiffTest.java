package fits;

import cz.jmare.file.FileUtil;
import cz.jmare.fits.header.WCS2d;
import cz.jmare.math.astro.RaDec;
import nom.tam.fits.Header;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TiffTest {
    @Test
    @Disabled
    void test1() throws IOException {
        String str = Files.readString(Paths.get("C:\\Users\\jmarencik\\wcs.txt"));
        String[] strings = FileUtil.splitToLinesQuick(str);
        WCS2d wcs2d = new WCS2d(new Header(strings));
        RaDec raDec = wcs2d.pixelToFK5(1917.0, 1703.0);
        System.out.println(raDec.toNormalizedRahDec());
    }

    @Test
    @Disabled
    void test2() throws IOException {
        String str = Files.readString(Paths.get("C:\\Users\\jmarencik\\file2.txt"));
        String[] strings = FileUtil.splitToLinesQuick(str);
        WCS2d wcs2d = new WCS2d(new Header(strings));
        RaDec raDec = wcs2d.pixelToFK5(249.6, 289.14);
        System.out.println(raDec.toNormalizedRahDec());
    }
}
