package quality;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;

public class QualCompar {
    //@Test
    public void testQual() throws IOException {
        BufferedImage tif = ImageIO.read(new File("C:\\Users\\jmarencik\\Pictures\\hvezdy-20220227\\stack\\alnitak-iso2000-10s\\convert\\DSC_0134-alnitak.tif"));
        BufferedImage jpg = ImageIO.read(new File("C:\\Users\\jmarencik\\Pictures\\hvezdy-20220227\\stack\\alnitak-iso2000-10s\\convert\\DSC_0134-alnitak.NEF.jpg"));
        int height = tif.getHeight();
        int width = tif.getWidth();
        long count = 0;
        long diffs = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                RGBEntity rgbTif = RGBEntUtil.rHighestToRGB(tif.getRGB(j, i));
                RGBEntity rgbJpg = RGBEntUtil.rHighestToRGB(jpg.getRGB(j, i));
                int diffR = Math.abs(rgbTif.red - rgbJpg.red);
                int diffG = Math.abs(rgbTif.green - rgbJpg.green);
                int diffB = Math.abs(rgbTif.blue - rgbJpg.blue);
                count += 3;
                diffs += diffR;
                diffs += diffG;
                diffs += diffB;
            }
        }
        System.out.println("Avg diff: " + (diffs / (double) count));
        System.out.println("diffs: " + diffs);
    }
}
