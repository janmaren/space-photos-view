# make sure you haven't snapshot in pom.xml, like <cz.jmare.version>1.61</cz.jmare.version>

# set some version
mvn versions:set -DgenerateBackupPoms=false

# and then
./release.sh

