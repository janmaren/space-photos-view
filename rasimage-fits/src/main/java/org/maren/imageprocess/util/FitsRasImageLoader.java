package org.maren.imageprocess.util;

import cz.jmare.fits.util.FitsUtil;
import cz.jmare.image.ras.RasImage;
import cz.jmare.image.ras.RasImageLoader;
import cz.jmare.image.ras.RasImages;
import cz.jmare.image.util.ImFilesUtil;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.ImageHDU;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class FitsRasImageLoader implements RasImageLoader {
    @Override
    public RasImage getRasImage(File file) throws IOException {
        try (Fits fits = openFits(file)) {
            return getRasImage(fits);
        }
    }

    @Override
    public Set<String> getSupportedFormats() {
        return Set.of("fit", "fits");
    }

    public RasImage getRasImage(Fits fits) throws IOException {
        try {
            return getRGBRasImage(fits);
        } catch (Exception e) {
            return getGreyRasImage(fits);
        }
    }

    public RasImage getRGBRasImage(Fits fits) throws IOException {
        try {
            ImageHDU imageHDU = FitsUtil.imageHDU(fits);
            int axesCount = FitsUtil.getAxesCount(imageHDU);
            if (axesCount == 3) {
                if (FitsUtil.getAxeDim(imageHDU, 2) == 3) {
                    return FitsBufImageUtil.rgbToRasImage(fits,
                            new int[]{0}, new int[]{1}, new int[]{2});
                }
            }
            throw new IOException("Unsupported file type");
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    public RasImage getGreyRasImage(Fits fits) throws IOException {
        try {
            ImageHDU imageHDU = FitsUtil.imageHDU(fits);
            int axesCount = FitsUtil.getAxesCount(imageHDU);
            if (axesCount == 2) {
                return FitsBufImageUtil.toRasImageSingle(fits);
            }
            throw new IOException("Unsupported file type");
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    private static Fits openFits(File file) throws IOException {
        try {
            return new Fits(file);
        } catch (FitsException e) {
            throw new IOException("Unable to open " + file, e);
        }
    }

    public static void main(String[] args) throws IOException {
        String source = "C:\\Users\\jmarencik\\Pictures\\pal\\pal5_3_8bit.fits";
        String dest = "C:\\Users\\jmarencik\\Pictures\\pal\\pal5_3_8bit.tiff";

        FitsRasImageLoader fitsRasImageLoader = new FitsRasImageLoader();
        RasImage rasImage = fitsRasImageLoader.getRasImage(new File(source));
        int bitsPerPixel = RasImages.getBitsPerPixel(rasImage, 32);
        if (bitsPerPixel == 8) {
            ImFilesUtil.save8BitImage(new File(dest), rasImage);
        } else if (bitsPerPixel == 16) {
            ImFilesUtil.save16BitImage(new File(dest), rasImage);
        } else {
            ImFilesUtil.saveFloatsImage(new File(dest), rasImage);
        }
    }
}
