package org.maren.imageprocess.util;

import cz.jmare.fits.image.DataTypeNotSupportedException;
import cz.jmare.fits.util.FitsUtil;
import cz.jmare.image.ras.RGB3ShortsRasImage;
import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RGBPix;
import cz.jmare.image.ras.RasBufImage;
import cz.jmare.image.ras.RasImage;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.ImageHDU;

public class FitsBufImageUtil {
    public static RasImage rgbToRasImage(Fits fits, int[] indexR, int[] indexG, int[] indexB) {
        ImageHDU hdu = FitsUtil.imageHDU(fits);

        int bitpix;
        try {
            bitpix = hdu.getBitPix();
        } catch (FitsException e) {
            throw new RuntimeException(e);
        }

        return switch (bitpix) {
            case 8 -> rgbToRasImage(getBytesData(hdu, indexR), getBytesData(hdu, indexG), getBytesData(hdu, indexB));
            case 16 -> rgbToRasImage(getShortsData(hdu, indexR), getShortsData(hdu, indexG), getShortsData(hdu, indexB));
            case 32 -> rgbToRasImage(getIntsData(hdu, indexR), getIntsData(hdu, indexG), getIntsData(hdu, indexB));
            case -32 -> rgbToRasImage(getFloatsData(hdu, indexR), getFloatsData(hdu, indexG), getFloatsData(hdu, indexB));
            case -64 -> rgbToRasImage(getDoublesData(hdu, indexR), getDoublesData(hdu, indexG), getDoublesData(hdu, indexB));
            default -> throw new DataTypeNotSupportedException(bitpix);
        };
    }


    public static RasImage toRasImageSingle(Fits fits) {
        ImageHDU hdu = FitsUtil.imageHDU(fits);

        int bitpix;
        try {
            bitpix = hdu.getBitPix();
        } catch (FitsException e) {
            throw new RuntimeException(e);
        }

        return switch (bitpix) {
            case 8 -> greyToRasImage(getBytesData(hdu, new int[0]));
            case 16 -> greyToRasImage(getShortsData(hdu, new int[0]));
            case 32 -> greyToRasImage(getIntsData(hdu, new int[0]));
            case -32 -> greyToRasImage(getFloatsData(hdu, new int[0]));
            case -64 -> greyToRasImage(getDoublesData(hdu, new int[0]));
            default -> throw new DataTypeNotSupportedException(bitpix);
        };
    }

    private static RasImage greyToRasImage(double[][] r) {
        int width = r[0].length;
        int height = r.length;
        RasBufImage rasBuf = RasBufImage.createFloatsInstance(width, height);
        RGBFloat rgbPix = new RGBFloat(0, 0, 0);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                float value = (float) r[height - y - 1][x];
                rgbPix.red = value;
                rgbPix.green = value;
                rgbPix.blue = value;
                rasBuf.setRGBFloat(x, y, rgbPix);
            }
        }
        return rasBuf;
    }

    private static RasImage greyToRasImage(float[][] r) {
        int width = r[0].length;
        int height = r.length;
        RasBufImage rasBuf = RasBufImage.createFloatsInstance(width, height);
        RGBFloat rgbPix = new RGBFloat(0, 0, 0);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                float value = r[height - y - 1][x];
                rgbPix.red = value;
                rgbPix.green = value;
                rgbPix.blue = value;
                rasBuf.setRGBFloat(x, y, rgbPix);
            }
        }
        return rasBuf;
    }

    private static RasImage greyToRasImage(int[][] r) {
        int width = r[0].length;
        int height = r.length;
        RasBufImage rasBuf = RasBufImage.createFloatsInstance(width, height);
        RGBFloat rgbPix = new RGBFloat(0, 0, 0);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                float value = (r[height - y - 1][x] + 2147483648f) / 0xffffffff;
                rgbPix.red = value;
                rgbPix.green = value;
                rgbPix.blue = value;
                rasBuf.setRGBFloat(x, y, rgbPix);
            }
        }
        return rasBuf;
    }

    private static RasImage greyToRasImage(byte[][] r) {
        int width = r[0].length;
        int height = r.length;
        RasBufImage rasBuf = RasBufImage.createBytesInstance(width, height, false);
        RGBPix rgbPix = new RGBPix(0, 0, 0);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                byte value = r[height - y - 1][x];
                rgbPix.red = value;
                rgbPix.green = value;
                rgbPix.blue = value;
                rasBuf.setRGB(x, y, rgbPix);
            }
        }
        return rasBuf;
    }

    private static RasImage greyToRasImage(short[][] r) {
        int width = r[0].length;
        int height = r.length;
        RGB3ShortsRasImage rasBuf = (RGB3ShortsRasImage) RasBufImage.createShortsInstance(width, height);
        RGBFloat rgbPix = new RGBFloat(0, 0, 0);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                float value = (float) (r[height - y - 1][x] + 32768) / 0xffff;
                rgbPix.red = value;
                rgbPix.green = value;
                rgbPix.blue = value;
                rasBuf.setRGBFloat(x, y, rgbPix);
            }
        }
        return rasBuf;
    }

    private static RasImage rgbToRasImage(double[][] r, double[][] g, double[][] b) {
        int width = r[0].length;
        int height = r.length;
        RasBufImage rasBuf = RasBufImage.createFloatsInstance(width, height);
        RGBFloat rgbPix = new RGBFloat(0, 0, 0);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                rgbPix.red = (float) r[height - y - 1][x];
                rgbPix.green = (float) g[height - y - 1][x];
                rgbPix.blue = (float) b[height - y - 1][x];
                rasBuf.setRGBFloat(x, y, rgbPix);
            }
        }
        return rasBuf;
    }

    private static RasImage rgbToRasImage(float[][] r, float[][] g, float[][] b) {
        int width = r[0].length;
        int height = r.length;
        RasBufImage rasBuf = RasBufImage.createFloatsInstance(width, height);
        RGBFloat rgbPix = new RGBFloat(0, 0, 0);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                rgbPix.red = r[height - y - 1][x];
                rgbPix.green = g[height - y - 1][x];
                rgbPix.blue = b[height - y - 1][x];
                rasBuf.setRGBFloat(x, y, rgbPix);
            }
        }
        return rasBuf;
    }

    private static RasImage rgbToRasImage(int[][] r, int[][] g, int[][] b) {
        int width = r[0].length;
        int height = r.length;
        RasBufImage rasBuf = RasBufImage.createFloatsInstance(width, height);
        RGBFloat rgbPix = new RGBFloat(0, 0, 0);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                rgbPix.red = (r[height - y - 1][x] + 2147483648f) / 0xffffffff;
                rgbPix.green = (g[height - y - 1][x] + 2147483648f) / 0xffffffff;
                rgbPix.blue = (b[height - y - 1][x] + 2147483648f) / 0xffffffff;
                rasBuf.setRGBFloat(x, y, rgbPix);
            }
        }
        return rasBuf;
    }

    private static RasImage rgbToRasImage(short[][] r, short[][] g, short[][] b) {
        int width = r[0].length;
        int height = r.length;
        RGB3ShortsRasImage rasBuf = (RGB3ShortsRasImage) RasBufImage.createShortsInstance(width, height);
        RGBFloat rgbPix = new RGBFloat(0, 0, 0);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                rgbPix.red = (float) (r[height - y - 1][x] + 32768) / 0xffff;
                rgbPix.green = (float) (g[height - y - 1][x] + 32768) / 0xffff;
                rgbPix.blue = (float) (b[height - y - 1][x] + 32768) / 0xffff;
                rasBuf.setRGBFloat(x, y, rgbPix);
            }
        }
        return rasBuf;
    }

    private static RasImage rgbToRasImage(byte[][] r, byte[][] g, byte[][] b) {
        int width = r[0].length;
        int height = r.length;
        RasBufImage rasBuf = RasBufImage.createBytesInstance(width, height, false);
        RGBPix rgbPix = new RGBPix(0, 0, 0);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                rgbPix.red = r[height - y - 1][x];
                rgbPix.green = g[height - y - 1][x];
                rgbPix.blue = b[height - y - 1][x];
                rasBuf.setRGB(x, y, rgbPix);
            }
        }
        return rasBuf;
    }


    public static byte[][] getBytesData(ImageHDU imageHDU, int[] itemIndex) {
        Object data = imageHDU.getData().getData();
        if (itemIndex == null || itemIndex.length == 0) {
            return (byte[][]) data;
        }
        if (itemIndex.length == 1) {
            return ((byte[][][]) data)[itemIndex[0]];
        }
        if (itemIndex.length == 2) {
            return ((byte[][][][]) data)[itemIndex[0]][itemIndex[1]];
        }
        throw new IllegalStateException("Not supported dimension " + (itemIndex.length + 2));
    }

    public static short[][] getShortsData(ImageHDU imageHDU, int[] itemIndex) {
        Object data = imageHDU.getData().getData();
        if (itemIndex == null || itemIndex.length == 0) {
            return (short[][]) data;
        }
        if (itemIndex.length == 1) {
            return ((short[][][]) data)[itemIndex[0]];
        }
        if (itemIndex.length == 2) {
            return ((short[][][][]) data)[itemIndex[0]][itemIndex[1]];
        }
        throw new IllegalStateException("Not supported dimension " + (itemIndex.length + 2));
    }

    public static int[][] getIntsData(ImageHDU imageHDU, int[] itemIndex) {
        Object data = imageHDU.getData().getData();
        if (itemIndex == null || itemIndex.length == 0) {
            return (int[][]) data;
        }
        if (itemIndex.length == 1) {
            return ((int[][][]) data)[itemIndex[0]];
        }
        if (itemIndex.length == 2) {
            return ((int[][][][]) data)[itemIndex[0]][itemIndex[1]];
        }
        throw new IllegalStateException("Not supported dimension " + (itemIndex.length + 2));
    }

    public static float[][] getFloatsData(ImageHDU imageHDU, int[] itemIndex) {
        Object data = imageHDU.getData().getData();
        if (itemIndex == null || itemIndex.length == 0) {
            return (float[][]) data;
        }
        if (itemIndex.length == 1) {
            return ((float[][][]) data)[itemIndex[0]];
        }
        if (itemIndex.length == 2) {
            return ((float[][][][]) data)[itemIndex[0]][itemIndex[1]];
        }
        throw new IllegalStateException("Not supported dimension " + (itemIndex.length + 2));
    }

    public static double[][] getDoublesData(ImageHDU imageHDU, int[] itemIndex) {
        Object data = imageHDU.getData().getData();
        if (itemIndex == null || itemIndex.length == 0) {
            return (double[][]) data;
        }
        if (itemIndex.length == 1) {
            return ((double[][][]) data)[itemIndex[0]];
        }
        if (itemIndex.length == 2) {
            return ((double[][][][]) data)[itemIndex[0]][itemIndex[1]];
        }
        throw new IllegalStateException("Not supported dimension " + (itemIndex.length + 2));
    }
}
