export myhome=$HOME
if [[ $HOME == /c/* ]]
then
        myhome="c:/"
        myhome+=${HOME:3}
fi

prjDir=`pwd`
cd $myhome/git/space-photos-view/rasimage-fits/
git pull
cd $prjDir

mvn versions:set -DremoveSnapshot -DgenerateBackupPoms=false
mvn clean deploy -Dmyhome=$myhome
if [[ $? -ne 0 ]] ; then
    exit 1
fi
mvn versions:set -DnextSnapshot -DgenerateBackupPoms=false

cd $myhome/git/jmare-maven-repo/repository/
git add --all
git commit -m "new libraries"
git push

cd $prjDir
git add --all
git commit -m "set development version"
git push
