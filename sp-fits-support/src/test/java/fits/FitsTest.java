package fits;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.junit.jupiter.api.Test;

import cz.jmare.fits.header.WCS2d;
import cz.jmare.fits.image.DataTypeNotSupportedException;
import cz.jmare.fits.image.FITSImage;
import cz.jmare.fits.image.NoImageDataFoundException;
import cz.jmare.fits.util.FitsUtil;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.astro.RahDec;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.ImageHDU;

public class FitsTest {
    @Test
    public void fits3dTo2d() throws FitsException, IOException, DataTypeNotSupportedException, NoImageDataFoundException {
        File file = classpathFile("/WFPC2u5780205r_c0fx.fits");
        Fits fits = new Fits(file);
        FITSImage fitsImage = new FITSImage(fits, 1, new int[] {0});
        ImageIO.write(fitsImage, "PNG", new File("target/WFPC2u5780205r_c0fx.png")); 
    }
    
    @Test
    public void fitsSimpleWcs() throws FitsException, IOException, DataTypeNotSupportedException, NoImageDataFoundException {
        File file = classpathFile("/WFPC2u5780205r_c0fx.fits");
        WCS2d wcs = new WCS2d(new Fits(file));
        RaDec pixelToPolarDeg = wcs.pixelToRaDec(100, 100);
        CompPolarRaDec.assertClose(new RahDec("12:10:32.85, +39:24:00.06"), pixelToPolarDeg.toNormalizedRahDec());
        
        RaDec polarDeg2 = wcs.pixelToRaDec(104, 200 - 125 - 1);
        CompPolarRaDec.assertClose(new RahDec("12:10:32.937+39:23:59.62"), polarDeg2.toNormalizedRahDec());
    }
    
    @Test
    public void fits3dTo2dOpt() throws FitsException, IOException, DataTypeNotSupportedException, NoImageDataFoundException {
        File file = classpathFile("/m82rad.fits");
        Fits fits = new Fits(file);
        FITSImage fitsImage = new FITSImage(fits, 0, new int[] {0, 0});
        int width = fitsImage.getWidth();
        int height = fitsImage.getHeight();
        ImageHDU imageHDU = FitsUtil.imageHDU(fits);
        WCS2d wcs = new WCS2d(imageHDU);
        RaDec polarDeg = wcs.pixelToRaDec(258, 262 - 120 - 1);
    }
    
    private File classpathFile(String path) {
        URL url = FitsTest.class.getResource(path);
        File file = null;
        try {
            file = Paths.get(url.toURI()).toFile();
            return file;
        } catch (URISyntaxException e) {
            throw new IllegalStateException("unable to load " + path);
        }
    }
}
