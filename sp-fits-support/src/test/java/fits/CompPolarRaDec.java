package fits;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.math.astro.RahDec;

public class CompPolarRaDec {
    public static double distance(RahDec p1, RahDec p2) {
        double p1Hours = ACooUtil.hoursToDegrees(p1.rah);
        double p2Hours = ACooUtil.hoursToDegrees(p2.rah);
        double p1Dec = p1.dec;
        double p2Dec = p2.dec;
        double dist = Math.sqrt((p1Hours - p2Hours) * (p1Hours - p2Hours) + (p1Dec - p2Dec) * (p1Dec - p2Dec));
        return dist;
    }

    /**
     * For testing purposes
     * @param p1
     * @param p2
     * @return true when both coordinates are closer than about 0d0m0.1s
     */
    public static boolean closeEnough(RahDec p1, RahDec p2) {
        double distance = distance(p1, p2);
        return distance < 9e-4;
    }

    public static void assertClose(RahDec p1, RahDec p2) {
        double distance = distance(p1, p2);
        if (distance < 9e-4) {
            return;
        }
        throw new AssertionError("Expected max distance 9e-4 but it is " + distance);
    }

    public static void main(String[] args) {
        RahDec polarRaDec1 = new RahDec("0:0:0.1 +40:0:0.0");
        RahDec polarRaDec2 = new RahDec("0:0:0 +40:0:0.0");
        System.out.println(distance(polarRaDec1, polarRaDec2));
    }
}
