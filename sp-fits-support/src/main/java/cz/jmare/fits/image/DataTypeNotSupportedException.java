package cz.jmare.fits.image;

public class DataTypeNotSupportedException extends RuntimeException {
    private static final long serialVersionUID = 3091781944353290460L;

    public DataTypeNotSupportedException(int bitpix) {
        super(bitpix + " is not a valid FITS data type.");
    }
}