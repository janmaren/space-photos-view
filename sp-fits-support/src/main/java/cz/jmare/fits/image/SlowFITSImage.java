package cz.jmare.fits.image;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferUShort;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Vector;

import nom.tam.fits.BasicHDU;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.ImageHDU;

public class SlowFITSImage extends FITSImage {
    protected Fits fits;
    protected ImageHDU imageHDU;

    protected int scaleMethod;
    protected short[] scaledData = null;
    protected double sigma = Double.NaN;
    protected double min = Double.NaN;
    protected double max = Double.NaN;
    protected Histogram histogram;
    protected BufferedImage delegate;
    
    public SlowFITSImage(Fits fits)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(fits, SCALE_LINEAR, null);
    }

    public SlowFITSImage(Fits fits, int scaleMethod,
            int[] itemIndex)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(fits, createScaledImage(fits, scaleMethod, itemIndex), scaleMethod);
    }

    public SlowFITSImage(Fits fits, BufferedImage delegate, int scaleMethod)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        super(delegate.getColorModel(), delegate.getRaster().createCompatibleWritableRaster(), true, null);

        setFits(fits);
        setHistogram((Histogram) delegate.getProperty("histogram"));
        setDelegate(delegate);
        this.scaleMethod = scaleMethod;
        scaledData = (short[]) delegate.getProperty("scaledData");

        ImageHDU imageHDU = (ImageHDU) delegate.getProperty("imageHDU");
        if (imageHDU == null || !(imageHDU instanceof ImageHDU)) {
            imageHDU = findFirstImageHDU(fits);
        }
        setImageHDU(imageHDU);

        min = getHistogram().getMin();
        max = getHistogram().getMax();
        sigma = getHistogram().estimateSigma();
    }

    public SlowFITSImage(File file)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(file));
    }

    public SlowFITSImage(File file, int scaleMethod)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(file), scaleMethod, null);
    }

    public SlowFITSImage(String filename)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(filename));
    }

    public SlowFITSImage(String filename, int scaleMethod)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(filename), scaleMethod, null);
    }

    public SlowFITSImage(URL url)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(url));
    }

    public SlowFITSImage(URL url, int scaleMethod)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(url), scaleMethod, null);
    }

    public static String[] getScaleNames() {
        return ScaleUtils.getScaleNames();
    }

    public Fits getFits() {
        return fits;
    }

    public ImageHDU getImageHDU() {
        return imageHDU;
    }

    protected void setHistogram(Histogram histogram) {
        this.histogram = histogram;
    }

    public Histogram getHistogram() {
        return this.histogram;
    }

    public double getOriginalValue(int x, int y, int[] itemIndex) throws FitsException {
        double result = Double.NaN;
        double bZero = getImageHDU().getBZero();
        double bScale = getImageHDU().getBScale();
        ImageHDU hdu = getImageHDU();

        switch (getImageHDU().getBitPix()) {
        case 8:
            int dataVal = (getBytesData(hdu, itemIndex))[y][x];
            if (dataVal < 0) {
                dataVal += 256;
            }
            result = bZero + bScale * dataVal;
            break;
        case 16:
            result = bZero + bScale * ((double) (getShortsData(hdu, itemIndex))[y][x]);
            break;
        case 32:
            result = bZero + bScale * ((double) (getIntsData(hdu, itemIndex))[y][x]);
            break;
        case -32:
            result = bZero + bScale * ((double) (getFloatsData(hdu, itemIndex))[y][x]);
            break;
        case -64:
            result = bZero + bScale * (getDoublesData(hdu, itemIndex))[y][x];
            break;
        default:
            break;
        }

        return result;
    }

    public int getScaleMethod() {
        return this.scaleMethod;
    }

    public void setScaleMethod(int scaleMethod)
    // throws FitsException, DataTypeNotSupportedException
    {
        if (scaleMethod != this.scaleMethod) {
            try {
                setDelegate(
                        createScaledImage(getImageHDU(), scaledData, getHistogram(), min, max, sigma, scaleMethod));
                this.scaleMethod = scaleMethod;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void rescale(double min, double max, double sigma) {
        if (min != this.min || max != this.max || sigma != this.sigma) {
            try {
                this.min = min;
                this.max = max;
                this.sigma = sigma;
                setDelegate(
                        createScaledImage(getImageHDU(), scaledData, getHistogram(), min, max, sigma, scaleMethod));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    protected BufferedImage getDelegate() {
        return this.delegate;
    }

    protected void setDelegate(BufferedImage delegate) {
        this.delegate = delegate;
    }

    // BEGIN BufferedImage METHODS
    /*
     * public void addTileObserver(TileObserver to) { // throw new
     * RuntimeException(new Exception().getStackTrace()[0]. // getMethodName() +
     * " not supported"); getDelegate().addTileObserver(to); }
     * 
     * public void coerceData(boolean isAlphaPremultiplied) { // throw new
     * RuntimeException(new Exception().getStackTrace()[0]. // getMethodName() +
     * " not supported"); getDelegate().coerceData(isAlphaPremultiplied); }
     */

    public WritableRaster copyData(WritableRaster outRaster) {
        return getDelegate().copyData(outRaster);
    }

    public Graphics2D createGraphics() {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public void flush() {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public WritableRaster getAlphaRaster() {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public ColorModel getColorModel() {
        return getDelegate().getColorModel();
    }

    public Raster getData() {
        return getDelegate().getData();
    }

    public Raster getData(Rectangle rect) {
        return getDelegate().getData(rect);
    }

    public Graphics getGraphics() {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public int getHeight() {
        return getDelegate().getHeight();
    }

    public int getHeight(ImageObserver observer) {
        return getDelegate().getHeight(observer);
    }

    public int getMinTileX() {
        return getDelegate().getMinTileX();
    }

    public int getMinTileY() {
        return getDelegate().getMinTileY();
    }

    public int getMinX() {
        return getDelegate().getMinX();
    }

    public int getMinY() {
        return getDelegate().getMinY();
    }

    public int getNumXTiles() {
        return getDelegate().getNumXTiles();
    }

    public int getNumYTiles() {
        return getDelegate().getNumYTiles();
    }

    public Object getProperty(String name) {
        return getDelegate().getProperty(name);
    }

    public Object getProperty(String name, ImageObserver observer) {
        return getDelegate().getProperty(name, observer);
    }

    public String[] getPropertyNames() {
        return getDelegate().getPropertyNames();
    }

    public WritableRaster getRaster() {
        return getDelegate().getRaster();
    }

    public int getRGB(int x, int y) {
        return getDelegate().getRGB(x, y);
    }

    public int[] getRGB(int startX, int startY, int w, int h, int[] rgbArray, int offset, int scansize) {
        return getDelegate().getRGB(startX, startY, w, h, rgbArray, offset, scansize);
    }

    public SampleModel getSampleModel() {
        return getDelegate().getSampleModel();
    }

    public ImageProducer getSource() {
        return getDelegate().getSource();
    }

    public Vector<RenderedImage> getSources() {
        return getDelegate().getSources();
    }

    public BufferedImage getSubimage(int x, int y, int w, int h) {
        return getDelegate().getSubimage(x, y, w, h);
    }

    public Raster getTile(int tileX, int tileY) {
        return getDelegate().getTile(tileX, tileY);
    }

    public int getTileGridXOffset() {
        return getDelegate().getTileGridXOffset();
    }

    public int getTileGridYOffset() {
        return getDelegate().getTileGridYOffset();
    }

    public int getTileHeight() {
        return getDelegate().getTileHeight();
    }

    public int getTileWidth() {
        return getDelegate().getTileWidth();
    }

    public int getType() {
        return getDelegate().getType();
    }

    public int getWidth() {
        return getDelegate().getWidth();
    }

    public int getWidth(ImageObserver observer) {
        return getDelegate().getWidth(observer);
    }

    public WritableRaster getWritableTile(int tileX, int tileY) {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public Point[] getWritableTileIndices() {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public boolean hasTileWriters() {
        return false;
    }

    public boolean isAlphaPremultiplied() {
        return true;
    }

    public boolean isTileWritable(int tileX, int tileY) {
        return false;
    }

    public void releaseWritableTile(int tileX, int tileY) {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    /*
     * public void removeTileObserver(TileObserver to) { // throw new
     * RuntimeException(new Exception().getStackTrace()[0]. // getMethodName() +
     * " not supported"); getDelegate().removeTileObserver(to); }
     */

    public void setData(Raster r) {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public void setRGB(int x, int y, int rgb) {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public void setRGB(int startX, int startY, int w, int h, int[] rgbArray, int offset, int scansize) {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public String toString() {
        return getDelegate().toString();
    }

    // END BufferedImage METHODS

    protected void setFits(Fits fits) {
        this.fits = fits;
    }

    protected void setImageHDU(ImageHDU imageHDU) {
        this.imageHDU = imageHDU;
    }

    /*
     * protected void setScaledImages(BufferedImage[] scaledImages) { _scaledImages
     * = scaledImages; }
     * 
     * protected BufferedImage[] getScaledImages() { return _scaledImages; }
     */

    protected static ImageHDU findFirstImageHDU(Fits fits) throws FitsException, IOException {
        ImageHDU result = null;
        int i = 0;

        for (BasicHDU<?> hdu = fits.getHDU(i); hdu != null && result == null; ++i) {
            if (hdu instanceof ImageHDU) {
                result = (ImageHDU) hdu;
            }
        }

        return result;
    }

    protected static BufferedImage createScaledImage(Fits fits, int scaleMethod,
            int[] itemIndex)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        BufferedImage result = null;
        ImageHDU imageHDU = findFirstImageHDU(fits);

        if (imageHDU != null) {
            result = createScaledImage(imageHDU, scaleMethod, itemIndex);
        } else {
            throw new NoImageDataFoundException();
        }

        return result;
    }

    /**
     * @return An array of BufferedImages from hdu with intensity values scaled to
     *         short range using linear, log, square root, and square scales, in
     *         that order.
     */
    public static BufferedImage createScaledImage(ImageHDU hdu, int scaleMethod, int[] itemIndex)
            throws FitsException, DataTypeNotSupportedException {
        int bitpix = hdu.getBitPix();
        double bZero = hdu.getBZero();
        double bScale = hdu.getBScale();
        Histogram hist = null;

        switch (bitpix) {
        case 8:
            hist = ScaleUtils.computeHistogram(getBytesData(hdu, itemIndex), bZero, bScale);
            break;
        case 16:
            hist = ScaleUtils.computeHistogram(getShortsData(hdu, itemIndex), bZero, bScale);
            break;
        case 32:
            hist = ScaleUtils.computeHistogram(getIntsData(hdu, itemIndex), bZero, bScale);
            break;
        case -32:
            hist = ScaleUtils.computeHistogram(getFloatsData(hdu, itemIndex), bZero, bScale);
            break;
        case -64:
            hist = ScaleUtils.computeHistogram(getDoublesData(hdu, itemIndex), bZero, bScale);
            break;
        default:
            throw new DataTypeNotSupportedException(bitpix);
        }

        return createScaledImage(hdu, null, hist, hist.getMin(), hist.getMax(), hist.estimateSigma(), scaleMethod);
    }

    public static BufferedImage createScaledImage(ImageHDU hdu, short[] result, Histogram hist, double min, double max,
            double sigma, int scaleMethod) throws FitsException, DataTypeNotSupportedException {
        Object data = hdu.getData().getData();
        int width = hdu.getAxes()[1]; // yes, the axes are in the wrong order
        int height = hdu.getAxes()[0];
        double bZero = hdu.getBZero();
        double bScale = hdu.getBScale();
        short[] scaledData = SlowScaleUtils.scale(data, result, width, height, bZero, bScale, min, max, sigma, hist,
                scaleMethod);

        ColorModel cm = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), false, false,
                Transparency.OPAQUE, DataBuffer.TYPE_USHORT);
        SampleModel sm = cm.createCompatibleSampleModel(width, height);

        Hashtable<String, Object> properties = new Hashtable<String, Object>();
        properties.put("histogram", hist);
        properties.put("imageHDU", hdu);
        properties.put("scaledData", scaledData);

        DataBuffer db = new DataBufferUShort(scaledData, height);
        WritableRaster r = Raster.createWritableRaster(sm, db, null);

        return new BufferedImage(cm, r, false, properties);
    }
}
