package cz.jmare.fits.image;

public class Histogram {
    protected double min;
    protected double max;
    protected double range;
    protected int[] counts;
    protected double[] cdf;
    protected int totalCount;
    
    public Histogram(double min, double max, int totalCount, int[] counts) {
        if (max < min) {
            throw new IllegalArgumentException("max is less than min: max = " + max + " min = " + min);
        }

        setMin(min);
        setMax(max);
        setRange(max - min);
        setTotalCount(totalCount);
        setCounts(counts);
        cdf = calculateCdf(counts, totalCount);
    }

    public double getMin() {
        return this.min;
    }

    public double getMax() {
        return this.max;
    }

    public double getRange() {
        return this.range;
    }

    /*
     * public int getCount(short value) { return _counts[value - Short.MIN_VALUE]; }
     */

    public int getTotalCount() {
        return this.totalCount;
    }

    public int[] getCounts() {
        return this.counts;
    }

    /**
     * @return A histogram equalized value for <CODE>val</CODE>. <CODE>val</CODE>
     *         must be in the range [0, 65535], and is interpreted to be a linear
     *         scaling of an original data value.
     */
    public int getEqualizedValue(int val) {
        double v = cdf[val];
        double vMin = cdf[0];

        return (int) (((v - vMin) / (1.0 - vMin)) * (double) (counts.length - 1) + 0.5);
    }

    /**
     * @return A reasonable guess at a good sigma value for the inverse hyperbolic
     *         sine scaling. The estimate is made by finding the mode value of the
     *         histogram, then moving to the next bin and returning the value (in
     *         the original data range with bZero and bScale applied) at the
     *         "bottom" of that bin. The current implementation is actually a
     *         mangled implementation which happens to produce better sigmas than
     *         the correct implementation. An explanation for this behavior is
     *         currently being investigated.
     */
    public double estimateSigma() {
        double modeIndex = (double) findModeIndex(counts);
        double linearScaleFactor = (Math.pow(2.0, 16.0) - 1.0) / (max - min);

        return (modeIndex + 1.0) / linearScaleFactor;
    }

    protected static int findModeIndex(int[] counts) {
        int index = 0;

        for (int i = 0; i < counts.length; ++i) {
            if (counts[i] > counts[index]) {
                index = i;
            }
        }

        return index;
    }

    /**
     * @return An instance of <CODE>Bounds</CODE> containing the min and max values
     *         that will result in keeping <CODE>percentKept</CODE> percent of the
     *         values in the image. <CODE>percentKept</CODE> is a slightly deceptive
     *         name, as it must be in the range [0, 1].
     */
    public Bounds calculateBounds(double percentKept) throws IllegalArgumentException {
        if (percentKept < 0.0 || percentKept > 1.0) {
            throw new IllegalArgumentException("percentKept must be in [0 ... 1]");
        }

        double halfPercentDiscarded = (1.0 - percentKept) / 2.0;

        int lowIndex = 0;
        double lowCount = 0.0;
        double totalCount = (double) getTotalCount();
        boolean done = false;

        while (!done && lowIndex < counts.length && (lowCount / totalCount < halfPercentDiscarded)) {
            lowCount += (double) counts[lowIndex];

            if (lowCount / totalCount < halfPercentDiscarded) {
                ++lowIndex;
            } else {
                done = true;
            }
        }

        int highIndex = getCounts().length - 1;
        double highCount = 0.0;
        done = false;

        while (!done && highIndex >= 0 && (highCount / totalCount < halfPercentDiscarded)) {
            highCount += (double) counts[highIndex];

            if (highCount / totalCount < halfPercentDiscarded) {
                --highIndex;
            } else {
                done = true;
            }
        }

        return new Bounds(lowIndex, highIndex);
    }

    /**
     * A container class used by the <CODE>calculateBounds</CODE> method.
     */
    public static class Bounds {
        public Bounds(double low, double high) {
            this.low = low;
            this.high = high;
        }

        public String toString() {
            return getClass().getName() + " low = " + low + " high = " + high;
        }

        public double low;
        public double high;
    }

    protected static double[] calculateCdf(int[] counts, int totalCount) {
        double[] result = new double[counts.length];
        int cumulativeCount = 0;

        for (int i = 0; i < result.length; ++i) {
            cumulativeCount += counts[i];
            result[i] = (double) cumulativeCount / (double) totalCount;
        }

        return result;
    }

    protected void setMin(double min) {
        this.min = min;
    }

    protected void setMax(double max) {
        this.max = max;
    }

    protected void setRange(double range) {
        this.range = range;
    }

    protected void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    protected void setCounts(int[] counts) {
        this.counts = counts;
    }

}

