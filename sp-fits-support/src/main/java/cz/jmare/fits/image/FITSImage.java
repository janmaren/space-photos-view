package cz.jmare.fits.image;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferUShort;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Vector;

import cz.jmare.fits.util.FitsUtil;
import nom.tam.fits.BasicHDU;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.ImageHDU;

/**
 * The scaling algorithms offered are linear, log, square root, square,
 * histogram equalization, and inverse hyperbolic sine. Note that the histogram
 * equalization algorithm is just that; it works to fit the values to a uniform
 * distribution curve. The inverse hyperbolic sine scaling has linear behavior
 * below the sigma parameter and logarithmic behavior above the sigma parameter.
 */
public class FITSImage extends BufferedImage {
    public static final int SCALE_LINEAR = ScaleUtils.LINEAR;
    public static final int SCALE_LOG = ScaleUtils.LOG;
    public static final int SCALE_SQUARE_ROOT = ScaleUtils.SQUARE_ROOT;
    public static final int SCALE_SQUARE = ScaleUtils.SQUARE;
    public static final int SCALE_HISTOGRAM_EQUALIZATION = ScaleUtils.HIST_EQ;
    public static final int SCALE_ASINH = ScaleUtils.ASINH;

    protected Fits fits;
    protected ImageHDU imageHDU;

    protected int scaleMethod;
    protected double sigma = Double.NaN;
    protected double min = Double.NaN;
    protected double max = Double.NaN;
    protected BufferedImage[] scaledImages;
    
    public FITSImage(Fits fits)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(fits, SCALE_LINEAR, null);
    }

    /**
     * 
     * @param fits
     * @param scaleMethod
     * @param itemIndex   Index besides x, y (first 2 dimensions omitted). Value {3}
     *                    means Z1 axe with 3 items, {3, 5} means Z2 axe with 3
     *                    items and each is Z1 with 5 items... When null the naxis=2
     *                    supposed
     * @throws FitsException
     * @throws DataTypeNotSupportedException
     * @throws NoImageDataFoundException
     * @throws IOException
     */
    public FITSImage(Fits fits, int scaleMethod, int... itemIndex)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(fits, createScaledImages(fits, itemIndex), scaleMethod);
    }

    public FITSImage(Fits fits, int... itemIndex)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(fits, createScaledImages(fits, itemIndex), 0);
    }
    
    public FITSImage(Fits fits, BufferedImage[] scaledImages, int scaleMethod)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        super(scaledImages[0].getColorModel(), scaledImages[0].getRaster().createCompatibleWritableRaster(), true,
                null);

        setFits(fits);
        setScaledImages(scaledImages);
        setScaleMethod(scaleMethod);

        ImageHDU imageHDU = (ImageHDU) scaledImages[0].getProperty("imageHDU");
        if (imageHDU == null || !(imageHDU instanceof ImageHDU)) {
            imageHDU = findFirstImageHDU(fits);
        }
        setImageHDU(imageHDU);

        min = getHistogram().getMin();
        max = getHistogram().getMax();
    }

    public FITSImage(File file)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(file));
    }

    public FITSImage(File file, int scaleMethod)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(file), scaleMethod, null);
    }

    public FITSImage(String filename)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(filename));
    }

    public FITSImage(String filename, int scaleMethod)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(filename), scaleMethod, null);
    }

    public FITSImage(URL url)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(url));
    }

    public FITSImage(URL url, int scaleMethod)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        this(new Fits(url), scaleMethod, null);
    }

    /**
     * @return Printable names of the different scaling algorithms, indexed as
     *         <CODE>SCALE_LINEAR</CODE>, <CODE>SCALE_LOG</CODE>, etc.
     */
    public static String[] getScaleNames() {
        return ScaleUtils.getScaleNames();
    }

    public Fits getFits() {
        return fits;
    }

    public ImageHDU getImageHDU() {
        return imageHDU;
    }

    public Histogram getHistogram() {
        return (Histogram) getScaledImages()[SCALE_LINEAR].getProperty("histogram");
    }

    /**
     * @return The actual data value at postion (x, y), with bZero and bScale
     *         applied.
     */
    public double getOriginalValue(int x, int y) throws FitsException {
        double result = Double.NaN;
        double bZero = getImageHDU().getBZero();
        double bScale = getImageHDU().getBScale();
        Object data = getImageHDU().getData().getData();

        switch (getImageHDU().getBitPix()) {
        case 8:
            int dataVal = ((byte[][]) data)[y][x];
            if (dataVal < 0) {
                dataVal += 256;
            }
            result = bZero + bScale * dataVal;
            break;
        case 16:
            result = bZero + bScale * ((double) ((short[][]) data)[y][x]);
            break;
        case 32:
            result = bZero + bScale * ((double) ((int[][]) data)[y][x]);
            break;
        case -32:
            result = bZero + bScale * ((double) ((float[][]) data)[y][x]);
            break;
        case -64:
            result = bZero + bScale * ((double[][]) data)[y][x];
            break;
        default:
            break;
        }

        return result;
    }

    /**
     * @return One of <CODE>SCALE_LINEAR</CODE>, <CODE>SCALE_LOG</CODE>, etc.
     */
    public int getScaleMethod() {
        return this.scaleMethod;
    }

    /**
     * <CODE>scaleMethod</CODE> must be one of <CODE>SCALE_LINEAR</CODE>,
     * <CODE>SCALE_LOG</CODE>, etc. The image must be redrawn after this method is
     * called for the change to be visible.
     */
    public void setScaleMethod(int scaleMethod) {
        this.scaleMethod = scaleMethod;
    }

    /**
     * Rescales the image with the given min and max range values.
     * <CODE>sigma</CODE> is used for the inverse hyperbolic sine scaling as the
     * value (in the range of the data values with bZero and bScale applied) at
     * which the behavior becomes more logarithmic and less linear. The image must
     * be redrawn after this method is called for the change to be visible.
     */
    public void rescale(double min, double max, double sigma, int[] itemIndex)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        if (min != this.min || max != this.max || sigma != this.sigma) {
            this.min = min;
            this.max = max;
            this.sigma = sigma;
            setScaledImages(createScaledImages(getImageHDU(), getHistogram(), min, max, sigma, itemIndex));
            setScaleMethod(getScaleMethod());
        }
    }

    protected BufferedImage getDelegate() {
        return getScaledImages()[getScaleMethod()];
    }

    // BEGIN BufferedImage METHODS
    /*
     * public void addTileObserver(TileObserver to) { // throw new
     * RuntimeException(new Exception().getStackTrace()[0]. // getMethodName() +
     * " not supported"); getDelegate().addTileObserver(to); }
     * 
     * public void coerceData(boolean isAlphaPremultiplied) { // throw new
     * RuntimeException(new Exception().getStackTrace()[0]. // getMethodName() +
     * " not supported"); getDelegate().coerceData(isAlphaPremultiplied); }
     */

    public WritableRaster copyData(WritableRaster outRaster) {
        return getDelegate().copyData(outRaster);
    }

    public Graphics2D createGraphics() {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public void flush() {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public WritableRaster getAlphaRaster() {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public ColorModel getColorModel() {
        return getDelegate().getColorModel();
    }

    public Raster getData() {
        return getDelegate().getData();
    }

    public Raster getData(Rectangle rect) {
        return getDelegate().getData(rect);
    }

    public Graphics getGraphics() {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public int getHeight() {
        return getDelegate().getHeight();
    }

    public int getHeight(ImageObserver observer) {
        return getDelegate().getHeight(observer);
    }

    public int getMinTileX() {
        return getDelegate().getMinTileX();
    }

    public int getMinTileY() {
        return getDelegate().getMinTileY();
    }

    public int getMinX() {
        return getDelegate().getMinX();
    }

    public int getMinY() {
        return getDelegate().getMinY();
    }

    public int getNumXTiles() {
        return getDelegate().getNumXTiles();
    }

    public int getNumYTiles() {
        return getDelegate().getNumYTiles();
    }

    public Object getProperty(String name) {
        return getDelegate().getProperty(name);
    }

    public Object getProperty(String name, ImageObserver observer) {
        return getDelegate().getProperty(name, observer);
    }

    public String[] getPropertyNames() {
        return getDelegate().getPropertyNames();
    }

    public WritableRaster getRaster() {
        return getDelegate().getRaster();
    }

    public int getRGB(int x, int y) {
        return getDelegate().getRGB(x, y);
    }

    public int[] getRGB(int startX, int startY, int w, int h, int[] rgbArray, int offset, int scansize) {
        return getDelegate().getRGB(startX, startY, w, h, rgbArray, offset, scansize);
    }

    public SampleModel getSampleModel() {
        return getDelegate().getSampleModel();
    }

    public ImageProducer getSource() {
        return getDelegate().getSource();
    }

    public Vector<RenderedImage> getSources() {
        return getDelegate().getSources();
    }

    public BufferedImage getSubimage(int x, int y, int w, int h) {
        return getDelegate().getSubimage(x, y, w, h);
    }

    public Raster getTile(int tileX, int tileY) {
        return getDelegate().getTile(tileX, tileY);
    }

    public int getTileGridXOffset() {
        return getDelegate().getTileGridXOffset();
    }

    public int getTileGridYOffset() {
        return getDelegate().getTileGridYOffset();
    }

    public int getTileHeight() {
        return getDelegate().getTileHeight();
    }

    public int getTileWidth() {
        return getDelegate().getTileWidth();
    }

    public int getType() {
        return getDelegate().getType();
    }

    public int getWidth() {
        return getDelegate().getWidth();
    }

    public int getWidth(ImageObserver observer) {
        return getDelegate().getWidth(observer);
    }

    public WritableRaster getWritableTile(int tileX, int tileY) {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public Point[] getWritableTileIndices() {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public boolean hasTileWriters() {
        return false;
    }

    public boolean isAlphaPremultiplied() {
        return true;
    }

    public boolean isTileWritable(int tileX, int tileY) {
        return false;
    }

    public void releaseWritableTile(int tileX, int tileY) {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    /*
     * public void removeTileObserver(TileObserver to) { // throw new
     * RuntimeException(new Exception().getStackTrace()[0]. // getMethodName() +
     * " not supported"); getDelegate().removeTileObserver(to); }
     */

    public void setData(Raster r) {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public void setRGB(int x, int y, int rgb) {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public void setRGB(int startX, int startY, int w, int h, int[] rgbArray, int offset, int scansize) {
        throw new RuntimeException(new Exception().getStackTrace()[0].getMethodName() + " not supported");
    }

    public String toString() {
        return getDelegate().toString();
    }

    // END BufferedImage METHODS

    protected FITSImage(ColorModel cm, WritableRaster r, boolean isRasterPremultiplied, Hashtable<String, Object> properties) {
        super(cm, r, isRasterPremultiplied, properties);
    }

    protected void setFits(Fits fits) {
        this.fits = fits;
    }

    protected void setImageHDU(ImageHDU imageHDU) {
        this.imageHDU = imageHDU;
    }

    protected void setScaledImages(BufferedImage[] scaledImages) {
        this.scaledImages = scaledImages;
    }

    protected BufferedImage[] getScaledImages() {
        return this.scaledImages;
    }

    protected static ImageHDU findFirstImageHDU(Fits fits) throws FitsException, IOException {
        ImageHDU result = null;
        int i = 0;

        for (BasicHDU<?> hdu = fits.getHDU(i); hdu != null && result == null; ++i) {
            if (hdu instanceof ImageHDU) {
                result = (ImageHDU) hdu;
            }
        }

        return result;
    }

    protected static BufferedImage[] createScaledImages(Fits fits, int[] itemIndex)
            throws FitsException, DataTypeNotSupportedException, NoImageDataFoundException, IOException {
        BufferedImage[] result = null;
        ImageHDU imageHDU = FitsUtil.imageHDU(fits);

        if (imageHDU != null) {
            result = createScaledImages(imageHDU, itemIndex);
        } else {
            throw new NoImageDataFoundException();
        }

        return result;
    }

    /**
     * @return An array of BufferedImages from hdu with intensity values scaled to
     *         short range using linear, log, square root, and square scales, in
     *         that order.
     */
    public static BufferedImage[] createScaledImages(ImageHDU hdu, int[] itemIndex)
            throws FitsException, DataTypeNotSupportedException {
        int bitpix = hdu.getBitPix();
        double bZero = hdu.getBZero();
        double bScale = hdu.getBScale();
        Histogram hist = null;

        switch (bitpix) {
        case 8:
            hist = ScaleUtils.computeHistogram(getBytesData(hdu, itemIndex), bZero, bScale);
            break;
        case 16:
            hist = ScaleUtils.computeHistogram(getShortsData(hdu, itemIndex), bZero, bScale);
            break;
        case 32:
            hist = ScaleUtils.computeHistogram(getIntsData(hdu, itemIndex), bZero, bScale);
            break;
        case -32:
            hist = ScaleUtils.computeHistogram(getFloatsData(hdu, itemIndex), bZero, bScale);
            break;
        case -64:
            hist = ScaleUtils.computeHistogram(getDoublesData(hdu, itemIndex), bZero, bScale);
            break;
        default:
            throw new DataTypeNotSupportedException(bitpix);
        }

        return createScaledImages(hdu, hist, hist.getMin(), hist.getMax(), hist.estimateSigma(), itemIndex);
    }
    
    public static int[] getWidthHeight(ImageHDU hdu) throws FitsException {
        int[] res = new int[2];
        int[] axes = hdu.getAxes();
        if (axes.length == 4) {
            res[0] = axes[3];
            res[1] = axes[2];
        } else if (axes.length == 3) {
            res[0] = axes[2];
            res[1] = axes[1];
        } else if (axes.length == 2) {
            res[0] = axes[1];
            res[1] = axes[0];
        } else if (axes.length == 1) {
            throw new IllegalArgumentException("Unable to find height for 1d");
        }
        return res;
    }
    
    public static BufferedImage[] createScaledImages(ImageHDU hdu, Histogram hist, double min, double max, double sigma,
            int[] itemIndex) throws FitsException, DataTypeNotSupportedException {
        int bitpix = hdu.getBitPix();
        int[] widthHeight = getWidthHeight(hdu);
        int width = widthHeight[0];
        int height = widthHeight[1];
        double bZero = hdu.getBZero();
        double bScale = hdu.getBScale();
        short[][] scaledData = null;

        switch (bitpix) {
        case 8:
            scaledData = ScaleUtils.scaleToUShort(getBytesData(hdu, itemIndex), hist, width, height, bZero, bScale, min,
                    max, sigma);
            break;
        case 16:
            scaledData = ScaleUtils.scaleToUShort(getShortsData(hdu, itemIndex), hist, width, height, bZero, bScale,
                    min, max, sigma);
            break;
        case 32:
            scaledData = ScaleUtils.scaleToUShort(getIntsData(hdu, itemIndex), hist, width, height, bZero, bScale, min,
                    max, sigma);
            break;
        case -32:
            scaledData = ScaleUtils.scaleToUShort(getFloatsData(hdu, itemIndex), hist, width, height, bZero, bScale,
                    min, max, sigma);
            break;
        case -64:
            scaledData = ScaleUtils.scaleToUShort(getDoublesData(hdu, itemIndex), hist, width, height, bZero, bScale,
                    min, max, sigma);
            break;
        default:
            throw new DataTypeNotSupportedException(bitpix);
        }

        ColorModel cm = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), false, false,
                Transparency.OPAQUE, DataBuffer.TYPE_USHORT);
        SampleModel sm = cm.createCompatibleSampleModel(width, height);

        Hashtable<String, Object> properties = new Hashtable<String, Object>();
        properties.put("histogram", hist);
        properties.put("imageHDU", hdu);

        BufferedImage[] result = new BufferedImage[scaledData.length];

        for (int i = 0; i < result.length; ++i) {
            DataBuffer db = new DataBufferUShort(scaledData[i], height);
            WritableRaster r = Raster.createWritableRaster(sm, db, null);

            result[i] = new BufferedImage(cm, r, false, properties);
        }

        return result;
    }

    public static byte[][] getBytesData(ImageHDU imageHDU, int[] itemIndex) {
        Object data = imageHDU.getData().getData();
        if (itemIndex == null || itemIndex.length == 0) {
            return (byte[][]) data;
        }
        if (itemIndex.length == 1) {
            return ((byte[][][]) data)[itemIndex[0]];
        }
        if (itemIndex.length == 2) {
            return ((byte[][][][]) data)[itemIndex[0]][itemIndex[1]];
        }
        throw new IllegalStateException("Not supported dimension " + (itemIndex.length + 2));
    }

    public static short[][] getShortsData(ImageHDU imageHDU, int[] itemIndex) {
        Object data = imageHDU.getData().getData();
        if (itemIndex == null || itemIndex.length == 0) {
            return (short[][]) data;
        }
        if (itemIndex.length == 1) {
            return ((short[][][]) data)[itemIndex[0]];
        }
        if (itemIndex.length == 2) {
            return ((short[][][][]) data)[itemIndex[0]][itemIndex[1]];
        }
        throw new IllegalStateException("Not supported dimension " + (itemIndex.length + 2));
    }

    public static int[][] getIntsData(ImageHDU imageHDU, int[] itemIndex) {
        Object data = imageHDU.getData().getData();
        if (itemIndex == null || itemIndex.length == 0) {
            return (int[][]) data;
        }
        if (itemIndex.length == 1) {
            return ((int[][][]) data)[itemIndex[0]];
        }
        if (itemIndex.length == 2) {
            return ((int[][][][]) data)[itemIndex[0]][itemIndex[1]];
        }
        throw new IllegalStateException("Not supported dimension " + (itemIndex.length + 2));
    }

    public static float[][] getFloatsData(ImageHDU imageHDU, int[] itemIndex) {
        Object data = imageHDU.getData().getData();
        if (itemIndex == null || itemIndex.length == 0) {
            return (float[][]) data;
        }
        if (itemIndex.length == 1) {
            return ((float[][][]) data)[itemIndex[0]];
        }
        if (itemIndex.length == 2) {
            return ((float[][][][]) data)[itemIndex[0]][itemIndex[1]];
        }
        throw new IllegalStateException("Not supported dimension " + (itemIndex.length + 2));
    }

    public static double[][] getDoublesData(ImageHDU imageHDU, int[] itemIndex) {
        Object data = imageHDU.getData().getData();
        if (itemIndex == null || itemIndex.length == 0) {
            return (double[][]) data;
        }
        if (itemIndex.length == 1) {
            return ((double[][][]) data)[itemIndex[0]];
        }
        if (itemIndex.length == 2) {
            return ((double[][][][]) data)[itemIndex[0]][itemIndex[1]];
        }
        throw new IllegalStateException("Not supported dimension " + (itemIndex.length + 2));
    }
}
