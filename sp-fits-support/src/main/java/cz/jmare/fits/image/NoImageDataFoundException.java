package cz.jmare.fits.image;

public class NoImageDataFoundException extends RuntimeException {
    private static final long serialVersionUID = 3806359167243016313L;

    public NoImageDataFoundException() {
        super("No image data found in FITS file.");
    }
}