package cz.jmare.fits.util;

import java.io.IOException;

import cz.jmare.fits.header.Fiu;
import cz.jmare.fits.header.InconsistentFitsException;
import nom.tam.fits.BasicHDU;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.ImageHDU;

public class FitsUtil {
    public static ImageHDU imageHDU(Fits fits) {
        boolean noNaxis = false;
        int i = 0;
        try {
            BasicHDU<?> hdu = null;
            while ((hdu = fits.getHDU(i++)) != null) {
                if (hdu instanceof ImageHDU) {
                    ImageHDU imageHDU = (ImageHDU) hdu;
                    int[] axes = imageHDU.getAxes();
                    if (axes == null) {
                        noNaxis = true;
                        continue;
                    }
                    return imageHDU;
                }
            }
        } catch (FitsException | IOException e) {
            throw new IllegalArgumentException("Unable to find image hdu in file", e);
        }
        if (noNaxis) {
            throw new InconsistentFitsException("Found image hdu in file but no NAXIS exists");
        }
        throw new IllegalArgumentException("Unable to find image hdu in file");
    }

    public static ImageHDU firstImageHDURegadlessNaxis(Fits fits) {
        boolean noNaxis = false;
        int i = 0;
        try {
            BasicHDU<?> hdu = null;
            while ((hdu = fits.getHDU(i++)) != null) {
                if (hdu instanceof ImageHDU) {
                    ImageHDU imageHDU = (ImageHDU) hdu;
                    return imageHDU;
                }
            }
        } catch (FitsException | IOException e) {
            throw new IllegalArgumentException("Unable to find image hdu in file", e);
        }
        throw new IllegalArgumentException("Unable to find image hdu in file");
    }
    
    /**
     * Number of axes. For 2d determination at least 2 should be returned
     * @param imageHDU
     * @return
     */
    public static int getAxesCount(ImageHDU imageHDU) {
        int naxis = Fiu.intValue("NAXIS", imageHDU.getHeader());
        return naxis;
    }
    
    /**
     * Get dimension for concrete axe.
     * @param imageHDU
     * @param axeIndex index of axe starting with 0. Index must be less than returned
     *        by {@link #getAxesCount(ImageHDU)}
     * @return
     */
    public static int getAxeDim(ImageHDU imageHDU, int axeIndex) {
        int axeDim = Fiu.intValue("NAXIS" + (axeIndex + 1), imageHDU.getHeader());
        return axeDim;
    }
}
