package cz.jmare.fits.util;

/**
 * The FK4 is an equatorial coordinate system (coordinate system linked to the
 * Earth) based on its B1950 position. The units used for time specification is
 * the Besselian Year.
 *
 * The conversion to/from FK5 (which coincides with ICRS) uses the algorithm
 * published by Standish (1982A&A...115...20S)
 * 
 * @author Francois Ochsenbein (CDS)
 *
 */

public class FK4 {

    /**
     * Constants for Conversion from FK4 to FK5 The 6x6 matrix to move from FK4 to
     * FK5 Proper motions are in arcsec/century (10mas/yr)
     **/

    /** Matrix 6x1 to compute the e-term */
    static protected double[] A = { // For e-term
            // -0.33530/206265., -0.06584/206265., -0.02855/206265.,
            -1.62557e-6, -0.31919e-6, -0.13843e-6,
            // 1.245e-3, -1.580e-3, -0.659e-3
            1.244e-3, -1.579e-3, -0.660e-3 };

    /**
     * Table 2 of Standish (1982A&A...115...20S), rediscussed by Soma and Aoki
     * (1990A&A...240..150S), and apparently slightly modified by the starlink
     * group.
     **/
    static protected double[][] EM = { //
            { 0.9999256782, -0.0111820611, -0.0048579477, 2.42395018e-6, -0.02710663e-6, -0.01177656e-6 },
            { 0.0111820610, 0.9999374784, -0.0000271765, 0.02710663e-6, 2.42397878e-6, -0.00006587e-6 },
            { 0.0048579479, -0.0000271474, 0.9999881997, 0.01177656e-6, -0.00006582e-6, 2.42410173e-6 },
            { -0.000551, -0.238565, 0.435739, 0.99994704e0, -0.01118251e0, -0.00485767e0 },
            { 0.238514, -0.002667, -0.008541, 0.01118251e0, 0.99995883e0, -0.00002718e0 },
            { -0.435623, 0.012254, 0.002117, 0.00485767e0, -0.00002714e0, 1.00000956e0 } };

    /** The 6x6 matrix to move from FK5 to FK4 */
    static protected double[][] EM1 = { /* FK5 ==> FK4 */
            { /* 0.9999256795, 0.0111814828, 0.0048590039, */
                    0.9999256795, 0.0111814829, 0.0048590038,
                    /*-2.42389840e-6,      -0.02710544e-6,     -0.01177742e-6*/
                    -2.42389840e-6, -0.02710545e-6, -0.01177742e-6 },
            { -0.0111814828, 0.9999374849, -0.0000271771,
                    /* 0.02710544e-6, -2.42392702e-6, 0.00006585e-6 */
                    0.02710545e-6, -2.42392702e-6, 0.00006585e-6 },
            { /*-0.0048590040,     -0.0000271557,     0.9999881946,*/
                    -0.0048590040, -0.0000271558, 0.9999881946, 0.01177742e-6, 0.00006585e-6, -2.42404995e-6 },
            { /*-0.000551,          0.238509,        -0.435614,*/
                    -0.000550, 0.238509, -0.435613, 0.99990432e0, 0.01118145e0, 0.00485852e0 },
            { /*-0.238560,         -0.002667,         0.012254,*/
                    -0.238559, -0.002668, 0.012254, -0.01118145e0, 0.99991613e0, -0.00002717e0 },
            { /* 0.435730, -0.008541, 0.002117, */
                    0.435730, -0.008541, 0.002116, -0.00485852e0, -0.00002716e0, 0.99996684e0 } };

    /** To estimate the proper motions in FK4 **/
    static double[][] EM2 = null; /* new double[6][6]; */

    /**
     * Convert the position from standard B1950 FK4 to standard J2000 FK5 system
     * with Standish's algorithm. Standish uses the 6x6 matrix applied on the
     * 6-vector (x y z xd yd zd) representing the position + derivative (derivatives
     * expressed in arcsec/century) with the EM matrix applied on the vector after
     * subtraction of the E-term The E-term (due to the Earth's elliptical orbit)
     * can be expressed r ^ (A ^ r) = A - (A*r)r where ^ represents the vectorial
     * product, and * the scalar product.
     * 
     * @param u6 6-vector containing position + mouvement (rad/yr)
     **/
    public static void toFK5(double[] u6) {
        double v[] = new double[6];
        double w, wd;
        int i, j;
        // *** Unknown positions: do nothing !
        if ((u6[0] == 0) && (u6[1] == 0) && (u6[2] == 0))
            return;
        // Convert the derivative part, originally in rad/yr, into arcsec/cy
        v[0] = u6[0];
        v[1] = u6[1];
        v[2] = u6[2];
        v[3] = u6[3] * (360000. * 180. / Math.PI);
        v[4] = u6[4] * (360000. * 180. / Math.PI);
        v[5] = u6[5] * (360000. * 180. / Math.PI);

        // Remove e-term (replace r by r - r^(A^r))
        w = v[0] * A[0] + v[1] * A[1] + v[2] * A[2]; // A*r
        wd = v[0] * A[3] + v[1] * A[4] + v[2] * A[5]; // A1*r
        v[0] -= A[0] - w * u6[0];
        v[1] -= A[1] - w * u6[1];
        v[2] -= A[2] - w * u6[2];
        // Correction to the proper motions (not in Standish)
        v[3] -= A[3] - wd * u6[0];
        v[4] -= A[4] - wd * u6[1];
        v[5] -= A[5] - wd * u6[2];
        // Rotate with the EM matrix
        for (i = 0; i < 6; i++) {
            w = 0;
            for (j = 0; j < 6; j++) {
                w += EM[i][j] * v[j];
            }
            u6[i] = w;
        }

        // Convert the derivative part to rad/yr
        u6[3] /= (360000. * 180. / Math.PI);
        u6[4] /= (360000. * 180. / Math.PI);
        u6[5] /= (360000. * 180. / Math.PI);
        // Renormalize
        w = Math.sqrt(u6[0] * u6[0] + u6[1] * u6[1] + u6[2] * u6[2]);
        for (i = 0; i < 6; i++) {
            u6[i] /= w;
        }
    }
    
    public static void main(String[] args) {
    }
}
