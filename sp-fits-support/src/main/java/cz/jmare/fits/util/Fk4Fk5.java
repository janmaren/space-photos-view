package cz.jmare.fits.util;

import static java.lang.Math.PI;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

public class Fk4Fk5 {
    /**
     * Convert B1950 RA/Dec to J2000 RA/Dec (RA in hours, Dec in degrees)
     * 
     * @param ra    B1950 Right ascension in hours
     * @param dec   B1950 Declination in degrees
     * @param rapm  Proper motion in right ascension (mas/yr)
     * @param decpm Proper motion in declination (mas/yr)
     * @param usePM Include proper motion in conversion
     * @return J2000 RA in hours Dec in degrees as {ra, dec}
     */
    public static double[] B1950toJ2000(double ra, double dec, double rapm, double decpm, boolean usePM) {
        // This method is heavily based on the WCSTools routine fk425pv

        /*
         * This routine converts stars from the old Bessel-Newcomb FK4 system to the IAU
         * 1976 FK5 Fricke system, using Yallop's implementation (see ref 2) of a matrix
         * method due to Standish (see ref 3). The numerical values of ref 2 are used
         * canonically.
         * 
         * Conversion from other than Besselian epoch 1950.0 to other than Julian epoch
         * 2000.0 will require use of the appropriate precession, proper motion, and
         * e-terms routines before and/or after fk425 is called.
         * 
         * In the FK4 catalogue the proper motions of stars within 10 degrees of the
         * poles do not embody the differential e-term effect and should, strictly
         * speaking, be handled in a different manner from stars outside these regions.
         * however, given the general lack of homogeneity of the star data available for
         * routine astrometry, the difficulties of handling positions that may have been
         * determined from astrometric fields spanning the polar and non-polar regions,
         * the likelihood that the differential e-terms effect was not taken into
         * account when allowing for proper motion in past astrometry, and the
         * undesirability of a discontinuity in the algorithm, the decision has been
         * made in this routine to include the effect of differential e-terms on the
         * proper motions for all stars, whether polar or not, at epoch 2000, and
         * measuring on the sky rather than in terms of dra, the errors resulting from
         * this simplification are less than 1 milliarcsecond in position and 1
         * milliarcsecond per century in proper motion.
         * 
         * References:
         * 
         * 1 "Mean and apparent place computations in the new IAU System. I. The
         * transformation of astrometric catalog systems to the equinox J2000.0." Smith,
         * C.A.; Kaplan, G.H.; Hughes, J.A.; Seidelmann, P.K.; Yallop, B.D.; Hohenkerk,
         * C.Y. Astronomical Journal vol. 97, Jan. 1989, p. 265-273.
         * 
         * 2 "Mean and apparent place computations in the new IAU System. II.
         * Transformation of mean star places from FK4 B1950.0 to FK5 J2000.0 using
         * matrices in 6-space." Yallop, B.D.; Hohenkerk, C.Y.; Smith, C.A.; Kaplan,
         * G.H.; Hughes, J.A.; Seidelmann, P.K.; Astronomical Journal vol. 97, Jan.
         * 1989, p. 274-279.
         * 
         * 3 "Conversion of positions and proper motions from B1950.0 to the IAU system
         * at J2000.0", Standish, E.M. Astronomy and Astrophysics, vol. 115, no. 1, Nov.
         * 1982, p. 20-22.
         * 
         * P.T.Wallace Starlink 20 December 1993 Doug Mink Smithsonian Astrophysical
         * Observatory 7 June 1995
         */

        double r1950, d1950; /* B1950.0 ra,dec (rad) */
        double r2000, d2000; /* J2000.0 ra,dec (rad) */

        /* Miscellaneous */
        double ur = 0.0, ud = 0.0, sr, cr, sd, cd, w, wd;
        double x, y, z, xd, yd, zd, dra, ddec;
        double rxyz, rxysq, rxy, rxyzsq, spxy, spxyz;
        int i, j;
        int diag = 0;

        double[] r0 = new double[3];
        double[] rd0 = new double[3]; /* star position and velocity vectors */
        double[] v1 = new double[6];
        double[] v2 = new double[6]; /* combined position and velocity vectors */

        /* Constants */
        double zero = (double) 0.0;
        double vf = 21.095; /* Km per sec to AU per tropical century */
        /* = 86400 * 36524.2198782 / 149597870 */

        /* Convert B1950.0 FK4 star data to J2000.0 FK5 */
        double[][] em = { { 0.9999256782, /* em[0][0] */
                -0.0111820611, /* em[0][1] */
                -0.0048579477, /* em[0][2] */
                0.00000242395018, /* em[0][3] */
                -0.00000002710663, /* em[0][4] */
                -0.00000001177656 }, /* em[0][5] */

                { 0.0111820610, /* em[1][0] */
                        0.9999374784, /* em[1][1] */
                        -0.0000271765, /* em[1][2] */
                        0.00000002710663, /* em[1][3] */
                        0.00000242397878, /* em[1][4] */
                        -0.00000000006587 }, /* em[1][5] */

                { 0.0048579479, /* em[2][0] */
                        -0.0000271474, /* em[2][1] */
                        0.9999881997, /* em[2][2] */
                        0.00000001177656, /* em[2][3] */
                        -0.00000000006582, /* em[2][4] */
                        0.00000242410173 }, /* em[2][5] */

                { -0.000551, /* em[3][0] */
                        -0.238565, /* em[3][1] */
                        0.435739, /* em[3][2] */
                        0.99994704, /* em[3][3] */
                        -0.01118251, /* em[3][4] */
                        -0.00485767 }, /* em[3][5] */

                { 0.238514, /* em[4][0] */
                        -0.002667, /* em[4][1] */
                        -0.008541, /* em[4][2] */
                        0.01118251, /* em[4][3] */
                        0.99995883, /* em[4][4] */
                        -0.00002718 }, /* em[4][5] */

                { -0.435623, /* em[5][0] */
                        0.012254, /* em[5][1] */
                        0.002117, /* em[5][2] */
                        0.00485767, /* em[5][3] */
                        -0.00002714, /* em[5][4] */
                        1.00000956 } /* em[5][5] */
        };

        /*
         * Constant vector and matrix (by columns) These values were obtained by
         * inverting C.Hohenkerk's forward matrix (private communication), which agrees
         * with the one given in reference 2 but which has one additional decimal place.
         */
        double[] a = { -1.62557e-6, -0.31919e-6, -0.13843e-6 };
        double[] ad = { 1.245e-3, -1.580e-3, -0.659e-3 };
        double d2pi = 6.283185307179586476925287; /* two PI */
        double tiny = 1.e-30; /* small number to avoid arithmetic problems */
        double rv = 0;
        double parallax = 0;

        /* Convert B1950 RA in hours and Dec in degrees to radians */
        r1950 = ra * PI / 12;
        d1950 = dec * PI / 180;

        /* Convert B1950 RA and Dec proper motion from mas/year to arcsec/tc */
        if (usePM) {
            ur = rapm / (10 * cos(d1950)); // *100/(1000*cos(d1950))
            ud = decpm / 10; // *100/1000
        }

        /* Convert direction to Cartesian */
        sr = sin(r1950);
        cr = cos(r1950);
        sd = sin(d1950);
        cd = cos(d1950);
        r0[0] = cr * cd;
        r0[1] = sr * cd;
        r0[2] = sd;

        /* Convert motion to Cartesian */
        w = vf * rv * parallax;
        if (ur != zero || ud != zero || (rv != zero && parallax != zero)) {
            rd0[0] = (-sr * cd * ur) - (cr * sd * ud) + (w * r0[0]);
            rd0[1] = (cr * cd * ur) - (sr * sd * ud) + (w * r0[1]);
            rd0[2] = (cd * ud) + (w * r0[2]);
        } else {
            rd0[0] = zero;
            rd0[1] = zero;
            rd0[2] = zero;
        }

        /* Remove e-terms from position and express as position+velocity 6-vector */
        w = (r0[0] * a[0]) + (r0[1] * a[1]) + (r0[2] * a[2]);
        for (i = 0; i < 3; i++)
            v1[i] = r0[i] - a[i] + (w * r0[i]);

        /* Remove e-terms from proper motion and express as 6-vector */
        wd = (r0[0] * ad[0]) + (r0[1] * ad[1]) + (r0[2] * ad[2]);
        for (i = 0; i < 3; i++)
            v1[i + 3] = rd0[i] - ad[i] + (wd * r0[i]);

        /*
         * Alternately: Put proper motion in 6-vector without adding e-terms for (i = 0;
         * i < 3; i++) v1[i+3] = rd0[i];
         */

        /* Convert position + velocity vector to FK5 system */
        for (i = 0; i < 6; i++) {
            w = zero;
            for (j = 0; j < 6; j++) {
                w += em[i][j] * v1[j];
            }
            v2[i] = w;
        }

        /* Vector components */
        x = v2[0];
        y = v2[1];
        z = v2[2];
        xd = v2[3];
        yd = v2[4];
        zd = v2[5];

        /* Magnitude of position vector */
        rxysq = x * x + y * y;
        rxy = sqrt(rxysq);
        rxyzsq = rxysq + z * z;
        rxyz = sqrt(rxyzsq);

        spxy = (x * xd) + (y * yd);
        spxyz = spxy + (z * zd);

        /* Convert back to spherical coordinates */
        if (x == zero && y == zero)
            r2000 = zero;
        else {
            r2000 = atan2(y, x);
            if (r2000 < zero)
                r2000 = r2000 + d2pi;
        }
        d2000 = atan2(z, rxy);

        /* Return results */

        ra = r2000 * 12.0 / PI;
        dec = d2000 * 180.0 / PI;

        return new double[] { ra, dec };
    }
}
