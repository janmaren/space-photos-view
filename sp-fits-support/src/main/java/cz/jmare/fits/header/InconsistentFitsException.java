package cz.jmare.fits.header;

/**
 * TODO: rename to InconsistentWcsException
 */
public class InconsistentFitsException extends RuntimeException {
    private static final long serialVersionUID = 545046646820830437L;

    public InconsistentFitsException(String message) {
        super(message);
    }
}
