// WCS.java

package cz.jmare.fits.header;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.fits.util.FitsUtil;
import cz.jmare.fits.util.Fk4Fk5;
import cz.jmare.math.geometry.entity.PointInt;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.astro.RahDec;
import nom.tam.fits.Fits;
import nom.tam.fits.Header;
import nom.tam.fits.ImageHDU;

/**
 * A simple WCS object for the world coordinate systems used in most
 * astronomical FITS files. Uses the FitsJ class methods.
 *
 * The processing follows the general guidelines in Greisen, E.W. & Calabretta
 * M.R. 2002, Astron. & Astrophys. Vol. 395, 1061-1075 Calabretta M.R. &
 * Greisen, E.W. 2002, Astron. & Astrophys. Vol. 395, 1077-1122
 *
 * Modifications by K. Collins 7/2010 Fixed three issues related to proper RA
 * and DEC calculations for the TAN projection - The "RA---TAN-SIP" extension
 * caused problems with the "-TAN" substring matching changed: int i =
 * CTYPE[0].lastIndexOf("-"); projection = CTYPE[0].substring(i+1); to:
 * projection = CTYPE[0].substring(5, 8);
 *
 * - Reversed the indices when loading the PC and CD Matrix coefficients
 * changed: CD[k-1][i-1] and PC[k-1][i-1] to: CD[i-1][k-1] and PC[i-1][k-1]
 *
 * - Reversed the order of the arguments in one statement when calculating the
 * TAN projection changed: s[0] = DEGRAD*Math.atan2(-x[1],x[0]); to: s[0] =
 * DEGRAD*Math.atan2(x[0],-x[1]);
 *
 * - Added code to support the SIP non-linear distortion corrections following
 * SHUPE, D.L. ET AL. 2008
 *
 * Modifications by F. Hessman 9/2012 - Renamed NAXIS to NAXES to avoid
 * confusion with FITS keywords NAXISi - Added wcs2pixel(), including support
 * for inverse SIP corrections - Added get and set methods for standard FITS WCS
 * quantities - Removed support for clumsy and vague WCS preferences.
 *
 * https://fits.gsfc.nasa.gov/standard40/fits_standard40draft1.pdf
 */
public class WCS2d {
    private static final Logger LOGGER = LoggerFactory.getLogger(WCS2d.class);
    public static final double PIXELCENTER = 0.5;
    
    private int NAXES = 0; // CALLED "NAXIS" IN FITS
    private int WCSAXES = 0;
    private int[] NAXIS; // CALLED "NAXIXi" IN FITS
    private double[] CDELT; // CALLED "CDELTi" IN FITS
    private double[] CRPIX; // CALLED "CRPIXi" IN FITS
    private double[] CRVAL; // CALLED "CRVALi" IN FITS
    private String[] CTYPE = null; // CALLED "CTYPEi" IN FITS
    private String[] CUNIT = null; // CALLED "CUNITi" IN FITS
    private double[][] PC; // CALLED "PCi_j" IN FITS
    private double[][] CD; // CALLED "CDi_j" IN FITS
    private double[][] PCinv = null;
    private double[][] CDinv = null;
    private double LONPOLE = 180.; // DEGREES

    private double[][] A = null;
    private double[][] B = null;
    private int A_ORDER = -1;
    private int B_ORDER = -1;
    private boolean useSIPA = false;
    private boolean useSIPB = false;

    private double[][] AP = null;
    private double[][] BP = null;
    private int AP_ORDER = -1;
    private int BP_ORDER = -1;
    private boolean useSIPAP = false;
    private boolean useSIPBP = false;

    private String coordsys;
    private String projection;
    private boolean useCD = false;
    private boolean hasRADEC = false;
    private boolean typeContainsSIP = false;

    private String RADECSYS;
    private Double EQUINOX;
    
    public WCS2d(Fits fits) {
        this(FitsUtil.imageHDU(fits));
    }
    
    public WCS2d(ImageHDU img) {
        this(img.getHeader());
    }

    public WCS2d(Header hdr) {
        NAXES = hdr.getIntValue("NAXIS");
        if (NAXES == 0) {
            throw new IllegalArgumentException("No NAXIS with at least 2d");
        }
        if (NAXES > 2) {
            NAXES = 2; // we are calculating 2d coordinates
        }
        initVariables(hdr);
    }

    protected void initVariables(Header hdr) {
        // FITS HEADER PRESENT?
        
        if (!hdr.containsKey("SIMPLE")) {
            if (!hdr.containsKey("XTENSION")) {
                throw new InconsistentFitsException("No SIMPLE key exists");
            } else {
                String stringValue = Fiu.stringValue("XTENSION", hdr);
                if (!"IMAGE".equals(stringValue)) {
                    throw new InconsistentFitsException("Not a SIMPLE header type nor XTENSION with value IMAGE");
                }
            }
        }
        if (!hdr.containsKey("END")) {
            throw new InconsistentFitsException("No END key exists");
        }

        hasRADEC = false;

        // CHECK IF THE NUMBER OF WCS AXES IS KNOWN AND DIFFERENT

        if (hdr.containsKey("WCSAXES")) {
            WCSAXES = Fiu.intValue("WCSAXES", hdr);
            NAXES = WCSAXES;
            if (NAXES > 2) {
                NAXES = 2; // we are calculating 2d coordinates
            }
        }

        // GIVEN NUMBER OF WCS AXES, RE-INITIALIZE MATRICES, ETC.

        createVars(NAXES);

        // GET SIZES OF AXES

        for (int j = 1; j <= NAXES; j++) {
            if (!hdr.containsKey("NAXIS" + j)) {
                throw new InconsistentFitsException("Cannot find keyword NAXIS" + j + " in FITS header");
            }
            NAXIS[j - 1] = Fiu.intValue("NAXIS" + j, hdr);
        }

        // TRY TO FIND RA-- and DEC- COORDINATE TYPES

        CTYPE[0] = null;
        String typ = "";
        String prefix = "";
        String abc[] = new String[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
                "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        for (int k = 0; k < abc.length && prefix == null; k++) {
            if (hdr.containsKey("CTYPE1" + abc[k])) {
                typ = Fiu.stringValue("CTYPE1" + abc[k], hdr);
                if (typ != null && typ.startsWith("RA--") && typ.length() >= 8) {
                    CTYPE[0] = typ;
                    prefix = abc[k];
                    projection = CTYPE[0].substring(5, 8);
                    if (CTYPE[0].length() >= 12) {
                        if (CTYPE[0].substring(9, 12).equals("SIP")) {
                            typeContainsSIP = true;
                        }
                    }
                    break;
                }
            }
        }
        if (CTYPE[0] == null) // NO "RA--" FOUND, SO TAKE WHAT YOU CAN GET
        {
            if (hdr.containsKey("CTYPE1")) {
                CTYPE[0] = Fiu.stringValue("CTYPE1", hdr);
                if (CTYPE[0].length() < 8) {
                    throw new InconsistentFitsException("Unknown coordinate system (" + CTYPE[0] + ") in CTYPE1");
                }
                projection = CTYPE[0].substring(5, 8);
                if (CTYPE[0].length() >= 12) {
                    if (CTYPE[0].substring(9, 12).equals("SIP")) {
                        typeContainsSIP = true;
                    }
                }
            } else {
                throw new InconsistentFitsException("Cannot define world coordinate system (CTYPE1): not enough information in FITS header");
            }
        }

        // GET MATCHING COORDINATE TYPES

        for (int k = 2; k <= NAXES; k++) {
            if (hdr.containsKey("CTYPE" + k + prefix)) {
                CTYPE[k - 1] = Fiu.stringValue("CTYPE" + k + prefix, hdr);
            }
        }

        // CHECK IF CTYPE2n IS "DEC" AND COORDINATE SYSTEMS MATCH

        if (CTYPE[0].startsWith("RA--") && CTYPE[1].startsWith("DEC-")) {
            hasRADEC = true;
            if (CUNIT == null) {
                CUNIT = new String[NAXES];
                CUNIT[0] = "deg     ";
                CUNIT[1] = "deg     ";
            }
        }

        // GET TRANSFORMATION COEFFICIENTS (SCALE, TRANSLATION)
        for (int k = 1; k <= NAXES; k++) {
            if (hdr.containsKey("CDELT" + k + prefix)) {
                CDELT[k - 1] = Fiu.doubleValue("CDELT" + k + prefix, hdr);
            }
            if (hdr.containsKey("CRPIX" + k + prefix)) {
                CRPIX[k - 1] = Fiu.doubleValue("CRPIX" + k + prefix, hdr);
            }
            if (hdr.containsKey("CRVAL" + k + prefix)) {
                CRVAL[k - 1] = Fiu.doubleValue("CRVAL" + k + prefix, hdr);
            }
        }

        // GET LINEAR TRANSFORMATION COEFFICIENTS (ROTATION, SCALE, SKEWNESS)


        for (int k = 1; k <= NAXES; k++) {
            for (int i = 1; i <= NAXES; i++) {
                if (hdr.containsKey("PC" + i + "_" + k + prefix)) {
                    PC[i - 1][k - 1] = Fiu.doubleValue("PC" + i + "_" + k + prefix, hdr);
                }
                if (hdr.containsKey("CD" + i + "_" + k + prefix)) {
                    useCD = true;
                    CD[i - 1][k - 1] = Fiu.doubleValue("CD" + i + "_" + k + prefix, hdr);
                }
            }
        }
        if (useCD) {
            CDinv = invert(CD);
            // experiment:
            if (CDinv == null) {
                throw new InconsistentFitsException("Wrong number of CD elements");
            }
        } else {
            PCinv = invert(PC);
            // experiment:
            if (PCinv == null) {
                throw new InconsistentFitsException("Wrong number of PC elements");
            }
        }
        if (CDinv == null && PCinv == null) {
            LOGGER.warn("Cannot invert WCS matrices!");
        }


        // IF AVAILABLE, GET SIP NON-LINEAR DISTORTION COEFFICIENTS
        // SEE SHUPE, D.L. ET AL., 2005, ASPC 347, 491
        // (http://adsabs.harvard.edu/abs/2005ASPC..347..491S)
        if (typeContainsSIP) {
            try {
                // LOOK FOR A AND B POLYNOMIAL ORDERS
                if (NAXES > 1) {
                    A_ORDER = -1;
                    if (hdr.containsKey("A_ORDER")) {
                        A_ORDER = Fiu.intValue("A_ORDER", hdr);
                        if (A_ORDER < 2 || A_ORDER > 9) {
                            LOGGER.warn("SIP A_ORDER out of range (" + A_ORDER + ") in FITS header");
                            A_ORDER = -1;
                        }
                    } else {
                        LOGGER.warn("SIP A_ORDER not found in FITS header");
                    }

                    B_ORDER = -1;
                    if (hdr.containsKey("B_ORDER")) {
                        B_ORDER = Fiu.intValue("B_ORDER", hdr);
                        if (B_ORDER < 2 || B_ORDER > 9) {
                            LOGGER.warn("SIP B_ORDER out of range (" + B_ORDER + ") in FITS header");
                            B_ORDER = -1;
                        }
                    } else {
                        LOGGER.warn("SIP B_ORDER not found in FITS header");
                    }

                    AP_ORDER = -1;
                    if (hdr.containsKey("AP_ORDER")) {
                        AP_ORDER = Fiu.intValue("AP_ORDER", hdr);
                        if (AP_ORDER < 2 || AP_ORDER > 9) {
                            LOGGER.warn("SIP AP_ORDER out of range (" + AP_ORDER + ") in FITS header");
                            AP_ORDER = -1;
                        }
                    } else {
                        LOGGER.warn("SIP AP_ORDER not found in FITS header");
                        LOGGER.warn("Cannot perform correct RA,DEC->i,j tranformation");
                    }

                    BP_ORDER = -1;
                    if (hdr.containsKey("BP_ORDER")) {
                        BP_ORDER = Fiu.intValue("BP_ORDER", hdr);
                        if (BP_ORDER < 2 || BP_ORDER > 9) {
                            LOGGER.warn("SIP BP_ORDER out of range (" + BP_ORDER + ") in FITS header");
                            BP_ORDER = -1;
                        }
                    } else {
                        LOGGER.warn("SIP BP_ORDER not found in FITS header");
                        LOGGER.warn("Cannot perform correct RA,DEC->i,j tranformation");
                    }
                }
            } catch (NumberFormatException e) {
                LOGGER.warn("Cannot read SIP order keywords in FITS header");
                A_ORDER = -1;
                B_ORDER = -1;
                AP_ORDER = -1;
                BP_ORDER = -1;
            }

            // INITIALIZE SIP MATRICES
            if (A_ORDER >= 2 && A_ORDER <= 9) {
                A = new double[A_ORDER + 1][A_ORDER + 1];
                for (int q = 0; q <= A_ORDER; q++) {
                    for (int p = 0; p <= A_ORDER; p++)
                        A[p][q] = 0.0;
                }
            }
            if (B_ORDER >= 2 && B_ORDER <= 9) {
                B = new double[B_ORDER + 1][B_ORDER + 1];
                for (int q = 0; q <= B_ORDER; q++) {
                    for (int p = 0; p <= B_ORDER; p++)
                        B[p][q] = 0.0;
                }
            }
            if (AP_ORDER >= 2 && AP_ORDER <= 9) {
                AP = new double[AP_ORDER + 1][AP_ORDER + 1];
                for (int q = 0; q <= AP_ORDER; q++) {
                    for (int p = 0; p <= AP_ORDER; p++)
                        AP[p][q] = 0.0;
                }
            }
            if (BP_ORDER >= 2 && BP_ORDER <= 9) {
                BP = new double[BP_ORDER + 1][BP_ORDER + 1];
                for (int q = 0; q <= BP_ORDER; q++) {
                    for (int p = 0; p <= BP_ORDER; p++)
                        BP[p][q] = 0.0;
                }
            }

            // LOOK FOR SIP X-COORDINATE MATRIX COEFICIENTS A_p_q IN FITS HEADER
            if (A_ORDER >= 2 && A_ORDER <= 9) {
                for (int q = 0; q <= A_ORDER; q++) {
                    for (int p = 0; p <= A_ORDER; p++) {
                        if (q + p <= A_ORDER) {
                            if (hdr.containsKey("A_" + p + "_" + q + prefix)) {
                                A[p][q] = Fiu.doubleValue("A_" + p + "_" + q + prefix, hdr);
                                useSIPA = true;
                            }
                        }
                    }
                }
            }

            // LOOK FOR SIP Y-COORDINATE MATRIX COEFICIENTS B_p_q IN FITS HEADER
            if (B_ORDER >= 2 && B_ORDER <= 9) {
                for (int q = 0; q <= B_ORDER; q++) {
                    for (int p = 0; p <= B_ORDER; p++) {
                        if (q + p <= B_ORDER) {
                            if (hdr.containsKey("B_" + p + "_" + q + prefix)) {
                                B[p][q] = Fiu.doubleValue("B_" + p + "_" + q + prefix, hdr);
                                useSIPB = true;
                            }
                        }
                    }
                }
            }

            // LOOK FOR SIP INVERSE X-COORDINATE MATRIX COEFICIENTS AP_p_q IN FITS HEADER
            if (AP_ORDER >= 2 && AP_ORDER <= 9) {
                for (int q = 0; q <= AP_ORDER; q++) {
                    for (int p = 0; p <= AP_ORDER; p++) {
                        if (q + p <= AP_ORDER) {
                            if (hdr.containsKey("AP_" + p + "_" + q + prefix)) {
                                AP[p][q] = Fiu.doubleValue("AP_" + p + "_" + q + prefix, hdr);
                                useSIPAP = true;
                            }
                        }
                    }
                }
            }

            // LOOK FOR SIP INVERSE Y-COORDINATE MATRIX COEFICIENTS BP_p_q IN FITS HEADER
            if (BP_ORDER >= 2 && BP_ORDER <= 9) {
                for (int q = 0; q <= BP_ORDER; q++) {
                    for (int p = 0; p <= BP_ORDER; p++) {
                        if (q + p <= BP_ORDER) {
                            if (hdr.containsKey("BP_" + p + "_" + q + prefix)) {
                                BP[p][q] = Fiu.doubleValue("BP_" + p + "_" + q + prefix, hdr);
                                useSIPBP = true;
                            }
                        }
                    }
                }
            }
        }

        // GET PROJECTION PARAMETERS, IF ANY

        LONPOLE = 180.0;// DEGS
        if (hdr.containsKey("LONPOLE" + prefix)) {
            LONPOLE = Fiu.doubleValue("LONPOLE" + prefix, hdr);
        }

        // COORDINATE SYSTEM

        coordsys = CTYPE[0].trim() + "," + CTYPE[1].trim();
        
        // radecsys and equinox - https://fits.gsfc.nasa.gov/users_guide/users_guide/node61.html
        if (hdr.containsKey("RADECSYS")) {
            RADECSYS = Fiu.stringValue("RADECSYS", hdr);
        }
        if (hdr.containsKey("EQUINOX")) {
            try {
                EQUINOX = Fiu.doubleValue("EQUINOX", hdr);
            } catch (Exception e) {
                String stringValue = Fiu.stringValue("EQUINOX", hdr);
                if (stringValue.startsWith("J2000")) {
                    EQUINOX = 2000.0;
                }
            }
        }
        if (EQUINOX == null) {
            if (hdr.containsKey("EPOCH")) {
                EQUINOX = Fiu.doubleValue("EPOCH", hdr);
            }
            if (EQUINOX == null) {
                if (RADECSYS != null && "FK4".equals(RADECSYS.toUpperCase())) {
                    EQUINOX = 1950.0; 
                } else if (RADECSYS != null && "FK5".equals(RADECSYS.toUpperCase())) {
                    EQUINOX = 2000.0; 
                }
            }
        }
        if (RADECSYS == null) {
            if (EQUINOX != null) {
                if (EQUINOX < 1984) {
                    RADECSYS = "FK4";
                } else {
                    RADECSYS = "FK5";
                }
            } else {
                RADECSYS = "ICRS";
            }
        }
    }

    /**
     * Inverts 2x2 matrices
     */
    private static double[][] invert(double[][] m) {
        if (m == null || m.length != 2) {
            LOGGER.warn("Cannot invert matrix");
            if (m != null) {
                LOGGER.info("dim=" + m.length);
            }
            return null;
        }
        double detm = m[0][0] * m[1][1] - m[0][1] * m[1][0];
        if (detm == 0.0) {
            LOGGER.warn("WCS matrix is singular!");
            return null;
        }
        double[][] inv = new double[2][2];
        inv[0][0] = m[1][1] / detm;
        inv[0][1] = -m[0][1] / detm;
        inv[1][0] = -m[1][0] / detm;
        inv[1][1] = m[0][0] / detm;
        return inv;
    }

    public void createVars(int naxes) {
        NAXES = naxes;
        if (naxes == 0)
            return;

        NAXIS = new int[NAXES];
        CDELT = new double[NAXES];
        CRPIX = new double[NAXES];
        CRVAL = new double[NAXES];
        PC = new double[NAXES][NAXES];
        CD = new double[NAXES][NAXES];
        CTYPE = new String[NAXES];
        CUNIT = new String[NAXES];
        for (int i = 0; i < NAXES; i++) {
            NAXIS[i] = 0;
            CDELT[i] = 1.0;
            CRPIX[i] = 1.0;
            CRVAL[i] = 0.0;
            PC[i][i] = 1.0;
            CD[i][i] = 1.0;
        }
        CDinv = invert(CD);
        PCinv = invert(PC);
        typeContainsSIP = false;
        useCD = false;
        useSIPA = false;
        useSIPB = false;
        useSIPAP = false;
        useSIPBP = false;
        coordsys = "";
        projection = "";
    }

    /**
     * Return RaDec using its' system which can be FK4, FK5, ...
     * @param x
     * @param y
     * @return
     */
    public RaDec pixelToRaDec(double x, double y) {
        double[] raDecDegs = pix2wcs(new double[] {x, y}, true);
        return new RaDec(raDecDegs[0], raDecDegs[1]);
    }
    
    /**
     * Return RaDec for FK5(J2000). When needed, convert to such system.
     * @param x
     * @param y
     * @return
     */
    public RaDec pixelToFK5(double x, double y) {
        double[] raDecDegs = pix2wcs(new double[] {x, y}, true);
        RaDec result = new RaDec(raDecDegs[0], raDecDegs[1]);
        if ("FK4".equals(RADECSYS) || (RADECSYS == null && EQUINOX != null && EQUINOX != 2000)) {
            RahDec polarRaDec = result.toNormalizedRahDec();
            double[] j2000toB1950 = Fk4Fk5.B1950toJ2000(polarRaDec.rah, polarRaDec.dec, 0, 0, false);
            RahDec polarRaDec2 = new RahDec(j2000toB1950[0], j2000toB1950[1]);
            result = polarRaDec2.toRaDec();
        }
        return result;
    }
    
    /**
     * Converts 2-D ImageJ pixel positions to coordinate system values.
     */
    public double[] pixels2wcs(double x, double y) {
        double[] xy = new double[2];
        xy[0] = x;
        xy[1] = y;
        return pixels2wcs(xy);
    }

    /**
     * Converts ImageJ pixel positions to coordinate system values.
     */
    public double[] pixels2wcs(double[] pixels) {
        return pix2wcs(pixels, true);
    }

    /**
     * Converts ImageJ pixel positions to coordinate system values. This version
     * lets the rotation/scaling using CD/PC matrices be optional.
     */
    public double[] pix2wcs(double[] pixels, Boolean useCDPC) {
        int n = pixels.length;
        if (n != NAXES) {
            LOGGER.warn("pix2wcd: number of axes doesn't match: n=" + n + ", NAXIS=" + NAXES);
            return null;
        }

        // TRANSLATE ImageJ COORDINATES TO FITS COORDINATES, CORRECTING FOR CRPIX
        double[] p = imagej2fits(pixels, true);

        // NON-LINEAR DISTORTION CORRECTION USING SIP CORRECTION MATRICES (SEE SHUPE,
        // D.L. ET AL. 2008)
        if (useSIPA) {
            // CALCULATE SIP X-COORDINATE CORRECTION
            double xCorrection = 0.0;
            for (int qq = 0; qq <= A_ORDER; qq++) {
                for (int pp = 0; pp <= A_ORDER; pp++) {
                    if (pp + qq <= A_ORDER)
                        xCorrection += A[pp][qq] * Math.pow(p[0], (double) pp) * Math.pow(p[1], (double) qq);
                }
            }
            p[0] += xCorrection;
        }
        if (useSIPB) {
            // CALCULATE SIP Y-COORDINATE CORRECTION
            double yCorrection = 0.0;
            for (int qq = 0; qq <= B_ORDER; qq++) {
                for (int pp = 0; pp <= B_ORDER; pp++) {
                    if (pp + qq <= B_ORDER)
                        yCorrection += B[pp][qq] * Math.pow(p[0], (double) pp) * Math.pow(p[1], (double) qq);
                }
            }
            p[1] += yCorrection;
        }

        // CORRECT FOR ROTATION, SKEWNESS, SCALE

        double[] x = new double[n]; // IN PSEUDO DEGREES
        if (useCD && useCDPC) {
            for (int j = 0; j < n; j++) {
                x[j] = 0.;
                for (int i = 0; i < n; i++)
                    x[j] += CD[j][i] * p[i];
            }
        } else if (useCDPC) {
            for (int j = 0; j < n; j++) {
                x[j] = 0.;
                for (int i = 0; i < n; i++)
                    x[j] += PC[j][i] * p[i];
                x[j] *= CDELT[j];
            }
        } else {
            for (int j = 0; j < n; j++)
                x[j] = p[j] * CDELT[j];
        }

        // PROJECTION PLANE COORDINATES x TO NATIVE SPHERICAL COORDINATES s WHERE
        // s[0] INCREASES TO EAST! (SEE FIG. 3 IN CALABRETTA & GREISEN 2002; NOTE
        // THEIR arg(-y,x) = atan2(x,-y)).

        double[] s = new double[n]; // IN PSEUDO DEGREES
        double DEGRAD = 180.0 / Math.PI;
        for (int j = 0; j < n; j++)
            s[j] = x[j];
        if (projection.equals("TAN")) {
            double Rtheta = Math.sqrt(x[0] * x[0] + x[1] * x[1]);
            s[0] = DEGRAD * Math.atan2(x[0], -x[1]); // NATIVE phi (E.G. R.A.) IN DEGS
            s[1] = DEGRAD * Math.atan2(DEGRAD, Rtheta); // NATIVE theta~90 DEG (E.G. DEC.) IN DEGS
        }

        // NATIVE SPHERICAL COORDINATES s TO CELESTIAL COORDINATES c : SEE EQN.2 OF
        // Calabretta & Greisen 2002, P. 1079
        double[] c = new double[n];
        for (int j = 0; j < n; j++)
            c[j] = s[j] + CRVAL[j];
        if (projection.equals("TAN")) {
            double phi = s[0] / DEGRAD; // NATIVE PHI IN RADIANS
            double theta = s[1] / DEGRAD; // NATIVE THETA IN RADIANS
            double phiPole = LONPOLE / DEGRAD; // IN RADIANS
            double deltaPole = CRVAL[1] / DEGRAD; // IN RADIANS
            double sintcosP = Math.sin(theta) * Math.cos(deltaPole);
            double costsinPcosdp = Math.cos(theta) * Math.sin(deltaPole) * Math.cos(phi - phiPole);
            double costsindp = Math.cos(theta) * Math.sin(phi - phiPole);
            double sintsinP = Math.sin(theta) * Math.sin(deltaPole);
            double costcosPcosdp = Math.cos(theta) * Math.cos(deltaPole) * Math.cos(phi - phiPole);
            c[0] = CRVAL[0] + Math.atan2(-costsindp, sintcosP - costsinPcosdp) * DEGRAD;
            c[1] = Math.asin(sintsinP + costcosPcosdp) * DEGRAD;
        }
        return c;
    }

    /**
     * Converts coordinate system values to ImageJ pixel positions.
     */
    public double[] wcs2pixels(double a, double d) {
        double[] ad = new double[2];
        ad[0] = a;
        ad[1] = d;
        return wcs2pixels(ad);
    }

    /**
     * Converts coordinate system values to ImageJ pixel positions.
     */
    public double[] wcs2pixels(double[] c) {
        return wcs2pix(c, true);
    }

    /**
     * Converts coordinate system values to ImageJ pixel positions. This version has
     * an optional CD/PC transformation.
     */
    public double[] wcs2pix(double[] c, Boolean useCDPC) {
        int n = c.length;
        if (n != NAXES) {
            LOGGER.warn("Number of axes doesn't match: n=" + n + ", NAXIS=" + NAXES);
            return null;
        }

        // CELESTIAL c to NATIVE SPHERICAL s: EQN.6 OF Calabretta & Greisen 2002, P.
        // 1079
        double DEGRAD = 180.0 / Math.PI;
        double[] s = new double[n];
        for (int j = 0; j < n; j++)
            s[j] = c[j]; // RA,DEC IN DEG
        if (projection.equals("TAN")) {
            double alpha = c[0] / DEGRAD; // RA IN RADIANS
            double delta = c[1] / DEGRAD; // DEC IN RADIANS
            double phiPole = LONPOLE / DEGRAD; // IN RADIANS
            double alphaPole = CRVAL[0] / DEGRAD; // IN RADIANS
            double deltaPole = CRVAL[1] / DEGRAD; // IN RADIANS
            double sindcosP = Math.sin(delta) * Math.cos(deltaPole);
            double cosdsinPcosda = Math.cos(delta) * Math.sin(deltaPole) * Math.cos(alpha - alphaPole);
            double cosdsinda = Math.cos(delta) * Math.sin(alpha - alphaPole);
            double sindsinP = Math.sin(delta) * Math.sin(deltaPole);
            double cosdcosPcosda = Math.cos(delta) * Math.cos(deltaPole) * Math.cos(alpha - alphaPole);
            double phi = phiPole + Math.atan2(-cosdsinda, sindcosP - cosdsinPcosda);
            double theta = Math.asin(sindsinP + cosdcosPcosda);
            s[0] = phi * DEGRAD; // NATIVE PHI IN DEG
            s[1] = theta * DEGRAD; // NATIVE THETA IN DEG
        }

        // NATIVE SPHERICAL s TO PROJECTION PLANE x
        double[] x = new double[n];
        for (int j = 0; j < n; j++)
            x[j] = s[j]; // IN DEGS
        if (projection.equals("TAN")) {
            double Rtheta = DEGRAD / Math.tan(s[1] / DEGRAD); // IN DEGS (EQN. 54)
            x[0] = Rtheta * Math.sin(s[0] / DEGRAD); // IN DEGS (EQN. 12)
            x[1] = -Rtheta * Math.cos(s[0] / DEGRAD); // IN DEGS (EQN. 13)
        }

        // FINAL ANGULAR COORDINATES CORRECTED FOR ROTATION, SKEWNESS, SCALE
        double[] p = new double[n]; // IN PSEUDO DEGREES
        if (useCD && useCDPC) {
            if (CDinv == null) {
                throw new IllegalStateException("No inverse WCS CD matrix");
            }
            for (int j = 0; j < n; j++) {
                p[j] = 0.0;
                for (int i = 0; i < n; i++) {
                    p[j] += CDinv[j][i] * x[i];
                }
            }
        } else if (useCDPC) {
            if (PCinv == null) {
                throw new IllegalStateException("No inverse WCS PC matrix");
            }
            for (int j = 0; j < n; j++) {
                p[j] = 0.0;
                for (int i = 0; i < n; i++) {
                    p[j] += PCinv[j][i] * x[i] / CDELT[i];
                }
            }
        } else {
            for (int j = 0; j < n; j++) {
                p[j] = x[j] / CDELT[j];
            }
        }

        // ADD SIP PIXEL DISTORTIONS
        if (useSIPAP) {
            // CALCULATE SIP X-COORDINATE CORRECTION
            double xCorrection = 0.0;
            for (int qq = 0; qq <= AP_ORDER; qq++) {
                for (int pp = 0; pp <= AP_ORDER; pp++) {
                    if (pp + qq <= AP_ORDER)
                        xCorrection += AP[pp][qq] * Math.pow(p[0], (double) pp) * Math.pow(p[1], (double) qq);
                }
            }
            p[0] += xCorrection;
        }
        if (useSIPBP) {
            // CALCULATE SIP Y-COORDINATE CORRECTION
            double yCorrection = 0.0;
            for (int qq = 0; qq <= BP_ORDER; qq++) {
                for (int pp = 0; pp <= BP_ORDER; pp++) {
                    if (pp + qq <= BP_ORDER)
                        yCorrection += BP[pp][qq] * Math.pow(p[0], (double) pp) * Math.pow(p[1], (double) qq);
                }
            }
            p[1] += yCorrection;
        }

        // TRANSLATE FITS COORDINATES TO ImageJ COORDINATES
        double[] pixels = fits2imagej(p, true);
        return pixels;
    }

    public boolean hasRaDec() {
        return hasRADEC;
    }

    public String[] getWCSUnits() {
        if (CUNIT == null)
            return null;
        String[] units = CUNIT.clone();
        return units;
    }

    public double[] getCRPIX() {
        return CRPIX.clone();
    }

    public double[] getCRVAL() {
        return CRVAL.clone();
    }

    public double[] getCDELT() {
        return CDELT.clone();
    }

    public double[][] getPC() {
        return PC.clone();
    }

    public double[][] getCD() {
        return CD.clone();
    }

    public String[] getCTYPE() {
        return CTYPE.clone();
    }

    public String[] getCUNIT() {
        return CUNIT.clone();
    }

    public double getLONPOLE() {
        return LONPOLE;
    }

    public int getNAXES() {
        return NAXES;
    }

    public int getWCSAXES() {
        return WCSAXES;
    }

    public int[] getNAXIS() {
        return NAXIS.clone();
    }
    
    public String getRADECSYS() {
        return RADECSYS;
    }
    
    public Double getEQUINOX() {
        return EQUINOX;
    }

    public void log() {
        LOGGER.debug("NAXIS    =" + NAXES);
        LOGGER.debug("WCSNAXES =" + WCSAXES);
        for (int i = 0; i < NAXES; i++) {
            LOGGER.debug("NAXIS" + (i + 1) + "=" + NAXIS[i]);
        }
        for (int i = 0; i < NAXES; i++) {
            LOGGER.debug("CDELT" + (i + 1) + "=" + CDELT[i]);
        }
        for (int i = 0; i < NAXES; i++) {
            LOGGER.debug("CRPIX" + (i + 1) + "=" + CRPIX[i]);
        }
        for (int i = 0; i < NAXES; i++) {
            LOGGER.debug("CRVAL" + (i + 1) + "=" + CRVAL[i]);
        }
        for (int i = 0; i < NAXES; i++) {
            LOGGER.debug("CTYPE" + (i + 1) + "=" + CTYPE[i]);
        }
        for (int i = 0; i < NAXES; i++) {
            LOGGER.debug("CUNIT" + (i + 1) + "=" + CUNIT[i]);
        }
        if (useCD && CD != null) {
            for (int i = 0; i < NAXES; i++) {
                for (int j = 0; j < NAXES; j++) {
                    LOGGER.debug("CD" + (i + 1) + "_" + (j + 1) + "=" + CD[i][j]);
                }
            }
            if (CDinv != null) {
                for (int i = 0; i < NAXES; i++) {
                    for (int j = 0; j < NAXES; j++) {
                        LOGGER.debug("INVCD" + (i + 1) + "_" + (j + 1) + "=" + CDinv[i][j]);
                    }
                }
            }
        } else if (!useCD && PC != null) {
            for (int i = 0; i < NAXES; i++) {
                for (int j = 0; j < NAXES; j++) {
                    LOGGER.debug("PC" + (i + 1) + "_" + (j + 1) + "=" + PC[i][j]);
                }
            }
            if (PCinv != null) {
                for (int i = 0; i < NAXES; i++) {
                    for (int j = 0; j < NAXES; j++) {
                        LOGGER.debug("INVPC" + (i + 1) + "_" + (j + 1) + "=" + PCinv[i][j]);
                    }
                }
            }
        }
        LOGGER.debug("LONPOLE=" + LONPOLE);
        LOGGER.debug("coordsys=" + coordsys);
        LOGGER.debug("projection=" + projection);
        LOGGER.debug("useCD=" + useCD);
        LOGGER.debug("hasRADEC=" + hasRADEC);
        if (useSIPA) {
            LOGGER.debug("A_ORDER=" + A_ORDER);
        }
        if (useSIPB) {
            LOGGER.debug("B_ORDER=" + B_ORDER);
        }
    }

    private double[] imagej2fits(double[] pixels, Boolean useCRPIX) {
        double[] p = new double[pixels.length];
        for (int j = 0; j < p.length; j++) {
            if (j == 1) {
                // p[1] = NAXIS[1] - (pixels[1] - Centroid.PIXELCENTER) - CRPIX[1];
                p[1] = NAXIS[1] - (pixels[1] - PIXELCENTER);
            } else {
                // p[j] = (pixels[j] - Centroid.PIXELCENTER) + 1.0 - CRPIX[j];
                p[j] = (pixels[j] - PIXELCENTER) + 1.0;
            }
            if (useCRPIX) {
                p[j] -= CRPIX[j];
            }
        }
        return p;
    }

    /**
     * Converts FITS pixels to ImageJ pixels. Note that ImageJ pixels are assumed to
     * be shifted by Centroid.PIXELCENTER pixels relative to the FITS standard.
     */
    public double[] fits2imagej(double[] p, Boolean useCRPIX) {
        double[] pixels = new double[2];
        for (int j = 0; j < pixels.length; j++) {
            if (j == 1) {
                // p[1] = NAXIS[1] - (pixels[1] - Centroid.PIXELCENTER) - CRPIX[1];
                pixels[1] = -p[1] + NAXIS[1] + PIXELCENTER;
                if (useCRPIX) {
                    pixels[1] -= CRPIX[1];
                }
            } else {
                // p[j] = (pixels[j] - Centroid.PIXELCENTER) + 1.0 - CRPIX[j];
                pixels[j] = p[j] + PIXELCENTER - 1.0;
                if (useCRPIX) {
                    pixels[j] += CRPIX[j];
                }
            }
        }
        return pixels;
    }

    /**
     * Converts 2-D FITS pixels to ImageJ pixels. Note that ImageJ pixels are
     * assumed to be shifted by Centroid.PIXELCENTER pixels relative to the FITS
     * standard.
     */
    public double[] fits2imagej(double xpix, double ypix, Boolean useCRPIX) {
        double[] p = new double[] { xpix, ypix };
        return fits2imagej(p, useCRPIX);
    }
    
    public PointInt getNaxisCoords() {
        PointInt pointInt = new PointInt(NAXIS[0], NAXIS[1]);
        return pointInt;
    }

    public String getCoordsys() {
        return coordsys;
    }
}
