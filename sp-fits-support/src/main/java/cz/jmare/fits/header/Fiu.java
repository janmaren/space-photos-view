package cz.jmare.fits.header;

import nom.tam.fits.Header;
import nom.tam.fits.HeaderCard;

public class Fiu {
    public static double doubleValue(String key, Header hdr) {
        HeaderCard card = hdr.findCard(key);
        if (card == null) {
            throw new InconsistentFitsException("Not found mandatory key " + key);
        }
        String value = card.getValue();
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new InconsistentFitsException("Key " + key + " doesn't contain a double value as expected");
        }
    }
    
    public static String stringValue(String key, Header hdr) {
        HeaderCard card = hdr.findCard(key);
        if (card == null) {
            throw new InconsistentFitsException("Not found mandatory key " + key);
        }
        String value = card.getValue();
        if (value == null) {
            throw new InconsistentFitsException("Key " + key + " doesn't contain a string value as expected");
        }
        return value.trim();
    }

    public static int intValue(String key, Header hdr) {
        HeaderCard card = hdr.findCard(key);
        if (card == null) {
            throw new InconsistentFitsException("Not found mandatory key " + key);
        }
        String value = card.getValue();
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            try {
                return (int) Double.parseDouble(value);
            } catch (Exception e2) {
                throw new InconsistentFitsException("Key " + key + " doesn't contain a double value as expected");
            }
        }
    }
}
